<?php defined('BASEPATH') OR exit('No direct script access allowed');

class StageRound extends CI_Controller {

  function __construct(){
    parent::__construct();
    //$this->load->database('default',TRUE);
    $this->load->model("query_model");
    if($this->session->userdata('user_id') == NULL)
    {
      redirect("Adminlogin","location");
    }
  }

  public function getLoadRounds()
  {
    
    $arr = array();
     $sql = "SELECT * FROM 
      ( SELECT l.league_id as league_id,l.league_name as league_name,s.season_id as season_id,s.ref_season_id as ref_season_id,s.season_name as season_name,ls.stage_id AS stage_id, r.round_id AS round_id,ls.stage_name AS stage_name,r.round_name AS round_name, ls.ref_stage_id AS ref_stage_id,r.ref_round_id AS ref_round_id,r.status as status
      FROM tp_rounds AS r LEFT JOIN tp_league_stages AS ls ON r.stage_id = ls.stage_id LEFT JOIN tp_season AS s ON r.season_id = s.season_id  LEFT JOIN tp_league AS l ON s.league_id = l.league_id 
      WHERE s.status ='O' )X  WHERE 1=1";
      $sql .=" ORDER BY round_name";
      $sql2 = "SELECT * FROM 
      ( SELECT l.league_name AS league_name, s.season_name AS season_name, ls.stage_name AS stage_name,l.country_name as country_name,ls.stage_id as stage_id
      FROM tp_league_stages AS ls LEFT JOIN tp_season AS s ON ls.ref_stage_id = s.ref_current_stage_id  LEFT JOIN tp_league AS l ON s.league_id = l.league_id WHERE s.status ='O' ORDER BY s.season_name)X ";
    
    $result = $this->query_model->getDataArray($sql);
    $result2 = $this->query_model->getDataArray($sql2);
    $json_data = array("rounds"=>$result,"stages"=>$result2);
    echo json_encode($json_data);
  } 
  public function updRounds()
  {
    $result = array();
    $round_id = $this->query_model->clean('round_id','id');
    $ref_round_id = $this->query_model->clean('ref_round_id');
    $ref_season_id = $this->query_model->clean('ref_season_id');
    $status = $this->query_model->clean('status');

    $sql = "SELECT * FROM tp_setting WHERE id='1'";
    $apiresult = $this->query_model->getDataArray($sql);
    $APIkey = $apiresult[0]['api_key'];

    $curl_options = array(
      CURLOPT_URL => "https://sportickr.com/api/v1.0/seasons/$ref_season_id?api-key=$APIkey",
      CURLOPT_RETURNTRANSFER => true,
      CURLOPT_HEADER => false,
      CURLOPT_TIMEOUT => 30,
      CURLOPT_CONNECTTIMEOUT => 5
      );

      $curl = curl_init();
      curl_setopt_array( $curl, $curl_options );
      $result = curl_exec( $curl );

      $result = (array) json_decode($result,true);
      $res = "";
    if($status=='O'){
      if($result['data']['current_round_id']==$ref_round_id){
        $sql ="UPDATE tp_rounds SET status = '".$status."' WHERE round_id = ".$round_id;
        $this->query_model->execSQL($sql);

      $res = array("typ" => "success", "ttl" => "Update Round", "msg" => "Round has been updated!");
      }
      else{
        $res = array("typ" => "error", "ttl" => "Update Round", "msg" => "This Round Not yet available!");
      }
    }
    else{
        $sql ="UPDATE tp_rounds SET status = '".$status."' WHERE round_id = ".$round_id;
        $this->query_model->execSQL($sql);
        $res = array("typ" => "success", "ttl" => "Update Round", "msg" => "Round has been updated!");
       }
      

   

    echo json_encode($res);
   
  }
  public function MatchShow()
  {
    $round_id = $this->query_model->clean('round_id','id'); 
  
   
   
    $sql = "SELECT *FROM tp_stage_match WHERE round_id = ".$round_id."";
   
    $result = $this->query_model->getDataArray($sql);
    
    $data = array();

    foreach ($result as $row) {
      $nestedData=array();
      $homesql = "SELECT *FROM tp_teams WHERE ref_team_id = ".$row["home_team_id"]."";
      $home_team = $this->query_model->getDataArray($homesql);
      // var_dump($home_team);

      $awaysql = "SELECT *FROM tp_teams WHERE ref_team_id = ".$row["away_team_id"]."";
     
      $away_team = $this->query_model->getDataArray($awaysql);

      $home_team_name = $home_team[0]['team_name'];
      $away_team_name = $away_team[0]['team_name'];

      $home_team_logo = $home_team[0]['logo_path'];
      $away_team_logo = $away_team[0]['logo_path'];

      $nestedData["home_team_name"] = $home_team_name;
      $nestedData["away_team_name"] = $away_team_name;

      $nestedData["home_team_logo"] = $home_team_logo;
      $nestedData["away_team_logo"] = $away_team_logo;
    

      $data[] = $nestedData;  
    }
  
    echo json_encode($data);
  }
}