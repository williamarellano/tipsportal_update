<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Sites extends CI_Controller {

  function __construct(){
    parent::__construct();
    //$this->load->database('default',TRUE);
    $this->load->model("query_model");
    $this->load->library('utilities');
  } 

  public function sites()
  {
    $data['url'] = array_flip($_REQUEST);
  	$this->load->view('templates/admin_header');
    $this->load->view('templates/admin_menu',$data);
    $this->load->view('adminpages/sites_view');
    // $this->load->view('templates/admin_footer');
  }

  public function tips()
  {
    $data['url'] = array_flip($_REQUEST);
    $tips['leagues'] = $this->query_model->getDataArray("SELECT * FROM tp_league WHERE status <> 'X' ORDER BY league_name ASC");
  	$this->load->view('templates/admin_header');
    $this->load->view('templates/admin_menu',$data);
    $this->load->view('adminpages/tips_view',$tips);
    // $this->load->view('templates/admin_footer');
  }

  public function tptips()
  {
    $data['url'] = array_flip($_REQUEST);
    $tips['leagues'] = $this->query_model->getDataArray("SELECT * FROM tp_league WHERE status <> 'X' ORDER BY league_name ASC");
    $this->load->view('templates/admin_header');
    $this->load->view('templates/admin_menu',$data);
    $this->load->view('adminpages/tptips_view',$tips);
    // $this->load->view('templates/admin_footer');
  }

  public function getStages()
  {
    
    $arr = array();
    $league_id = $this->query_model->clean('league_id');
    $result = $this->query_model->getDataArray("SELECT * FROM tp_league_stages WHERE status <> 'X' AND league_id = '".$league_id."' ORDER BY stage_name ASC");
    // echo "SELECT * FROM tp_league_stages WHERE status <> 'X' AND league_id = '".$league_id."' ORDER BY stage_name ASC";
    echo json_encode($result);
  }

  public function getMatches()
  {
    
    $arr = array();
    $stage_id = $this->query_model->clean('stageid');
    $result = $this->query_model->getDataArray("SELECT m.match_id, 
    CONCAT((IFNULL((SELECT team_name FROM tp_teams WHERE team_id = m.home_team_id),'')),' vs ',(IFNULL((SELECT team_name FROM tp_teams WHERE team_id = m.away_team_id),''))) AS match_title
    FROM tp_stage_match m WHERE m.status <> 'X' AND m.stage_id = '".$stage_id."' ORDER BY match_id ASC");
    // echo "SELECT * FROM tp_league_stages WHERE status <> 'X' AND league_id = '".$league_id."' ORDER BY stage_name ASC";
    echo json_encode($result);
  }

  public function getTipDesc()
  {
    // $match_id = $this->uri->segment(3);
    $tip_id = $this->query_model->clean('tip_id','id');
    // echo $tip_id;
    $result = $this->query_model->getDataArray("SELECT * FROM tp_tips WHERE tip_id = ".$tip_id);

    // echo "SELECT * FROM tp_tptips WHERE match_id = ".$tip_id;

    echo json_encode($result);

  }

  // ----------- Sites Controllers
  public function listing_site()
  {
    $requestData= $_REQUEST;
    $columns = array( 
      0 => 'site_id',
      1 => 'image_path',
      2 => 'site',
      3 => 'link',
      4 => 'status',
      5 => 'created_date',
      6 => 'created_by'
    );

    $sql = "";
    $totalData = 0;
    $totalFiltered = 0;

    $sql = "SELECT * FROM (SELECT u.site_id, u.image_path, u.site, u.link, u.status, u.created_date, (SELECT realname FROM tp_users WHERE user_id = u.created_by) AS created_by FROM tp_tipsters u WHERE u.status <> 'X')X WHERE 1=1";

    $records = $this->query_model->getDataCount($sql);

    $totalData = $records["count"];
    $totalFiltered = $totalData;

    //This is for search function...
      if( !empty($requestData['search']['value']) ) {
        $sql.=" AND site LIKE '%".$requestData['search']['value']."%' OR link LIKE '%".$requestData['search']['value']."%'";    
        //$sql.=" OR status LIKE '%".$requestData['search']['value']."%' )";
      }  
      $totalFiltered = $this->query_model->getDataCount($sql)["count"];
      //end of search function

      $sql.=" ORDER BY ". $columns[$requestData['order'][0]['column']]." ".$requestData['order'][0]['dir']." LIMIT ".$requestData['start']." ,".$requestData['length']."   ";
    
    //$totalFiltered = $this->query_model->getDataCount($sql)["count"];

    $records = $this->query_model->getDataArray($sql);

    $data = array();

    foreach ($records as $row) {
      $nestedData=array();
      
      $nestedData["site_id"] = $row["site_id"];
      $nestedData["image_path"] = $row["image_path"];
      $nestedData["site"] = $row["site"];
      $nestedData["link"] = $row["link"];
      $nestedData["status"] = $row["status"];
      $nestedData["created_date"] = $row["created_date"];
      $nestedData["created_by"] = $row["created_by"];

      $data[] = $nestedData;  
    }

    $json_data = array(
      "draw"            => intval( $requestData['draw'] ),   // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw. 
      "recordsTotal"    => intval( $totalData ),  // total number of records
      "recordsFiltered" => intval( $totalFiltered ), // total number of records after searching, if there is no searching then totalFiltered = totalData
      "data"            => $data   // total data array
    );

    echo json_encode($json_data);       
  }

  public function createTipster()
  {
  	$result = array();
  	$tipstername = $this->query_model->clean('tipstername');
  	$tipsterlink = $this->query_model->clean('tipsterlink');
  	$status = $this->query_model->clean('status');
    $path = $this->config->item('upload_tipsters_logo');

    
    
    $chklink = $this->query_model->getCell("SELECT COUNT(1) AS cnt FROM tp_tipsters WHERE link = '".$tipsterlink."' AND status <> 'X'","cnt");
    if($chklink > 0)
    {
       $result = array("typ" => "error", "ttl" => "Add Tipster", "msg" => "Tipster exists! Please input another Tipster.");
     }else{
      $oldfilename = explode(".", $_FILES['addxFileTxt']['name']);
      $extension = $oldfilename[count($oldfilename)-1];
      $tp = str_replace(' ','',$tipstername);
      $filename = $tp."_logo.".$extension;
      $upload = $this->utilities->upload_img_logo("addxFileTxt",$path,$filename);
      if($upload["status"] == 1)
      {

        $sql = "INSERT INTO tp_tipsters(site, image_path, link, status, created_date, created_by)
          VALUES ('".$tipstername."','".$filename."','".$tipsterlink."','".$status."','".date('Y-m-d H:i:s')."','".$this->session->userdata('user_id')."')";
        $exec = $this->query_model->execSQL($sql);
        $result = array("typ" => "success", "ttl" => "Add Tipster", "msg" => "Tipster has been added!");
      }else{
        $result = array("typ" => "error", "ttl" => "Add Tipster", "msg" => "Error encountered in uploading the image!");
      }

    }

  	echo json_encode($result);
  }

  public function updTipster()
  {
  	$result = array();
  	$tipid = $this->query_model->clean('tipid', 'id');
  	$tipstername = $this->query_model->clean('tipstername');
  	$tipsterlink = $this->query_model->clean('tipsterlink');
  	$status = $this->query_model->clean('status');

    $path = $this->config->item('upload_tipsters_logo');
    
    $sql = "UPDATE tp_tipsters SET site = '".$tipstername."', link = '".$tipsterlink."', status = '".$status."' ";
   //  $ex = $this->query_model->execSQL($sql);
   //  $result = array("typ" => "success", "ttl" => "Edit Site", "msg" => "Site has been updated!");
  	// echo json_encode($result);

    if(isset($_FILES['updxFileTxt'])) 
    { 
      $oldfilename = explode(".", $_FILES['updxFileTxt']['name']);
      $extension = $oldfilename[count($oldfilename)-1];
      $tp = str_replace(' ','',$tipstername);
      $filename = $tp."_logo.".$extension;

      $upload = $this->utilities->upload_img_logo("updxFileTxt",$path,$filename);
      if($upload["status"] == 1)
      {
        $sql .=" , image_path = '".$filename."' ";
        $sql .= " WHERE site_id = ".$tipid;

        // echo $sql;
        $this->query_model->execSQL($sql);
        $result = array("typ" => "success", "ttl" => "Update Tipster", "msg" => "Tipster has been updated!");
      }
      else{
        $result = array("typ" => "error", "ttl" => "Update Tipster", "msg" => "Error!");
      }

    }else{
      $sql .= " WHERE site_id = ".$tipid;
      // echo $sql;
      $this->query_model->execSQL($sql);
      $result = array("typ" => "success", "ttl" => "Update Tipster", "msg" => "Tipster has been updated!");
    }

    echo json_encode($result);
  }

  public function delSite()
  {
  	$user_id = $this->query_model->clean('delID','id');
  	// echo $user_id;

  	$delUser = $this->query_model->execSQL("UPDATE tp_tipsters SET status = 'X' WHERE site_id = ".$user_id);
  	$result = array("typ" => "success", "ttl" => "Delete User", "msg" => "User has been deleted!");
  	echo json_encode($result);
  }
  // ----------- End of Sites Controllers
  
  // ----------- Tips Controllers
  public function listing_tips()
  {
    $requestData= $_REQUEST;
    $columns = array( 
      0 => 'tip_id',
      1 => 'match_id',
      2 => 'match_title',
      3 => 'site_name',
      4 => 'tip_form',
      5 => 'tip_desc',
      6 => 'tip_main',
      7 => 'link',
      8=> 'odds',
      9 => 'priority',
      10 => 'featured',
      11 => 'status',
      12 => 'created_date',
      13 => 'created_by'
    );

    $sql = "";
    $totalData = 0;
    $totalFiltered = 0;

    $sql = "SELECT * FROM (SELECT t.tip_id, t.match_id, CONCAT(IFNULL((SELECT team_name FROM tp_teams WHERE team_id = m.home_team_id),''), ' vs ',IFNULL((SELECT team_name FROM tp_teams WHERE team_id = m.away_team_id),'')) AS match_title,
      t.site_id, (SELECT site FROM tp_tipsters WHERE site_id = t.site_id) AS site_name, t.tip_desc ,t.tip_main, t.tip_form, t.odds, IFNULL(t.link,'') AS link, 
      t.status, t.featured, t.priority, t.created_date,(SELECT realname FROM tp_users WHERE user_id = t.created_by) AS created_by, t.updated_date, t.updated_by
      FROM tp_tips t 
      INNER JOIN tp_stage_match m ON t.match_id = m.match_id
      WHERE t.status <> 'X'
        )X WHERE 1=1 ";

    $records = $this->query_model->getDataCount($sql);

    $totalData = $records["count"];
    $totalFiltered = $totalData;

    //This is for search function...
      if( !empty($requestData['search']['value']) ) {
        $sql.=" AND tip_desc LIKE '%".$requestData['search']['value']."%'";    
        $sql.=" OR site_name LIKE '%".$requestData['search']['value']."%' ";
        //$sql.=" OR status LIKE '%".$requestData['search']['value']."%' )";
      }  
      $totalFiltered = $this->query_model->getDataCount($sql)["count"];
      //end of search function

      $sql.=" ORDER BY ". $columns[$requestData['order'][0]['column']]." ".$requestData['order'][0]['dir']." LIMIT ".$requestData['start']." ,".$requestData['length']."   ";
    
    //$totalFiltered = $this->query_model->getDataCount($sql)["count"];

    $records = $this->query_model->getDataArray($sql);

    $data = array();

    foreach ($records as $row) {
      $nestedData=array();
      
      $nestedData["tip_id"] = $row["tip_id"];
      $nestedData["match_id"] = $row["match_id"];
      $nestedData["site_id"] = $row["site_id"];
      $nestedData["site_name"] = $row["site_name"];
      $nestedData["match_title"] = $row["match_title"];
      $nestedData["tip_desc"] = $row["tip_desc"];
      $nestedData["tip_main"] = $row["tip_main"];
      $nestedData["tip_form"] = $row["tip_form"];
      $nestedData["odds"] = $row["odds"];
      $nestedData["link"] = $row["link"];
      $nestedData["priority"] = $row["priority"];
      $nestedData["featured"] = $row["featured"];
      $nestedData["status"] = $row["status"];
      $nestedData["created_date"] = $row["created_date"];
      $nestedData["created_by"] = $row["created_by"];

      $data[] = $nestedData;  
    }

    $json_data = array(
          "draw"            => intval( $requestData['draw'] ),   // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw. 
          "recordsTotal"    => intval( $totalData ),  // total number of records
          "recordsFiltered" => intval( $totalFiltered ), // total number of records after searching, if there is no searching then totalFiltered = totalData
          "data"            => $data   // total data array
          );

    echo json_encode($json_data);       
  }

  public function addTip()
  {
  	$result = array();
  	$league_id = $this->query_model->clean('league','id');
    $stage_id = $this->query_model->clean('stage','id');
    $match_id = $this->query_model->clean('match','id');
    $site_id = $this->query_model->clean('site','id');
    $featured = $this->query_model->clean('featured','id');
    $priority = $this->query_model->clean('priority');
    $odds = $this->query_model->clean('odds');
    $tiplink = $this->query_model->clean('tiplink');
    $tipdesc = $this->query_model->clean('tipdesc');
    $tipmain = $this->query_model->clean('tipmain');
    $tipform = $this->query_model->clean('tipform','id');
    $status = $this->query_model->clean('status');
    $tipdesc = html_entity_decode($tipdesc);
    $tipdesc = htmlspecialchars_decode($tipdesc, ENT_QUOTES);


    if($featured == 1)
    {
      $priority = 11;
      $this->query_model->execSQL("UPDATE tp_tips SET priority = 10 WHERE featured = 1");
      $this->query_model->execSQL("UPDATE tp_tips SET featured = 0");

      $sql = "INSERT IGNORE INTO tp_tips(match_id, site_id, tip_desc, tip_main, tip_form, odds, link, status, featured, priority, created_date, created_by)
              VALUES('".$match_id."','".$site_id."','".trim(str_replace("'","''", $tipdesc))."','".$tipmain."','".$tipform."','".$odds."','".$tiplink."','".$status."','".$featured."','".$priority."','".date('Y-m-d H:i:s')."','".$this->session->userdata('user_id')."')";

    }else{
      $sql = "INSERT IGNORE INTO tp_tips(match_id, site_id,  tip_desc, tip_main, tip_form,  odds, link, status, featured, priority, created_date, created_by)
              VALUES('".$match_id."','".$site_id."','".trim(str_replace("'","''", $tipdesc))."','".$tipmain."','".$tipform."','".$odds."','".$tiplink."','".$status."','".$featured."','".$priority."','".date('Y-m-d H:i:s')."','".$this->session->userdata('user_id')."')";
    }


    // echo $sql;
    $exec = $this->query_model->execSQL($sql);
    $result = array("typ" => "success", "ttl" => "Add Tip", "msg" => "Tip has been added!");
  	echo json_encode($result);
  }

  public function updateTip()
  {
  	$result = array();
    $tip_id = $this->query_model->clean('tipid','id');
  	$match_id = $this->query_model->clean('matchid','id');
    $site_id = $this->query_model->clean('siteid','id');
    $featured = $this->query_model->clean('featured');
    $priority = $this->query_model->clean('priority');
    $odds = $this->query_model->clean('odds');
    $tiplink = $this->query_model->clean('tiplink');
    $tipdesc = $this->query_model->clean('tipdesc');
    $tipmain = $this->query_model->clean('tipmain');
    $tipform = $this->query_model->clean('tipform','id');
    $status = $this->query_model->clean('status');
    $tipdesc = htmlspecialchars_decode($tipdesc, ENT_QUOTES);$tipdesc = html_entity_decode($tipdesc);
    $tipdesc = html_entity_decode($tipdesc);


    if($featured == 1)
    {
      $priority = 11;
      $this->query_model->execSQL("UPDATE tp_tips SET priority = 10 WHERE featured = 1");
      $this->query_model->execSQL("UPDATE tp_tips SET featured = 0");
    }

    $sql = "UPDATE tp_tips SET site_id = '".$site_id."', priority = '".$priority."', featured = '".$featured."', tip_desc = '".str_replace("'","''", $tipdesc)."', tip_main = '".$tipmain."', tip_form = '".$tipform."',odds = '".$odds."', link = '".$tiplink."', status = '".$status."' WHERE tip_id = '".$tip_id."'";
    $this->query_model->execSQL($sql);

    $result = array("typ" => "success", "ttl" => "Edit Tip", "msg" => "Tip has been updated!");
    echo json_encode($result);
  }

  public function delTip()
  {
  	$tip_id = $this->query_model->clean('delID','id');
  	// echo $user_id;

  	$this->query_model->execSQL("UPDATE tp_tips SET status = 'X' WHERE tip_id = ".$tip_id);

    // echo "UPDATE tp_tips SET status = 'X' WHERE tip_id = ".$tip_id;
  	$result = array("typ" => "success", "ttl" => "Delete Tip", "msg" => "Tip has been deleted!");
  	echo json_encode($result);
  }

  public function getSiteList()
  {
    
    $arr = array();
    $result = $this->query_model->getDataArray("SELECT * FROM tp_tipsters WHERE status <> 'X'");
    // foreach ($data as $d) {

    echo json_encode($result);
  }

  public function populateSite(){

    $sql = "SELECT * FROM tp_tipsters WHERE status <> 'X' AND status <> 'I'";
    $sites = $this->query_model->getDataArray($sql);

    // $html = "<option value=''>".$this->lang->line('txt_Select')."</option>";
    $html = '';
    $html .= "<option value=''>-- Select --</option>";
    foreach ($sites as $s){
      $html .= "<option value='".$s["site_id"]."'>".$s["site"]."</option>";
    }

    // $html .= "</select><div class='select__arrow'></div>";

    echo $html; 
  }

  public function populateMatch(){

    $match_id = $this->uri->segment(3);
    $stage_id = $this->query_model->getCell("SELECT stage_id FROM tp_stage_match WHERE match_id = 1","stage_id");
    $sql = "SELECT m.match_id, 
    CONCAT((IFNULL((SELECT team_name FROM tp_teams WHERE team_id = m.home_team_id),'')),' vs ',(IFNULL((SELECT team_name FROM tp_teams WHERE team_id = m.away_team_id),''))) AS match_title
    FROM tp_stage_match m WHERE m.status <> 'X' AND m.stage_id = '".$stage_id."' ORDER BY match_id ASC";
    $sites = $this->query_model->getDataArray($sql);

    // $html = "<option value=''>".$this->lang->line('txt_Select')."</option>";
    $html = '';
    $html .= "<option value=''>-- Select --</option>";
    foreach ($sites as $s){
      $html .= "<option value='".$s["match_id"]."'>".$s["match_title"]."</option>";
    }

    // $html .= "</select><div class='select__arrow'></div>";

    echo $html; 
  }
}
?>