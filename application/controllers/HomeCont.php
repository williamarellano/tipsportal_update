<?php defined('BASEPATH') OR exit('No direct script access allowed');

class HomeCont extends CI_Controller {

  function __construct(){
    parent::__construct();
    //$this->load->database('default',TRUE);
    $this->load->model("query_model");
    // if($this->session->userdata('user_id') == NULL)
    // {
    //   redirect("Adminlogin","location");
    // }
  }
    public function index()
  {
    $data['url'] = array_flip($_REQUEST);
    $this->load->view('templates/admin_header');
    $this->load->view('templates/admin_menu',$data);
    $this->load->view('adminpages/homecms_view');


  }
  public function getLeague()
  {
    
    
    $sql = "SELECT s.season_id as season_id,c.country_name as country_name,l.priority as priority,l.league_id,l.league_name FROM tp_league AS l LEFT JOIN tp_season AS s ON l.league_id = s.league_id LEFT JOIN tp_countries AS c ON l.country_id = c.country_id WHERE s.status = 'O' ORDER BY l.priority ASC";
    $result = $this->query_model->getDataArray($sql);
   

   echo json_encode($result);
      
  } 
  public function getCntStanding()
  {
    
    $sid = $this->query_model->clean('sid','id');
    $sql = "SELECT COUNT(*) AS cnt FROM tp_league_standings WHERE season_id = $sid";
    $result = $this->query_model->getDataArray($sql);
   
   
   echo json_encode($result);
      
  } 
  public function getPriority()
  {
    
    
    $sql = "SELECT s.season_id AS season_id FROM tp_league AS l LEFT JOIN tp_season AS s ON l.league_id = s.league_id  WHERE l.priority =1 AND s.status='O'";
    $result = $this->query_model->getDataArray($sql);
   
    echo json_encode($result);
  } 
  public function getFixtures()
  {
    
    $sid = $this->query_model->clean('season_id','id');
    $sql = "SELECT fixture_id,season_id,league_id,DATE_FORMAT(date_time, '%H:%i') AS date_time,visitorteam_id,localteam_id FROM tp_fixtures WHERE season_id =$sid";
    $result = $this->query_model->getDataArray($sql);
    $max_fixture = $this->query_model->getDataArray("SELECT max_fixture FROM tp_setting");
    $max_fix = $max_fixture[0]['max_fixture'];
    $data = array();
    foreach ($result as $row) {
      $nestedData=array();
      
      $hometeam_arr = $this->query_model->getDataArray("SELECT COUNT(1) AS cnt, team_name, team_id FROM tp_teams WHERE ref_team_id =".$row['localteam_id']);
      $awayteam_arr = $this->query_model->getDataArray("SELECT COUNT(1) AS cnt, team_name, team_id FROM 
        tp_teams WHERE ref_team_id =".$row['visitorteam_id']);

      // var_dump($awayteam_arr[0]['team_name']);
      $nestedData["fixture_id"] = $row["fixture_id"];

      $nestedData["season_id"] = $row["season_id"];
      $nestedData["league_id"] = $row["league_id"];
      $nestedData["time"] = $row["date_time"];
      $nestedData["home_team"] = $hometeam_arr[0]['team_name'];
      $nestedData["away_team"] = $awayteam_arr[0]['team_name'];
  

      $data[] = $nestedData;  
    }
    $json_data = array("data"=>$data,"max_fix"=>$max_fix);
    // var_dump($json_data['data'][0]['time']);
    echo json_encode($json_data);
  } 
  public function getStandings()
  {
    $sid = $this->query_model->clean('season_id','id');
    
    $sql = "SELECT * FROM tp_league_standings WHERE season_id =$sid ORDER BY position";
    $sql2= "SELECT l.format_standing FROM tp_league AS l LEFT JOIN tp_season AS s ON l.league_id = s.league_id LEFT JOIN tp_countries AS c ON l.country_id = c.country_id WHERE s.status = 'O' AND season_id = $sid  ";
    $result = $this->query_model->getDataArray($sql);
    $result2 = $this->query_model->getDataArray($sql2);
    

    $json_data = array("data"=>$result,"format"=>$result2);

    echo json_encode($json_data);
   
   
  } 

}