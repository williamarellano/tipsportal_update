<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Teams extends CI_Controller {

  function __construct(){
    parent::__construct();
    //$this->load->database('default',TRUE);
    $this->load->model("query_model");
    $this->load->library('utilities');
  } 

  public function index()
  {
    $data['url'] = array_flip($_REQUEST);
    $this->load->view('templates/admin_header');
    $this->load->view('templates/admin_menu',$data);
    $this->load->view('adminpages/teams_view');
  }

  public function getTeamDet()
  {
    $team_id = $this->query_model->clean('team_id','id');
    $result = $this->query_model->getDataArray("SELECT * FROM tp_teams WHERE team_id = ". $team_id);
    echo json_encode($result);
  }

  public function addTeam()
  {
    $result = array();
    $team_name = $this->query_model->clean('team_name');
    $team_nickname = $this->query_model->clean('team_nickname');
    $twitter = $this->query_model->clean('twitter');
    $country_id = $this->query_model->clean('country_id');
    $founded = $this->query_model->clean('founded');
    $status = $this->query_model->clean('status');
    $path = $this->config->item('upload_teams_logo');

    $oldfilename = explode(".", $_FILES['addxFileTxt']['name']);
    $extension = $oldfilename[count($oldfilename)-1];
    $tm = htmlspecialchars_decode($team_name, ENT_QUOTES);
    $tm = html_entity_decode($tm);
    $tm = str_replace("'","''",$tm);
    $tm = str_replace("&","",$tm);
    $tm = str_replace(";","",$tm);

    $tn = htmlspecialchars_decode($team_nickname, ENT_QUOTES);
    $tn = html_entity_decode($tm);
    $tn = str_replace("'","''",$tm);;
    $filename = $tm."_logo.".$extension;
    $upload = $this->utilities->upload_img_logo("addxFileTxt",$path,$filename);

    if($upload["status"] == 1)
    {
      
      $team = htmlspecialchars_decode($team_name, ENT_QUOTES);
      // $team = html_entity_decode($team);
      $team = str_replace(" ","",$team);
      $team = str_replace("'","",$team);
      $team = str_replace("&","",$team);
      $team = str_replace(";","",$team);
      
      $sql ="INSERT IGNORE INTO tp_teams(team_name, team_nickname, twitter, country_id, founded, logo_path, created_by, created_date, status)
          VALUES ('".$tm."','".$tm."','".$twitter ."','".$country_id."','".$founded."','".$filename."','".$this->session->userdata('user_id')."','".date('Y-m-d H:i:s')."','".$status."')";
      $this->query_model->execSQL($sql);

    $result = array("typ" => "success", "ttl" => "Add Team", "msg" => "Team has been added!");
    }else{
        
      $result = array("typ" => "error", "ttl" => "Add Team", "msg" => "Error!");
    }


    echo json_encode($result);
    // echo json_encode($upload);
  }

  public function updTeam()
  {
    header('Content-Type: text/html; charset=UTF-8');
    $result = array();
    $oldfilename = "";
    $extension = "";
    $filename = "";
    $sql = "";
    $team_id = $this->query_model->clean('team_id','id');
    $team_name = $this->query_model->clean('team_name');
    $team_nickname = $this->query_model->clean('team_nickname');
    $twitter = $this->query_model->clean('twitter');
    $founded = $this->query_model->clean('founded');
    $path = $this->config->item('upload_teams_logo');
    $tm = htmlspecialchars_decode($team_name, ENT_QUOTES);
    $tm = html_entity_decode($tm);
    $tm = str_replace("'","''",$tm);
    $tn = htmlspecialchars_decode($team_nickname, ENT_QUOTES);
    $tn = html_entity_decode($tn);
    $tn = str_replace("'","''",$tn);


    $sql ="UPDATE tp_teams SET team_name = '".$tm."', team_nickname = '".$tn."', twitter = '".$twitter."', founded = '".$founded."' "; 

    if(isset($_FILES['updxFileTxt'])) 
    { 
      $oldfilename = explode(".", $_FILES['updxFileTxt']['name']); 
      $extension = $oldfilename[count($oldfilename)-1];
      // htmlspecialchars($tm, ENT_NOQUOTES)
      // $team = htmlspecialchars_decode($team_name, ENT_QUOTES);
      // $team = html_entity_decode($team);
      $team = htmlentities($team_name, ENT_QUOTES, "UTF-8");
      $team = str_replace(" ","",$team);
      $team = str_replace("'","",$team);
      $team = str_replace("&","",$team);
      $team = str_replace(";","",$team);

      $filename = $team."_logo.".$extension;
      // echo $filename;
      $upload = $this->utilities->upload_img_logo("updxFileTxt",$path,$filename);
      if($upload["status"] == 1)
      {
        // echo $upload["status"];
        $sql .=" , logo_path = '".$filename."' ";
        $sql .= " WHERE team_id = ".$team_id;

        // echo $sql;
        $this->query_model->execSQL($sql);
        $result = array("typ" => "success", "ttl" => "Update Team", "msg" => "Team has been updated!");
      }
      else{
        $result = array("typ" => "error", "ttl" => "Update Team", "msg" => "Error!");
      }

    }else{
      $sql .= " WHERE team_id = ".$team_id;
      // echo $sql;
      $this->query_model->execSQL($sql);
      $result = array("typ" => "success", "ttl" => "Update Team", "msg" => "Team has been updated!");
    }

    // var_dump(expression)


    echo json_encode($result);
    // echo json_encode($upload);
  }

  public function getCountryList()
  {
    
    $arr = array();
    $result = $this->query_model->getDataArray("SELECT * FROM tp_countries WHERE status <>'X' ORDER BY country_name ASC");
    echo json_encode($result);
  }
 

  public function teamsListing()
  {
    // $country_id = $this->query_model->clean('cntry');
    // $league_id = $this->query_model->clean('selLeague');
    // $season = $this->query_model->clean('selSeason');
    // $status = $this->query_model->clean('selstatus');
    // var_dump($country_id);

    // echo "SELECT * FROM (SELECT * FROM tp_league WHERE status <>'X')X WHERE 1=1 AND country_id = '".$country_id."' ";
    // echo $country_id." ".$league_id." ".$season." ".$status;
    // echo "COUNTRY ID: ".$country_id;
    // exit;

    $requestData= $_REQUEST;
    $columns = array( 
      0 => 'team_id',
      1 => 'logo_path',
      2 => 'team_name',
      3 => 'team_nickname',
      4 => 'twitter',
      5 => 'founded',
      6 => 'status'
    );

    $sql = "";
    $totalData = 0;
    $totalFiltered = 0;

      // $sql = "SELECT * FROM (SELECT l.`league_id`, l.`country_id`, l.`ref_league_id`, l.`ref_country_id`, l.`master_id`, l.`country_name`, l.`league_name`,
      //   s.season_id,s.season_name AS season,l.`date_start`,l.`date_end`, s.`status`,l.`created_date`,IFNULL((SELECT realname FROM tp_users WHERE user_id = l.`created_by`),'') AS created_by,
      //   l.`updated_date`, IFNULL((SELECT realname FROM tp_users WHERE user_id = l.`updated_by`),'') AS `updated_by`, l.`ref_current_round_id`,
      //   l.`ref_current_season_id`, l.`ref_current_stage_id`, l.`is_cup`
      //   FROM tp_league l
      //   INNER JOIN tp_season s ON l.league_id = s.league_id
      //   WHERE s.status <> 'X'
      //   )X WHERE 1=1  ";
     $sql = "SELECT * FROM (SELECT * FROM tp_teams WHERE status <> 'X')X WHERE 1=1  ";
   
    $records = $this->query_model->getDataCount($sql);

      $totalData = $records["count"];
      $totalFiltered = $totalData;
      // if(!empty($brandfilter)){
      //   $filterbrand = $this->query_model->getDataArray("SELECT * from brands Where status='A' AND brand_name='".$brandfilter."'");
      //   $sql.=" AND brand_name = '".$filterbrand[0]['id']."'"; 
      // }
      // if(!empty($country_id)){
      //   $sql.=" AND team_id = '".$country_id."' "; 
      // }

      // if(!empty($league_id)){
      //   $sql.=" AND league_id = '".$league_id."'"; 
      // }

      // if(!empty($season)){
      //   $sql.=" AND season = '".$season."'"; 
      // }

      // if(!empty($status)){
      //   $sql.=" AND status = '".$status."'"; 
      // }
      // if(!empty($statusfilter)){
      //   $sql.=" AND status LIKE '%".$statusfilter."%'"; 
      // }
      //This is for search function...
      if( !empty($requestData['search']['value']) ) {
        $sql.=" AND (team_name LIKE '%".$requestData['search']['value']."%' "; 
        $sql.=" OR team_nickname LIKE '%".$requestData['search']['value']."%' ";    
        $sql.=" OR twitter LIKE '%".$requestData['search']['value']."%' ";    
        $sql.=" OR founded LIKE '%".$requestData['search']['value']."%') ";

        // $sql.=" OR season LIKE '%".$requestData['search']['value']."%') ";
      }  
      $totalFiltered = $this->query_model->getDataCount($sql)["count"];
      //end of search function


    $sql.=" ORDER BY ". $columns[$requestData['order'][0]['column']]." ".$requestData['order'][0]['dir']." LIMIT ".$requestData['start']." ,".$requestData['length']."   ";
    
    //$totalFiltered = $this->query_model->getDataCount($sql)["count"];
    // echo $sql;

    $records = $this->query_model->getDataArray($sql);


    $data = array();

    foreach ($records as $row) {
      $nestedData=array();
      
      $nestedData["team_id"] = $row["team_id"];
      $nestedData["logo_path"] = $row["logo_path"];
      $nestedData["team_name"] = $row["team_name"];
      $nestedData["team_nickname"] = $row["team_nickname"];
      $nestedData["twitter"] = $row["twitter"];
      $nestedData["founded"] = $row["founded"];
      $nestedData["created_by"] = $row["created_by"];
      $nestedData["created_date"] = $row["created_date"];
      $nestedData["updated_by"] = $row["updated_by"];
      $nestedData["updated_date"] = $row["updated_date"];
      $nestedData["status"] = $row["status"];
    

      $data[] = $nestedData;  
    }
    $json_data = array(
          "draw"            => intval( $requestData['draw'] ),   // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw. 
          "recordsTotal"    => intval( $totalData ),  // total number of records
          "recordsFiltered" => intval( $totalFiltered ), // total number of records after searching, if there is no searching then totalFiltered = totalData
          "data"            => $data   // total data array
          );

    echo json_encode($json_data);  
  }


}
?>

