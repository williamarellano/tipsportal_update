<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Results extends CI_Controller {

  function __construct(){
    parent::__construct();
    //$this->load->database('default',TRUE);
    $this->load->model("query_model");
    if($this->session->userdata('user_id') == NULL)
    {
      redirect("Adminlogin","location");
    }
  } 

  public function bets()
  {
    $data['url'] = array_flip($_REQUEST);
  	$this->load->view('templates/admin_header');
    $this->load->view('templates/admin_menu',$data);
    $this->load->view('adminpages/bets');
    // $this->load->view('templates/admin_footer');
  }

  public function tipRes()
  {
    $data['url'] = array_flip($_REQUEST);
  	$this->load->view('templates/admin_header');
    $this->load->view('templates/admin_menu',$data);
    $this->load->view('adminpages/tip_results');
    
  }

  public function stageMatch()
  {
  	$this->load->view('templates/admin_header');
    $this->load->view('templates/admin_menu');
    $this->load->view('adminpages/stage_matches');
    $this->load->view('templates/admin_footer');
  }
   public function listing_bets(){
      $requestData= $_REQUEST;
      $columns = array( 
        0 => 'bet_id',
        1 => 'match_id',
        2 => 'home_team',
        3 => 'away_team',
        4 => 'home_score_bet',
        5 => 'away_score_bet',
        6 => 'user_id',
        7 => 'points',
        7 => 'date_submitted',
      );

      $sql = "";
      $totalData = 0;
      $totalFiltered = 0;

      $sql = "SELECT * FROM (SELECT * FROM tp_bets)X WHERE 1=1 ";

      $records = $this->query_model->getDataCount($sql);

      $totalData = $records["count"];
      $totalFiltered = $totalData;

      //This is for search function...
      if( !empty($requestData['search']['value']) ) {
        $sql.=" AND match_id LIKE '%".$requestData['search']['value']."%'";    
        //$sql.=" OR status LIKE '%".$requestData['search']['value']."%' )";
      }  
      $totalFiltered = $this->query_model->getDataCount($sql)["count"];
      //end of search function

      $sql.=" ORDER BY ". $columns[$requestData['order'][0]['column']]." ".$requestData['order'][0]['dir']." LIMIT ".$requestData['start']." ,".$requestData['length']."   ";
      
      //$totalFiltered = $this->query_model->getDataCount($sql)["count"];

      $records = $this->query_model->getDataArray($sql);


      $data = array();

      $status = "";

      foreach ($records as $row) {
        $nestedData=array();

        // $sqlactive = "SELECT * FROM domains WHERE brand_name = '".$row['id']."' AND STATUS <> 'X'";
        // $sqltotal = "SELECT * FROM domains WHERE brand_name = '".$row['id']."'";

        // $resactive = $this->query_model->getDataCount($sqlactive);
        // $restotal = $this->query_model->getDataCount($sqltotal);

        // $url_active = $resactive['count'];
        // $url_total = $restotal['count'];


        // if($row['status']=='A'){
        //   $status = 'Active';
        // }
        // else{
        //   $status = 'Inactive';
        // }

        $nestedData["bet_id"] = $row["bet_id"];
        $nestedData["match_name"] = $row["match_id"];
        $nestedData["home_team"] = $row["home_team"];
        $nestedData["away_team"] = $row["away_team"];
        $nestedData["home_score_bet"] = $row["home_score_bet"];
        $nestedData["username"] = $row["user_id"];
        $nestedData["home_score_bet"] = $row["home_score_bet"]; 
        $nestedData["points"] = $row["points"];
        $nestedData["date_submitted"] = $row["date_submitted"];
        


       
        $data[] = $nestedData;  
      }

      $json_data = array(
            "draw"            => intval( $requestData['draw'] ),   // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw. 
            "recordsTotal"    => intval( $totalData ),  // total number of records
            "recordsFiltered" => intval( $totalFiltered ), // total number of records after searching, if there is no searching then totalFiltered = totalData
            "data"            => $data   // total data array
            );

      echo json_encode($json_data);       
    }
    public function listing_tips(){
      $requestData= $_REQUEST;
      $columns = array( 
        0 => 'tip_id',
        1 => 'match_id',
        2 => 'site_id',
        3 => 'tips',
        4 => 'status',
        5 => 'created_date',
        6 => 'created_by',
        7 => 'updated_date',
        7 => 'updated_by',
      );

      $sql = "";
      $totalData = 0;
      $totalFiltered = 0;

      $sql = "SELECT * FROM (SELECT * FROM tp_tips)X WHERE 1=1 ";

      $records = $this->query_model->getDataCount($sql);

      $totalData = $records["count"];
      $totalFiltered = $totalData;

      //This is for search function...
      if( !empty($requestData['search']['value']) ) {
        $sql.=" AND match_id LIKE '%".$requestData['search']['value']."%'";    
        //$sql.=" OR status LIKE '%".$requestData['search']['value']."%' )";
      }  
      $totalFiltered = $this->query_model->getDataCount($sql)["count"];
      //end of search function

      $sql.=" ORDER BY ". $columns[$requestData['order'][0]['column']]." ".$requestData['order'][0]['dir']." LIMIT ".$requestData['start']." ,".$requestData['length']."   ";
      
      //$totalFiltered = $this->query_model->getDataCount($sql)["count"];

      $records = $this->query_model->getDataArray($sql);


      $data = array();

      $status = "";

      foreach ($records as $row) {
        $nestedData=array();

        // $sqlactive = "SELECT * FROM domains WHERE brand_name = '".$row['id']."' AND STATUS <> 'X'";
        // $sqltotal = "SELECT * FROM domains WHERE brand_name = '".$row['id']."'";

        // $resactive = $this->query_model->getDataCount($sqlactive);
        // $restotal = $this->query_model->getDataCount($sqltotal);

        // $url_active = $resactive['count'];
        // $url_total = $restotal['count'];


        // if($row['status']=='A'){
        //   $status = 'Active';
        // }
        // else{
        //   $status = 'Inactive';
        // }

        $nestedData["tip_id"] = $row["tip_id"];
        $nestedData["match_name"] = $row["match_id"];
        $nestedData["tips"] = $row["tips"];
        $nestedData["status"] = $row["status"];
        $nestedData["created_date"] = $row["created_date"];
        $nestedData["created_by"] = $row["created_by"];
        $nestedData["updated_date"] = $row["updated_date"]; 
        $nestedData["updated_by"] = $row["updated_by"];
      
       
        $data[] = $nestedData;  
      }

      $json_data = array(
            "draw"            => intval( $requestData['draw'] ),   // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw. 
            "recordsTotal"    => intval( $totalData ),  // total number of records
            "recordsFiltered" => intval( $totalFiltered ), // total number of records after searching, if there is no searching then totalFiltered = totalData
            "data"            => $data   // total data array
            );

      echo json_encode($json_data);       
    }

}

?>