<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Management extends CI_Controller {

  function __construct(){
    parent::__construct();
    //$this->load->database('default',TRUE);
    $this->load->model("query_model");
    if($this->session->userdata('user_id') == NULL)
    {
      redirect("Adminlogin","location");
    }
  } 

  public function users()
  {
    $data['url'] = array_flip($_REQUEST);
  	$this->load->view('templates/admin_header');
    $this->load->view('templates/admin_menu',$data);
    $this->load->view('adminpages/users_view');
    // $this->load->view('templates/admin_footer');
  }

  public function members()
  {
    $data['url'] = array_flip($_REQUEST);
    $this->load->view('templates/admin_header');
    $this->load->view('templates/admin_menu',$data);
    $this->load->view('adminpages/members_view');
    // $this->load->view('templates/admin_footer');
  }
  public function category()
  {
    $data['url'] = array_flip($_REQUEST);
    $this->load->view('templates/admin_header');
    $this->load->view('templates/admin_menu',$data);
    $this->load->view('adminpages/category_view');
    // $this->load->view('templates/admin_footer');
  }
  public function league_masterlist()
  {
    $data['url'] = array_flip($_REQUEST);
    $this->load->view('templates/admin_header');
    $this->load->view('templates/admin_menu',$data);
    $this->load->view('adminpages/league_masterlist');
    // $this->load->view('templates/admin_footer');
  }
  public function match_masterlist()
  {
    $data['url'] = array_flip($_REQUEST);
    $this->load->view('templates/admin_header');
    $this->load->view('templates/admin_menu',$data);
    $this->load->view('adminpages/match_masterlist');
    // $this->load->view('templates/admin_footer');
  }
  public function stages_masterlist()
  {
    $data['url'] = array_flip($_REQUEST);
    $this->load->view('templates/admin_header');
    $this->load->view('templates/admin_menu',$data);
    $this->load->view('adminpages/stage_masterlist_view');
    // $this->load->view('templates/admin_footer');
  }

  public function players_masterlist()
  {
    $data['url'] = array_flip($_REQUEST);
    $this->load->view('templates/admin_header');
    $this->load->view('templates/admin_menu',$data);
    $this->load->view('adminpages/players_masterlist_view');
    // $this->load->view('templates/admin_footer');
  }

  public function setting()
  {
    $data['url'] = array_flip($_REQUEST);
    $apikey['key'] = $this->query_model->getCell("SELECT api_key FROM tp_setting WHERE id = 1","api_key");
    $this->load->view('templates/admin_header');
    $this->load->view('templates/admin_menu',$data);
    $this->load->view('adminpages/setting_view',$apikey);
    // $this->load->view('templates/admin_footer');
  }

  public function teams_masterlist()
  {
    $data['url'] = array_flip($_REQUEST);
    $this->load->view('templates/admin_header');
    $this->load->view('templates/admin_menu',$data);
    $this->load->view('adminpages/teams_view');
  }

  public function getLeague()
  { 
        $sql = '';

        $sql = "SELECT * FROM (SELECT * FROM tp_league)X WHERE 1=1 ORDER BY country_name ";

        $result = $this->query_model->getDataArray($sql);

        echo json_encode($result);

  }
   public function getCountries()
  { 
       

        $sql = '';

        $sql = "SELECT * FROM (SELECT * FROM tp_allcountries)X WHERE 1=1 ORDER BY country_name ";

        $result = $this->query_model->getDataArray($sql);

        echo json_encode($result);

  }
  public function getStage()
  {

        $league_id = $this->query_model->clean("league_id");

        $sql = '';

        $sql = "SELECT * FROM (SELECT * FROM tp_league_stages WHERE league_id='$league_id' AND status <> 'X')X WHERE 1=1 ORDER BY stage_name ";

        $result = $this->query_model->getDataArray($sql);

        if(!$result)
        {
          $result = array('alert_typ' => 'error', 'alert_ttl' => "ERROR MESSAGE", 'alert_msg' => "NO STAGES!"); 
        }


      echo json_encode($result);


  }
   public function getStageD()
  {

        $sid = $this->query_model->clean("sid");

        $sql = '';

        $sql = "SELECT * FROM (SELECT * FROM tp_league_stages WHERE stage_id='$sid' AND status <> 'X')X WHERE 1=1";

        $result = $this->query_model->getDataArray($sql);

        if(!$result)
        {
          $result = array('alert_typ' => 'error', 'alert_ttl' => "ERROR MESSAGE", 'alert_msg' => "NO STAGES!"); 
        }


      echo json_encode($result);


  }
   public function addStage()
  {
          $league_id = $this->query_model->clean("league_id");
          $stage_name = $this->input->post("stage_name");
          $dstartend = $this->query_model->clean("dstartend");
          $darray = explode('-', $dstartend);
          $dstart = $darray[0];
          $dend = $darray[1];
          $datenow = date('Y-m-d h:i:s');
          $userdata = $this->session->userdata('user_id');
         
          $exist = $this->query_model->getCell("SELECT COUNT(*) as ttl FROM tp_league_stages WHERE stage_name = '$stage_name' AND league_id = '$league_id' ","ttl");
          
          if($exist == 0)
          {
            

                $sql = "INSERT INTO tp_league_stages (league_id,stage_name,date_start,date_end,status,created_by,created_date) 
                VALUES ('".$league_id."','".$stage_name."','".$dstart."','".$dend."','N','".$userdata."','".$datenow."')";

                $res = $this->query_model->execSQL($sql);
          

                $result = array('alert_typ' => 'success', 'alert_ttl' => "SUCCESS MESSAGE", 'alert_msg' => "ADD STAGE SUCCESSFUL!");   
                
                 
          }
          else
          {
             $result = array('alert_typ' => 'error', 'alert_ttl' => "ERROR MESSAGE", 'alert_msg' => "EXISTING STAGE NAME!"); 
          }
            
            
           echo json_encode($result);
  }
   public function editStage()
  {
          $league = $this->query_model->clean("league_id");
          $stages = $this->input->post("stages");
          $startend = $this->query_model->clean("startend");
          $sid = $this->query_model->clean("sid","id");
          // var_dump($status);
          $darray = explode('-', $startend);
          $dstart = $darray[0];
          $dend = $darray[1];
          $datenow = date('Y-m-d h:i:s');
          $userdata = $this->session->userdata('user_id');
         
          // $sql = "UPDATE category SET category_name='".$category_name."', status = '".$status."', updated_date = '".$datenow."', updated_by = '".$userdata."' 
          // WHERE category_name='".$category_name."' AND category_type='".$category_type."' ";

          $sql = "UPDATE tp_league_stages SET stage_name='".$stages."',status='N',date_start='".$dstart."',date_end='".$dend."',updated_by='".$userdata."',updated_date='".$datenow."' WHERE stage_id=$sid";

              if($sql)
              {
                $res = $this->query_model->execSQL($sql);
          

                $result = array('alert_typ' => 'success', 'alert_ttl' => "SUCCESS MESSAGE", 'alert_msg' => "EDIT STAGE SUCCESSFUL!"); 
              }
                  
            
                 echo json_encode($result);
  }
  
  public function delStage()
  {
     $sid = $this->query_model->clean('sid','id');
     $datenow = date('Y-m-d h:i:s');
    $userdata = $this->session->userdata('user_id');
    // var_dump($sid);
      $this->query_model->execSQL("UPDATE tp_league_stages SET status='X',updated_by='".$userdata."',updated_date='".$datenow."' WHERE stage_id=$sid");
      $result = array('alert_typ' => 'success', 'alert_ttl' => "DEL MESSAGE", 'alert_msg' => "DELETE STAGE SUCCESS!"); 
      echo json_encode($result);

  }
   public function addLeague()
  {
          $league_name = $this->query_model->clean("league_name");
          $country = $this->input->post("country");
          $dstartend = $this->query_model->clean("dstartend");
          $season_name = $this->query_model->clean("season_name");
          $darray = explode('-', $dstartend);
          $dstart = $darray[0];
          $dend = $darray[1];
          $datenow = date('Y-m-d h:i:s');
          $userdata = $this->session->userdata('user_id');
         
          $exist = $this->query_model->getCell("SELECT COUNT(*) as ttl FROM tp_league WHERE league_name = '$league_name' AND season = '$season_name'  ","ttl");
          
          if($exist == 0)
          {
            

                $sql = "INSERT INTO tp_league (league_name,country_id,season,date_start,date_end,status,created_by,created_date) 
                VALUES ('".$league_name."','".$country."','".$season_name."','".$dstart."','".$dend."','N','".$userdata."','".$datenow."')";

                $res = $this->query_model->execSQL($sql);
          

                $result = array('alert_typ' => 'success', 'alert_ttl' => "SUCCESS MESSAGE", 'alert_msg' => "ADD STAGE SUCCESSFUL!");   
                
                 
          }
          else
          {
             $result = array('alert_typ' => 'error', 'alert_ttl' => "ERROR MESSAGE", 'alert_msg' => "EXISTING STAGE NAME!"); 
          }
            
            
           echo json_encode($result);
  }

 
}

?>