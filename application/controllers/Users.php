<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Users extends CI_Controller {

  function __construct(){
    parent::__construct();
    //$this->load->database('default',TRUE);
    $this->load->model("query_model");
  } 

  public function createUser(){
  	$result = array();
  	$editID = $this->query_model->clean('editID','id');
  	$username = $this->query_model->clean('username');
  	$password = $this->query_model->clean('password');
  	$realname = $this->query_model->clean('realname');
  	$userrole = $this->query_model->clean('selUserRole','id');
  	$status = $this->query_model->clean('selStatus');
  	if($editID != 0 || $editID != '')
  	{
  		$usernamechk = $this->query_model->getDataArray("SELECT username FROM tp_users WHERE user_id = ".$editID);
  		if($usernamechk[0]['username'] == $username)
  		{
  			// echo "1";
  			if($password != '')
			{
  				$sql = "UPDATE tp_users SET username = '".$username."',password = '".md5($password)."',realname = '".$realname."',user_role = ".$userrole.",status = '".$status."'
  				WHERE user_id = ".$editID;
  			}else{
  				$sql = "UPDATE tp_users SET username = '".$username."',realname = '".$realname."',user_role = ".$userrole.",status = '".$status."'
  				WHERE user_id = ".$editID;
			}
  			$ex = $this->query_model->execSQL($sql);
  			$result = array("typ" => "success", "ttl" => "Edit User", "msg" => "User has been updated!");

  		}
  		else 
  			// ($usernamechk[0]['username'] != $username)
  		{
  			// echo "2";
  			$checkuser = $this->query_model->getCell("SELECT COUNT(1) AS cnt FROM tp_users WHERE username = '".$username."'","cnt");
  			

  			if($checkuser>0)
  			{
				  $result = array("typ" => "error", "ttl" => "Edit User", "msg" => "username exist!");
			  }else{
				if($password != '')
				{
	  				$sql = "UPDATE tp_users SET username = '".$username."',password = '".md5($password)."',realname = '".$realname."',user_role = ".$userrole.",status = '".$status."'
	  				WHERE user_id = ".$editID;
	  			}else{
	  				$sql = "UPDATE tp_users SET username = '".$username."',realname = '".$realname."',user_role = ".$userrole.",status = '".$status."'
	  				WHERE user_id = ".$editID;
	  			}
	  			$ex = $this->query_model->execSQL($sql);
	  			$result = array("typ" => "success", "ttl" => "Edit User", "msg" => "User has been updated!");
			}
  		}
  			
  	}else{
  		$checkuser = $this->query_model->getCell("SELECT COUNT(1) AS cnt FROM tp_users WHERE username = '".$username."'","cnt");
  		if($checkuser>0)
		{
			// echo "3";
			$result = array("typ" => "error", "ttl" => "Edit User", "msg" => "username exist!");
		}
		else
		{
			// echo "4";
	  		$sql = "INSERT INTO tp_users(username, password, realname, user_role, status, created_date, created_by)
	  			VALUES ('".$username."','".md5($password)."','".$realname."','".$userrole."','".$status."','".date('Y-m-d H:i:s')."','".$this->session->userdata('user_id')."')";
		  	$exec = $this->query_model->execSQL($sql);

		  	$result = array("typ" => "success", "ttl" => "Add User", "msg" => "User has been added!");
	  	}
  	}

  	

  	echo json_encode($result);
  }

  public function delUser()
  {
  	$user_id = $this->query_model->clean('delID','id');
  	// echo $user_id;

  	$delUser = $this->query_model->execSQL("UPDATE tp_users SET status = 'X' WHERE user_id = ".$user_id);
  	$result = array("typ" => "success", "ttl" => "Delete User", "msg" => "User has been deleted!");
  	echo json_encode($result);
  }

   public function listing(){
    $requestData= $_REQUEST;
    $columns = array( 
      0 => 'user_id',
      1 => 'user_id',
      2 => 'username',
      3 => 'realname',
      4 => 'user_role',
      5 => 'status',
      6 => 'created_date',
      7 => 'created_by'
    );


    $sql = "";
    $totalData = 0;
    $totalFiltered = 0;

    $sql = "SELECT * FROM (SELECT u.user_id, u.username, u.realname, u.user_role, u.status, u.created_date, (SELECT realname FROM tp_users WHERE user_id = u.created_by) AS created_by FROM tp_users u WHERE u.status <> 'X')X WHERE 1=1 ";

    $records = $this->query_model->getDataCount($sql);

    $totalData = $records["count"];
    $totalFiltered = $totalData;

    //This is for search function...
      if( !empty($requestData['search']['value']) ) {
        $sql.=" AND username LIKE '%".$requestData['search']['value']."%' OR username LIKE '%".$requestData['search']['value']."%'";    
        //$sql.=" OR status LIKE '%".$requestData['search']['value']."%' )";
      }  
      $totalFiltered = $this->query_model->getDataCount($sql)["count"];
      //end of search function

      $sql.=" ORDER BY ". $columns[$requestData['order'][0]['column']]." ".$requestData['order'][0]['dir']." LIMIT ".$requestData['start']." ,".$requestData['length']."   ";
    
    //$totalFiltered = $this->query_model->getDataCount($sql)["count"];

    $records = $this->query_model->getDataArray($sql);


    $data = array();

    foreach ($records as $row) {
      $nestedData=array();
      
      $nestedData["user_id"] = $row["user_id"];
      $nestedData["username"] = $row["username"];
      $nestedData["realname"] = $row["realname"];
      $nestedData["user_role"] = $row["user_role"];
      $nestedData["status"] = $row["status"];
      $nestedData["created_date"] = $row["created_date"];
      $nestedData["created_by"] = $row["created_by"];

      $data[] = $nestedData;  
    }

    $json_data = array(
          "draw"            => intval( $requestData['draw'] ),   // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw. 
          "recordsTotal"    => intval( $totalData ),  // total number of records
          "recordsFiltered" => intval( $totalFiltered ), // total number of records after searching, if there is no searching then totalFiltered = totalData
          "data"            => $data   // total data array
          );

    echo json_encode($json_data);       
  }  
}
?>