<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Setting extends CI_Controller {

  function __construct(){
    parent::__construct();
    //$this->load->database('default',TRUE);
    $this->load->model("query_model");
    $this->load->library('utilities');
    if($this->session->userdata('user_id') == NULL)
    {
      redirect("Adminlogin","location");
    }
    ini_set('max_execution_time', 0); 
    ini_set('memory_limit','2048M');
  } 

  public function getCountries()
  {
    $sql = "SELECT * FROM tp_setting WHERE id='1'";
    $apiresult = $this->query_model->getDataArray($sql);
    $APIkey = $apiresult[0]['api_key'];

    $from = '2016-10-30';
    $to = '2016-11-01';
    $league_id = 62;

    $curl_options = array(
      CURLOPT_URL => "https://sportickr.com/api/v1.0/countries?api-key=$APIkey",
      CURLOPT_RETURNTRANSFER => true,
      CURLOPT_HEADER => false,
      CURLOPT_TIMEOUT => 30,
      CURLOPT_CONNECTTIMEOUT => 5
    );

    $curl = curl_init();
    curl_setopt_array( $curl, $curl_options );
    $result = curl_exec( $curl );

    $result = (array) json_decode($result);

    // $sql = "";
    // // var_dump($result);
    foreach ($result['data'] as $r) {

      $chkcntry = $this->query_model->getCell("SELECT COUNT(1) as cnt FROM tp_countries WHERE ref_country_id =".$r->id,"cnt");
      if($chkcntry==0)
      {
        $name = str_replace("'","''",$r->name);
        // echo $r->country_id;
        $sql = "INSERT IGNORE INTO tp_countries (ref_country_id, ref_continent_id, country_name,status)
        VALUES ('".$r->id."','".$r->continent_id."','".$name."','N')";
        $this->query_model->execSQL($sql);
      }
    }

    $getCountries = $this->query_model->getDataCount("SELECT * FROM tp_countries WHERE status <> 'X'");

    if($getCountries['count']>0)
    {
      for($i=0; $i<$getCountries['count']; $i++)
      {
        $cntryid = $getCountries['data'][$i]['country_id'];
        $refcntryid = $getCountries['data'][$i]['ref_country_id'];
        

        $curl_options = array(
          CURLOPT_URL => "https://sportickr.com/api/v1.0/leagues?api-key=$APIkey",
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_HEADER => false,
          CURLOPT_TIMEOUT => 30,
          CURLOPT_CONNECTTIMEOUT => 5
        );

        $curl = curl_init();
        curl_setopt_array( $curl, $curl_options );
        $result_league = curl_exec( $curl );

        $result_league = (array) json_decode($result_league);
        // var_dump($result_league);

        foreach ($result_league['data'] as $l) {

          if($refcntryid == $l->country_id)
          {
            // echo $refcntryid;
            $chkLeague = $this->query_model->getCell("SELECT COUNT(1) AS cnt FROM tp_league WHERE ref_country_id = ".$l->country_id." AND ref_league_id=".$l->id,"cnt");
            if($chkLeague==0)
            {
              $cntry_name = $this->query_model->getCell("SELECT country_name FROM tp_countries WHERE ref_country_id = ".$l->country_id,"country_name");
              
              $name= str_replace("'","''",$cntry_name);

              $sqlins = "INSERT IGNORE INTO tp_league (country_id, ref_league_id, ref_country_id, country_name, league_name, status, is_cup, ref_current_season_id, ref_current_round_id, ref_current_stage_id, created_date, created_by)
              VALUES ('".$cntryid."','".$l->id."','".$refcntryid."','".$name."','".$l->name."','N','".$l->is_cup."','".$l->current_season_id."','".$l->current_round_id."','".$l->current_stage_id."','".date('Y-m-d H:i:s')."','".$this->session->userdata('user_id')."')";
              // echo $sqlins;
              $this->query_model->execSQL($sqlins);
            }
          }
        }

        
        
      }// end for($i=0; $i<$getCountries['count']; $i++)

    }// end if($getCountries['count']>0)

    // TO GET SEASON
    $this->getSeason();
    // TO GET Stages
    // $this->getStages();

    $res = array("typ" => "success", "ttl" => "Get Countries / Leagues / Seasons ", "msg" => "Done!");    
    echo json_encode($res);
  }

   public function leagueListing()
  {
    $country_id = $this->query_model->clean('cntry');
    $league_id = $this->query_model->clean('selLeague');
    $season = $this->query_model->clean('selSeason');
    $status = $this->query_model->clean('selstatus');
    // var_dump($country_id);

    // echo "SELECT * FROM (SELECT * FROM tp_league WHERE status <>'X')X WHERE 1=1 AND country_id = '".$country_id."' ";
    // echo $country_id." ".$league_id." ".$season." ".$status;
    // echo "COUNTRY ID: ".$country_id;
    // exit;

    $requestData= $_REQUEST;
    $columns = array( 
      0 => 'league_id',
      1 => 'league_id',
      2 => 'country_id',
      3 => 'ref_league_id',
      4 => 'ref_country_id',
      5 => 'country_name',
      6 => 'league_name',
      7 => 'img_path',
      8 => 'league_nick',
      // 7 => 'year_start',
      // 8 => 'year_end',
      // 7 => 'season',
      9 => 'status'
    );

    $sql = "";
    $totalData = 0;
    $totalFiltered = 0;

      // $sql = "SELECT * FROM (SELECT l.`league_id`, l.`country_id`, l.`ref_league_id`, l.`ref_country_id`, l.`master_id`, l.`country_name`, l.`league_name`,
      //   s.season_id,s.season_name AS season,l.`date_start`,l.`date_end`, s.`status`,l.`created_date`,IFNULL((SELECT realname FROM tp_users WHERE user_id = l.`created_by`),'') AS created_by,
      //   l.`updated_date`, IFNULL((SELECT realname FROM tp_users WHERE user_id = l.`updated_by`),'') AS `updated_by`, l.`ref_current_round_id`,
      //   l.`ref_current_season_id`, l.`ref_current_stage_id`, l.`is_cup`
      //   FROM tp_league l
      //   INNER JOIN tp_season s ON l.league_id = s.league_id
      //   WHERE s.status <> 'X'
      //   )X WHERE 1=1  ";
     $sql = "SELECT * FROM (SELECT l.`league_id`, l.`country_id`, l.`ref_league_id`, l.`ref_country_id`, l.`master_id`, l.`country_name`, l.`league_name`, l.`img_path`, IFNULL(l.`league_nick`,'') AS league_nick,
        l.`date_start`,l.`date_end`, l.`status`,l.`created_date`,
        IFNULL((SELECT realname FROM tp_users WHERE user_id = l.`created_by`),'') AS created_by,
        l.`updated_date`, IFNULL((SELECT realname FROM tp_users WHERE user_id = l.`updated_by`),'') AS `updated_by`, l.`ref_current_round_id`,
        l.`ref_current_season_id`, l.`ref_current_stage_id`, l.`is_cup`
        FROM tp_league l WHERE l.status <> 'X')X WHERE 1=1 ";
   
    $records = $this->query_model->getDataCount($sql);

      $totalData = $records["count"];
      $totalFiltered = $totalData;
      // if(!empty($brandfilter)){
      //   $filterbrand = $this->query_model->getDataArray("SELECT * from brands Where status='A' AND brand_name='".$brandfilter."'");
      //   $sql.=" AND brand_name = '".$filterbrand[0]['id']."'"; 
      // }
      if(!empty($country_id)){
        $sql.=" AND country_id = '".$country_id."' "; 
      }

      if(!empty($league_id)){
        $sql.=" AND league_id = '".$league_id."'"; 
      }

      // if(!empty($season)){
      //   $sql.=" AND season = '".$season."'"; 
      // }

      if(!empty($status)){
        $sql.=" AND status = '".$status."'"; 
      }
      // if(!empty($statusfilter)){
      //   $sql.=" AND status LIKE '%".$statusfilter."%'"; 
      // }
      //This is for search function...
      if( !empty($requestData['search']['value']) ) {
        $sql.=" AND (country_name LIKE '%".$requestData['search']['value']."%' ";    
        $sql.=" OR league_nick LIKE '%".$requestData['search']['value']."%' ";    
        $sql.=" OR league_name LIKE '%".$requestData['search']['value']."%') ";

        // $sql.=" OR season LIKE '%".$requestData['search']['value']."%') ";
      }  
      $totalFiltered = $this->query_model->getDataCount($sql)["count"];
      //end of search function


    $sql.=" ORDER BY ". $columns[$requestData['order'][0]['column']]." ".$requestData['order'][0]['dir']." LIMIT ".$requestData['start']." ,".$requestData['length']."   ";
    
    //$totalFiltered = $this->query_model->getDataCount($sql)["count"];
    // echo $sql;

    $records = $this->query_model->getDataArray($sql);


    $data = array();

    foreach ($records as $row) {
      $nestedData=array();
      
      $nestedData["league_id"] = $row["league_id"];
      $nestedData["country_id"] = $row["country_id"];
      $nestedData["ref_league_id"] = $row["ref_league_id"];
      $nestedData["ref_country_id"] = $row["ref_country_id"];
      $nestedData["country_name"] = $row["country_name"];
      $nestedData["league_name"] = $row["league_name"];
      $nestedData["img_path"] = $row["img_path"];
      $nestedData["league_nick"] = $row["league_nick"];
      // $nestedData["year_start"] = $row["year_start"];
      // $nestedData["year_end"] = $row["year_end"];
      // $nestedData["season_id"] = $row["season_id"];
      // $nestedData["season"] = $row["season"];
      $nestedData["status"] = $row["status"];
    

      $data[] = $nestedData;  
    }
    $json_data = array(
          "draw"            => intval( $requestData['draw'] ),   // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw. 
          "recordsTotal"    => intval( $totalData ),  // total number of records
          "recordsFiltered" => intval( $totalFiltered ), // total number of records after searching, if there is no searching then totalFiltered = totalData
          "data"            => $data   // total data array
          );

    echo json_encode($json_data);  
  }

 public function updLeague()
  {
    $result = array();
    $result = array();
    $oldfilename = "";
    $extension = "";
    $filename = "";
    $sql = "";
    $country_id = $this->query_model->clean('cntryid','id');
    $league_nick = $this->query_model->clean('lgnick');
    $league_id = $this->query_model->clean('league_id','id');
    $path = $this->config->item('upload_leagues_logo');
    $tm = str_replace(' ','',$league_nick);
  

    $sql ="UPDATE tp_league SET league_nick = '".$league_nick."' ";

    if(isset($_FILES['updxFileTxt'])) 
    { 
      $oldfilename = explode(".", $_FILES['updxFileTxt']['name']); 
      $extension = $oldfilename[count($oldfilename)-1];
      $filename = $country_id."_".$tm."_logo.".$extension;

      $upload = $this->utilities->upload_img_logo("updxFileTxt",$path,$filename);
      if($upload["status"] == 1)
      {
        $sql .=" , img_path = '".$filename."' ";
        $sql .= " WHERE league_id = ".$league_id;

        // echo $sql;
        $this->query_model->execSQL($sql);
        $result = array("typ" => "success", "ttl" => "Update League", "msg" => "League has been updated!");
      }
      else{
        $result = array("typ" => "error", "ttl" => "Update League", "msg" => "Error!");
      }

    }else{
      $sql .= " WHERE league_id = ".$league_id;
      // echo $sql;
      $this->query_model->execSQL($sql);
      $result = array("typ" => "success", "ttl" => "Update League", "msg" => "League has been updated!");
    }

  
    echo json_encode($result);
  }
  public function getSeason()
  {
    
    $sql = "SELECT * FROM tp_setting WHERE id='1'";
    $seasonsql = "SELECT ref_current_season_id FROM tp_league";

    $apiresult = $this->query_model->getDataArray($sql);
    $ref_season = $this->query_model->getDataArray($seasonsql);

    $APIkey = $apiresult[0]['api_key'];

    for($x = 0 ;$x<count($ref_season); $x++){
       $season_id = $ref_season[$x]['ref_current_season_id'];

      $curl_options = array(
        CURLOPT_URL => "https://sportickr.com/api/v1.0/seasons/$season_id?api-key=$APIkey",
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_HEADER => false,
        CURLOPT_TIMEOUT => 30,
        CURLOPT_CONNECTTIMEOUT => 5
      );

      $curl = curl_init();
      curl_setopt_array( $curl, $curl_options );
      $result = curl_exec( $curl );

      $result = (array) json_decode($result,true);
      // var_dump($result['data']);
     
     
        
       if($result['data']['is_current_season']==true){
        // var_dump("TRUE");
        $chkseason = $this->query_model->getCell("SELECT COUNT(1) as cnt FROM tp_season WHERE ref_season_id =".$result['data']['id'],"cnt");
        if($chkseason==0)
        {
          $name = str_replace("'","''",$result['data']['name']);
          $league_id = $this->query_model->getDataArray("SELECT COUNT(1) AS cnt, league_id, league_name FROM tp_league WHERE ref_league_id =".$result['data']['league_id']);


          if($league_id[0]['cnt'] > 0)
          {
            // echo "SELECT COUNT(1) AS cnt, league_id, league_name FROM tp_league WHERE ref_league_id =".$r->league_id."<br>";

            // echo $league_id[0]['league_name']."<br>";

            $sql = "INSERT IGNORE INTO tp_season (ref_season_id, season_name, league_id, ref_league_id, is_current_season, ref_current_round_id, ref_current_stage_id, status, created_date, created_by)
            VALUES ('".$result['data']['id']."','".$name."','".$league_id[0]['league_id']."','".$result['data']['league_id']."','".$result['data']['is_current_season']."','".$result['data']['current_round_id']."','".$result['data']['current_stage_id']."','"."N"."','".date('Y-m-d H:i:s')."','".$this->session->userdata('user_id')."')";
            $this->query_model->execSQL($sql);

            // $this->getStages($result['data']['id']);
          }
          // echo $r->country_id;
          
        }
       }
        
     
    } 
  }
  public function getSeasonList()
  {
    $country_id = $this->query_model->clean('cntry'); 
    $league_id = $this->query_model->clean('selLeague'); 
    // echo $country_id;
    $arr = array();

    $sql = "";
    $sql .= "SELECT DISTINCT(season_name) FROM tp_season WHERE status <> 'X'";
    // if($country_id != '')
    // {
    //   $sql .="AND country_id = ".$country_id." ";
    // }

    if($league_id != '')
    {
      $sql .="AND league_id = ".$league_id." ";
    }
    $sql .= " ORDER BY season_name ASC";
    $result = $this->query_model->getDataArray($sql);

    // echo $sql;

    echo json_encode($result);
  }
   public function getLeagueList()
  {
    $country_id = $this->query_model->clean('cntry'); 
    // echo $country_id;
    $arr = array();

    $sql = "";
    $sql .= "SELECT * FROM tp_league WHERE status <> 'X' ";
    if($country_id != '')
    {
      $sql .="AND country_id = ".$country_id;
    }

    $sql .= " ORDER BY league_name ASC";
    $result = $this->query_model->getDataArray($sql);

    // echo $sql;

    echo json_encode($result);
  }
    public function getCountryList()
  {
    
    $arr = array();
    $result = $this->query_model->getDataArray("SELECT * FROM tp_countries WHERE status <> 'X' ORDER BY country_name ASC");
    echo json_encode($result);
  }
  //  public function getHead2Head()
  // {
  //   $sql = "SELECT * FROM tp_setting WHERE id='1'";
  //   $apiresult = $this->query_model->getDataArray($sql);
  //   $APIkey = $apiresult[0]['api_key'];

    
  //   $teamres  = $this->query_model->getDataArray("SELECT ref_team_id  FROM tp_teams");
  //   $storeid = '';

  //   $res = "";
  //   for($x = 0 ; $x<count($teamres) ; $x++){
  //     $teamid = $teamres[$x]['ref_team_id'];

  //     for($xx = 0 ; $xx<count($teamres) ; $xx++){
  //       $teamid2 = $teamres[$xx]['ref_team_id'];
  //       if($teamid != $teamid2){
  //         $curl_options = array(
  //         CURLOPT_URL => "https://sportickr.com/api/v1.0/head2head/$teamid/$teamid2?api-key=$APIkey",
  //         CURLOPT_RETURNTRANSFER => true,
  //         CURLOPT_HEADER => false,
  //         CURLOPT_TIMEOUT => 30,
  //         CURLOPT_CONNECTTIMEOUT => 5
  //         );

  //         $curl = curl_init();
  //         curl_setopt_array( $curl, $curl_options );
  //         $result = curl_exec( $curl );

  //         $result = (array) json_decode($result,true);
  //         // var_dump($result);
  //         if(count($result['data'])>0){
  //           for($i = 0 ; $i<count($result['data']); $i ++){

  //             $chkmatches = $this->query_model->getCell("SELECT COUNT(1) as cnt FROM tp_stage_match WHERE ref_match_id =".$result['data'][$i]['id'],"cnt");
  //             if($chkmatches==0)
  //             {
                
  //               $league_id = $this->query_model->getDataArray("SELECT COUNT(1) AS cnt, league_id, league_name FROM tp_league WHERE ref_league_id =".$result['data'][$i]['league_id']);
  //               $season_id = $this->query_model->getDataArray("SELECT COUNT(1) AS cnt, season_id, season_name FROM tp_season WHERE ref_season_id =".$result['data'][$i]['season_id']);
  //               $stage_id = $this->query_model->getDataArray("SELECT COUNT(1) AS cnt, stage_id, stage_name FROM tp_league_stages WHERE ref_stage_id =".$result['data'][$i]['stage_id']);
  //               $round_id = $this->query_model->getDataArray("SELECT COUNT(1) AS cnt, round_id, round_name FROM tp_rounds WHERE ref_round_id =".$result['data'][$i]['round_id']);


  //               if($league_id[0]['cnt'] > 0 && $season_id[0]['cnt']>0 && $stage_id[0]['cnt']>0 && $round_id[0]['cnt']>0)
  //               {
  //                 // echo "SELECT COUNT(1) AS cnt, league_id, league_name FROM tp_league WHERE ref_league_id =".$r->league_id."<br>";

  //                 // echo $league_id[0]['league_name']."<br>";

  //                 $sql = "INSERT IGNORE INTO tp_stage_match (ref_match_id, stage_id, league_id, season_id, round_id, status, created_date, created_by,home_team_id,away_team_id)
  //                 VALUES ('".$result['data'][$i]['id']."','".$stage_id[0]['stage_id']."','".$league_id[0]['league_id']."','".$season_id[0]['season_id']."','".$round_id[0]['round_id']."','"."N"."','".date('Y-m-d H:i:s')."','".$this->session->userdata('user_id')."','".$result['data'][$i]['localteam_id']."','".$result['data'][$i]['visitorteam_id']."')";
  //                 $this->query_model->execSQL($sql);
  //                 $res = "SUCCESS";

  //                 // $this->getStages($result['data']['id']);
  //               }
               
  //               // echo $r->country_id;
            
  //               }
               
  //             }
           
  //         }
  //         else{
  //           $res = "ERROR";
  //         }
         
         
  //         }

  //      }
     
  //   }
  //   if($res == "SUCCESS")
  //   {
  //     $res = array("typ" => "success", "ttl" => "Get Head2Head", "msg" => "Done!");    
      
  //   }
  //   else{
  //     $res = array("typ" => "error", "ttl" => "Get Head2Head", "msg" => "ERROR!");    
      
  //   }
  // echo json_encode($res);
   

  // }
 public function getHead2Head()
  {
    $sql = "SELECT * FROM tp_setting WHERE id='1'";
    $apiresult = $this->query_model->getDataArray($sql);
    $APIkey = $apiresult[0]['api_key'];
    $today = date('Y-m-d');
    $tomorrow = date("Y-m-d", strtotime("+ 2 day"));
    
    $teamres  = $this->query_model->getDataArray("SELECT ref_team_id  FROM tp_teams");
    $storeid = '';

   
   
          $curl_options = array(
          CURLOPT_URL => "https://sportickr.com/api/v1.0/fixtures/between/$today/$tomorrow?api-key=$APIkey",
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_HEADER => false,
          CURLOPT_TIMEOUT => 30,
          CURLOPT_CONNECTTIMEOUT => 5
          );

          $curl = curl_init();
          curl_setopt_array( $curl, $curl_options );
          $result = curl_exec( $curl );

          $result = (array) json_decode($result,true);
          // var_dump($result['data']);
          // var_dump(count($result['data'])>0);
          if(count($result['data'])>0){
             $res = "";
            for($i = 0 ; $i<count($result['data']); $i ++){

              $chkmatches = $this->query_model->getCell("SELECT COUNT(1) as cnt FROM tp_stage_match WHERE ref_match_id =".$result['data'][$i]['id'],"cnt");
              if($chkmatches==0)
              {
                
                $league_id = $this->query_model->getDataArray("SELECT COUNT(1) AS cnt, league_id, league_name FROM tp_league WHERE ref_league_id =".$result['data'][$i]['league_id']);
                $season_id = $this->query_model->getDataArray("SELECT COUNT(1) AS cnt, season_id, season_name FROM tp_season WHERE ref_season_id =".$result['data'][$i]['season_id']);
                $stage_id = $this->query_model->getDataArray("SELECT COUNT(1) AS cnt, stage_id, stage_name FROM tp_league_stages WHERE ref_stage_id =".$result['data'][$i]['stage_id']);
                $round_id = $this->query_model->getDataArray("SELECT COUNT(1) AS cnt, round_id, round_name FROM tp_rounds WHERE ref_round_id =".$result['data'][$i]['round_id']);


                if($league_id[0]['cnt'] > 0 && $season_id[0]['cnt']>0 && $stage_id[0]['cnt']>0 && $round_id[0]['cnt']>0)
                {
                  // echo "SELECT COUNT(1) AS cnt, league_id, league_name FROM tp_league WHERE ref_league_id =".$r->league_id."<br>";

                  // echo $league_id[0]['league_name']."<br>";

                  $sql = "INSERT IGNORE INTO tp_stage_match (ref_match_id, stage_id, league_id, season_id, round_id, status, created_date, created_by,home_team_id,away_team_id)
                  VALUES ('".$result['data'][$i]['id']."','".$stage_id[0]['stage_id']."','".$league_id[0]['league_id']."','".$season_id[0]['season_id']."','".$round_id[0]['round_id']."','"."N"."','".date('Y-m-d H:i:s')."','".$this->session->userdata('user_id')."','".$result['data'][$i]['localteam_id']."','".$result['data'][$i]['visitorteam_id']."')";
                  $this->query_model->execSQL($sql);
                  $res = "SUCCESS";

                  // $this->getStages($result['data']['id']);
                }
                else{
                  $res = "SUCCESS";
                  continue;
                }
               
                // echo $r->country_id;
            
                }
               
              }
           
          
        
     
           }
           else{
             $res = "ERROR";
           }
    if($res != "ERROR")
    {
      $res = array("typ" => "success", "ttl" => "Get Matches", "msg" => "Done!");    
      
    }
    else{
      $res = array("typ" => "error", "ttl" => "Get Matches", "msg" => "ERROR!");    
      
    }
  echo json_encode($res);
   

  }

  public function getStages($sid)
  {
    $sql = "SELECT * FROM tp_setting WHERE id='1'";
    $apiresult = $this->query_model->getDataArray($sql);
    $APIkey = $apiresult[0]['api_key'];

    $season_id = $this->query_model->getDataArray("SELECT DISTINCT ref_season_id FROM tp_season");

    // foreach ($season_id as $s) 
    // {
      // echo $s['ref_season_id']." ";

      $curl_options = array(
      CURLOPT_URL => "https://sportickr.com/api/v1.0/stages/season/".$sid."?api-key=$APIkey",
      CURLOPT_RETURNTRANSFER => true,
      CURLOPT_HEADER => false,
      CURLOPT_TIMEOUT => 30,
      CURLOPT_CONNECTTIMEOUT => 5
      );

      $curl = curl_init();
      curl_setopt_array( $curl, $curl_options );
      $result = curl_exec( $curl );

      $result = (array) json_decode($result,true);

      var_dump($result);

      foreach ($result['data'] as $r) 
      {

          $chkstage = $this->query_model->getCell("SELECT COUNT(1) as cnt FROM tp_league_stages WHERE ref_stage_id =".$r->id,"cnt");
          if($chkstage==0)
          {
            $name = str_replace("'","''",$r->name);
            $league_id = $this->query_model->getDataArray("SELECT COUNT(1) AS cnt, league_id, league_name FROM tp_league WHERE ref_league_id =".$r->league_id);
            $season_id = $this->query_model->getDataArray("SELECT COUNT(1) AS cnt, season_id, season_name FROM tp_season WHERE ref_season_id =".$r->season_id);


            if($league_id[0]['cnt'] > 0)
            {

              $sql = "INSERT IGNORE INTO tp_league_stages (ref_stage_id, league_id,stage_name, status, created_date, created_by, ref_season_id, season_id)
              VALUES ('".$r->id."','".$league_id[0]['league_id']."','".$name."','"."N"."','".date('Y-m-d H:i:s')."','".$this->session->userdata('user_id')."','".$r->season_id."','".$season_id[0]['season_id']."')";
              $this->query_model->execSQL($sql);
            }
            
          }
        } //foreach ($result['data'] as $r) 

    // } // end foreach ($season_id as $s) {

  }

   public function getTeam()
  {
    $sql = "SELECT * FROM tp_setting WHERE id='1'";
    $apiresult = $this->query_model->getDataArray($sql);
    $APIkey = $apiresult[0]['api_key'];
    $standresult = $this->query_model->getDataArray("SELECT * FROM tp_league_standings");
    for($x = 0 ; $x<count($standresult) ; $x++){
      $tid =   $standresult[$x]['team_id'];
      
       $curl_options = array(
      CURLOPT_URL => "https://sportickr.com/api/v1.0/teams/$tid?api-key=$APIkey",
      CURLOPT_RETURNTRANSFER => true,
      CURLOPT_HEADER => false,
      CURLOPT_TIMEOUT => 30,
      CURLOPT_CONNECTTIMEOUT => 5
    );

    $curl = curl_init();
    curl_setopt_array( $curl, $curl_options );
    $result = curl_exec( $curl );

    $result = (array) json_decode($result,true);

    $chkTeam = $this->query_model->getCell("SELECT COUNT(1) as cnt FROM tp_teams WHERE ref_team_id =".$result['data']['id'],"cnt");

      if($chkTeam==0)
        {
        $country_id = $this->query_model->getDataArray("SELECT COUNT(1) as cnt,country_id FROM tp_countries WHERE ref_country_id =".$result['data']['country_id']);
        $name = str_replace("'","''",$result['data']['name']);     
        $sql = "INSERT IGNORE INTO tp_teams (ref_team_id, team_name,team_nickname, twitter, country_id, ref_country_id, founded, logo_path,created_date,created_by,updated_date,updated_by)
        VALUES ('".$result['data']['id']."','".$name."','','','".$country_id[0]['country_id']."','".$result['data']['country_id']."','".$result['data']['founded']."','','".date('Y-m-d H:i:s')."','".$this->session->userdata('user_id')."','','')";
        $res =  $this->query_model->execSQL($sql);

        
        

            
        }
        if(count($result['data']['squad'])>0){
          for($x = 0 ; $x<count($result['data']['squad']) ; $x++){
            $this->getPlayers($result['data']['squad'][$x]['player_id']);
          }
        }
       
    }
   

  

    $res = array("typ" => "success", "ttl" => "Get Teams", "msg" => "Done!");    
    echo json_encode($res);

  }

   public function getPlayers($pid)
  {
    $sql = "SELECT * FROM tp_setting WHERE id='1'";
    $apiresult = $this->query_model->getDataArray($sql);
    $APIkey = $apiresult[0]['api_key'];

   
      $curl_options = array(
      CURLOPT_URL => "https://sportickr.com/api/v1.0/player/$pid?api-key=$APIkey",
      CURLOPT_RETURNTRANSFER => true,
      CURLOPT_HEADER => false,
      CURLOPT_TIMEOUT => 30,
      CURLOPT_CONNECTTIMEOUT => 5
    );

    $curl = curl_init();
    curl_setopt_array( $curl, $curl_options );
    $result = curl_exec( $curl );

    $result = (array) json_decode($result,true);

    $chkPlayer = $this->query_model->getCell("SELECT COUNT(1) as cnt FROM tp_players WHERE ref_player_id =".$result['data']['id'],"cnt");

      if($chkPlayer==0)
        {
        $team_id = $this->query_model->getDataArray("SELECT COUNT(1) as cnt,team_id FROM tp_teams WHERE ref_team_id =".$result['data']['team_id']);
        $country_id = $this->query_model->getDataArray("SELECT COUNT(1) as cnt,country_id FROM tp_countries WHERE ref_country_id =".$result['data']['country_id']);
        $common_name = str_replace("'","''",$result['data']['common_name']);
        $fullname = str_replace("'","''",$result['data']['fullname']);
        $firstname = str_replace("'","''",$result['data']['firstname']);
        $lastname = str_replace("'","''",$result['data']['lastname']);
        $nationality = str_replace("'","''",$result['data']['nationality']);

        
        $sql = "INSERT IGNORE INTO tp_players (ref_player_id, team_id,ref_team_id, country_id, ref_country_id, common_name, fullname, firstname,lastname,nationality,birthdate,birthcountry,birthplace,height,weight,image_path,position,created_date,created_by,updated_date,updated_by)
        VALUES ('".$result['data']['id']."','".$team_id[0]['team_id']."','".$result['data']['team_id']."','".$country_id[0]['country_id']."','".$result['data']['country_id']."','".$common_name."','".$fullname."','".$firstname."','".$lastname."','".$nationality."','','','','','','',".$result['data']['position']['name'].",'".date('Y-m-d H:i:s')."','".$this->session->userdata('user_id')."','','')";

        $res =  $this->query_model->execSQL($sql);
        
            
        }
    

   
       

  }

public function getFixtures()
  {
    $sql = "SELECT * FROM tp_setting WHERE id='1'";
    $apiresult = $this->query_model->getDataArray($sql);
    $APIkey = $apiresult[0]['api_key'];
    $today = date('Y-m-d');
    $tomorrow = date("Y-m-d", strtotime("+ 2 day"));
    
    $teamres  = $this->query_model->getDataArray("SELECT ref_team_id  FROM tp_teams");
    $storeid = '';

   
   
          $curl_options = array(
          CURLOPT_URL => "https://sportickr.com/api/v1.0/fixtures/between/$today/$tomorrow?api-key=$APIkey",
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_HEADER => false,
          CURLOPT_TIMEOUT => 30,
          CURLOPT_CONNECTTIMEOUT => 5
          );

          $curl = curl_init();
          curl_setopt_array( $curl, $curl_options );
          $result = curl_exec( $curl );

          $result = (array) json_decode($result,true);
          // var_dump($result['data']);
          // var_dump(count($result['data'])>0);
           $res = "";
          if(count($result['data'])>0){
            
            for($i = 0 ; $i<count($result['data']); $i ++){

              $chkFixtures = $this->query_model->getCell("SELECT COUNT(1) as cnt FROM tp_fixtures WHERE ref_fixture_id =".$result['data'][$i]['id'],"cnt");
              if($chkFixtures==0)
              {
                
                $league_id = $this->query_model->getDataArray("SELECT COUNT(1) AS cnt, league_id, league_name FROM tp_league WHERE ref_league_id =".$result['data'][$i]['league_id']);
                $season_id = $this->query_model->getDataArray("SELECT COUNT(1) AS cnt, season_id, season_name FROM tp_season WHERE ref_season_id =".$result['data'][$i]['season_id']);
                $stage_id = $this->query_model->getDataArray("SELECT COUNT(1) AS cnt, stage_id, stage_name FROM tp_league_stages WHERE ref_stage_id =".$result['data'][$i]['stage_id']);
                $round_id = $this->query_model->getDataArray("SELECT COUNT(1) AS cnt, round_id, round_name FROM tp_rounds WHERE ref_round_id =".$result['data'][$i]['round_id']);


                if($league_id[0]['cnt'] > 0 && $season_id[0]['cnt']>0 && $stage_id[0]['cnt']>0 && $round_id[0]['cnt']>0)
                {
                  // echo "SELECT COUNT(1) AS cnt, league_id, league_name FROM tp_league WHERE ref_league_id =".$r->league_id."<br>";

                  // echo $league_id[0]['league_name']."<br>";
                  $updated_at=$result['data'][$i]['updated_at'];
                  $updated_date = gmdate("Y-m-d\TH:i:s\Z", $updated_at);

                  $sql = "INSERT IGNORE INTO tp_fixtures (ref_fixture_id, league_id,season_id ,stage_id, round_id,group_id, aggegate_id, venue_id, referee_id,localteam_id
                  ,visitorteam_id,attendance,winning_odds_calculated,commentaries,areTvstations,deleted,plan_profile,updated_at,localteam_score,visitorteam_score,localteam_pen_score,visitorteam_pen_score,time_status,minute,extra_minute,injury_time,timezone,date_time,localteam_coach_id,visitor_coach_id,localteam_position,visitor_position)

                  VALUES ('".$result['data'][$i]['id']."','".$league_id[0]['league_id']."','".$season_id[0]['season_id']."','".$stage_id[0]['stage_id']."','".$round_id[0]['round_id']."','".$result['data'][$i]['group_id']."'
                  ,'".$result['data'][$i]['aggregate_id']."','".$result['data'][$i]['venue_id']."','".$result['data'][$i]['referee_id']."','".$result['data'][$i]['localteam_id']."','".$result['data'][$i]['visitorteam_id']."'
                  ,'".$result['data'][$i]['attendance']."','".$result['data'][$i]['winning_odds_calculated']."','".$result['data'][$i]['commentaries']."','".$result['data'][$i]['areTvstations']."'
                  ,'".$result['data'][$i]['deleted']."','".$result['data'][$i]['plan_profile']."','".$updated_date."','".$result['data'][$i]['scores']['localteam_score']."'
                  ,'".$result['data'][$i]['scores']['visitorteam_score']."','".$result['data'][$i]['scores']['localteam_pen_score']."','".$result['data'][$i]['scores']['visitorteam_pen_score']."'
                  ,'".$result['data'][$i]['time']['status']."','".$result['data'][$i]['time']['minute']."','".$result['data'][$i]['time']['extra_minute']."','".$result['data'][$i]['time']['injury_time']."'
                  ,'".$result['data'][$i]['time']['timezone']."','".$result['data'][$i]['time']['date_time']."','".$result['data'][$i]['coaches']['localteam_coach_id']."','".$result['data'][$i]['coaches']['visitorteam_coach_id']."'
                  ,'".$result['data'][$i]['standings']['localteam_position']."','".$result['data'][$i]['standings']['visitorteam_position']."')";

                  // die($sql);
                  $this->query_model->execSQL($sql);
                  $res = "SUCCESS";

                  // $this->getStages($result['data']['id']);
                }
                else{
                  $res = "SUCCESS";
                  continue;
                  
                }
                 
                // echo $r->country_id;
            
                }
                else{
                  $res = "SUCCESS";
                  continue;
                  
                }
               
              }
           
          
        
     
           }
           else{
             $res = "ERROR";
           }
    if($res == "SUCCESS")
    {
      $res = array("typ" => "success", "ttl" => "Get Fixtures", "msg" => "Done!");    
      
    }
    else{
      $res = array("typ" => "error", "ttl" => "Get Fixtures", "msg" => "No Available Fixtures!");    
      
    }
  echo json_encode($res);
   

  }
   public function getStandings()
  {
    $sql = "SELECT * FROM tp_setting WHERE id='1'";
    $apiresult = $this->query_model->getDataArray($sql);
    $APIkey = $apiresult[0]['api_key'];


    $season_id = $this->query_model->getDataArray("SELECT s.ref_season_id AS ref_season_id,l.league_name AS league_name FROM tp_season AS s LEFT JOIN tp_league AS l ON s.league_id = l.league_id WHERE s.status='O'");

    for($y = 0; $y<count($season_id) ; $y++)
    { 

      $sid = $season_id[$y]['ref_season_id'];
      $league_name = $season_id[$y]['league_name'];
      
      $curl_options = array(
      CURLOPT_URL => "https://sportickr.com/api/v1.0/standings/season/$sid?api-key=$APIkey",
      CURLOPT_RETURNTRANSFER => true,
      CURLOPT_HEADER => false,
      CURLOPT_TIMEOUT => 30,
      CURLOPT_CONNECTTIMEOUT => 5
      );

      $curl = curl_init();
      curl_setopt_array( $curl, $curl_options );
      $result = curl_exec( $curl );
       
      $result = (array) json_decode($result,true);
      if($league_name != "Champions League"){
       // var_dump($result['data']['standings']);
        // var_dump($result);
        if(count($result['data']['standings'])>0){
          for($x = 0 ; $x<count($result['data']['standings']) ; $x++) {
            // var_dump($result['data']['standings'][$x]['points']);
               $gd = $result['data']['standings'][$x]['statistics'][0]['goal_difference'];
                if($gd[0] == '-')
                {
                  $gd = substr($gd, 1);
                }

                  $team_name = str_replace("'","''",$result['data']['standings'][$x]['team_name']);   
                  $sql = "UPDATE tp_league_standings SET points ='".$result['data']['standings'][$x]['points']."',goal_diff='".$gd."',goal_diff='".$gd."',games_played='".$result['data']['standings'][$x]['statistics'][0]['games_played']."' WHERE ref_standings_id = '".$result['data']['standings'][$x]['id']."'";
                  var_dump($sql);
                // $this->query_model->execSQL($sql);
         
           }
           $res = array("typ" => "success", "ttl" => "Get Standings", "msg" => "UPDATED!");  
       }
       else{
         $res = array("typ" => "success", "ttl" => "Get Standings", "msg" => "NO AVAILABLE STANDINGS ON OPEN LEAGUES!");  
       }
         
    
      }
      else{

      }
     
    
    }
         // echo json_encode($res);
  }
  

  public function updAPI()
  {
    $result = array();
    $apikey = $this->query_model->clean('api');
    // echo $apikey;

    $sql = "UPDATE tp_setting SET api_key ='".$apikey."' WHERE id = 1";
    $this->query_model->execSQL($sql);

    $result = array("typ" => "success", "ttl" => "Update API Key", "msg" => "API Key has been updated!");    
    
    echo json_encode($result);

  }

public function testAPI()
  {

    $sql = "SELECT * FROM tp_setting WHERE id='1'";
    $apiresult = $this->query_model->getDataArray($sql);
    $APIkey = $apiresult[0]['api_key'];

  
    $curl_options = array(
      CURLOPT_URL => "https://sportickr.com/api/v1.0/seasons/12962?api-key=$APIkey",
      CURLOPT_RETURNTRANSFER => true,
      CURLOPT_HEADER => false,
      CURLOPT_TIMEOUT => 30,
      CURLOPT_CONNECTTIMEOUT => 5
      );

      $curl = curl_init();
      curl_setopt_array( $curl, $curl_options );
      $result = curl_exec( $curl );

      $result = (array) json_decode($result,true);
     
     
      var_dump($result['data']);
    // $result = (array) json_decode($result,true);
    // var_dump($result['data']['standings'][0]['statistics']);
  }
 
}
?>