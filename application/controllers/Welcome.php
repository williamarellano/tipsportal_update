<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	function __construct(){
    parent::__construct();
    //$this->load->database('default',TRUE);
    $this->load->model("query_model");
   
  } 
	public function index()
	{
		$this->load->view('welcome_message');
	}

	public function home()
	{
		$data['title'] = "Home";
		$dt['cms'] = $this->query_model->getDataArray("SELECT * FROM tp_cms");
		$dt['tips']	= $this->query_model->getDataArray("SELECT * FROM (SELECT t.tptip_id, m.league_id, (SELECT league_name FROM tp_league WHERE league_id = m.league_id) AS league_name, 
			IFNULL((SELECT league_nick FROM tp_league WHERE league_id = m.league_id),'NO LEAGUE') AS league_nickname,  
			t.match_id, m.home_team_id, 
			IFNULL((SELECT team_name FROM tp_teams WHERE team_id = m.home_team_id),'NO NAME') AS home_team,
			IFNULL((SELECT team_nickname FROM tp_teams WHERE team_id = m.home_team_id),'NO NAME') AS home_team_nickname,
			IFNULL((SELECT logo_path FROM tp_teams WHERE team_id = m.home_team_id),'no_photo_yet.png') AS home_team_logo,
			m.away_team_id, IFNULL((SELECT team_name FROM tp_teams WHERE team_id = m.away_team_id),'NO NAME') AS away_team,
			IFNULL((SELECT team_nickname FROM tp_teams WHERE team_id = m.away_team_id),'NO NAME') AS away_team_nickname,
			IFNULL((SELECT logo_path FROM tp_teams WHERE team_id = m.away_team_id),'no_photo_yet.png') AS away_team_logo,
			CONCAT(IFNULL((SELECT team_name FROM tp_teams WHERE team_id = m.home_team_id),''), ' vs ',IFNULL((SELECT team_name FROM tp_teams WHERE team_id = m.away_team_id),'')) AS match_title, m.date_start,
			      t.tptip, t.tptip_desc ,  
			      t.status, t.featured, t.priority, t.created_date,(SELECT realname FROM tp_users WHERE user_id = t.created_by) AS created_by, t.updated_date, t.updated_by
			      FROM tp_tptips t 
			      INNER JOIN tp_stage_match m ON t.match_id = m.match_id
			      WHERE t.status <> 'X'
			        )X WHERE 1=1 ORDER BY priority DESC ");
		$dt['tipster']	= $this->query_model->getDataArray("SELECT * FROM (SELECT t.tip_id,  t.match_id, m.home_team_id, IFNULL((SELECT team_name FROM tp_teams WHERE team_id = m.home_team_id),'NO NAME') AS home_team,
		IFNULL((SELECT team_nickname FROM tp_teams WHERE team_id = m.home_team_id),'NO NAME') AS home_team_nickname,
		IFNULL((SELECT logo_path FROM tp_teams WHERE team_id = m.home_team_id),'no_photo_yet.png') AS home_team_logo,
		m.away_team_id, IFNULL((SELECT team_name FROM tp_teams WHERE team_id = m.away_team_id),'NO NAME') AS away_team,
		IFNULL((SELECT team_nickname FROM tp_teams WHERE team_id = m.away_team_id),'NO NAME') AS away_team_nickname,
		IFNULL((SELECT logo_path FROM tp_teams WHERE team_id = m.away_team_id),'no_photo_yet.png') AS away_team_logo,
		CONCAT(IFNULL((SELECT team_name FROM tp_teams WHERE team_id = m.home_team_id),''), ' vs ',IFNULL((SELECT team_name FROM tp_teams WHERE team_id = m.away_team_id),'')) AS match_title,
		      t.site_id, (SELECT image_path FROM tp_tipsters WHERE site_id = t.site_id) AS tipster_logo, (SELECT site FROM tp_tipsters WHERE site_id = t.site_id) AS site_name, t.tip_desc ,t.tip_main, t.tip_form, t.odds, 
		      t.status, t.featured, t.priority, t.created_date,(SELECT realname FROM tp_users WHERE user_id = t.created_by) AS created_by, t.updated_date, t.updated_by
		      FROM tp_tips t 
		      INNER JOIN tp_stage_match m ON t.match_id = m.match_id
		      WHERE t.status <> 'X'
		        )X WHERE 1=1 ORDER BY priority DESC ");
		$this->load->view("template/header",$data);
		$this->load->view("home",$dt);
		$this->load->view("template/socmed");
		
	}



	public function postheader()
	{
		$data['title'] = "";
		$this->load->view("template/post-header",$data);
		$this->load->view("home.php");
		$this->load->view("template/footer");
	}
	public function tcomparison()
	{
		$data['title'] = "Tipsters Comparison - Latest Tips";
		$dt['tips']	= $this->query_model->getDataArray("SELECT * FROM (SELECT t.tip_id,  t.match_id, m.home_team_id, IFNULL((SELECT team_name FROM tp_teams WHERE team_id = m.home_team_id),'') AS home_team,
		IFNULL((SELECT team_nickname FROM tp_teams WHERE team_id = m.home_team_id),'') AS home_team_nickname,
		IFNULL((SELECT logo_path FROM tp_teams WHERE team_id = m.home_team_id),'no_photo_yet.png') AS home_team_logo,
		m.away_team_id, IFNULL((SELECT team_name FROM tp_teams WHERE team_id = m.away_team_id),'') AS away_team,
		IFNULL((SELECT team_nickname FROM tp_teams WHERE team_id = m.away_team_id),'') AS away_team_nickname,
		IFNULL((SELECT logo_path FROM tp_teams WHERE team_id = m.away_team_id),'no_photo_yet.png') AS away_team_logo,
		CONCAT(IFNULL((SELECT team_name FROM tp_teams WHERE team_id = m.home_team_id),''), ' vs ',IFNULL((SELECT team_name FROM tp_teams WHERE team_id = m.away_team_id),'')) AS match_title,
		      t.site_id, (SELECT image_path FROM tp_tipsters WHERE site_id = t.site_id) AS tipster_logo, (SELECT site FROM tp_tipsters WHERE site_id = t.site_id) AS site_name, t.tip_desc ,t.tip_main, t.tip_form, t.odds, IFNULL(t.link,'') AS link,
		      t.status, t.featured, t.priority, t.created_date,(SELECT realname FROM tp_users WHERE user_id = t.created_by) AS created_by, t.updated_date, t.updated_by
		      FROM tp_tips t 
		      INNER JOIN tp_stage_match m ON t.match_id = m.match_id
		      WHERE t.status <> 'X'
		        )X WHERE 1=1 ORDER BY priority DESC ");

		$this->load->view("template/header",$data);
		$this->load->view("tcomparison.php",$dt);
		$this->load->view("template/socmed");
		$this->load->view("template/footer");
	}
	public function tcomparison2()
	{	
		$data['title'] = "Tipsters Comparison - Previous Tips";
		$dt['tips']	= $this->query_model->getDataArray("SELECT * FROM (SELECT t.tip_id,  t.match_id, m.home_team_id, IFNULL((SELECT team_name FROM tp_teams WHERE team_id = m.home_team_id),'') AS home_team,
		IFNULL((SELECT team_nickname FROM tp_teams WHERE team_id = m.home_team_id),'') AS home_team_nickname,
		IFNULL((SELECT logo_path FROM tp_teams WHERE team_id = m.home_team_id),'no_photo_yet.png') AS home_team_logo,
		m.away_team_id, IFNULL((SELECT team_name FROM tp_teams WHERE team_id = m.away_team_id),'') AS away_team,
		IFNULL((SELECT team_nickname FROM tp_teams WHERE team_id = m.away_team_id),'') AS away_team_nickname,
		IFNULL((SELECT logo_path FROM tp_teams WHERE team_id = m.away_team_id),'no_photo_yet.png') AS away_team_logo,
		CONCAT(IFNULL((SELECT team_name FROM tp_teams WHERE team_id = m.home_team_id),''), ' vs ',IFNULL((SELECT team_name FROM tp_teams WHERE team_id = m.away_team_id),'')) AS match_title,
		      t.site_id, (SELECT image_path FROM tp_tipsters WHERE site_id = t.site_id) AS tipster_logo, (SELECT site FROM tp_tipsters WHERE site_id = t.site_id) AS site_name, t.tip_desc ,t.tip_main, t.tip_form, t.odds, IFNULL(t.link,'') AS link,
		      t.status, t.featured, t.priority, t.created_date,(SELECT realname FROM tp_users WHERE user_id = t.created_by) AS created_by, t.updated_date, t.updated_by
		      FROM tp_tips t 
		      INNER JOIN tp_stage_match m ON t.match_id = m.match_id
		      WHERE t.status <> 'X'
		        )X WHERE 1=1 AND CAST(created_date AS DATE)  BETWEEN '".$this->getLastWeekDates()[0]."' AND '".$this->getLastWeekDates()[6]."' 
		        ORDER BY priority DESC ");
		$this->load->view("template/header",$data);
		$this->load->view("tcomparison2.php",$dt);
		$this->load->view("template/socmed");
		// $this->load->view("template/footer");
	}
	public function goldengoal()
	{
		$data['title'] = "Golden Goal | Free to Play Competition";
		$this->load->view("template/header",$data);
		$this->load->view("goldengoal.php");
		$this->load->view("template/socmed");
		$this->load->view("template/footer");
	}
	public function goldengoalv1()
	{
		$data['title'] = "Golden Goal | Free to Play Competition";
		$this->load->view("template/header",$data);
		$this->load->view("goldengoal-v1.php");
		$this->load->view("template/socmed");
		$this->load->view("template/footer");
	}
	public function previewstips()
	{
		$data['title'] = "Previews Tips - Latest Tips";
		$dt['tips']	= $this->query_model->getDataArray("SELECT * FROM (SELECT t.tptip_id, m.league_id, (SELECT league_name FROM tp_league WHERE league_id = m.league_id) AS league_name, 
			IFNULL((SELECT league_nick FROM tp_league WHERE league_id = m.league_id),'NO LEAGUE') AS league_nickname,  
			t.match_id, m.home_team_id, 
			IFNULL((SELECT team_name FROM tp_teams WHERE team_id = m.home_team_id),'') AS home_team,
			IFNULL((SELECT team_nickname FROM tp_teams WHERE team_id = m.home_team_id),'') AS home_team_nickname,
			IFNULL((SELECT logo_path FROM tp_teams WHERE team_id = m.home_team_id),'no_photo_yet.png') AS home_team_logo,
			m.away_team_id, IFNULL((SELECT team_name FROM tp_teams WHERE team_id = m.away_team_id),'') AS away_team,
			IFNULL((SELECT team_nickname FROM tp_teams WHERE team_id = m.away_team_id),'') AS away_team_nickname,
			IFNULL((SELECT logo_path FROM tp_teams WHERE team_id = m.away_team_id),'no_photo_yet.png') AS away_team_logo,
			CONCAT(IFNULL((SELECT team_name FROM tp_teams WHERE team_id = m.home_team_id),''), ' vs ',IFNULL((SELECT team_name FROM tp_teams WHERE team_id = m.away_team_id),'')) AS match_title, m.date_start,
			      t.tptip, t.tptip_desc ,  
			      t.status, t.featured, t.priority, t.created_date,(SELECT realname FROM tp_users WHERE user_id = t.created_by) AS created_by, t.updated_date, t.updated_by
			      FROM tp_tptips t 
			      INNER JOIN tp_stage_match m ON t.match_id = m.match_id
			      WHERE t.status <> 'X'
			        )X WHERE 1=1 ORDER BY priority DESC ");
		$this->load->view("template/header",$data);
		$this->load->view("previewstips.php", $dt);
		$this->load->view("template/socmed");
		$this->load->view("template/footer");
	}
	public function previewstips2()
	{
		$data['title'] = "Previews Tips";
		
		$this->load->view("template/header",$data);
		$this->load->view("previewstips-2.php");
		$this->load->view("template/socmed");
		$this->load->view("template/footer");
	}
	public function previewstips3()
	{
		$data['title'] = "Previews Tips - Previous Tips";
		$this->load->view("template/header",$data);
		$this->load->view("previewstips3.php");
		$this->load->view("template/socmed");
		// $this->load->view("template/footer");
	}
	public function livestream()
	{
		$data['title'] = "Live Stream";
		$this->load->view("template/header",$data);
		$this->load->view("live-stream.php");
		$this->load->view("template/socmed");
		$this->load->view("template/footer");
	}
	public function seasonalLeaderboard()
	{
		$data['title'] = "Seasonal Leaderboard";
		$this->load->view("template/header",$data);
		$this->load->view("seasonal-leaderboard.php");
		$this->load->view("template/socmed");
		$this->load->view("template/footer");
	}
	public function freetoplay()
	{
		$data['title'] = "Free to Play Competion";
		$this->load->view("template/header",$data);
		$this->load->view("freetoplay.php");
		$this->load->view("template/socmed");
		$this->load->view("template/footer");
	}
	public function ftpperfect10()
	{
		$data['title'] = "The Perfect 10 | Free to Play Competition";
		$this->load->view("template/header",$data);
		$this->load->view("ftpperfect10.php");
		$this->load->view("template/socmed");
		$this->load->view("template/footer");
	}
	public function livescore()
	{
		$data['title'] = "The Perfect 10 | Free to Play Competition";
		$this->load->view("template/header",$data);
		$this->load->view("livescore.php");
		$this->load->view("template/socmed");
		$this->load->view("template/footer");
	}
	public function blog()
	{
		$data['title'] = "Blog";
		$this->load->view("template/header",$data);
		$this->load->view("blog.php");
		$this->load->view("template/socmed");
		$this->load->view("template/footer");
	}
	public function aboutus()
	{
		$data['title'] = "About Us";
		$this->load->view("template/header",$data);
		$this->load->view("aboutus.php");
		$this->load->view("template/socmed");
		$this->load->view("template/footer");
	}
	public function contact()
	{
		$data['title'] = "Contact Us";
		$this->load->view("template/header",$data);
		$this->load->view("contact.php");
		$this->load->view("template/socmed");
		$this->load->view("template/footer");
	}
	public function blogcontent()
	{
		$data['title'] = "Article";
		$this->load->view("template/header",$data);
		$this->load->view("blogcontent.php");
		$this->load->view("template/socmed");
		$this->load->view("template/footer");
	}
	public function profile()
	{
		$data['title'] = "Member Profile";
		$this->load->view("template/post-header",$data);
		$this->load->view("profile.php");
		$this->load->view("template/socmed");
		$this->load->view("template/footer");
	}

	function getLastWeekDates()
	{
		// Monday to sunday
	    $lastWeek = array();
	 
	    $prevMon = abs(strtotime("previous monday"));
	    $currentDate = abs(strtotime("today"));
	    $seconds = 86400; //86400 seconds in a day
	 
	    $dayDiff = ceil( ($currentDate-$prevMon)/$seconds ); 
	 
	    if( $dayDiff < 7 )
	    {
	        $dayDiff += 1; //if it's monday the difference will be 0, thus add 1 to it
	        $prevMon = strtotime( "previous monday", strtotime("-$dayDiff day") );
	    }
	 
	    $prevMon = date("Y-m-d",$prevMon);
	 
	    // create the dates from Monday to Sunday
	    for($i=0; $i<7; $i++)
	    {
	        $d = date("Y-m-d", strtotime( $prevMon." + $i day") );
	        $lastWeek[]=$d;
	    }
	 	return $lastWeek;
	    // var_dump($lastWeek);
	}

	public function lastlastWeekTips()
	{
		$this->load->library('pagination');
		$config["base_url"] = "#";
		$config['total_rows'] = $this->query_model->getCell("SELECT COUNT(1) AS cnt FROM tp_tips WHERE status <> 'X'","cnt");
		$config["per_page"] = 4;
		$config["uri_segment"] = 3;
		$config["use_page_numbers"] = TRUE;
		$config["full_tag_open"] = '<ul class="pagination">';
		$config["full_tag_close"] = '</ul>';
		$config["first_tag_open"] = '<li>';
		$config["first_tag_close"] = '</li>';
		$config["last_tag_open"] = '<li>';
		$config["last_tag_close"] = '</li>';
		$config['next_link'] = '&gt;';
		$config["next_tag_open"] = '<li>';
		$config["next_tag_close"] = '</li>';
		$config["prev_link"] = "&lt;";
		$config["prev_tag_open"] = "<li>";
		$config["prev_tag_close"] = "</li>";
		$config["cur_tag_open"] = "<li class='active'><a href='#'>";
		$config["cur_tag_close"] = "</a></li>";
		$config["num_tag_open"] = "<li>";
		$config["num_tag_close"] = "</li>";
		$config["num_links"] = 6;
		$this->pagination->initialize($config);
		// $page = 1;
		$page = $this->uri->segment(3);
		$start = ($page - 1) * $config["per_page"];

		$output = array(
		'pagination_link'  => $this->pagination->create_links(),
		'tipsters_table'   => $this->query_model->getDataArray("SELECT * FROM (SELECT t.tip_id,  t.match_id, m.home_team_id, IFNULL((SELECT team_name FROM tp_teams WHERE team_id = m.home_team_id),'') AS home_team,
		IFNULL((SELECT team_nickname FROM tp_teams WHERE team_id = m.home_team_id),'') AS home_team_nickname,
		IFNULL((SELECT logo_path FROM tp_teams WHERE team_id = m.home_team_id),'no_photo_yet.png') AS home_team_logo,
		m.away_team_id, IFNULL((SELECT team_name FROM tp_teams WHERE team_id = m.away_team_id),'') AS away_team,
		IFNULL((SELECT team_nickname FROM tp_teams WHERE team_id = m.away_team_id),'') AS away_team_nickname,
		IFNULL((SELECT logo_path FROM tp_teams WHERE team_id = m.away_team_id),'no_photo_yet.png') AS away_team_logo,
		CONCAT(IFNULL((SELECT team_name FROM tp_teams WHERE team_id = m.home_team_id),''), ' vs ',IFNULL((SELECT team_name FROM tp_teams WHERE team_id = m.away_team_id),'')) AS match_title,
		      t.site_id, (SELECT image_path FROM tp_tipsters WHERE site_id = t.site_id) AS tipster_logo, (SELECT site FROM tp_tipsters WHERE site_id = t.site_id) AS site_name, t.tip_desc ,t.tip_main, t.tip_form, t.odds, IFNULL(t.link,'') AS link,
		      t.status, t.featured, t.priority, t.created_date,(SELECT realname FROM tp_users WHERE user_id = t.created_by) AS created_by, t.updated_date, t.updated_by
		      FROM tp_tips t 
		      INNER JOIN tp_stage_match m ON t.match_id = m.match_id
		      WHERE t.status <> 'X'
		        )X WHERE 1=1 AND CAST(created_date AS DATE)  BETWEEN '".$this->getLastWeekDates()[0]."' AND '".$this->getLastWeekDates()[6]."' 
		        ORDER BY priority DESC LIMIT ".$config["per_page"]." OFFSET ".$start)
		// 'tipsters_table'   => $this->query_model->getDataArray("SELECT * FROM tp_tips WHERE status <> 'X'")
		);
		// echo "SELECT * FROM tp_tips WHERE status <> 'X' LIMIT ".$config["per_page"].",".$start;
		echo json_encode($output);
	}

	public function viewPrevTips()
	{
		$this->load->library('pagination');
		$config["base_url"] = "#";
		$config['total_rows'] = $this->query_model->getCell("SELECT COUNT(1) AS cnt FROM tp_tptips WHERE status <> 'X'","cnt");
		$config["per_page"] = 9;
		$config["uri_segment"] = 3;
		$config["use_page_numbers"] = TRUE;
		$config["full_tag_open"] = '<ul class="pagination">';
		$config["full_tag_close"] = '</ul>';
		$config["first_tag_open"] = '<li>';
		$config["first_tag_close"] = '</li>';
		$config["last_tag_open"] = '<li>';
		$config["last_tag_close"] = '</li>';
		$config['next_link'] = '&gt;';
		$config["next_tag_open"] = '<li>';
		$config["next_tag_close"] = '</li>';
		$config["prev_link"] = "&lt;";
		$config["prev_tag_open"] = "<li>";
		$config["prev_tag_close"] = "</li>";
		$config["cur_tag_open"] = "<li class='active'><a href='#'>";
		$config["cur_tag_close"] = "</a></li>";
		$config["num_tag_open"] = "<li>";
		$config["num_tag_close"] = "</li>";
		$config["num_links"] = 6;
		$this->pagination->initialize($config);
		// $page = 1;
		$page = $this->uri->segment(3);
		$start = ($page - 1) * $config["per_page"];

		$output = array(
		'pagination_link'  => $this->pagination->create_links(),
		'tips_table'   => $this->query_model->getDataArray("SELECT * FROM (SELECT t.tptip_id, m.league_id, (SELECT league_name FROM tp_league WHERE league_id = m.league_id) AS league_name, 
			IFNULL((SELECT league_nick FROM tp_league WHERE league_id = m.league_id),'NO LEAGUE') AS league_nickname,  
			t.match_id, m.home_team_id, 
			IFNULL((SELECT team_name FROM tp_teams WHERE team_id = m.home_team_id),'') AS home_team,
			IFNULL((SELECT team_nickname FROM tp_teams WHERE team_id = m.home_team_id),'') AS home_team_nickname,
			IFNULL((SELECT logo_path FROM tp_teams WHERE team_id = m.home_team_id),'no_photo_yet.png') AS home_team_logo,
			m.away_team_id, IFNULL((SELECT team_name FROM tp_teams WHERE team_id = m.away_team_id),'') AS away_team,
			IFNULL((SELECT team_nickname FROM tp_teams WHERE team_id = m.away_team_id),'') AS away_team_nickname,
			IFNULL((SELECT logo_path FROM tp_teams WHERE team_id = m.away_team_id),'no_photo_yet.png') AS away_team_logo,
			CONCAT(IFNULL((SELECT team_name FROM tp_teams WHERE team_id = m.home_team_id),''), ' vs ',IFNULL((SELECT team_name FROM tp_teams WHERE team_id = m.away_team_id),'')) AS match_title, m.date_start,
			      t.tptip, t.tptip_desc ,  
			      t.status, t.featured, t.priority, t.created_date,(SELECT realname FROM tp_users WHERE user_id = t.created_by) AS created_by, t.updated_date, t.updated_by
			      FROM tp_tptips t 
			      INNER JOIN tp_stage_match m ON t.match_id = m.match_id
			      WHERE t.status <> 'X'
		        )X WHERE 1=1 AND CAST(created_date AS DATE)  BETWEEN '".$this->getLastWeekDates()[0]."' AND '".$this->getLastWeekDates()[6]."' 
		        ORDER BY priority DESC LIMIT ".$config["per_page"]." OFFSET ".$start)
		// 'tipsters_table'   => $this->query_model->getDataArray("SELECT * FROM tp_tips WHERE status <> 'X'")
		);
		// echo "SELECT * FROM tp_tips WHERE status <> 'X' LIMIT ".$config["per_page"].",".$start;
		echo json_encode($output);
	}

	
	

}
