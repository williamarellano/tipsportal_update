<?php defined('BASEPATH') OR exit('No direct script access allowed');

class HomeCms extends CI_Controller {

  function __construct(){
    parent::__construct();
    //$this->load->database('default',TRUE);
    $this->load->model("query_model");
    $this->load->library('utilities');
    if($this->session->userdata('user_id') == NULL)
    {
      redirect("Adminlogin","location");
    }

  }
  public function index()
  {
    $data['url'] = array_flip($_REQUEST);
    $this->load->view('templates/admin_header');
    $this->load->view('templates/admin_menu',$data);
    $this->load->view('adminpages/cms_home_view');

  }

  public function tipCom()
  {
    $data['url'] = array_flip($_REQUEST);
    $this->load->view('templates/admin_header');
    $this->load->view('templates/admin_menu',$data);
    $this->load->view('adminpages/cms_tipCom_view');
  }

  public function prevnTips()
  {
    $data['url'] = array_flip($_REQUEST);
    $this->load->view('templates/admin_header');
    $this->load->view('templates/admin_menu',$data);
    $this->load->view('adminpages/cms_prevnTips_view');
  }

  public function freeGames()
  {
    $data['url'] = array_flip($_REQUEST);
    $this->load->view('templates/admin_header');
    $this->load->view('templates/admin_menu',$data);
    $this->load->view('adminpages/cms_freeGames_view');
  }

  public function liveStream()
  {
    $data['url'] = array_flip($_REQUEST);
    $this->load->view('templates/admin_header');
    $this->load->view('templates/admin_menu',$data);
    $this->load->view('adminpages/cms_liveStream_view');
  }
  public function updBanner()
  {
    $result = array();
    $oldfilename = "";
    $extension = "";
    $filename = "";
    $sql = "";
    $cmsid = $this->query_model->clean('cmsid','id');
    $path = $this->config->item('cms_home');
    $fn = "";
    $file = "";

    switch($cmsid)
    {
      case 1:
        $fn = "home_top_banner";
        $file = "updtopxFileTxt";
      break;
      case 2:
        $fn = "home_mid_banner";
         $file = "updmidxFileTxt";
      break;
      case 3:
        $fn = "home_side_banner";
        $file = "updsidexFileTxt";
      break;
    }
   
    $sql ="UPDATE tp_cms SET "; 

    if(isset($_FILES[$file])) 
    { 
      $oldfilename = explode(".", $_FILES[$file]['name']); 
      $extension = $oldfilename[count($oldfilename)-1];
      // htmlspecialchars($tm, ENT_NOQUOTES)
      

      $filename = $fn.".".$extension;
      // echo $team;
      $upload = $this->utilities->upload_img_logo($file,$path,$filename);
      // echo $upload["status"];
      if($upload["status"] == 1)
      {
        $sql ="UPDATE tp_cms SET "; 
        $sql .="  img_path = '".$filename."' ";
        $sql .= " WHERE cms_id = ".$cmsid;

        // echo $sql;
        $this->query_model->execSQL($sql);
        $result = array("typ" => "success", "ttl" => "Update Image", "msg" => "Image has been updated!");
      }
      else{
        $result = array("typ" => "error", "ttl" => "Update Image", "msg" => "Error!");
      }

    }else{
      // $sql .= " WHERE cms_id = ".$team_id;
      // echo $sql;
      $this->query_model->execSQL($sql);
      $result = array("typ" => "success", "ttl" => "Image Team", "msg" => "Image has been updated!");
    }

    // var_dump(expression)


    echo json_encode($result);
    // echo json_encode($upload);
  }

  

  public function getLeague()
  {
    
    
    $sql = "SELECT s.season_id as season_id,c.country_name as country_name,l.priority as priority,l.league_id,l.league_name FROM tp_league AS l LEFT JOIN tp_season AS s ON l.league_id = s.league_id LEFT JOIN tp_countries AS c ON l.country_id = c.country_id WHERE s.status = 'O'";
    $result = $this->query_model->getDataArray($sql);
   

   echo json_encode($result);
      
  } 
  public function getCntStanding()
  {
    
    $sid = $this->query_model->clean('sid','id');
    $sql = "SELECT COUNT(*) AS cnt FROM tp_league_standings WHERE season_id = $sid";
    $result = $this->query_model->getDataArray($sql);
   
   
   echo json_encode($result);
      
  } 
  public function getPriority()
  {
    
    
    $sql = "SELECT s.season_id AS season_id FROM tp_league AS l LEFT JOIN tp_season AS s ON l.league_id = s.league_id  WHERE l.priority =1 AND s.status='O'";
    $result = $this->query_model->getDataArray($sql);
   
    echo json_encode($result);
  } 
  public function getFixtures()
  {
    
    $sid = $this->query_model->clean('season_id','id');
    $sql = "SELECT * FROM tp_fixtures WHERE season_id =$sid";
    $result = $this->query_model->getDataArray($sql);
    var_dump($result);
    // echo json_encode($result);
  } 
  public function getStandings()
  {
    $sid = $this->query_model->clean('season_id','id');
    
    $sql = "SELECT * FROM tp_league_standings WHERE season_id =$sid ";
    $sql2= "SELECT l.format_standing FROM tp_league AS l LEFT JOIN tp_season AS s ON l.league_id = s.league_id LEFT JOIN tp_countries AS c ON l.country_id = c.country_id WHERE s.status = 'O' AND season_id = $sid  ";
    $result = $this->query_model->getDataArray($sql);
    $result2 = $this->query_model->getDataArray($sql2);
    

    $json_data = array("data"=>$result,"format"=>$result2);

    echo json_encode($json_data);
   
   
  } 
  public function updFixture()
  {
    $result = array();
    $max_fix = $this->query_model->clean('max_fix');
    
    $sql ="UPDATE tp_setting SET max_fixture = ".$max_fix." WHERE id =1";
    $res = $this->query_model->execSQL($sql);
           
    $result = array("typ" => "success", "ttl" => "Update League Fixtures CMS", "msg" => "CMS has been updated!");
        
    echo json_encode($result);
  }
  public function updPriority()
  {
    $result = array();
    $league_id = $this->query_model->clean('league_id','id');
    $priority_sel = $this->query_model->clean('priority_sel');
    $num_top_sel = $this->query_model->clean('num_top_sel');
    $num_high_sel = $this->query_model->clean('num_high_sel');
    $num_mid_sel = $this->query_model->clean('num_mid_sel');
    $num_low_sel = $this->query_model->clean('num_low_sel');

    $format = $num_top_sel . "," . $num_high_sel . "," . $num_mid_sel . "," . $num_low_sel;
    $chkprio = $this->query_model->getDataArray("SELECT COUNT(1) as cnt,league_id FROM tp_league WHERE priority=1");
    if($priority_sel == 1){
      if($chkprio[0]['league_id']!=$league_id){
          if($chkprio[0]['cnt']==0){
          $sql ="UPDATE tp_league SET priority = ".$priority_sel.", format_standing = '".$format."' WHERE league_id = ".$league_id;
           $res = $this->query_model->execSQL($sql);
           // var_dump($res);
          
          $result = array("typ" => "success", "ttl" => "Update League Standings CMS", "msg" => "CMS has been updated!");
        
          
        }
        else{
            
            $result = array("typ" => "error", "ttl" => "Update League Standings CMS", "msg" => "Already have 1st priority League!");
        }
      }
      else{
          $sql ="UPDATE tp_league SET priority = ".$priority_sel.", format_standing = '".$format."' WHERE league_id = ".$league_id;
          $res = $this->query_model->execSQL($sql);
          $result = array("typ" => "success", "ttl" => "Update League Standings CMS", "msg" => "CMS has been updated!");
         
      }
       
    }
    else{
          $sql ="UPDATE tp_league SET priority = ".$priority_sel.", format_standing = '".$format."' WHERE league_id = ".$league_id;
          $res = $this->query_model->execSQL($sql);
          
          $result = array("typ" => "success", "ttl" => "Update League Standings CMS", "msg" => "CMS has been updated!");
        
    }
   
    


    echo json_encode($result);
  }

}