<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller {
	function __construct(){

    parent::__construct();

    $this->load->model('query_model');
    // $this->load->helper(array('form', 'url','file'));
    $this->load->library('excel');

    if($this->session->userdata('user_id') == NULL)
    {
    	redirect("Adminlogin","location");
    }

  }	
	public function index()
	{
    $data['url'] = array_flip($_REQUEST);
		$this->load->view('templates/admin_header');
    $this->load->view('templates/admin_menu',$data);
    $this->load->view('adminpages/dashboard');


	}
  public function admin()
  {
    $data['url'] = array_flip($_REQUEST);
    $this->load->view('templates/admin_header');
    $this->load->view('templates/admin_menu',$data);
    $this->load->view('adminpages/dashboard');


  }
    public function changePass()
  {
      $id = $this->session->userdata('user_id');
      // $cur_pass = $this->query_model->clean('cur_pass');
      $new_pass = $this->query_model->clean('new_pass');

       $res = $this->query_model->getDataCount("SELECT * FROM tp_users WHERE user_id='".$id."' AND password='".md5($new_pass)."' AND status='A'");


      if ($res["count"] == 0){

          $change_pass = "UPDATE tp_users SET password='".md5($new_pass)."' WHERE user_id=$id";
          
          $this->query_model->execSQL($change_pass);

          $result = array("typ" => "success", "ttl" => "CHANGE PASSWORD", "msg" => "CHANGE PASSWORD SUCCESSFUL!");
        }
      else
      {
          $result = array("typ" => "error", "ttl" => "CHANGE PASSWORD", "msg" => "YOUR NEW PASSWORD IS YOUR CURRENT PASSWORD!");
      }
      echo json_encode($result);
  }
	 public function logout()
  {
    $this->session->sess_destroy();
   redirect("Adminlogin","location");
  }
}
