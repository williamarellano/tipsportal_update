<?php defined('BASEPATH') OR exit('No direct script access allowed');

class TPtips extends CI_Controller {

  function __construct(){
    parent::__construct();
    //$this->load->database('default',TRUE);
    $this->load->model("query_model");
    $this->load->library('utilities');
  } 

  public function getTipDesc()
  {
    // $match_id = $this->uri->segment(3);
    $tip_id = $this->query_model->clean('tip_id','id');
    // echo $tip_id;
    $result = $this->query_model->getDataArray("SELECT * FROM tp_tptips WHERE tptip_id = ".$tip_id);

    // echo "SELECT * FROM tp_tptips WHERE match_id = ".$tip_id;

    echo json_encode($result);

  }

  public function addTip()
  {
    $result = array();
    $league_id = $this->query_model->clean('league','id');
    $stage_id = $this->query_model->clean('stage','id');
    $match_id = $this->query_model->clean('match','id');
    $featured = $this->query_model->clean('featured','id');
    $priority = $this->query_model->clean('priority');
    $tipdesc = $this->query_model->clean('tipdesc');
    $tipmain = $this->query_model->clean('tipmain');
    $status = $this->query_model->clean('status');
    $tipdesc = html_entity_decode($tipdesc);
    $tipdesc = htmlspecialchars_decode($tipdesc, ENT_QUOTES);

    // echo $status;

    if($featured == 1)
    {
      $priority = 11;
      $this->query_model->execSQL("UPDATE tp_tptips SET priority = 10 WHERE featured = 1");
      $this->query_model->execSQL("UPDATE tp_tptips SET featured = 0");

      $sql = "INSERT IGNORE INTO tp_tips(match_id, site_id, tptip_desc, tptip, status, featured, priority, created_date, created_by)
              VALUES('".$match_id."','".$site_id."','".trim(str_replace("'","''", $tipdesc))."','".$tipmain."','".$status."','".$featured."','".$priority."','".date('Y-m-d H:i:s')."','".$this->session->userdata('user_id')."')";

    }else{
      $sql = "INSERT IGNORE INTO tp_tptips(match_id, tptip_desc, tptip, status, featured, priority, created_date, created_by)
              VALUES('".$match_id."','".trim(str_replace("'","''", $tipdesc))."','".$tipmain."','".$status."','".$featured."','".$priority."','".date('Y-m-d H:i:s')."','".$this->session->userdata('user_id')."')";
    }



    $exec = $this->query_model->execSQL($sql);
    $result = array("typ" => "success", "ttl" => "Add Tip", "msg" => "Tip has been added!");
    echo json_encode($result);
  }

  public function updateTip()
  {
    $result = array();
    $tip_id = $this->query_model->clean('tipid','id');
    $match_id = $this->query_model->clean('matchid','id');
    $featured = $this->query_model->clean('featured');
    $priority = $this->query_model->clean('priority');
    $tipdesc = $this->query_model->clean('tipdesc');
    $tipmain = $this->query_model->clean('tipmain');
    $status = $this->query_model->clean('status');
    $tipdesc = html_entity_decode($tipdesc);
    $tipdesc = htmlspecialchars_decode($tipdesc, ENT_QUOTES);
    // $tipdesc = str_ireplace("'", "''", $tipdesc);


    if($featured == 1)
    {
      $priority = 11;
      $this->query_model->execSQL("UPDATE tp_tptips SET priority = 10 WHERE featured = 1");
      $this->query_model->execSQL("UPDATE tp_tptips SET featured = 0");
    }

    $sql = "UPDATE tp_tptips SET  priority = '".$priority."', featured = '".$featured."', tptip_desc = '".str_replace("'","''", $tipdesc)."', tptip = '".$tipmain."',  status = '".$status."' WHERE tptip_id = '".$tip_id."'";
    $this->query_model->execSQL($sql);

    $result = array("typ" => "success", "ttl" => "Edit Tip", "msg" => "Tip has been updated!");
    echo json_encode($result);
  }

  public function delTip()
  {
    $tip_id = $this->query_model->clean('delID','id');
    // echo $user_id;

    $this->query_model->execSQL("UPDATE tp_tptips SET status = 'X' WHERE tptip_id = ".$tip_id);

    // echo "UPDATE tp_tips SET status = 'X' WHERE tip_id = ".$tip_id;
    $result = array("typ" => "success", "ttl" => "Delete Tip", "msg" => "Tip has been deleted!");
    echo json_encode($result);
  }

  public function listing_tptips()
  {
    $requestData= $_REQUEST;
    $columns = array( 
      0 => 'tptip_id',
      1 => 'match_id',
      2 => 'match_title',
      3 => 'tptip_desc',
      4 => 'tptip',
      5 => 'priority',
      6 => 'featured',
      7 => 'status',
      8 => 'created_date',
      9 => 'created_by'
    );

    $sql = "";
    $totalData = 0;
    $totalFiltered = 0;

    $sql = "SELECT * FROM (SELECT t.tptip_id, t.match_id, CONCAT(IFNULL((SELECT team_name FROM tp_teams WHERE team_id = m.home_team_id),''), ' vs ',IFNULL((SELECT team_name FROM tp_teams WHERE team_id = m.away_team_id),'')) AS match_title,
      t.tptip_desc ,t.tptip,  t.status, t.featured, t.priority, t.created_date,
      (SELECT realname FROM tp_users WHERE user_id = t.created_by) AS created_by, t.updated_date, t.updated_by
      FROM tp_tptips t 
      INNER JOIN tp_stage_match m ON t.match_id = m.match_id
      WHERE t.status <> 'X'
          )X WHERE 1=1 ";

    $records = $this->query_model->getDataCount($sql);

    $totalData = $records["count"];
    $totalFiltered = $totalData;

    //This is for search function...
      if( !empty($requestData['search']['value']) ) {
        $sql.=" AND tptip LIKE '%".$requestData['search']['value']."%'";    
        $sql.=" OR tptip_desc LIKE '%".$requestData['search']['value']."%' ";
        //$sql.=" OR status LIKE '%".$requestData['search']['value']."%' )";
      }  
      $totalFiltered = $this->query_model->getDataCount($sql)["count"];
      //end of search function

      $sql.=" ORDER BY ". $columns[$requestData['order'][0]['column']]." ".$requestData['order'][0]['dir']." LIMIT ".$requestData['start']." ,".$requestData['length']."   ";
    
    //$totalFiltered = $this->query_model->getDataCount($sql)["count"];

    $records = $this->query_model->getDataArray($sql);

    $data = array();

    foreach ($records as $row) {
      $nestedData=array();
      
      $nestedData["tptip_id"] = $row["tptip_id"];
      $nestedData["match_id"] = $row["match_id"];
      $nestedData["match_title"] = $row["match_title"];
      $nestedData["tptip_desc"] = $row["tptip_desc"];
      $nestedData["tptip"] = $row["tptip"];
      $nestedData["priority"] = $row["priority"];
      $nestedData["featured"] = $row["featured"];
      $nestedData["status"] = $row["status"];
      $nestedData["created_date"] = $row["created_date"];
      $nestedData["created_by"] = $row["created_by"];

      $data[] = $nestedData;  
    }

    $json_data = array(
          "draw"            => intval( $requestData['draw'] ),   // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw. 
          "recordsTotal"    => intval( $totalData ),  // total number of records
          "recordsFiltered" => intval( $totalFiltered ), // total number of records after searching, if there is no searching then totalFiltered = totalData
          "data"            => $data   // total data array
          );

    echo json_encode($json_data);       
  }

}

?>