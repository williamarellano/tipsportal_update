<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Members extends CI_Controller {

  function __construct(){
    parent::__construct();
    //$this->load->database('default',TRUE);
    $this->load->model("query_model");
    if($this->session->userdata('user_id') == NULL)
    {
      redirect("Adminlogin","location");
    }
  } 

  public function createMember(){
  	$result = array();
  	$editID = $this->query_model->clean('editID','id');
  	$username = $this->query_model->clean('username');
  	$password = $this->query_model->clean('password');
  	$firstname = $this->query_model->clean('firstname');
    $lastname = $this->query_model->clean('lastname');
  	$userrole = $this->query_model->clean('selUserRole','id');
  	$status = $this->query_model->clean('selStatus');
  	if($editID != 0 || $editID != '')
  	{
  		$usernamechk = $this->query_model->getDataArray("SELECT username FROM tp_members WHERE m_id = ".$editID);
  		if($usernamechk[0]['username'] == $username)
  		{
  			// echo "1";
  			if($password != '')
			{
  				$sql = "UPDATE tp_members SET username = '".$username."',password = '".md5($password)."',firstname = '".$firstname."', lastname = '".$lastname."',status = '".$status."'
  				WHERE m_id = ".$editID;
  			}else{
  				$sql = "UPDATE tp_members SET username = '".$username."',firstname = '".$firstname."', lastname = '".$lastname."',status = '".$status."'
  				WHERE m_id = ".$editID;
			}
  			$ex = $this->query_model->execSQL($sql);
  			$result = array("typ" => "success", "ttl" => "Edit Member", "msg" => "Member has been updated!");

  		}
  		else 
  			// ($usernamechk[0]['username'] != $username)
  		{
  			// echo "2";
  			$checkuser = $this->query_model->getCell("SELECT COUNT(1) AS cnt FROM tp_members WHERE username = '".$username."'","cnt");
  			

  			if($checkuser>0)
  			{
				  $result = array("typ" => "error", "ttl" => "Edit Member", "msg" => "username exist!");
			  }else{
				if($password != '')
				{
	  				$sql = "UPDATE tp_members SET username = '".$username."',password = '".md5($password)."',firstname = '".$firstname."', lastname = '".$lastname."',status = '".$status."'
	  				WHERE m_id = ".$editID;
	  			}else{
	  				$sql = "UPDATE tp_members SET username = '".$username."',firstname = '".$firstname."', lastname = '".$lastname."',status = '".$status."'
	  				WHERE m_id = ".$editID;
	  			}
	  			$ex = $this->query_model->execSQL($sql);
	  			$result = array("typ" => "success", "ttl" => "Edit Member", "msg" => "Member has been updated!");
			}
  		}
  			
  	}else{
  		$checkuser = $this->query_model->getCell("SELECT COUNT(1) AS cnt FROM tp_members WHERE username = '".$username."'","cnt");
  		if($checkuser>0)
		{
			// echo "3";
			$result = array("typ" => "error", "ttl" => "Edit Member", "msg" => "username exist!");
		}
		else
		{
			// echo "4";
	  		$sql = "INSERT INTO tp_members(username, password, firstname, lastname, status, created_date, created_by)
	  			VALUES ('".$username."','".md5($password)."','".$firstname."','".$lastname."','".$status."','".date('Y-m-d H:i:s')."','".$this->session->userdata('user_id')."')";
		  	$exec = $this->query_model->execSQL($sql);

		  	$result = array("typ" => "success", "ttl" => "Add Member", "msg" => "Member has been added!");
	  	}
  	}

  	

  	echo json_encode($result);
  }

  public function delMember()
  {
  	$m_id = $this->query_model->clean('delID','id');
  	// echo $user_id;

  	$delMem = $this->query_model->execSQL("UPDATE tp_members SET status = 'X' WHERE m_id = ".$m_id);
  	$result = array("typ" => "success", "ttl" => "Delete User", "msg" => "Member has been deleted!");
  	echo json_encode($result);
  }

   public function listing(){
    $requestData= $_REQUEST;
    $columns = array( 
      0 => 'm_id',
      1 => 'm_id',
      2 => 'username',
      3 => 'firstname',
      4 => 'lastname',
      5 => 'status',
      6 => 'created_date',
      7 => 'created_by'
    );


    $sql = "";
    $totalData = 0;
    $totalFiltered = 0;

    $sql = "SELECT * FROM (SELECT u.m_id, u.username, IFNULL(u.firstname,'') AS firstname, IFNULL(u.lastname,'') AS lastname, u.status, u.created_date, (SELECT realname FROM tp_users WHERE user_id = u.created_by) AS created_by FROM tp_members u WHERE u.status <> 'X')X WHERE 1=1 ";

    $records = $this->query_model->getDataCount($sql);

    $totalData = $records["count"];
    $totalFiltered = $totalData;

    //This is for search function...
      if( !empty($requestData['search']['value']) ) {
        $sql.=" AND username LIKE '%".$requestData['search']['value']."%' OR username LIKE '%".$requestData['search']['value']."%'";    
        //$sql.=" OR status LIKE '%".$requestData['search']['value']."%' )";
      }  
      $totalFiltered = $this->query_model->getDataCount($sql)["count"];
      //end of search function

      $sql.=" ORDER BY ". $columns[$requestData['order'][0]['column']]." ".$requestData['order'][0]['dir']." LIMIT ".$requestData['start']." ,".$requestData['length']."   ";
    
    //$totalFiltered = $this->query_model->getDataCount($sql)["count"];

    $records = $this->query_model->getDataArray($sql);


    $data = array();

    foreach ($records as $row) {
      $nestedData=array();
      
      $nestedData["m_id"] = $row["m_id"];
      $nestedData["username"] = $row["username"];
      $nestedData["firstname"] = $row["firstname"];
      $nestedData["lastname"] = $row["lastname"];
      $nestedData["status"] = $row["status"];
      $nestedData["created_date"] = $row["created_date"];
      $nestedData["created_by"] = $row["created_by"];

      $data[] = $nestedData;  
    }

    $json_data = array(
          "draw"            => intval( $requestData['draw'] ),   // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw. 
          "recordsTotal"    => intval( $totalData ),  // total number of records
          "recordsFiltered" => intval( $totalFiltered ), // total number of records after searching, if there is no searching then totalFiltered = totalData
          "data"            => $data   // total data array
          );

    echo json_encode($json_data);       
  }


  
}

?>