<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Leagues extends CI_Controller {

  function __construct(){
    parent::__construct();
    //$this->load->database('default',TRUE);
    $this->load->model("query_model");
    if($this->session->userdata('user_id') == NULL)
    {
      redirect("Adminlogin","location");
    }
  } 

  public function leagues()
  {
    $data['url'] = array_flip($_REQUEST);
    $this->load->view('templates/admin_header');
    $this->load->view('templates/admin_menu',$data);
    // $this->load->view('adminpages/leagues_view');
    $this->load->view('adminpages/league_list_view');
  }

  public function leagueStages()
  {
    $data['url'] = array_flip($_REQUEST);
    $this->load->view('templates/admin_header');
    $this->load->view('templates/admin_menu',$data);
    $this->load->view('adminpages/league_stages_view');
  }

  public function stageRound()
  {
    $data['url'] = array_flip($_REQUEST);
    $this->load->view('templates/admin_header');
    $this->load->view('templates/admin_menu',$data);
    $this->load->view('adminpages/stage_round_view');
  }


  public function countrylisting()
  {
    $requestData= $_REQUEST;
    $columns = array( 
      0 => 'country_id',
      1 => 'ref_country_id',
      2 => 'country_name',
      3 => 'status'
    );


    $sql = "";
    $totalData = 0;
    $totalFiltered = 0;

    $sql = "SELECT * FROM (SELECT * FROM tp_countries WHERE status <> 'X')X WHERE 1=1 ";

    $records = $this->query_model->getDataCount($sql);

    $totalData = $records["count"];
    $totalFiltered = $totalData;

    //This is for search function...
      if( !empty($requestData['search']['value']) ) {
        $sql.=" AND country_name LIKE '%".$requestData['search']['value']."%'";    
        //$sql.=" OR status LIKE '%".$requestData['search']['value']."%' )";
      }  
      $totalFiltered = $this->query_model->getDataCount($sql)["count"];
      //end of search function

      $sql.=" ORDER BY ". $columns[$requestData['order'][0]['column']]." ".$requestData['order'][0]['dir']." LIMIT ".$requestData['start']." ,".$requestData['length']."   ";
    
    //$totalFiltered = $this->query_model->getDataCount($sql)["count"];

    $records = $this->query_model->getDataArray($sql);


    $data = array();

    foreach ($records as $row) {
      $nestedData=array();
      
      $nestedData["country_id"] = $row["country_id"];
      $nestedData["ref_country_id"] = $row["ref_country_id"];
      $nestedData["country_name"] = $row["country_name"];
      $nestedData["status"] = $row["status"];
    

      $data[] = $nestedData;  
    }

    $json_data = array(
          "draw"            => intval( $requestData['draw'] ),   // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw. 
          "recordsTotal"    => intval( $totalData ),  // total number of records
          "recordsFiltered" => intval( $totalFiltered ), // total number of records after searching, if there is no searching then totalFiltered = totalData
          "data"            => $data   // total data array
          );

    echo json_encode($json_data);           
    
  }
  

  public function viewLeagues()
  {
    $data['url'] = array_flip($_REQUEST);
    $data['country_id'] = $this->uri->segment(3);

    $this->load->view('templates/admin_header');
    $this->load->view('templates/admin_menu',$data);
    $this->load->view('adminpages/league_list_view',$data);

  }

  public function chkLeague(){
    // $league_id = $this->query_model->clean('lgid');
    $league_id = 62;
    $sql = "SELECT count(1) as cnt FROM tp_league WHERE league_id = ".$league_id;
    $cnt = $this->query_model->getCell($sql,"cnt");

    // var_dump($cnt);
    if($cnt>0)
    {
      $result['res'] = FALSE;
    }else{
      $result['res'] = TRUE;
    }

    echo json_encode($result);

  }
   public function getSeasonList()
  {
    $country_id = $this->query_model->clean('cntry'); 
    $league_id = $this->query_model->clean('selLeague'); 
    // echo $country_id;
    $arr = array();

    $sql = "";
    $sql .= "SELECT DISTINCT(season_name) FROM tp_season WHERE status <> 'X'";
    // if($country_id != '')
    // {
    //   $sql .="AND country_id = ".$country_id." ";
    // }

    if($league_id != '')
    {
      $sql .="AND league_id = ".$league_id." ";
    }
    $sql .= " ORDER BY season_name ASC";
    $result = $this->query_model->getDataArray($sql);

    // echo $sql;

    echo json_encode($result);
  }
   public function TeamShow()
  {
    $season_id = $this->query_model->clean('season_id','id'); 
  
   
   
    $sql = "SELECT ls.team_name as team_name,t.logo_path as logo_path FROM tp_league_standings AS ls LEFT JOIN tp_teams AS t ON ls.team_id = t.team_id WHERE ls.season_id = ".$season_id."";
   

   
    $sql .= " ORDER BY team_name";
    $result = $this->query_model->getDataArray($sql);


    echo json_encode($result);
  }
   public function getLeagueList()
  {
    $country_id = $this->query_model->clean('cntry'); 
    // echo $country_id;
    $arr = array();

    $sql = "";
    $sql .= "SELECT * FROM tp_league WHERE status <> 'X' ";
    if($country_id != '')
    {
      $sql .="AND country_id = ".$country_id;
    }

    $sql .= " ORDER BY league_name ASC";
    $result = $this->query_model->getDataArray($sql);

    // echo $sql;

    echo json_encode($result);
  }
  public function getCountryList()
  {
    
    $arr = array();
    $result = $this->query_model->getDataArray("SELECT * FROM tp_countries WHERE status <> 'X' ORDER BY country_name ASC");
    echo json_encode($result);
  }
  public function getLoadLeague()
  {
    
    $arr = array();
     $sql = "SELECT * FROM 
      ( SELECT s.ref_season_id,l.league_id,l.country_name,s.season_id,s.season_name,l.league_name,s.status,s.date_start,s.date_end,s.updated_date,s.updated_by 
      FROM tp_season AS s LEFT JOIN tp_league AS l ON s.league_id = l.league_id ORDER BY s.season_name)X WHERE 1=1";
      $sql .=" ORDER BY league_name";
      $sql2 = "SELECT * FROM 
      ( SELECT DISTINCT(season_name)  FROM tp_season ORDER BY season_name DESC)X WHERE 1=1";
    
    $result = $this->query_model->getDataArray($sql);
    $result2 = $this->query_model->getDataArray($sql2);
    $json_data = array("leagues"=>$result,"season"=>$result2);
    echo json_encode($json_data);
  }
   public function updLeague()
  {
    $result = array();
    $season_id = $this->query_model->clean('season_id','id');
    $status = $this->query_model->clean('status');
    $ref_season_id = $this->query_model->clean('ref_season_id','id');
    $res = "";
    // var_dump($status);
   if($status=='O'){
    $res = $this->getStages($ref_season_id); 
    $resstand = $this->getStandings($ref_season_id); 
   
    if($resstand != "END"){
      if($res=="SUCCESS"){
      $sql ="UPDATE tp_season SET status = '".$status."' WHERE season_id = '".$season_id."' AND ref_season_id = ".$ref_season_id;
      $this->query_model->execSQL($sql);

      
      $result = array("typ" => "success", "ttl" => "Update League", "msg" => "League has been updated!");
      
     }
     else if($res=="OPENED"){
        $sql ="UPDATE tp_season SET status = '".$status."' WHERE season_id = '".$season_id."' AND ref_season_id = ".$ref_season_id;
        $this->query_model->execSQL($sql);
       
        $result = array("typ" => "success", "ttl" => "Update League", "msg" => "League has been updated!");
     }
     else{
      
      $result = array("typ" => "error", "ttl" => "Open League", "msg" => "No Current Stages Avaiable!");
     }
    }
    else{
        $result = array("typ" => "error", "ttl" => "Open League", "msg" => "No data on Standings in this league!");
    }
      
   }
   else{
    $sql ="UPDATE tp_season SET status = '".$status."' WHERE season_id = '".$season_id."' AND ref_season_id = ".$ref_season_id;
    $sql2 ="UPDATE tp_league_stages SET status = '".$status."' WHERE season_id = '".$season_id."' AND ref_season_id = ".$ref_season_id;
    $sql3 ="UPDATE tp_rounds SET status = '".$status."' WHERE season_id = '".$season_id."' AND ref_season_id = ".$ref_season_id;
    $this->query_model->execSQL($sql);
    $this->query_model->execSQL($sql2);
    $this->query_model->execSQL($sql3);
    $result = array("typ" => "success", "ttl" => "Update League", "msg" => "League has been updated!");

   } 
     

    // // echo $sql;
    

    

    echo json_encode($result);
  }
   public function getStages($sid)
  {
    $sql = "SELECT * FROM tp_setting WHERE id='1'";
    $apiresult = $this->query_model->getDataArray($sql);
    $APIkey = $apiresult[0]['api_key'];

    $season_id = $this->query_model->getDataArray("SELECT DISTINCT ref_season_id FROM tp_season");

    // foreach ($season_id as $s) 
    // {
      // echo $s['ref_season_id']." ";

      $curl_options = array(
      CURLOPT_URL => "https://sportickr.com/api/v1.0/stages/season/".$sid."?api-key=$APIkey",
      CURLOPT_RETURNTRANSFER => true,
      CURLOPT_HEADER => false,
      CURLOPT_TIMEOUT => 30,
      CURLOPT_CONNECTTIMEOUT => 5
      );

      $curl = curl_init();
      curl_setopt_array( $curl, $curl_options );
      $result = curl_exec( $curl );

      $result = (array) json_decode($result,true);

     
      $res_stages = "";
      if(count($result['data'])>0){
        for ($x = 0 ; $x<count($result['data']) ; $x++) 
        {
       
          // var_dump($result['data']);
            $chkstage = $this->query_model->getCell("SELECT COUNT(1) as cnt FROM tp_league_stages WHERE ref_stage_id =".$result['data'][$x]['id'],"cnt");
            if($chkstage==0)
            {
              $name = str_replace("'","''",$result['data'][$x]['name']);
              // $league_id = $this->query_model->getDataArray("SELECT COUNT(1) AS cnt, league_id, league_name FROM tp_league WHERE ref_league_id =".$result['data'][$x]['league_id']);
              $season_id = $this->query_model->getDataArray("SELECT COUNT(1) AS cnt, season_id, season_name FROM tp_season WHERE ref_season_id =".$result['data'][$x]['season_id']);


              // if($league_id[0]['cnt'] > 0)
              // {

                $sql = "INSERT IGNORE INTO tp_league_stages (ref_stage_id, league_id,stage_name, status, created_date, created_by, ref_season_id, season_id)
                VALUES ('".$result['data'][$x]['id']."','".$result['data'][$x]['league_id']."','".$name."','"."N"."','".date('Y-m-d H:i:s')."','".$this->session->userdata('user_id')."','".$result['data'][$x]['season_id']."','".$season_id[0]['season_id']."')";

                $res =  $this->query_model->execSQL($sql);
                $res_stages = "SUCCESS";
          
              // }
              
              }
              else{
                $res_stages = "OPENED";
              }
              
          }
        }
    else{
        $res_stages = "ERROR";
      }
     
        return $res_stages;
        //foreach ($result['data'] as $r) 

      // } // end foreach ($season_id as $s) {

  }
  public function getStandings($sid)
  {
    $sql = "SELECT * FROM tp_setting WHERE id='1'";
    $apiresult = $this->query_model->getDataArray($sql);
    $APIkey = $apiresult[0]['api_key'];


    $season_id = $this->query_model->getDataArray("SELECT DISTINCT ref_season_id FROM tp_season");

    // foreach ($season_id as $s) 
    // {
      // echo $s['ref_season_id']." ";

      $curl_options = array(
      CURLOPT_URL => "https://sportickr.com/api/v1.0/standings/season/$sid?api-key=$APIkey",
      CURLOPT_RETURNTRANSFER => true,
      CURLOPT_HEADER => false,
      CURLOPT_TIMEOUT => 30,
      CURLOPT_CONNECTTIMEOUT => 5
      );

      $curl = curl_init();
      curl_setopt_array( $curl, $curl_options );
      $result = curl_exec( $curl );

      $result = (array) json_decode($result,true);
      $search = array_search("The data does not exist or your subscription does not fit your needs.",$result);
    
      if($search!="error_description"){
        $resmes = "";
          $season_id = $this->query_model->getDataArray("SELECT COUNT(1) as cnt,season_id FROM tp_season WHERE ref_season_id =".$result['data']['season_id']);
          $league_id = $this->query_model->getDataArray("SELECT COUNT(1) as cnt,league_id FROM tp_league WHERE ref_league_id =".$result['data']['league_id']);
          $stage_id = $this->query_model->getDataArray("SELECT COUNT(1) as cnt,stage_id FROM tp_league_stages WHERE ref_stage_id =".$result['data']['stage_id']);
     
      
        if(count($result['data']['standings'])>0){
          for($x = 0 ; $x<count($result['data']['standings']) ; $x++) {
              $chkstandings = $this->query_model->getCell("SELECT COUNT(1) as cnt FROM tp_league_standings WHERE ref_standings_id =".$result['data']['standings'][$x]['id'],"cnt");
              if($chkstandings==0)
              { 
                $gd = $result['data']['standings'][$x]['statistics'][0]['goal_difference'];
                if($gd[0] == '-')
                {
                  $gd = substr($gd, 1);
                }


                  $team_name = str_replace("'","''",$result['data']['standings'][$x]['team_name']);   
                  $sql = "INSERT IGNORE INTO tp_league_standings (ref_standings_id, league_id,season_id, stage_id, position, team_id, team_name, group_id,points,result,recent_form,ref_status,created_date,created_by,goal_diff,games_played)

                  VALUES ('".$result['data']['standings'][$x]['id']."','".$league_id[0]['league_id']."','".$season_id[0]['season_id']."','".$stage_id[0]['stage_id']."','".$result['data']['standings'][$x]['position']."','".$result['data']['standings'][$x]['team_id']."','".$team_name."','".$result['data']['standings'][$x]['group_id']."','".$result['data']['standings'][$x]['points']."','".$result['data']['standings'][$x]['result']."','".$result['data']['standings'][$x]['recent_form']."','".$result['data']['standings'][$x]['status']."','".date('Y-m-d H:i:s')."','".$this->session->userdata('user_id')."','".$gd."','".$result['data']['standings'][$x]['statistics'][0]['games_played']."')";

                  $res =  $this->query_model->execSQL($sql);
                  $this->getTeam($result['data']['standings'][$x]['team_id']);
                  $resmes = "SUCCESS";
            
                
                }
                else{
                  $resmes = "ALREADY";
                }
          }
        }
        else{
            $resmes = "ERROR";
        }
     
      }
       else{
        $resmes = "END"; 
      }
     
      return $resmes;
  }
  public function getTeam($tid)
  {
    $sql = "SELECT * FROM tp_setting WHERE id='1'";
    $apiresult = $this->query_model->getDataArray($sql);
    $APIkey = $apiresult[0]['api_key'];

    $curl_options = array(
      CURLOPT_URL => "https://sportickr.com/api/v1.0/teams/$tid?api-key=$APIkey",
      CURLOPT_RETURNTRANSFER => true,
      CURLOPT_HEADER => false,
      CURLOPT_TIMEOUT => 30,
      CURLOPT_CONNECTTIMEOUT => 5
    );

    $curl = curl_init();
    curl_setopt_array( $curl, $curl_options );
    $result = curl_exec( $curl );

    $result = (array) json_decode($result,true);

    $chkTeam = $this->query_model->getCell("SELECT COUNT(1) as cnt FROM tp_teams WHERE ref_team_id =".$result['data']['id'],"cnt");

      if($chkTeam==0)
        {
        $country_id = $this->query_model->getDataArray("SELECT COUNT(1) as cnt,country_id FROM tp_countries WHERE ref_country_id =".$result['data']['country_id']);
        $name = str_replace("'","''",$result['data']['name']);     
        $sql = "INSERT IGNORE INTO tp_teams (ref_team_id, team_name,team_nickname, twitter, country_id, ref_country_id, founded, logo_path,created_date,created_by,updated_date,updated_by,status)
        VALUES ('".$result['data']['id']."','".$name."','".$name."','','".$country_id[0]['country_id']."','".$result['data']['country_id']."','".$result['data']['founded']."','','".date('Y-m-d H:i:s')."','".$this->session->userdata('user_id')."','','','A')";
        $res =  $this->query_model->execSQL($sql);

        // if(count($result['data']['squad'])>0){
        //   for($x = 0 ; $x<count($result['data']['squad']) ; $x++){
        //     $this->getPlayers($result['data']['squad'][$x]['player_id']);
        //   }
        // }
        

            
        }
       

  }
   public function getPlayers($pid)
  {
    $sql = "SELECT * FROM tp_setting WHERE id='1'";
    $apiresult = $this->query_model->getDataArray($sql);
    $APIkey = $apiresult[0]['api_key'];

    $curl_options = array(
      CURLOPT_URL => "https://sportickr.com/api/v1.0/player/$pid?api-key=$APIkey",
      CURLOPT_RETURNTRANSFER => true,
      CURLOPT_HEADER => false,
      CURLOPT_TIMEOUT => 30,
      CURLOPT_CONNECTTIMEOUT => 5
    );

    $curl = curl_init();
    curl_setopt_array( $curl, $curl_options );
    $result = curl_exec( $curl );

    $result = (array) json_decode($result,true);

    $chkPlayer = $this->query_model->getCell("SELECT COUNT(1) as cnt FROM tp_players WHERE ref_player_id =".$result['data']['id'],"cnt");

      if($chkPlayer==0)
        {
        $team_id = $this->query_model->getDataArray("SELECT COUNT(1) as cnt,team_id FROM tp_teams WHERE ref_team_id =".$result['data']['team_id']);
        $country_id = $this->query_model->getDataArray("SELECT COUNT(1) as cnt,country_id FROM tp_countries WHERE ref_country_id =".$result['data']['country_id']);
        $common_name = str_replace("'","''",$result['data']['common_name']);
        $fullname = str_replace("'","''",$result['data']['fullname']);
        $firstname = str_replace("'","''",$result['data']['firstname']);
        $lastname = str_replace("'","''",$result['data']['lastname']);
        $nationality = str_replace("'","''",$result['data']['nationality']);

        
        $sql = "INSERT IGNORE INTO tp_players (ref_player_id, team_id,ref_team_id, country_id, ref_country_id, common_name, fullname, firstname,lastname,nationality,birthdate,birthcountry,birthplace,height,weight,image_path,position,created_date,created_by,updated_date,updated_by)
        VALUES ('".$result['data']['id']."','".$team_id[0]['team_id']."','".$result['data']['team_id']."','".$country_id[0]['country_id']."','".$result['data']['country_id']."','".$common_name."','".$fullname."','".$firstname."','".$lastname."','".$nationality."','','','','','','','".$result['data']['position']['name']."','".date('Y-m-d H:i:s')."','".$this->session->userdata('user_id')."','','')";

        $res =  $this->query_model->execSQL($sql);
        
            
        }
       

  }



}

?>