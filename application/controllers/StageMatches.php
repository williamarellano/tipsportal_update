<?php defined('BASEPATH') OR exit('No direct script access allowed');

class StageMatches extends CI_Controller {

  function __construct(){
    parent::__construct();
    //$this->load->database('default',TRUE);
    $this->load->model("query_model");
    if($this->session->userdata('user_id') == NULL)
    {
      redirect("Adminlogin","location");
    }
  } 

   public function updLeague()
  {
    $result = array();
    $match_id = $this->query_model->clean('matchid','id');
    $status = $this->query_model->clean('status');

    $sql ="UPDATE tp_stage_match SET status = '".$status."' WHERE match_id = ".$match_id;

    // echo $sql;
    $this->query_model->execSQL($sql);

    $result = array("typ" => "success", "ttl" => "Update League", "msg" => "Match has been updated!");

    echo json_encode($result);
  }


  public function getCountryList()
  {
    
    $arr = array();
    $result = $this->query_model->getDataArray("SELECT DISTINCT l.country_id, l.country_name
      FROM tp_league l 
      INNER JOIN tp_league_stages s ON l.league_id = s.league_id 
      INNER JOIN tp_stage_match m ON s.stage_id = m.stage_id AND s.league_id = m.league_id
      WHERE s.status <>'X' ORDER BY l.country_name ASC");
    echo json_encode($result);
  }

  public function getLeagueList()
  {
    $country_id = $this->query_model->clean('cntry'); 
    // echo $country_id;
    $arr = array();

    $sql = "";
    $sql .= "SELECT DISTINCT l.league_id, l.league_name
      FROM tp_league l 
      INNER JOIN tp_league_stages s ON l.league_id = s.league_id 
      INNER JOIN tp_stage_match m ON s.stage_id = m.stage_id AND s.league_id = m.league_id
      WHERE s.status <>'X' ";
    if($country_id != '')
    {
      $sql .="AND l.country_id = ".$country_id;
    }

    $sql.= " ORDER BY l.league_name ASC ";

    $result = $this->query_model->getDataArray($sql);

    // echo $sql;

    echo json_encode($result);
  }

  public function getSeasonList()
  {
    $country_id = $this->query_model->clean('cntry'); 
    $league_id = $this->query_model->clean('selLeague'); 
    // echo $country_id;
    $arr = array();

    $sql = "";
    $sql .= "SELECT DISTINCT l.season
      FROM tp_league l 
      INNER JOIN tp_league_stages s ON l.league_id = s.league_id 
      INNER JOIN tp_stage_match m ON s.stage_id = m.stage_id AND s.league_id = m.league_id
      WHERE s.status <>'X' AND (l.season <> '' OR l.season IS NOT NULL) ";

    if($country_id != '')
    {
      $sql .="AND l.country_id = ".$country_id." ";
    }

    if($league_id != '')
    {
      $sql .="AND l.league_id = ".$league_id." ";
    }

    $sql.= " ORDER BY l.season ASC ";
    $result = $this->query_model->getDataArray($sql);

    // echo $sql;

    echo json_encode($result);
  }

  public function getStageList()
  {
    $country_id = $this->query_model->clean('cntry'); 
    $league_id = $this->query_model->clean('selLeague'); 
    // echo $country_id;
    $arr = array();

    $sql = "";
    $sql .= "SELECT DISTINCT s.stage_id, s.stage_name
      FROM tp_league l 
      INNER JOIN tp_league_stages s ON l.league_id = s.league_id 
      INNER JOIN tp_stage_match m ON s.stage_id = m.stage_id AND s.league_id = m.league_id
      WHERE s.status <>'X' ";
    if($country_id != '')
    {
      $sql .="AND l.country_id = ".$country_id." ";
    }

    if($league_id != '')
    {
      $sql .="AND l.league_id = ".$league_id." ";
    }

    $sql.= " ORDER BY s.stage_name ASC ";
    $result = $this->query_model->getDataArray($sql);

    // echo $sql;

    echo json_encode($result);
  }

  public function listing_match()
  {
  	$country_id = $this->query_model->clean('cntry');
    $league_id = $this->query_model->clean('selLeague');
    $stage = $this->query_model->clean('selStage');
    $season = $this->query_model->clean('selSeason');
    $status = $this->query_model->clean('selstatus');

    $requestData= $_REQUEST;
    $columns = array( 
      0 => 'match_id',
      1 => 'stage_id',
      2 => 'league_id',
      3 => 'country_id',
      4 => 'country_name',
      5 => 'league_name',
      6 => 'stage_name',
      7 => 'season',
      8 => 'date_start',
      9 => 'date_end',
      10 => 'match_date',
      11=> 'home_team',
      12 > 'away_team',
      13=> 'home_team_score',
      14=> 'away_team_score',
      15 => 'status'
    );


    $sql = "";
    $totalData = 0;
    $totalFiltered = 0;

    $sql = "SELECT * FROM (
			SELECT m.match_id,s.stage_id, s.league_id, l.country_id, l.country_name, l.league_name, s.stage_name, l.season,
			m.date_start, m.date_end, m.match_date, m.home_team, m.away_team, m.home_team_score, m.away_team_score, m.status,
			m.created_date, IFNULL((SELECT realname FROM tp_users WHERE user_Id = m.created_by),'')AS created_by,
			m.updated_date, m.updated_by 
			FROM tp_league l 
			INNER JOIN tp_league_stages s ON l.league_id = s.league_id 
			INNER JOIN tp_stage_match m ON s.stage_id = m.stage_id AND s.league_id = m.league_id
			WHERE s.status <>'X')X WHERE 1=1 ";

    $records = $this->query_model->getDataCount($sql);

    $totalData = $records["count"];
    $totalFiltered = $totalData;

    if(!empty($country_id)){
      $sql.=" AND country_id = '".$country_id."' "; 
    }

    if(!empty($league_id)){
      $sql.=" AND league_id = '".$league_id."'"; 
    }

    if(!empty($stage)){
      $sql.=" AND stage_id = '".$stage."'"; 
    }

    if(!empty($season)){
      $sql.=" AND season = '".$season."'"; 
    }

    if(!empty($status)){
      $sql.=" AND status = '".$status."'"; 
    }


    //This is for search function...
      if( !empty($requestData['search']['value']) ) {
        $sql.=" AND country_name LIKE '%".$requestData['search']['value']."%'";    
        //$sql.=" OR status LIKE '%".$requestData['search']['value']."%' )";
      }  
      $totalFiltered = $this->query_model->getDataCount($sql)["count"];
      //end of search function

      $sql.=" ORDER BY ". $columns[$requestData['order'][0]['column']]." ".$requestData['order'][0]['dir']." LIMIT ".$requestData['start']." ,".$requestData['length']."   ";
    
    //$totalFiltered = $this->query_model->getDataCount($sql)["count"];

    $records = $this->query_model->getDataArray($sql);


    $data = array();

    foreach ($records as $row) {
      $nestedData=array();
      
      $nestedData["match_id"] = $row["match_id"];
      $nestedData["stage_id"] = $row["stage_id"];
      $nestedData["league_id"] = $row["league_id"];
      $nestedData["country_id"] = $row["country_id"];
      $nestedData["country_name"] = $row["country_name"];
      $nestedData["league_name"] = $row["league_name"];
      $nestedData["stage_name"] = $row["stage_name"];
      $nestedData["season"] = $row["season"];
      $nestedData["date_start"] = $row["date_start"];
      $nestedData["date_end"] = $row["date_end"];
      $nestedData["match_date"] = $row["match_date"];
      $nestedData["home_team"] = $row["home_team"];
      $nestedData["away_team"] = $row["away_team"];
      $nestedData["home_team_score"] = $row["home_team_score"];
      $nestedData["away_team_score"] = $row["away_team_score"];
      $nestedData["status"] = $row["status"];


      $data[] = $nestedData;  
    }

    $json_data = array(
          "draw"            => intval( $requestData['draw'] ),   // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw. 
          "recordsTotal"    => intval( $totalData ),  // total number of records
          "recordsFiltered" => intval( $totalFiltered ), // total number of records after searching, if there is no searching then totalFiltered = totalData
          "data"            => $data   // total data array
          );

    echo json_encode($json_data);           
    
  }

}
?>