<?php defined('BASEPATH') OR exit('No direct script access allowed');

class LeagueStages extends CI_Controller {

  function __construct(){
    parent::__construct();
    //$this->load->database('default',TRUE);
    $this->load->model("query_model");
    if($this->session->userdata('user_id') == NULL)
    {
      redirect("Adminlogin","location");
    }
  } 

  public function updLeagueStage()
  {
    $result = array();
    $stageid = $this->query_model->clean('stageid','id');
    $stagename = $this->query_model->clean('stagename');
    $stagenick = $this->query_model->clean('stagenick');
    $status = $this->query_model->clean('status');

    $sql ="UPDATE tp_league_stages SET status = '".$status."', stage_name = '".$stagename."', stage_nickname = '".$stagenick."', status = '".$status."' WHERE stage_id = ".$stageid;

    // echo $sql;
    $this->query_model->execSQL($sql);

    $result = array("typ" => "success", "ttl" => "Update Stage", "msg" => "Stage has been updated!");

    echo json_encode($result);
  }



  public function getCountryList()
  {
    
    $arr = array();
    $result = $this->query_model->getDataArray("SELECT DISTINCT l.country_id, l.country_name FROM tp_league l
        INNER JOIN tp_league_stages s ON l.league_id = s.league_id WHERE s.status <>'X' ORDER BY l.country_name ASC");
    echo json_encode($result);
  }

  public function getLeagueList()
  {
    $country_id = $this->query_model->clean('cntry'); 
    // echo $country_id;
    $arr = array();

    $sql = "";
    $sql .= "SELECT DISTINCT l.league_id, l.league_name FROM tp_league l
        INNER JOIN tp_league_stages s ON l.league_id = s.league_id WHERE s.status <>'X' ";
    if($country_id != '')
    {
      $sql .="AND l.country_id = ".$country_id;
    }

    $sql .= " ORDER BY l.league_name ASC";
    $result = $this->query_model->getDataArray($sql);

    // echo $sql;

    echo json_encode($result);
  }

  public function getSeasonList()
  {
    $country_id = $this->query_model->clean('cntry'); 
    $league_id = $this->query_model->clean('selLeague'); 
    // echo $country_id;
    $arr = array();

    $sql = "";
    // $sql .= "SELECT DISTINCT l.season FROM tp_league l
    //     INNER JOIN tp_league_stages s ON l.league_id = s.league_id WHERE s.status <>'X' AND (l.season <> '' OR l.season IS NOT NULL) ";

    $sql .= "SELECT DISTINCT(season_name) FROM tp_season WHERE status <> 'X'";
    // if($country_id != '')
    // {
    //   $sql .="AND l.country_id = ".$country_id." ";
    // }

    if($league_id != '')
    {
      $sql .="AND league_id = ".$league_id." ";
    }

    $sql .= " ORDER BY season_name ASC";
    $result = $this->query_model->getDataArray($sql);

    // echo $sql;

    echo json_encode($result);
  }

  public function getStageList()
  {
    $country_id = $this->query_model->clean('cntry'); 
    $league_id = $this->query_model->clean('selLeague'); 
    // echo $country_id;
    $arr = array();

    $sql = "";
    $sql .= "SELECT DISTINCT s.stage_id, s.stage_name FROM tp_league l
        INNER JOIN tp_league_stages s ON l.league_id = s.league_id WHERE s.status <>'X' ";
    if($country_id != '')
    {
      $sql .="AND l.country_id = ".$country_id." ";
    }

    if($league_id != '')
    {
      $sql .="AND l.league_id = ".$league_id." ";
    }

    $sql .= " ORDER BY s.stage_name ASC";
    $result = $this->query_model->getDataArray($sql);

    // echo $sql;

    echo json_encode($result);
  }
public function getLoadStages()
  {
    
    $arr = array();
     $sql = "SELECT * FROM 
      ( SELECT l.league_name,ls.stage_id,s.season_id,s.season_name,ls.ref_stage_id,ls.ref_season_id,ls.status AS stat,s.status AS STATUS,ls.stage_name AS stage_name
      FROM tp_league_stages AS ls LEFT JOIN tp_season AS s ON ls.season_id = s.season_id LEFT JOIN tp_league AS l ON s.league_id = l.league_id WHERE s.status ='O' )X WHERE 1=1";
      $sql .=" ORDER BY stage_name";
      $sql2 = "SELECT * FROM 
      ( SELECT s.ref_season_id,l.league_id,l.country_name,s.season_id,s.season_name,l.league_name,s.status,s.date_start,s.date_end,s.updated_date,s.updated_by 
      FROM tp_season AS s LEFT JOIN tp_league AS l ON s.league_id = l.league_id WHERE s.status ='O' ORDER BY s.season_name)X WHERE 1=1";
      $sql2 .=" ORDER BY league_name";
    
    $result = $this->query_model->getDataArray($sql);
    $result2 = $this->query_model->getDataArray($sql2);
    $json_data = array("stages"=>$result,"season"=>$result2);
    echo json_encode($json_data);
  }
 public function updStage()
  {
    $result = array();
    $season_id = $this->query_model->clean('season_id','id');
    $stage_id = $this->query_model->clean('stage_id','id');
    $status = $this->query_model->clean('status');
    $ref_season_id = $this->query_model->clean('ref_season_id','id');
    $res = "";

   
    // var_dump($status);
   if($status=='O'){
     $res = $this->getRounds($ref_season_id); 
    
     if($res=="SUCCESS"){
      $sql ="UPDATE tp_league_stages SET status = '".$status."' WHERE stage_id = '".$stage_id."' AND ref_season_id = ".$ref_season_id;
      $this->query_model->execSQL($sql);
      $result = array("typ" => "success", "ttl" => "Update Stages", "msg" => "Stage has been updated!");
     }
     else if($res=="OPENED"){
        $sql ="UPDATE tp_league_stages SET status = '".$status."' WHERE stage_id = '".$stage_id."' AND ref_season_id = ".$ref_season_id;
        $this->query_model->execSQL($sql);
        $result = array("typ" => "success", "ttl" => "Update Stages", "msg" => "Stage has been updated!");
     }
     else{
      
      $result = array("typ" => "error", "ttl" => "Open Stages", "msg" => "No Current Round Avaiable!");
     } 
   }
   else{
    $sql ="UPDATE tp_league_stages SET status = '".$status."' WHERE stage_id = '".$stage_id."' AND ref_season_id = ".$ref_season_id;
    $this->query_model->execSQL($sql);
    $result = array("typ" => "success", "ttl" => "Update Stages", "msg" => "Stages has been updated!");

   } 
     


    echo json_encode($result);
  }
  public function getRounds($sid)
  {
    $sql = "SELECT * FROM tp_setting WHERE id='1'";
    $apiresult = $this->query_model->getDataArray($sql);
    $APIkey = $apiresult[0]['api_key'];

    $season_id = $this->query_model->getDataArray("SELECT DISTINCT ref_season_id FROM tp_season");

    // foreach ($season_id as $s) 
    // {
      // echo $s['ref_season_id']." ";

      $curl_options = array(
      CURLOPT_URL => "https://sportickr.com/api/v1.0/rounds/season/".$sid."?api-key=$APIkey",
      CURLOPT_RETURNTRANSFER => true,
      CURLOPT_HEADER => false,
      CURLOPT_TIMEOUT => 30,
      CURLOPT_CONNECTTIMEOUT => 5
      );

      $curl = curl_init();
      curl_setopt_array( $curl, $curl_options );
      $result = curl_exec( $curl );

      $result = (array) json_decode($result,true);
     
      $res_stages = "";
      if(count($result['data'])>0){
        for ($x = 0 ; $x<count($result['data']) ; $x++) 
        {
       
          // var_dump($result['data']);
            $chkstage = $this->query_model->getCell("SELECT COUNT(1) as cnt FROM tp_rounds WHERE ref_round_id =".$result['data'][$x]['id'],"cnt");
            if($chkstage==0)
            {
              $name = str_replace("'","''",$result['data'][$x]['name']);
              // $league_id = $this->query_model->getDataArray("SELECT COUNT(1) AS cnt, league_id, league_name FROM tp_league WHERE ref_league_id =".$result['data'][$x]['league_id']);
              $season_id = $this->query_model->getDataArray("SELECT COUNT(1) AS cnt, season_id, season_name FROM tp_season WHERE ref_season_id =".$result['data'][$x]['season_id']);
              $stage_id = $this->query_model->getDataArray("SELECT COUNT(1) AS cnt, stage_id, stage_name FROM tp_league_stages WHERE ref_stage_id =".$result['data'][$x]['stage_id']);

              $timestamp_start = $result['data'][$x]['start'];
              $timestamp_end = $result['data'][$x]['end'];
              $datestart =  gmdate("Y-m-d\TH:i:s\Z", $timestamp_start);
              $dateend =  gmdate("Y-m-d\TH:i:s\Z", $timestamp_end);
              // if($league_id[0]['cnt'] > 0)
              // {

                $sql = "INSERT IGNORE INTO tp_rounds (ref_round_id,round_name, status, updated_at, ref_season_id, season_id,stage_id,start_datetime,end_datetime)
                VALUES ('".$result['data'][$x]['id']."',".$name.",'"."N"."','".date('Y-m-d H:i:s')."','".$result['data'][$x]['season_id']."','".$season_id[0]['season_id']."','".$stage_id[0]['stage_id']."','".$datestart."','".$dateend."')";

                $res =  $this->query_model->execSQL($sql);
                $res_stages = "SUCCESS";
          
              // }
              
              }
              else{
                $res_stages = "OPENED";
              }
              
          }
        }
    else{
        $res_stages = "ERROR";
      }
     
        return $res_stages;
       

  

  }

  public function listing_LeagueStages()
  {
    // $country_id = $this->query_model->clean('cntry');
    // $league_id = $this->query_model->clean('selLeague');
    // $stage = $this->query_model->clean('selStage');
    // $season = $this->query_model->clean('selSeason');
    // $status = $this->query_model->clean('selstatus');

    $requestData= $_REQUEST;
    $columns = array( 
      0 => 'stage_id',
      1 => 'stage_name',
      2 => 'stage_nickname',
      3 => 'league_name',
      4 => 'season',
      5 => 'date_start',
      6 => 'date_end',
      7 => 'status'
    );

    $sql = "";
    $totalData = 0;
    $totalFiltered = 0;

    $sql = "SELECT * FROM (SELECT s.stage_id, s.league_id, IFNULL((SELECT league_name FROM tp_league WHERE league_id = s.league_id),'') AS league_name,
      s.season_id, (SELECT season_name FROM tp_season WHERE season_id = s.season_id) AS season, s.stage_name, IFNULL(s.stage_nickname,'') AS stage_nickname, s.date_start, s.date_end,
      s.status, s.created_by, s.created_date
      FROM tp_league_stages s WHERE s.status <> 'X')X WHERE 1=1  ";

    $records = $this->query_model->getDataCount($sql);

    $totalData = $records["count"];
    $totalFiltered = $totalData;

    // if(!empty($country_id)){
    //   $sql.=" AND country_id = '".$country_id."' "; 
    // }

    // if(!empty($league_id)){
    //   $sql.=" AND league_id = '".$league_id."'"; 
    // }

    // if(!empty($stage)){
    //   $sql.=" AND stage_id = '".$stage."'"; 
    // }

    // if(!empty($season)){
    //   $sql.=" AND season = '".$season."'"; 
    // }

    // if(!empty($status)){
    //   $sql.=" AND status = '".$status."'"; 
    // }

    //This is for search function...
    if( !empty($requestData['search']['value']) ) {
      $sql.=" AND (league_name LIKE '%".$requestData['search']['value']."%'";   
      $sql.=" OR stage_name LIKE '%".$requestData['search']['value']."%' ";
      $sql.=" OR season LIKE '%".$requestData['search']['value']."%') ";
      //$sql.=" OR status LIKE '%".$requestData['search']['value']."%' )";
    }  
    $totalFiltered = $this->query_model->getDataCount($sql)["count"];
    //end of search function

    $sql.=" ORDER BY ". $columns[$requestData['order'][0]['column']]." ".$requestData['order'][0]['dir']." LIMIT ".$requestData['start']." ,".$requestData['length']."   ";
    
    //$totalFiltered = $this->query_model->getDataCount($sql)["count"];

    $records = $this->query_model->getDataArray($sql);


    $data = array();

    $status = "";

    foreach ($records as $row) {
      $nestedData=array();

      $nestedData["stage_id"] = $row["stage_id"];
      $nestedData["league_id"] = $row["league_id"];
      $nestedData["league_name"] = $row["league_name"];
      $nestedData["stage_name"] = $row["stage_name"];
      $nestedData["stage_nickname"] = $row["stage_nickname"];
      $nestedData["season_id"] = $row["season_id"];
      $nestedData["season"] = $row["season"];
      $nestedData["date_start"] = $row["date_start"];
      $nestedData["date_end"] = $row["date_end"];
      $nestedData["status"] = $row["status"]; 
      
      $data[] = $nestedData;  
    }

    $json_data = array(
          "draw"            => intval( $requestData['draw'] ),   // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw. 
          "recordsTotal"    => intval( $totalData ),  // total number of records
          "recordsFiltered" => intval( $totalFiltered ), // total number of records after searching, if there is no searching then totalFiltered = totalData
          "data"            => $data   // total data array
          );

    echo json_encode($json_data);       
  }
}
?>