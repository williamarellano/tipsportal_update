<?php
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}


if (!function_exists('chkAccess')) {
  function chkAccess($x){
    $ci = &get_instance();
    $ci->load->library('session');

    $obj = explode(",", $ci->session->userdata("roles"));

    if (!in_array($x, $obj)){
        return false;
    }
    return true;            

  }
}


if (!function_exists('chkSession')) {
    function chkSession()
    {
      $ci = &get_instance();
      $ci->load->library('session');

      return !empty($ci->session->userdata('user_id'));
        // if ($ci->session->userdata("user_id") == ""){
        //   //$CI->load->view("");
        //   $result = array("typ" => "error", "ttl" => $ci->lang->line("sessionExpired"), "msg" => $ci->lang->line("sessionExpiredDesc"));   
        //   return json_encode($result);
        // }   
    }
}