<?php
defined('BASEPATH') OR exit('No direct script access allowed');


$active_group = 'default';
$query_builder = TRUE;

$db['default'] = array(
	'dsn'	=> '',

	// 'hostname' => 'localhost',
	// 'username' => 'root',
	// 'password' => '',
	// 'database' => 'tipsportal',

	// 'hostname' => 'mysql5011.site4now.net',
	// 'username' => 'a40516_tips',
	// 'password' => 'tipsportal1',
	// 'database' => 'db_a40516_tips',

	'hostname' => 'mysql5011.site4now.net',
	'username' => 'a40667_tp',
	'password' => 'tipsportal1',
	'database' => 'db_a40667_tp',

	// UAT
	// 'hostname' => '127.0.0.1',
	// 'username' => 'ssimecgp_ssiit',
	// 'password' => 'p0w3r@SSI',
	// 'database' => 'ssimecgp_tipsportal',


	'dbdriver' => 'mysqli',
	'dbprefix' => '',
	'pconnect' => FALSE,
	'db_debug' => (ENVIRONMENT !== 'production'),
	'cache_on' => FALSE,
	'cachedir' => '',
	'char_set' => 'utf8',
	'dbcollat' => 'utf8_general_ci',
	'swap_pre' => '',
	'encrypt' => FALSE,
	'compress' => FALSE,
	'stricton' => FALSE,
	'failover' => array(),
	'save_queries' => TRUE
);
