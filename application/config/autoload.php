<?php
defined('BASEPATH') OR exit('No direct script access allowed');


$autoload['packages'] = array();


$autoload['libraries'] = array('database','session','form_validation','table','calendar', 'encrypt','email','upload','pagination');


$autoload['drivers'] = array();


$autoload['helper'] =  array('url','form','date','security', 'file','htmlpurifier','cookie','custom_function');


$autoload['config'] = array();


$autoload['language'] = array();


$autoload['model'] = array();