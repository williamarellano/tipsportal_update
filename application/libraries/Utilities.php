<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Utilities
{
	function __construct()
	{
		
	}

	public function upload_img_logo($formname, $path, $new_name='') 
	{
		$CI =& get_instance();

		$config['file_name'] = $new_name;  
    	$config['upload_path'] = $path;
		$config['allowed_types'] = 'jpeg|jpg|png';
		$config['max_size'] = 100000;
		$config['overwrite'] = TRUE;

		//if(file_exists($path)) unlink($path);

    if (!file_exists($path)) mkdir($path, 0777, true);

    $CI->load->library('upload', $config);
    $CI->upload->initialize($config);

    if (! ($CI->upload->do_upload($formname))){
    	$data = array('status' => 0, 'message' => $CI->upload->display_errors());
    } 
    else {
    	$data = array('status' => 1, 'message' => $CI->upload->data());
    }
  	return $data;
	}


	public function upload($formname, $path, $new_name='') 
	{
		$CI =& get_instance();

		$config['file_name'] = $new_name;  
    $config['upload_path'] = $path;
		$config['allowed_types'] = 'xlsx';
		$config['max_size'] = 100000;
		$config['overwrite'] = TRUE;

		//if(file_exists($path)) unlink($path);

    if (!file_exists($path)) mkdir($path, 0777, true);

    $CI->load->library('upload', $config);
    $CI->upload->initialize($config);

    if (! ($CI->upload->do_upload($formname))){
    	$data = array('status' => 0, 'message' => $CI->upload->display_errors());
    } 
    else {
    	$data = array('status' => 1, 'message' => $CI->upload->data());
    }
  	return $data;
	}

	public function uploadcsv($formname, $path, $new_name='') 
	{
		$CI =& get_instance();

		$config['file_name'] = $new_name;  
    $config['upload_path'] = $path;
		$config['allowed_types'] = 'csv';
		$config['max_size'] = 100000;
		$config['overwrite'] = TRUE;

		//if(file_exists($path)) unlink($path);

    if (!file_exists($path)) mkdir($path, 0777, true);

    $CI->load->library('upload', $config);
    $CI->upload->initialize($config);

    if (! ($CI->upload->do_upload($formname))){
    	$data = array('status' => 0, 'message' => $CI->upload->display_errors());
    } 
    else {
    	$data = array('status' => 1, 'message' => $CI->upload->data());
    }
  	return $data;
	}

}