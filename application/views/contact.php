<div id="contactUs" class="container-fluid no-pad">
  <div class="container liveStream no-pad ">
    <div class="col-md-8 no-pad left-bar text-center white-bg">
      <h1>Contact Us</h1>

      <div id="contact-c" class="col-md-12 text-left ">
        <div class="row">
          <div class="col-md-12">
            <div class="cu-top">
                  <p class="font-bold"><img src="<?=base_url();?>public/img/location.png" alt="Address"> Address</p>
                  <p class="cu-in">25 First Street, 2nd Floor Cambridge, MA 02141 Singapore</p>
                  <p class="font-bold"><img src="<?=base_url();?>public/img/whatsapp.png" alt="WhatsApp"> Whatsapp</p>
                  <p class="cu-in"><a href="tel:1-617-812-5820"> +1 617 812 5820</a></p>
                  <p class="font-bold"><img src="<?=base_url();?>public/img/email.png" alt="Email"> Email</p>
                  <p class="cu-in"><a href="mailto:tipsportal@gmail.com">tipsportal@gmail.com</a></p>

                  <p class="cu-if">If you have any questions or suggestion to improve our site, please drop us an email: <a href="mailto:hello@tipsportal.com"><span class="cs-mail">hello@tipsportal.com</span></a> or use the contact form below. We’d be happy to help you (we offer free help to fans and supporters of tipsportal!</p>
          </div>
            <form>
              <div class="form-group">
                <div class="col-sm-10">
                  <label for="exampleInputEmail1">Your Name (required)</label>
                  <input type="email" class="form-control" id="exampleInputEmail1" placeholder="">
                </div>
              </div>

              <div class="form-group">
                <div class="col-sm-10">
                  <label for="exampleInputEmail1">Your Email (required)</label>
                  <input type="email" class="form-control" id="exampleInputEmail1" placeholder="">
                </div>
              </div>

              <div class="form-group">
                <div class="col-sm-10">
                  <label for="exampleInputEmail1">Subject</label>
                  <input type="email" class="form-control" id="exampleInputEmail1" placeholder="">
                </div>
              </div>

              <div class="form-group">
                <div class="col-sm-10">
                  <label for="exampleInputEmail1">Your Message</label>
                  <textarea class="form-control" rows="15"></textarea>
                </div>
              </div>
            </form>
            <div class="clearfix"></div>
            <div class="col-xs-12 col-centered no-pad h-align hidden-lg hidden-md">
              <a href="#"><img src="<?=base_url();?>public/img/captcha.jpg" alt="sample only" class="img-responsive"></a>
            </div>
          <div class="col-md-10 v-align">
            <div class="col-xs-12 col-md-5 no-pad hidden-xs hidden-sm">
              <a href="#"><img src="<?=base_url();?>public/img/captcha.jpg" alt="sample only" class="img-responsive"></a>
            </div>
            <div class="clearfix hidden-lg hidden-md"></div>
            <div id="csmt" class="col-xs-12 col-md-5 no-pad">
              <button type="submit" class="btn btn-default cu-s">Submit</button>
            </div>
          </div>
          </div>
        </div>

      </div>
      <!--/ls-feed-->
      <!-- <div style="height:690px;"></div> -->

    </div>
    <!--/col-md-8 Left Panel-->
    <div class="clearfix hidden-lg hidden-md"></div>
    <!--======Right Panel=========-->
    <div class="col-md-4 side-bar sideTitle no-pad v-align h-align pull-right">
      <!-- <h1 class="no-margin">Hello</h1> -->
      <div class="hidden-sm hidden-xs">

      <div class="pttbl pttbl-side">
        <div class="pttop">07/25 &#8226; 01:30 AM</div>
        <div class="ptpagetitle">
          <h4>PREMIER LEAGUE</h4>
          <p>Betting Tips</p>
        </div>
        <div class="ptmatch">
          <div class="ptmatchie">
            <img src="<?=base_url();?>public/img/mancity.png">
            <p>Arsenal</p>
          </div>
          <div class="ptmatchie ptvs">vs</div>
          <div class="ptmatchie">
            <img src="<?=base_url();?>public/img/mancity.png">
            <p>Arsenal</p>
          </div>
        </div>
        <p class="ptph">
          Arsenal will most likely be glad when the Premier League comes to an end. Two games played so far have...
        </p>
        <div class="pttip">
          <p>OUT TIP:</p>
          <span>CHELSEA WIN</span>
          <a href="<?=base_url();?>Welcome/previewstips2"><button>View More</button></a>
        </div>
      </div>

        <div class="col-md-12 no-pad ls-ban">
          <a href="<?=base_url();?>Welcome/ftpperfect10">
          <img src="<?=base_url();?>public/img/ls-wp10.jpg" alt="Weekly Perfect 10">
        </a>
        </div>

        <div class="col-md-12 no-pad ls-ban">
          <a href="<?=base_url();?>Welcome/goldengoal">
          <img src="<?=base_url();?>public/img/ls-gg.jpg" alt="The Golden Goal">
        </a>
        </div>

      </div>
    </div>
    <!-- right panel end -->
    </div>
  </div>
  <!--/container-->
</div>
<!--/liveStream-->
