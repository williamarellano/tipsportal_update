<style type="text/css">
	#secMenu li:nth-of-type(1) a{background: #0e6857 !important;}
</style>
<div id="home-carousel" class="carousel slide" data-ride="carousel">
  <!-- Indicators -->
  <!-- <ol class="carousel-indicators">
      <li data-target="#home-carousel" data-slide-to="0" class="active"></li>
      <li data-target="#home-carousel" data-slide-to="1"></li>
    </ol> -->

  <!-- Wrapper for slides -->
  <div class="carousel-inner" role="listbox">
    <div class="item active">
      <img src="<?=base_url();?>public/img/home-banner.jpg" alt="...">
    </div>
    <div class="item">
      <img src="<?=base_url();?>public/img/home-banner.jpg" alt="...">
    </div>
  </div>
</div>
<!-- <div class="jumbotron home-banner">
  </div> -->

<div class="container no-pad">
  <div class="col-md-8 no-pad left-bar-h">
    <div class="col-xs-12 no-pad">
      <div id="home-panel" class="col-md-12 no-pad ">
        <div class="panel panel-default no-border no-borderr no-margin">
          <div class="panel-heading text-left med">Featured Tip of the Week - Manchester City vs Manchester United</div>
          <div class="panel-body">
            <div class="col-sm-4 home-ft-img">
              <div class="row orange-b col-sm-12 col-xs-8 col-sm-offset-0 col-xs-offset-2 no-pad" id="box-search">
                <div class="thumbnail text-center f-thumb">
                  <div class="caption">
                    <div class="col-xs-8 border-time light-grey no-pad col-centered">
                      <p>07/25 &#9679; 01:30 AM</p>
                    </div>
                    <h6 class=" font-bold">PREMIER LEAGUE</h6>
                    <div class="col-xs-6 h-vs no-pad">
                      <img src="<?=base_url();?>public/img/arsenal.png" alt="">
                      <div>Bournemouth</div>
                    </div>
                    <div class="col-xs-6 h-vs no-pad" style="pading-left:4px;">
                      <img src="<?=base_url();?>public/img/chelsea.png" alt="">
                      <div>Crystal Palace</div>
                    </div>
                  </div>
                </div>
              </div>
              <!--/Premiver League row-->
            </div>
            <!--/col-4-->
            <div id="home-panel-left" class="col-lg-8 no-pad">
              <!--======Limit Characters=====-->
              <div id="home-panel-desc" class="col-xs-12 col-sm-8 col-md-8 col-lg-12 no-pad poppins">
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quae deleniti magnam, cum eos alias labore, minima nisi, Quae deleniti magnam, cum eos alias labore, minima nisi, vitae aliquid consentur consentur shudgud  dssdwe ddsdyou
                   . . .
                </p>
                <h1 class="col-xs-6 col-sm-6 home-tip blueGreen font-bold no-pad">Tip: Over 2.5 Goals</h1>
                <a href="<?=base_url();?>Welcome/blog"><button id="home-rm" type="button" name="button" class="pull-right orange no-border no-borderr montserrat">Read more</button></a>
              </div>
              <!--/home-panel-desc-->
            </div>
            <!--/col-8-->
          </div>
          <!--/panel body-->
        </div>
        <!--/panel-->
        <div id="perfect-ten" class="col-xs-12 no-pad hidden-sm hidden-xs">
<a href="<?=base_url();?>Welcome/ftpperfect10"><img src="<?=base_url();?>public/img/perfect-10.jpg" alt="The Perfect 10 League" class="img-responsive">
          </a>        </div>

        <div id="perfect-ten-mobile" class="col-xs-12 no-pad hidden-lg hidden-md">
<a href="<?=base_url();?>Welcome/ftpperfect10"><img src="<?=base_url();?>public/img/perfect-10-mobile.jpg" alt="The Perfect 10 League" class="img-responsive">
          </a>        </div>

      </div>
      <!--/home panel-->
    </div>
    <!-- /col-12-->
    <div class="col-md-7 no-pad">
      <div id="home-panel-tips" class="col-xs-12 no-pad border">
        <div class="panel panel-default no-border no-borderr">
          <div class="panel-heading text-center">Tipsters Comparison</div>
        </div>
        <div class="col-xs-12 no-pad panel panel-default no-border no-borderr text-center med white">
          <div class="col-xs-2 no-pad tipsters dark-grey">Tipster</div>
          <div class="col-xs-3 no-pad tipsters border dark-grey">Tip Form</div>
          <div class="col-xs-7 no-pad tipsters dark-grey">Match</div>
        </div>
        <div class="col-xs-12 no-pad panel panel-default no-border no-borderr text-center font-bold">
          <div class="col-xs-2 no-pad hm-bet365 v-align">
            <img src="<?=base_url();?>public/img/bet365.jpg" alt="Bet 365" class="img-responsive">
          </div>
          <div class="col-xs-3 no-pad tipsters border v-align h-align tc-m">
            <span class="fa fa-star orange-c"></span>
            <span class="fa fa-star d-grey"></span>
            <span class="fa fa-star d-grey"></span>
            <span class="fa fa-star d-grey"></span>
            <span class="fa fa-star d-grey"></span>
          </div>
          <div class="col-xs-7 no-pad tipsters tc-m d-grey v-align h-align no-pad">
            <div class="col-xs-2  no-pad"><img src="<?=base_url();?>public/img/home-manchester.png" alt="" class=""></div>
            <div class="col-xs-7  no-pad match">Man City<br>Man Utd</div>
            <div class="col-xs-2  no-pad"><img src="<?=base_url();?>public/img/home-manchester-united.png" alt="" class=""></div>
          </div>
        </div>

        <div class="col-xs-12 no-pad panel panel-default no-border no-borderr text-center font-bold">
          <div class="col-xs-2 no-pad hm-bet-bright v-align">
            <img src="<?=base_url();?>public/img/bet-bright.jpg" alt="Bet 365" class="img-responsive">
          </div>
          <div class="col-xs-3 no-pad tipsters border v-align h-align tc-m l-grey">
            <span class="fa fa-star orange-c"></span>
            <span class="fa fa-star orange-c"></span>
            <span class="fa fa-star d-grey"></span>
            <span class="fa fa-star d-grey"></span>
            <span class="fa fa-star d-grey"></span>
          </div>
          <div class="col-xs-7 no-pad tipsters tc-m d-grey v-align h-align no-pad l-grey">
            <div class="col-xs-2  no-pad"><img src="<?=base_url();?>public/img/burnley38x38.png" alt="" class=""></div>
            <div class="col-xs-7  no-pad match">Burnley<br>Brighton</div>
            <div class="col-xs-2  no-pad"><img src="<?=base_url();?>public/img/brighton38x38.png" alt="" class=""></div>
          </div>
        </div>

        <div class="col-xs-12 no-pad panel panel-default no-border no-borderr text-center font-bold">
          <div class="col-xs-2 no-pad hm-bet-fred v-align">
            <img src="<?=base_url();?>public/img/bet-fred.jpg" alt="Bet 365" class="img-responsive">
          </div>
          <div class="col-xs-3 no-pad tipsters border v-align h-align tc-m">
            <span class="fa fa-star orange-c"></span>
            <span class="fa fa-star orange-c"></span>
            <span class="fa fa-star orange-c"></span>
            <span class="fa fa-star orange-c"></span>
            <span class="fa fa-star orange-c"></span>
          </div>
          <div class="col-xs-7 no-pad tipsters tc-m d-grey v-align h-align no-pad">
            <div class="col-xs-2  no-pad"><img src="<?=base_url();?>public/img/leicester38x38.png" alt="" class=""></div>
            <div class="col-xs-7  no-pad match">Leicester<br>Tottenham</div>
            <div class="col-xs-2  no-pad"><img src="<?=base_url();?>public/img/tottenham38x38.png" alt="" class=""></div>
          </div>
        </div>

        <div class="col-xs-12 no-pad panel panel-default no-border no-borderr text-center font-bold">
          <div class="col-xs-2 no-pad hm-bet365 v-align">
            <img src="<?=base_url();?>public/img/bet365.jpg" alt="Bet 365" class="img-responsive">
          </div>
          <div class="col-xs-3 no-pad tipsters border v-align h-align tc-m l-grey">
            <span class="fa fa-star orange-c"></span>
            <span class="fa fa-star orange-c"></span>
            <span class="fa fa-star d-grey"></span>
            <span class="fa fa-star d-grey"></span>
            <span class="fa fa-star d-grey"></span>
          </div>
          <div class="col-xs-7 no-pad tipsters tc-m d-grey v-align h-align no-pad l-grey">
            <div class="col-xs-2  no-pad"><img src="<?=base_url();?>public/img/arsenal38x38.png" alt="" class=""></div>
            <div class="col-xs-7  no-pad match">Arsenal<br>Man City</div>
            <div class="col-xs-2  no-pad"><img src="<?=base_url();?>public/img/westbrom38x38.png" alt="" class=""></div>
          </div>
        </div>

        <div class="col-xs-12 no-pad panel panel-default no-border no-borderr text-center font-bold">
          <div class="col-xs-2 no-pad hm-bet-fred v-align">
            <img src="<?=base_url();?>public/img/bet-fred.jpg" alt="Bet 365" class="img-responsive">
          </div>
          <div class="col-xs-3 no-pad tipsters border v-align h-align tc-m">
            <span class="fa fa-star orange-c"></span>
            <span class="fa fa-star orange-c"></span>
            <span class="fa fa-star orange-c"></span>
            <span class="fa fa-star orange-c"></span>
            <span class="fa fa-star orange-c"></span>
          </div>
          <div class="col-xs-7 no-pad tipsters tc-m d-grey v-align h-align no-pad">
            <div class="col-xs-2  no-pad"><img src="<?=base_url();?>public/img/home-manchester.png" alt="" class=""></div>
            <div class="col-xs-7  no-pad match">Man City<br>Man Utd</div>
            <div class="col-xs-2  no-pad"><img src="<?=base_url();?>public/img/home-manchester-united.png" alt="" class=""></div>
          </div>
        </div>

        <div class="col-xs-12 text-center more-b no-pad">
          <a href="<?=base_url();?>Welcome/tcomparison" class="">
            <p class="hm-more no-margin">More</p>
          </a>
        </div>
      </div>
    </div>
    <br>
    <div id="side-ad" class="col-md-5 no-pad hm-side-tip">
      <a href="<?=base_url();?>Welcome/goldengoal"><img src="<?=base_url();?>public/img/home-side-ads.jpg" alt="side advert" class="no-pad no-margin img-responsive">
        </a>
    </div>

    <div class="col-xs-12 no-pad border">
      <div class="col-xs-12 no-pad">
        <div class="panel-heading uls-head text-center">Upcoming Live Streams</div>
      </div>
      <div class="uls-hm-cont">
        <div class="uls-hm-feed">
          <div class="col-xs-12 uls-hm-feed-c no-pad no-margin white-bg">
            <div class="col-xs-12 col-md-3 hPl uls-hm text-center no-pad aa h-align v-align">
              <p class="text-center no-margin">Premier League <br> <span class="lsmdate hidden-sm hidden-xs">01 Aug - 19:30</span></p>
            </div>

            <div class="col-xs-12 col-md-7 hLeague no-pad h-align v-align aa">
              <div class="col-xs-2 no-pad v-align h-align">
                <img src="<?=base_url();?>public/img/burnley38x38.png" alt="" class=" pull-right">
              </div>
              <div class="col-xs-3 no-pad match">
                <p class="no-margin text-left">Man City</p>
              </div>
              <div class="col-xs-1 no-pad match text-center ">
                <p class="no-margin">vs</p>
              </div>
              <div class="col-xs-3 no-pad match">
                <p class="no-margin text-right">Man Utd</p>
              </div>
              <div class="col-xs-2 no-pad h-align v-align">
                <img src="<?=base_url();?>public/img/burnley38x38.png" alt="" class="pull-left">
              </div>
            </div>

            <div class="hWatch col-xs-12 col-md-2 no-pad text-center h-align v-align aa">
              <a href="<?=base_url();?>Welcome/livestream"><button class="submit no-pad h-align v-align" type="button" name="submit">Watch</button></a>
            </div>
          </div>
        </div>

        <div class="uls-hm-feed">
          <div class="col-xs-12 uls-hm-feed-c no-pad no-margin wash">
            <div class="col-xs-12 col-md-3 hPl uls-hm text-center no-pad lsm-b aa h-align v-align">
              <p class="text-center no-margin">Premier League <br> <span class="lsmdate hidden-sm hidden-xs">01 Aug - 19:30</span></p>
            </div>

            <div class="col-xs-12 col-md-7 hLeague no-pad h-align v-align aa">
              <div class="col-xs-2 no-pad v-align h-align">
                <img src="<?=base_url();?>public/img/burnley38x38.png" alt="" class=" pull-right">
              </div>
              <div class="col-xs-3 no-pad match">
                <p class="no-margin text-left">Man City</p>
              </div>
              <div class="col-xs-1 no-pad match text-center ">
                <p class="no-margin">vs</p>
              </div>
              <div class="col-xs-3 no-pad match">
                <p class="no-margin text-right">Man Utd</p>
              </div>
              <div class="col-xs-2 no-pad h-align v-align">
                <img src="<?=base_url();?>public/img/burnley38x38.png" alt="" class="pull-left">
              </div>
            </div>

            <div class="hWatch col-xs-12 col-md-2 no-pad text-center h-align v-align aa">
              <a href="<?=base_url();?>Welcome/livestream"><button class="submit no-pad h-align v-align" type="button" name="submit">Watch</button></a>
            </div>
          </div>
        </div>

        <div class="uls-hm-feed">
          <div class="col-xs-12 uls-hm-feed-c no-pad no-margin white-bg">
            <div class="col-xs-12 col-md-3 hPl uls-hm text-center no-pad lsm-b aa h-align v-align">
              <p class="text-center no-margin">Premier League <br> <span class="lsmdate hidden-sm hidden-xs">01 Aug - 19:30</span></p>
            </div>

            <div class="col-xs-12 col-md-7 hLeague no-pad h-align v-align aa">
              <div class="col-xs-2 no-pad v-align h-align">
                <img src="<?=base_url();?>public/img/burnley38x38.png" alt="" class=" pull-right">
              </div>
              <div class="col-xs-3 no-pad match">
                <p class="no-margin text-left">Man City</p>
              </div>
              <div class="col-xs-1 no-pad match text-center ">
                <p class="no-margin">vs</p>
              </div>
              <div class="col-xs-3 no-pad match">
                <p class="no-margin text-right">Man Utd</p>
              </div>
              <div class="col-xs-2 no-pad h-align v-align">
                <img src="<?=base_url();?>public/img/burnley38x38.png" alt="" class="pull-left">
              </div>
            </div>

            <div class="hWatch col-xs-12 col-md-2 no-pad text-center h-align v-align aa">
              <a href="<?=base_url();?>Welcome/livestream"><button class="submit no-pad h-align v-align" type="button" name="submit">Watch</button></a>
            </div>
          </div>
        </div>

        <div class="uls-hm-feed">
          <div class="col-xs-12 uls-hm-feed-c no-pad no-margin wash">
            <div class="col-xs-12 col-md-3 hPl uls-hm text-center no-pad lsm-b aa h-align v-align">
              <p class="text-center no-margin">Premier League <br> <span class="lsmdate hidden-sm hidden-xs">01 Aug - 19:30</span></p>
            </div>

            <div class="col-xs-12 col-md-7 hLeague no-pad h-align v-align aa">
              <div class="col-xs-2 no-pad v-align h-align">
                <img src="<?=base_url();?>public/img/burnley38x38.png" alt="" class=" pull-right">
              </div>
              <div class="col-xs-3 no-pad match">
                <p class="no-margin text-left">Man City</p>
              </div>
              <div class="col-xs-1 no-pad match text-center ">
                <p class="no-margin">vs</p>
              </div>
              <div class="col-xs-3 no-pad match">
                <p class="no-margin text-right">Man Utd</p>
              </div>
              <div class="col-xs-2 no-pad h-align v-align">
                <img src="<?=base_url();?>public/img/burnley38x38.png" alt="" class="pull-left">
              </div>
            </div>

            <div class="hWatch col-xs-12 col-md-2 no-pad text-center h-align v-align aa">
              <a href="<?=base_url();?>Welcome/livestream"><button class="submit no-pad h-align v-align" type="button" name="submit">Watch</button></a>
            </div>
          </div>
        </div>

        <div class="uls-hm-feed">
          <div class="col-xs-12 uls-hm-feed-c no-pad no-margin white-bg">
            <div class="col-xs-12 col-md-3 hPl uls-hm text-center no-pad aa h-align v-align">
              <p class="text-center no-margin">Premier League <br> <span class="lsmdate hidden-sm hidden-xs">01 Aug - 19:30</span></p>
            </div>

            <div class="col-xs-12 col-md-7 hLeague no-pad h-align v-align aa">
              <div class="col-xs-2 no-pad v-align h-align">
                <img src="<?=base_url();?>public/img/burnley38x38.png" alt="" class=" pull-right">
              </div>
              <div class="col-xs-3 no-pad match">
                <p class="no-margin text-left">Man City</p>
              </div>
              <div class="col-xs-1 no-pad match text-center ">
                <p class="no-margin">vs</p>
              </div>
              <div class="col-xs-3 no-pad match">
                <p class="no-margin text-right">Man Utd</p>
              </div>
              <div class="col-xs-2 no-pad h-align v-align">
                <img src="<?=base_url();?>public/img/burnley38x38.png" alt="" class="pull-left">
              </div>
            </div>

            <div class="hWatch col-xs-12 col-md-2 no-pad text-center h-align v-align aa">
              <a href="<?=base_url();?>Welcome/livestream"><button class="submit no-pad h-align v-align" type="button" name="submit">Watch</button></a>
            </div>
          </div>
        </div>
      </div>

      <div class="col-xs-12 text-center hmore-b no-pad">
        <a href="<?=base_url();?>Welcome/tcomparison" class="">
          <p class="hm-more no-margin">More</p>
        </a>
      </div>
    </div>

  </div>

  <!--/col-md-8-->
  <!--LeftPanel-->
  <div class="clearfix hidden-lg hidden-md"></div>
  <div class="col-md-4 side-bar-h sideTitle dark-bg">
    Fixtures
  </div>
  <div id="home-panel-right" class="col-md-4 side-bar-h border pull-right">
    <div class="col-xs-12 no-pad">
      <!-- <div class="hpr-title">
          <h4><strong>Football Scores</strong></h4>
        </div> -->
      <div class="col-xs-12 page-header no-margin no-pad">
        <!-- <h5 class="side-utitle pull-left"><strong>Champions League</strong></h5> -->

        <div class="btn-group " style="padding-top:8px;">
          <button class="btn btn-default side-btn btn-xs dropdown-toggle blueGreen font-bold" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            Champions League   <span class="caret"></span>
            </button>
          <ul class="dropdown-menu f-l">
            <li><a href="<?=base_url();?>Welcome/livescore">English Premier League</a></li>
            <li><a href="<?=base_url();?>Welcome/livescore">English Championship</a></li>
            <li><a href="<?=base_url();?>Welcome/livescore">Spain La Liga</a></li>
            <li><a href="<?=base_url();?>Welcome/livescore">German Bundes Liga</a></li>
            <li><a href="<?=base_url();?>Welcome/livescore">Nations League</a></li>
            <li><a href="<?=base_url();?>Welcome/livescore">French Ligue 1</a></li>
            <li><a href="<?=base_url();?>Welcome/livescore">Italy Serie A</a></li>
            <li><a href="<?=base_url();?>Welcome/livescore">Dutch Eredivisie</a></li>
            <li><a href="<?=base_url();?>Welcome/livescore"><strong>View all leagues</strong></a></li>
          </ul>
        </div>
      </div>
    </div>

    <div id="hm-champ-lg" class="col-xs-12 container no-pad">
      <div class="row">
        <p class="champ-text uppercase wash text-center med"> WEDNESDAY 1ST AUGUST 2018</p>
        <div class="col-xs-12 no-pad col-centered text-center first-champ">
          <div class="col-xs-12 side-fs">
            <a href="<?=base_url();?>Welcome/livescore" class="no-pad">
              <div class="col-xs-5 no-pad text-right">
                <p class="fs-team">Rosenborg</p>
              </div>
              <div class="col-xs-2 no-pad text-center">
                <span class="fs-score font-bold">19:45</span>
              </div>
              <div class="col-xs-5 no-pad text-left">
                <p class="fs-team2">Rosenborg</p>
              </div>
              <div class="col-xs-12 no-pad">
                <!-- <p class="no-pad agg no-margin">(Agg 1-3)</p> -->
              </div>
            </a>
          </div>

          <div class="col-xs-12 side-fs">
            <a href="<?=base_url();?>Welcome/livescore" class="no-pad">
              <div class="col-xs-5 no-pad text-right">
                <p class="fs-team">HJK Helsinki</p>
              </div>
              <div class="col-xs-2 no-pad text-center">
                <span class="fs-score font-bold">19:45</span>
              </div>
              <div class="col-xs-5 no-pad text-left">
                <p class="fs-team2">BATE Borisov</p>
              </div>
              <div class="col-xs-12 no-pad">
                <p class="no-pad agg">(Agg 1-3)</p>
              </div>
            </a>
          </div>

          <div class="col-xs-12 side-fs">
            <a href="<?=base_url();?>Welcome/livescore" class="no-pad">
              <div class="col-xs-5 no-pad text-right">
                <p class="fs-team">Rosenborg</p>
              </div>
              <div class="col-xs-2 no-pad text-center">
                <span class="fs-score font-bold">19:45</span>
              </div>
              <div class="col-xs-5 no-pad text-left">
                <p class="fs-team2">Rosenborg</p>
              </div>
              <div class="col-xs-12 no-pad">
                <!-- <p class="no-pad agg no-margin">(Agg 1-3)</p> -->
              </div>
            </a>
          </div>

          <div class="col-xs-12 side-fs">
            <a href="<?=base_url();?>Welcome/livescore" class="no-pad">
              <div class="col-xs-5 no-pad text-right">
                <p class="fs-team">HJK Helsinki</p>
              </div>
              <div class="col-xs-2 no-pad text-center">
                <span class="fs-score font-bold">19:45</span>
              </div>
              <div class="col-xs-5 no-pad text-left">
                <p class="fs-team2">BATE Borisov</p>
              </div>
              <div class="col-xs-12 no-pad">
                <!-- <p class="no-pad agg">(Agg 1-3)</p> -->
              </div>
            </a>
          </div>

          <div class="col-xs-12 side-fs last-champ">
            <a href="<?=base_url();?>Welcome/livescore" class="no-pad">
              <div class="col-xs-5 no-pad text-right">
                <p class="fs-team font-bold">FC Midtjylland</p>
              </div>
              <div class="col-xs-2 no-pad text-center">
                <span class="fs-score font-bold">19:45</span>
              </div>
              <div class="col-xs-5 no-pad text-left">
                <p class="fs-team2 font-bold">FC Astana</p>
              </div>
              <div class="col-xs-12 no-pad">
                <p class="no-pad agg">(Agg 1-3)</p>
              </div>
            </a>
          </div>
        </div>

        <div class="col-xs-12 text-center vm-pa">
          <a href="<?=base_url();?>Welcome/livescore" class="vm" role="button">
            <p class="med no-margin">View More</p>
          </a>
        </div>
        <!-- <div class="col-xs-12 text-center more-b no-pad">
            <a href="<?=base_url();?>Welcome/tcomparison" class="">
              <p class="hm-more no-margin">More</p>
            </a>
          </div> -->
      </div>
      <!--/row-->
    </div>
    <div class="clearfix"></div>
  </div>
  <!-- /col-4-->
  <!--RightPanel-->
  <div class="clearfix hidden-lg hidden-md"></div>
  <div class="col-md-4 side-bar-h sideTitle dark-bg">
    League Table
  </div>
  <div id="side-football-tables" class="col-xs-12 col-md-4 side-bar-h border pull-right">
    <div class="col-xs-12 no-pad">
      <div class="col-xs-12 page-header no-margin no-pad">
        <!-- <h5 class="side-utitle pull-left"><strong>Scottist Prem </strong></h5> -->
        <div class="btn-group pull-left" style="padding-top:8px;">
          <button class="btn btn-default side-btn btn-xs dropdown-toggle blueGreen font-bold" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              Scottist Prem <span class="caret"></span>
            </button>
          <ul class="dropdown-menu">
            ...
          </ul>
        </div>
      </div>
    </div>
    <div id="side-table-scores" class="col-xs-12">
      <table class="table table-hover">
        <thead class="home-thead">
          <tr>
            <th> </th>
            <th>Team</th>
            <th>P</th>
            <th>GD</th>
            <th>Pts</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td class="dash" scope="row">1</td>
            <th class="dash">Aberdeen</th>
            <td class="dash">1</td>
            <td class="dash">2</td>
            <td class="table-cell-bold">3</td>
          </tr>
          <tr class="table-break">
            <td scope="row">2</td>
            <th>Aberdeen</th>
            <td>0</td>
            <td>0</td>
            <td class="table-cell-bold">0</td>
          </tr>
          <tr>
            <td scope="row">3</td>
            <th>AJJJberdeen</th>
            <td>0</td>
            <td>0</td>
            <td class="table-cell-bold">0</td>
          </tr>
          <tr class="table-break">
            <td scope="row">4</td>
            <th>Aberdeen</th>
            <td>0</td>
            <td>0</td>
            <td class="table-cell-bold">0</td>
          </tr>
          <tr>
            <td scope="row">5</td>
            <th>Aberdeen</th>
            <td>0</td>
            <td>0</td>
            <td class="table-cell-bold">0</td>
          </tr>
          <tr>
            <td scope="row">6</td>
            <th>Aberdeen</th>
            <td>0</td>
            <td>0</td>
            <td class="table-cell-bold">0</td>
          </tr>
          <tr>
            <td scope="row">7</td>
            <th>Aberdeen</th>
            <td>0</td>
            <td>0</td>
            <td class="table-cell-bold">0</td>
          </tr>
          <tr>
            <td scope="row">8</td>
            <th>Aberdeen</th>
            <td>0</td>
            <td>0</td>
            <td class="table-cell-bold">0</td>
          </tr>
          <tr class="table-break">
            <td scope="row">9</td>
            <th>Aberdeen</th>
            <td>0</td>
            <td>0</td>
            <td class="table-cell-bold">0</td>
          </tr>
          <tr>
            <td scope="row">10</td>
            <th>Aberdeen</th>
            <td>0</td>
            <td>0</td>
            <td class="table-cell-bold">0</td>
          </tr>
          <tr>
            <td scope="row">11</td>
            <th>Aberdeen</th>
            <td>0</td>
            <td>0</td>
            <td class="table-cell-bold">0</td>
          </tr>
          <tr>
            <td scope="row">12</td>
            <th>Aberdeen</th>
            <td>0</td>
            <td>0</td>
            <td class="table-cell-bold">0</td>
          </tr>
          <tr>
            <td scope="row">13</td>
            <th>Aberdeen</th>
            <td>0</td>
            <td>0</td>
            <td class="table-cell-bold">0</td>
          </tr>
          <tr>
            <td scope="row">14</td>
            <th>Aberdeen</th>
            <td>0</td>
            <td>0</td>
            <td class="table-cell-bold">0</td>
          </tr>
          <tr>
            <td scope="row">15</td>
            <th>Aberdeen</th>
            <td>0</td>
            <td>0</td>
            <td class="table-cell-bold">0</td>
          </tr>
          <tr>
            <td scope="row">16</td>
            <th>Aberdeen</th>
            <td>0</td>
            <td>0</td>
            <td class="table-cell-bold">0</td>
          </tr>
          <tr>
            <td scope="row">17</td>
            <th>Aberdeen</th>
            <td>0</td>
            <td>0</td>
            <td class="table-cell-bold">0</td>
          </tr>
          <tr>
            <td scope="row">18</td>
            <th>Aberdeen</th>
            <td>0</td>
            <td>0</td>
            <td class="table-cell-bold">0</td>
          </tr>
          <tr>
            <td scope="row">19</td>
            <th>Aberdeen</th>
            <td>0</td>
            <td>0</td>
            <td class="table-cell-bold">0</td>
          </tr>
          <tr>
            <td scope="row">20</td>
            <th>Aberdeen</th>
            <td>0</td>
            <td>0</td>
            <td class="table-cell-bold">0</td>
          </tr>
        </tbody>
      </table>
    </div>
  </div>
</div>
