<style type="text/css">
	#secMenu li:nth-of-type(4) a{background: #0e6857 !important;}
</style>
<div id="ftp-carousel" class="carousel slide" data-ride="carousel">
  <!-- Wrapper for slides -->
  <div class="carousel-inner" role="listbox">
    <div class="item active">
      <img src="<?=base_url();?>public/img/ftpbanner.jpg" alt="...">
    </div>
    <div class="item">
      <img src="<?=base_url();?>public/img/home-banner.jpg" alt="...">
    </div>
  </div>
</div>
<div id="ftpmainpage" class="width100floatleft">
	<!-- <div class="ftpbancont">
	</div> -->
	<div class="width100floatleft">
	<div class="size1050px">
		<h1>GAME & COMPETITIONS</h1>
		<p>Play in our games and stand to win prizes every week and points to the season long leaderboard!</p>

		<div class="instra">
			<img src="<?=base_url();?>public/img/instra.png">
		</div>

		<a href="<?=base_url();?>Welcome/ftpperfect10">
			<div id="ftpopt1" class="ftpopts">
				<img src="<?=base_url();?>public/img/ptban1.jpg">
			</div>
		</a>
		<a href="<?=base_url();?>Welcome/goldengoal">
			<div id="ftpopt2" class="ftpopts">
				<img src="<?=base_url();?>public/img/ptban2.jpg">
			</div>
		</a>
		<div id="ftprankings">
			<span>COMPETITION RANKINGS</span>
			<table>
				<tbody>
					<tr>
						<th>Rank</th>
						<th>Points</th>
						<th>Username</th>
						<th>Prize</th>
					</tr>
					<tr>
						<td>1st</td>
						<td>123</td>
						<td>tran ng</td>
						<td>SGD 15,000</td>
					</tr>
					<tr>
						<td>2nd</td>
						<td>123</td>
						<td>tran ng</td>
						<td>SGD 15,000</td>
					</tr>
					<tr>
						<td>3rd</td>
						<td>123</td>
						<td>tran ng</td>
						<td>SGD 15,000</td>
					</tr>
					<tr>
						<td>4th</td>
						<td>123</td>
						<td>tran ng</td>
						<td>SGD 15,000</td>
					</tr>
					<tr>
						<td>5th</td>
						<td>123</td>
						<td>tran ng</td>
						<td>SGD 15,000</td>
					</tr>
					<tr>
						<td>6th</td>
						<td>123</td>
						<td>tran ng</td>
						<td>SGD 15,000</td>
					</tr>
					<tr>
						<td>7st</td>
						<td>123</td>
						<td>tran ng</td>
						<td>SGD 15,000</td>
					</tr>
					<tr>
						<td>8st</td>
						<td>123</td>
						<td>tran ng</td>
						<td>SGD 15,000</td>
					</tr>
					<tr>
						<td>9st</td>
						<td>123</td>
						<td>tran ng</td>
						<td>SGD 15,000</td>
					</tr>
					<tr>
						<td>10st</td>
						<td>123</td>
						<td>tran ng</td>
						<td>SGD 15,000</td>
					</tr>
					<tr>
						<td colspan="4"><a class="linkprop" href="<?=base_url();?>Welcome/seasonalLeaderboard">more info</a></td>
					</tr>
				</tbody>
			</table>
		</div>
	</div>
	</div>
</div>
