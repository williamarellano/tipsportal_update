<style type="text/css">
	#secMenu li:nth-of-type(2) a{background: #0e6857 !important;}
</style>
<div id="tipscomparison">
		<div id="tccontainer2">
		<div class="tccontainercont">
			<h3>Tipsters Comparison</h3>
			<p class="med">Bringing the best tips on the internet together for your convenience!</p>

			<p class="tiplabel">Last week tips</p>
			<div class="w100fl">
				<table class="tctable1">
				<tbody>
					<tr>
						<th>Tipster</th>
						<th>Tip Form</th>
						<th>Match</th>
						<th>Tip</th>
						<th>Odds</th>
						<th></th>
					</tr>
					<!-- <tr>
						<td><img src="<?=base_url();?>public/img/tc-bet365.jpg"></td>
						<td><img src="<?=base_url();?>public/img/star1.png"></td>
						<td>
							<img src="<?=base_url();?>public/img/mancity.png">
							<p>Manchester City
							Manchester United</p>
							<img src="<?=base_url();?>public/img/ls-manchester-united.png">
						</td>
						<td>Over 2.5 Goals</td>
						<td>0.95</td>
						<td><button>View More</button></td>
					</tr> -->
					<?php
						foreach ($tips as $t) {
					?>
					<tr>
						<td><img src="<?=base_url().$this->config->item('upload_tipsters_logo').'/'.$t['tipster_logo'];?>"></td>
						<td><img src="<?=base_url().$this->config->item('star_img').'/star'.$t['tip_form'];?>.png"></td>
						<td>
							<img src="<?=base_url().$this->config->item('upload_teams_logo').'/'.$t['home_team_logo'];?>">
							<p><?=$t['home_team_nickname'];?>
								<br>
							<?=$t['away_team_nickname'];?></p>
							<img src="<?=base_url().$this->config->item('upload_teams_logo').'/'.$t['away_team_logo'];?>">
						</td>
						<td><?=$t['tip_main'];?></td>
						<td><?=$t['odds'];?></td>
						<td><a href="<?=$t['link'];?>" target="_blank"><button>View More</button></a></td>
					</tr>
					<?php
						}
					?>

					
				</tbody>
				</table>
			</div>


			<p class="tiplabel">The week before last week tips</p>
			
			<div class="w100fl">
				<table class="tctable1" id="tipsters_table">
					<tbody>
						<tr>
							<th>Tipster</th>
							<th>Tip Form</th>
							<th>Match</th>
							<th>Tip</th>
							<th>Odds</th>
							<th></th>
						</tr>
						<!-- <tr>
							<td><img src="<?=base_url();?>public/img/tc-bet365.jpg"></td>
							<td><img src="<?=base_url();?>public/img/star1.png"></td>
							<td>
								<img src="<?=base_url();?>public/img/mancity.png">
								<p>Manchester City
								Manchester United</p>
								<img src="<?=base_url();?>public/img/ls-manchester-united.png">
							</td>
							<td>Over 2.5 Goals</td>
							<td>0.95</td>
							<td><button>View More</button></td>
						</tr> -->
						
					</tbody>
				</table>
			</div>

			<!-- <div class="tctickers" >
				<ul>
					<li><</li>
					<li class="tctickers-active">1</li>
					<li>2</li>
					<li>3</li>
					<li>4</li>
					<li>5</li>
					<li>6</li>
					<li>></li>
				</ul>
			</div> -->

			<div class="tctickers" id="pagination_link">
				<!-- <ul>
					<li><</li>
					<li class="tctickers-active">1</li>
					<li>2</li>
					<li>3</li>
					<li>4</li>
					<li>5</li>
					<li>6</li>
					<li>></li>
				</ul> -->
			</div>

			<!-- <div align="right" id="pagination_link"></div> -->
			
			<center>
			<a href="<?=base_url();?>Welcome/tcomparison"><button class="tcvpt">View  Latest Tips</button></a>
			</center>
			<div class="sideshit">
				<p><b>Football Betting Tips and Advice for successful football betting</b></p>
				<p>re you a football fan? Would you like to win more of your football bets? Discover the essential factors to bear in mind before making your football forecasts. </p>
				<br>
				<p>Whether you do the Football Pools or bet through a bookmaker, the following football betting tips are designed to help you improve your football results forecasts.</p>
				<br>
				<p><b>Tip 1: Consider the motivation and team spirit</b></p>
				<p>Motivation has a major influence on a football club's sporting performances. The consequences of a particular encounter (league title, Champions League qualification, relegation, etc.) and the size of the match win bonus are key motivating factors for a football team.</p>
			</div>
		</div>
	</div>
	</div>

	<?php $this->load->view('template/footer');?>

<script>

$(document).ready(function() {
// alert('asd');
var html;
load_data(1);

$(document).on("click", ".pagination li a", function(event){
  event.preventDefault();
  var page = $(this).data("ci-pagination-page");
  load_data(page);
 });

});

function load_data(page)
{
	var html = '';
	$.ajax({
	url:"<?php echo base_url(); ?>Welcome/lastlastWeekTips/"+page,
	method:"GET",
	dataType:"json",
	success:function(data)
	{
		var dtlength = data.tipsters_table.length;
		var dt = data.tipsters_table;
		// console.log(data.tipsters_table.length);
		for(var i=0; i<dtlength; i++)
		{
			console.log(dt[i].tipster_logo);
			html += '<tr>';
			html += '<td><img src="<?=base_url().$this->config->item('upload_tipsters_logo').'/';?>'+dt[i].tipster_logo+'"></td>';
			html += '<td><img src="<?=base_url().$this->config->item('star_img').'/star'?>'+dt[i].tip_form+'.png"></td>';
			html += '<td>';
			html += '<img src="<?=base_url().$this->config->item('upload_teams_logo').'/';?>'+dt[i].home_team_logo+'">';
			html += '<p>'+dt[i].home_team_nickname+'<br>';
			html += dt[i].home_team_nickname+'</p>';
			html += '<img src="<?=base_url().$this->config->item('upload_teams_logo').'/';?>'+dt[i].away_team_logo+'">';
			html += '</td>';
			html += '<td>'+ dt[i].tip_main +'</td>';
			html += '<td>'+ dt[i].odds +'</td>';
			html += '<td><a href="'+ dt[i].link +'" target="_blank"><button>View More</button></a></td>';
			html += '</tr>';
		}

	$('#tipsters_table').html(html);
	$('#pagination_link').html(data.pagination_link);
	}
	});
}


</script>
