<div id="memberProfile" class="container-fluid ">
  <div class="container liveStream no-pad ">
    <div class="col-md-8 no-pad left-bar text-center">
      <h1>Member Profile</h1>

      <div id="accts" class="col-md-12 text-left white-bg">
          <div class="col-md-12">
              <h3>Account Settings</h3>
              <form class="form-horizontal">
                <div class="form-group">
                  <label for="uname" class="col-sm-2 control-label">Username</label>
                  <div class="col-sm-10">
                    <input type="email" class="form-control" id="uname" placeholder="abcd" disabled>
                  </div>
                </div>
                <div class="form-group">
                  <label for="cpw" class="col-sm-2 control-label">Current Password</label>
                  <div class="col-sm-10">
                    <input type="password" class="form-control" id="cpw" placeholder="•••••••••">
                  </div>
                </div>
                <div class="form-group">
                  <label for="npw" class="col-sm-2 control-label">New Password</label>
                  <div class="col-sm-10">
                    <input type="password" class="form-control" id="npw" placeholder="•••••••••">
                  </div>
                </div>
                <div class="form-group">
                  <label for="confirmpw" class="col-sm-2 control-label">Confirm Password</label>
                  <div class="col-sm-10">
                    <input type="password" class="form-control" id="confirmpw" placeholder="•••••••••">
                  </div>
                </div>

                <div class="form-group">
                  <div class="col-sm-offset-2 col-sm-10">
                    <button type="submit" class="btn btn-default">Save</button>
                  </div>
                </div>
              </form>
          </div>
      </div>

      <div id="" class="col-md-12 text-left white-bg">
          <div class="col-md-12">
              <h3>Other Information</h3>
              <form class="form-horizontal">
                <div class="form-group">
                  <label for="uname" class="col-sm-2 control-label">Full Name</label>
                  <div class="col-sm-10">
                    <input type="email" class="form-control" id="uname" placeholder="Rain Lai">
                  </div>
                </div>
                <div class="form-group">
                  <label for="cpw" class="col-sm-2 control-label">Email</label>
                  <div class="col-sm-10">
                    <input type="password" class="form-control" id="cpw" placeholder="abc@gmail.com">
                  </div>
                </div>
                <div class="form-group">
                  <label for="npw" class="col-sm-2 control-label">Phone</label>
                  <div class="col-sm-10">
                    <input type="number" class="form-control" id="npw" placeholder="123456">
                  </div>
                </div>

                <div class="form-group">
                  <div class="col-sm-offset-2 col-sm-10">
                    <button type="submit" class="btn btn-default">Save</button>
                  </div>
                </div>
              </form>
          </div>
      </div>
      <!--/ls-feed-->
    </div>
    <!--/col-md-8 Left Panel-->
    <div class="clearfix hidden-lg hidden-md"></div>
    <!--======Right Panel=========-->
    <div class="col-md-4 side-bar sideTitle no-pad v-align h-align pull-right">
      <!-- <h1 class="no-margin">Hello</h1> -->
      <div class="hidden-sm hidden-xs">
        <div class="col-md-12 premiver ls-ban">
          <div class="thumbnail text-center ls-thumb">
            <div class="caption">
              <div class="col-xs-8 ls-bt light-grey no-pad col-centered">
                <p>07/25 &#9679; 01:30 AM</p>
              </div>
              <h4 class=" font-bold">PREMIER LEAGUE <br><span class="ls-bb">Betting Tips</span></h4>
              <div class="col-xs-12 h-align v-align">
                <div class="col-xs-3 no-pad">
                  <img src="<?=base_url();?>public/img/arsenal.png" alt="">
                </div>
                <div class="col-xs-6 no-pad">
                  <p class="vs">Arsenal - Chelsea</p>
                </div>
                <div class="col-xs-3 no-pad">
                  <img src="<?=base_url();?>public/img/chelsea.png" alt="">
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="col-md-12 no-pad ls-ban">
          <a href="#">
          <img src="<?=base_url();?>public/img/ls-wp10.jpg" alt="Weekly Perfect 10">
        </a>
        </div>

        <div class="col-md-12 no-pad ls-ban">
          <a href="#">
          <img src="<?=base_url();?>public/img/ls-gg.jpg" alt="The Golden Goal">
        </a>
        </div>



      </div>
    </div>
  </div>
  <!--/container-->
</div>
<!--/liveStream-->
