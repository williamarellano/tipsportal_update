<div id="liveStream" class="container-fluid no-pad ">
  <div class="container liveStream no-pad">
    <div class="col-md-8 no-pad left-bar text-center">
      <h1>Live Stream</h1>
      <h3>List of upcoming live matches we are featuring</h3>

      <div id="ls-feed" class="col-md-12 no-pad">
        <div class="lsTitle">
          <div class="text-center">Monday, August 20</div>
        </div>
        <div class="uls-hm-cont">
          <div class="uls-hm-feed">
            <div class="col-xs-12 uls-hm-feed-c no-pad no-margin white-bg">
              <div class="col-xs-12 col-md-3 hPl uls-hm text-center no-pad aa h-align v-align">
                <p class="text-center no-margin">Premier League <br> <span class="lsmdate hidden-sm hidden-xs">19:30</span></p>
              </div>

              <div class="col-xs-12 col-md-7 hLeague no-pad h-align v-align aa">
                <div class="col-xs-2 no-pad v-align h-align">
                  <img src="<?=base_url();?>public/img/burnley38x38.png" alt="" class=" pull-right">
                </div>
                <div class="col-xs-3 no-pad match">
                  <p class="no-margin text-left">Man City</p>
                </div>
                <div class="col-xs-1 no-pad match text-center ">
                  <p class="no-margin">vs</p>
                </div>
                <div class="col-xs-3 no-pad match">
                  <p class="no-margin text-right">Man Utd</p>
                </div>
                <div class="col-xs-2 no-pad h-align v-align">
                  <img src="<?=base_url();?>public/img/burnley38x38.png" alt="" class="pull-left">
                </div>
              </div>

              <div class="hWatch col-xs-12 col-md-2 no-pad text-center h-align v-align aa">
                <a href="<?=base_url();?>Welcome/livestream"><button class="submit no-pad h-align v-align" type="button" name="submit">Watch</button></a>
              </div>
            </div>
          </div>

          <div class="uls-hm-feed">
            <div class="col-xs-12 uls-hm-feed-c no-pad no-margin wash">
              <div class="col-xs-12 col-md-3 hPl uls-hm text-center no-pad lsm-b aa h-align v-align">
                <p class="text-center no-margin">Premier League <br> <span class="lsmdate hidden-sm hidden-xs">19:30</span></p>
              </div>

              <div class="col-xs-12 col-md-7 hLeague no-pad h-align v-align aa">
                <div class="col-xs-2 no-pad v-align h-align">
                  <img src="<?=base_url();?>public/img/burnley38x38.png" alt="" class=" pull-right">
                </div>
                <div class="col-xs-3 no-pad match">
                  <p class="no-margin text-left">Man City</p>
                </div>
                <div class="col-xs-1 no-pad match text-center ">
                  <p class="no-margin">vs</p>
                </div>
                <div class="col-xs-3 no-pad match">
                  <p class="no-margin text-right">Man Utd</p>
                </div>
                <div class="col-xs-2 no-pad h-align v-align">
                  <img src="<?=base_url();?>public/img/burnley38x38.png" alt="" class="pull-left">
                </div>
              </div>

              <div class="hWatch col-xs-12 col-md-2 no-pad text-center h-align v-align aa">
                <a href="<?=base_url();?>Welcome/livestream"><button class="submit no-pad h-align v-align" type="button" name="submit">Watch</button></a>
              </div>
            </div>
          </div>

          <div class="uls-hm-feed">
            <div class="col-xs-12 uls-hm-feed-c no-pad no-margin white-bg">
              <div class="col-xs-12 col-md-3 hPl uls-hm text-center no-pad lsm-b aa h-align v-align">
                <p class="text-center no-margin">Premier League <br> <span class="lsmdate hidden-sm hidden-xs">19:30</span></p>
              </div>

              <div class="col-xs-12 col-md-7 hLeague no-pad h-align v-align aa">
                <div class="col-xs-2 no-pad v-align h-align">
                  <img src="<?=base_url();?>public/img/burnley38x38.png" alt="" class=" pull-right">
                </div>
                <div class="col-xs-3 no-pad match">
                  <p class="no-margin text-left">Man City</p>
                </div>
                <div class="col-xs-1 no-pad match text-center ">
                  <p class="no-margin">vs</p>
                </div>
                <div class="col-xs-3 no-pad match">
                  <p class="no-margin text-right">Man Utd</p>
                </div>
                <div class="col-xs-2 no-pad h-align v-align">
                  <img src="<?=base_url();?>public/img/burnley38x38.png" alt="" class="pull-left">
                </div>
              </div>

              <div class="hWatch col-xs-12 col-md-2 no-pad text-center h-align v-align aa">
                <a href="<?=base_url();?>Welcome/livestream"><button class="submit no-pad h-align v-align" type="button" name="submit">Watch</button></a>
              </div>
            </div>
          </div>

          <div class="uls-hm-feed">
            <div class="col-xs-12 uls-hm-feed-c no-pad no-margin wash">
              <div class="col-xs-12 col-md-3 hPl uls-hm text-center no-pad lsm-b aa h-align v-align">
                <p class="text-center no-margin">Premier League <br> <span class="lsmdate hidden-sm hidden-xs">19:30</span></p>
              </div>

              <div class="col-xs-12 col-md-7 hLeague no-pad h-align v-align aa">
                <div class="col-xs-2 no-pad v-align h-align">
                  <img src="<?=base_url();?>public/img/burnley38x38.png" alt="" class=" pull-right">
                </div>
                <div class="col-xs-3 no-pad match">
                  <p class="no-margin text-left">Man City</p>
                </div>
                <div class="col-xs-1 no-pad match text-center ">
                  <p class="no-margin">vs</p>
                </div>
                <div class="col-xs-3 no-pad match">
                  <p class="no-margin text-right">Man Utd</p>
                </div>
                <div class="col-xs-2 no-pad h-align v-align">
                  <img src="<?=base_url();?>public/img/burnley38x38.png" alt="" class="pull-left">
                </div>
              </div>

              <div class="hWatch col-xs-12 col-md-2 no-pad text-center h-align v-align aa">
                <a href="<?=base_url();?>Welcome/livestream"><button class="submit no-pad h-align v-align" type="button" name="submit">Watch</button></a>
              </div>
            </div>
          </div>

        </div>
        <div class="clearfix"></div>
        <div class="lsTitle">
          <div class="text-center">Monday, August 20</div>
        </div>
        <div class="uls-hm-cont">
          <div class="uls-hm-feed">
            <div class="col-xs-12 uls-hm-feed-c no-pad no-margin white-bg">
              <div class="col-xs-12 col-md-3 hPl uls-hm text-center no-pad aa h-align v-align">
                <p class="text-center no-margin">Premier League <br> <span class="lsmdate hidden-sm hidden-xs">19:30</span></p>
              </div>

              <div class="col-xs-12 col-md-7 hLeague no-pad h-align v-align aa">
                <div class="col-xs-2 no-pad v-align h-align">
                  <img src="<?=base_url();?>public/img/burnley38x38.png" alt="" class=" pull-right">
                </div>
                <div class="col-xs-3 no-pad match">
                  <p class="no-margin text-left">Man City</p>
                </div>
                <div class="col-xs-1 no-pad match text-center ">
                  <p class="no-margin">vs</p>
                </div>
                <div class="col-xs-3 no-pad match">
                  <p class="no-margin text-right">Man Utd</p>
                </div>
                <div class="col-xs-2 no-pad h-align v-align">
                  <img src="<?=base_url();?>public/img/burnley38x38.png" alt="" class="pull-left">
                </div>
              </div>

              <div class="hWatch col-xs-12 col-md-2 no-pad text-center h-align v-align aa">
                <a href="<?=base_url();?>Welcome/livestream"><button class="submit no-pad h-align v-align" type="button" name="submit">Watch</button></a>
              </div>
            </div>
          </div>

          <div class="uls-hm-feed">
            <div class="col-xs-12 uls-hm-feed-c no-pad no-margin wash">
              <div class="col-xs-12 col-md-3 hPl uls-hm text-center no-pad lsm-b aa h-align v-align">
                <p class="text-center no-margin">Premier League <br> <span class="lsmdate hidden-sm hidden-xs">19:30</span></p>
              </div>

              <div class="col-xs-12 col-md-7 hLeague no-pad h-align v-align aa">
                <div class="col-xs-2 no-pad v-align h-align">
                  <img src="<?=base_url();?>public/img/burnley38x38.png" alt="" class=" pull-right">
                </div>
                <div class="col-xs-3 no-pad match">
                  <p class="no-margin text-left">Man City</p>
                </div>
                <div class="col-xs-1 no-pad match text-center ">
                  <p class="no-margin">vs</p>
                </div>
                <div class="col-xs-3 no-pad match">
                  <p class="no-margin text-right">Man Utd</p>
                </div>
                <div class="col-xs-2 no-pad h-align v-align">
                  <img src="<?=base_url();?>public/img/burnley38x38.png" alt="" class="pull-left">
                </div>
              </div>

              <div class="hWatch col-xs-12 col-md-2 no-pad text-center h-align v-align aa">
                <a href="<?=base_url();?>Welcome/livestream"><button class="submit no-pad h-align v-align" type="button" name="submit">Watch</button></a>
              </div>
            </div>
          </div>

          <div class="uls-hm-feed">
            <div class="col-xs-12 uls-hm-feed-c no-pad no-margin white-bg">
              <div class="col-xs-12 col-md-3 hPl uls-hm text-center no-pad lsm-b aa h-align v-align">
                <p class="text-center no-margin">Premier League <br> <span class="lsmdate hidden-sm hidden-xs">19:30</span></p>
              </div>

              <div class="col-xs-12 col-md-7 hLeague no-pad h-align v-align aa">
                <div class="col-xs-2 no-pad v-align h-align">
                  <img src="<?=base_url();?>public/img/burnley38x38.png" alt="" class=" pull-right">
                </div>
                <div class="col-xs-3 no-pad match">
                  <p class="no-margin text-left">Man City</p>
                </div>
                <div class="col-xs-1 no-pad match text-center ">
                  <p class="no-margin">vs</p>
                </div>
                <div class="col-xs-3 no-pad match">
                  <p class="no-margin text-right">Man Utd</p>
                </div>
                <div class="col-xs-2 no-pad h-align v-align">
                  <img src="<?=base_url();?>public/img/burnley38x38.png" alt="" class="pull-left">
                </div>
              </div>

              <div class="hWatch col-xs-12 col-md-2 no-pad text-center h-align v-align aa">
                <a href="<?=base_url();?>Welcome/livestream"><button class="submit no-pad h-align v-align" type="button" name="submit">Watch</button></a>
              </div>
            </div>
          </div>

          <div class="uls-hm-feed">
            <div class="col-xs-12 uls-hm-feed-c no-pad no-margin wash">
              <div class="col-xs-12 col-md-3 hPl uls-hm text-center no-pad lsm-b aa h-align v-align">
                <p class="text-center no-margin">Premier League <br> <span class="lsmdate hidden-sm hidden-xs">19:30</span></p>
              </div>

              <div class="col-xs-12 col-md-7 hLeague no-pad h-align v-align aa">
                <div class="col-xs-2 no-pad v-align h-align">
                  <img src="<?=base_url();?>public/img/burnley38x38.png" alt="" class=" pull-right">
                </div>
                <div class="col-xs-3 no-pad match">
                  <p class="no-margin text-left">Man City</p>
                </div>
                <div class="col-xs-1 no-pad match text-center ">
                  <p class="no-margin">vs</p>
                </div>
                <div class="col-xs-3 no-pad match">
                  <p class="no-margin text-right">Man Utd</p>
                </div>
                <div class="col-xs-2 no-pad h-align v-align">
                  <img src="<?=base_url();?>public/img/burnley38x38.png" alt="" class="pull-left">
                </div>
              </div>

              <div class="hWatch col-xs-12 col-md-2 no-pad text-center h-align v-align aa">
                <a href="<?=base_url();?>Welcome/livestream"><button class="submit no-pad h-align v-align" type="button" name="submit">Watch</button></a>
              </div>
            </div>
          </div>

          <div class="uls-hm-feed">
            <div class="col-xs-12 uls-hm-feed-c no-pad no-margin white-bg">
              <div class="col-xs-12 col-md-3 hPl uls-hm text-center no-pad aa h-align v-align">
                <p class="text-center no-margin">Premier League <br> <span class="lsmdate hidden-sm hidden-xs">19:30</span></p>
              </div>

              <div class="col-xs-12 col-md-7 hLeague no-pad h-align v-align aa">
                <div class="col-xs-2 no-pad v-align h-align">
                  <img src="<?=base_url();?>public/img/burnley38x38.png" alt="" class=" pull-right">
                </div>
                <div class="col-xs-3 no-pad match">
                  <p class="no-margin text-left">Man City</p>
                </div>
                <div class="col-xs-1 no-pad match text-center ">
                  <p class="no-margin">vs</p>
                </div>
                <div class="col-xs-3 no-pad match">
                  <p class="no-margin text-right">Man Utd</p>
                </div>
                <div class="col-xs-2 no-pad h-align v-align">
                  <img src="<?=base_url();?>public/img/burnley38x38.png" alt="" class="pull-left">
                </div>
              </div>

              <div class="hWatch col-xs-12 col-md-2 no-pad text-center h-align v-align aa">
                <a href="<?=base_url();?>Welcome/livestream"><button class="submit no-pad h-align v-align" type="button" name="submit">Watch</button></a>
              </div>
            </div>
          </div>

          <div class="uls-hm-feed">
            <div class="col-xs-12 uls-hm-feed-c no-pad no-margin wash">
              <div class="col-xs-12 col-md-3 hPl uls-hm text-center no-pad lsm-b aa h-align v-align">
                <p class="text-center no-margin">Premier League <br> <span class="lsmdate hidden-sm hidden-xs">19:30</span></p>
              </div>

              <div class="col-xs-12 col-md-7 hLeague no-pad h-align v-align aa">
                <div class="col-xs-2 no-pad v-align h-align">
                  <img src="<?=base_url();?>public/img/burnley38x38.png" alt="" class=" pull-right">
                </div>
                <div class="col-xs-3 no-pad match">
                  <p class="no-margin text-left">Man City</p>
                </div>
                <div class="col-xs-1 no-pad match text-center ">
                  <p class="no-margin">vs</p>
                </div>
                <div class="col-xs-3 no-pad match">
                  <p class="no-margin text-right">Man Utd</p>
                </div>
                <div class="col-xs-2 no-pad h-align v-align">
                  <img src="<?=base_url();?>public/img/burnley38x38.png" alt="" class="pull-left">
                </div>
              </div>

              <div class="hWatch col-xs-12 col-md-2 no-pad text-center h-align v-align aa">
                <a href="<?=base_url();?>Welcome/livestream"><button class="submit no-pad h-align v-align" type="button" name="submit">Watch</button></a>
              </div>
            </div>
          </div>
        </div>
        <div class="clearfix"></div>
        <div class="lsTitle">
          <div class="text-center">Monday, August 20</div>
        </div>
        <div class="uls-hm-cont">
          <div class="uls-hm-feed">
            <div class="col-xs-12 uls-hm-feed-c no-pad no-margin white-bg">
              <div class="col-xs-12 col-md-3 hPl uls-hm text-center no-pad aa h-align v-align">
                <p class="text-center no-margin">Premier League <br> <span class="lsmdate hidden-sm hidden-xs">19:30</span></p>
              </div>

              <div class="col-xs-12 col-md-7 hLeague no-pad h-align v-align aa">
                <div class="col-xs-2 no-pad v-align h-align">
                  <img src="<?=base_url();?>public/img/burnley38x38.png" alt="" class=" pull-right">
                </div>
                <div class="col-xs-3 no-pad match">
                  <p class="no-margin text-left">Man City</p>
                </div>
                <div class="col-xs-1 no-pad match text-center ">
                  <p class="no-margin">vs</p>
                </div>
                <div class="col-xs-3 no-pad match">
                  <p class="no-margin text-right">Man Utd</p>
                </div>
                <div class="col-xs-2 no-pad h-align v-align">
                  <img src="<?=base_url();?>public/img/burnley38x38.png" alt="" class="pull-left">
                </div>
              </div>

              <div class="hWatch col-xs-12 col-md-2 no-pad text-center h-align v-align aa">
                <a href="<?=base_url();?>Welcome/livestream"><button class="submit no-pad h-align v-align" type="button" name="submit">Watch</button></a>
              </div>
            </div>
          </div>

          <div class="uls-hm-feed">
            <div class="col-xs-12 uls-hm-feed-c no-pad no-margin wash">
              <div class="col-xs-12 col-md-3 hPl uls-hm text-center no-pad lsm-b aa h-align v-align">
                <p class="text-center no-margin">Premier League <br> <span class="lsmdate hidden-sm hidden-xs">19:30</span></p>
              </div>

              <div class="col-xs-12 col-md-7 hLeague no-pad h-align v-align aa">
                <div class="col-xs-2 no-pad v-align h-align">
                  <img src="<?=base_url();?>public/img/burnley38x38.png" alt="" class=" pull-right">
                </div>
                <div class="col-xs-3 no-pad match">
                  <p class="no-margin text-left">Man City</p>
                </div>
                <div class="col-xs-1 no-pad match text-center ">
                  <p class="no-margin">vs</p>
                </div>
                <div class="col-xs-3 no-pad match">
                  <p class="no-margin text-right">Man Utd</p>
                </div>
                <div class="col-xs-2 no-pad h-align v-align">
                  <img src="<?=base_url();?>public/img/burnley38x38.png" alt="" class="pull-left">
                </div>
              </div>

              <div class="hWatch col-xs-12 col-md-2 no-pad text-center h-align v-align aa">
                <a href="<?=base_url();?>Welcome/livestream"><button class="submit no-pad h-align v-align" type="button" name="submit">Watch</button></a>
              </div>
            </div>
          </div>

          <div class="uls-hm-feed">
            <div class="col-xs-12 uls-hm-feed-c no-pad no-margin white-bg">
              <div class="col-xs-12 col-md-3 hPl uls-hm text-center no-pad lsm-b aa h-align v-align">
                <p class="text-center no-margin">Premier League <br> <span class="lsmdate hidden-sm hidden-xs">19:30</span></p>
              </div>

              <div class="col-xs-12 col-md-7 hLeague no-pad h-align v-align aa">
                <div class="col-xs-2 no-pad v-align h-align">
                  <img src="<?=base_url();?>public/img/burnley38x38.png" alt="" class=" pull-right">
                </div>
                <div class="col-xs-3 no-pad match">
                  <p class="no-margin text-left">Man City</p>
                </div>
                <div class="col-xs-1 no-pad match text-center ">
                  <p class="no-margin">vs</p>
                </div>
                <div class="col-xs-3 no-pad match">
                  <p class="no-margin text-right">Man Utd</p>
                </div>
                <div class="col-xs-2 no-pad h-align v-align">
                  <img src="<?=base_url();?>public/img/burnley38x38.png" alt="" class="pull-left">
                </div>
              </div>

              <div class="hWatch col-xs-12 col-md-2 no-pad text-center h-align v-align aa">
                <a href="<?=base_url();?>Welcome/livestream"><button class="submit no-pad h-align v-align" type="button" name="submit">Watch</button></a>
              </div>
            </div>
          </div>

          <div class="uls-hm-feed">
            <div class="col-xs-12 uls-hm-feed-c no-pad no-margin wash">
              <div class="col-xs-12 col-md-3 hPl uls-hm text-center no-pad lsm-b aa h-align v-align">
                <p class="text-center no-margin">Premier League <br> <span class="lsmdate hidden-sm hidden-xs">19:30</span></p>
              </div>

              <div class="col-xs-12 col-md-7 hLeague no-pad h-align v-align aa">
                <div class="col-xs-2 no-pad v-align h-align">
                  <img src="<?=base_url();?>public/img/burnley38x38.png" alt="" class=" pull-right">
                </div>
                <div class="col-xs-3 no-pad match">
                  <p class="no-margin text-left">Man City</p>
                </div>
                <div class="col-xs-1 no-pad match text-center ">
                  <p class="no-margin">vs</p>
                </div>
                <div class="col-xs-3 no-pad match">
                  <p class="no-margin text-right">Man Utd</p>
                </div>
                <div class="col-xs-2 no-pad h-align v-align">
                  <img src="<?=base_url();?>public/img/burnley38x38.png" alt="" class="pull-left">
                </div>
              </div>

              <div class="hWatch col-xs-12 col-md-2 no-pad text-center h-align v-align aa">
                <a href="<?=base_url();?>Welcome/livestream"><button class="submit no-pad h-align v-align" type="button" name="submit">Watch</button></a>
              </div>
            </div>
          </div>

          <div class="uls-hm-feed">
            <div class="col-xs-12 uls-hm-feed-c no-pad no-margin white-bg">
              <div class="col-xs-12 col-md-3 hPl uls-hm text-center no-pad lsm-b aa h-align v-align">
                <p class="text-center no-margin">Premier League <br> <span class="lsmdate hidden-sm hidden-xs">19:30</span></p>
              </div>

              <div class="col-xs-12 col-md-7 hLeague no-pad h-align v-align aa">
                <div class="col-xs-2 no-pad v-align h-align">
                  <img src="<?=base_url();?>public/img/burnley38x38.png" alt="" class=" pull-right">
                </div>
                <div class="col-xs-3 no-pad match">
                  <p class="no-margin text-left">Man City</p>
                </div>
                <div class="col-xs-1 no-pad match text-center ">
                  <p class="no-margin">vs</p>
                </div>
                <div class="col-xs-3 no-pad match">
                  <p class="no-margin text-right">Man Utd</p>
                </div>
                <div class="col-xs-2 no-pad h-align v-align">
                  <img src="<?=base_url();?>public/img/burnley38x38.png" alt="" class="pull-left">
                </div>
              </div>

              <div class="hWatch col-xs-12 col-md-2 no-pad text-center h-align v-align aa">
                <a href="<?=base_url();?>Welcome/livestream"><button class="submit no-pad h-align v-align" type="button" name="submit">Watch</button></a>
              </div>
            </div>
          </div>

          <div class="uls-hm-feed">
            <div class="col-xs-12 uls-hm-feed-c no-pad no-margin wash">
              <div class="col-xs-12 col-md-3 hPl uls-hm text-center no-pad lsm-b aa h-align v-align">
                <p class="text-center no-margin">Premier League <br> <span class="lsmdate hidden-sm hidden-xs">19:30</span></p>
              </div>

              <div class="col-xs-12 col-md-7 hLeague no-pad h-align v-align aa">
                <div class="col-xs-2 no-pad v-align h-align">
                  <img src="<?=base_url();?>public/img/burnley38x38.png" alt="" class=" pull-right">
                </div>
                <div class="col-xs-3 no-pad match">
                  <p class="no-margin text-left">Man City</p>
                </div>
                <div class="col-xs-1 no-pad match text-center ">
                  <p class="no-margin">vs</p>
                </div>
                <div class="col-xs-3 no-pad match">
                  <p class="no-margin text-right">Man Utd</p>
                </div>
                <div class="col-xs-2 no-pad h-align v-align">
                  <img src="<?=base_url();?>public/img/burnley38x38.png" alt="" class="pull-left">
                </div>
              </div>

              <div class="hWatch col-xs-12 col-md-2 no-pad text-center h-align v-align aa">
                <a href="<?=base_url();?>Welcome/livestream"><button class="submit no-pad h-align v-align" type="button" name="submit">Watch</button></a>
              </div>
            </div>
          </div>

          <div class="uls-hm-feed">
            <div class="col-xs-12 uls-hm-feed-c no-pad no-margin white-bg">
              <div class="col-xs-12 col-md-3 hPl uls-hm text-center no-pad lsm-b aa h-align v-align">
                <p class="text-center no-margin">Premier League <br> <span class="lsmdate hidden-sm hidden-xs">19:30</span></p>
              </div>

              <div class="col-xs-12 col-md-7 hLeague no-pad h-align v-align aa">
                <div class="col-xs-2 no-pad v-align h-align">
                  <img src="<?=base_url();?>public/img/burnley38x38.png" alt="" class=" pull-right">
                </div>
                <div class="col-xs-3 no-pad match">
                  <p class="no-margin text-left">Man City</p>
                </div>
                <div class="col-xs-1 no-pad match text-center ">
                  <p class="no-margin">vs</p>
                </div>
                <div class="col-xs-3 no-pad match">
                  <p class="no-margin text-right">Man Utd</p>
                </div>
                <div class="col-xs-2 no-pad h-align v-align">
                  <img src="<?=base_url();?>public/img/burnley38x38.png" alt="" class="pull-left">
                </div>
              </div>

              <div class="hWatch col-xs-12 col-md-2 no-pad text-center h-align v-align aa">
                <a href="<?=base_url();?>Welcome/livestream"><button class="submit no-pad h-align v-align" type="button" name="submit">Watch</button></a>
              </div>
            </div>
          </div>

          <div class="uls-hm-feed">
            <div class="col-xs-12 uls-hm-feed-c uls-end no-pad no-margin wash">
              <div class="col-xs-12 col-md-3 hPl uls-hm text-center no-pad lsm-b aa h-align v-align">
                <p class="text-center no-margin">Premier League <br> <span class="lsmdate hidden-sm hidden-xs">19:30</span></p>
              </div>

              <div class="col-xs-12 col-md-7 hLeague no-pad h-align v-align aa">
                <div class="col-xs-2 no-pad v-align h-align">
                  <img src="<?=base_url();?>public/img/burnley38x38.png" alt="" class=" pull-right">
                </div>
                <div class="col-xs-3 no-pad match">
                  <p class="no-margin text-left">Man City</p>
                </div>
                <div class="col-xs-1 no-pad match text-center ">
                  <p class="no-margin">vs</p>
                </div>
                <div class="col-xs-3 no-pad match">
                  <p class="no-margin text-right">Man Utd</p>
                </div>
                <div class="col-xs-2 no-pad h-align v-align">
                  <img src="<?=base_url();?>public/img/burnley38x38.png" alt="" class="pull-left">
                </div>
              </div>

              <div class="hWatch col-xs-12 col-md-2 no-pad text-center h-align v-align aa">
                <a href="<?=base_url();?>Welcome/livestream"><button class="submit no-pad h-align v-align" type="button" name="submit">Watch</button></a>
              </div>
            </div>
          </div>
        </div>
      </div><!--/ls-feed-->

    </div>
    <!--/col-md-8 Left Panel-->
    <div class="clearfix hidden-lg hidden-md"></div>
    <!--======Right Panel=========-->
    <div class="col-md-4 side-bar sideTitle no-pad v-align h-align pull-right">
      <!-- <h1 class="no-margin">Hello</h1> -->
      <div class="hidden-sm hidden-xs">

      <div class="pttbl pttbl-side">
        <div class="pttop">07/25 &#8226; 01:30 AM</div>
        <div class="ptpagetitle">
          <h4>PREMIER LEAGUE</h4>
          <p>Betting Tips</p>
        </div>
        <div class="ptmatch">
          <div class="ptmatchie">
            <img src="<?=base_url();?>public/img/mancity.png">
            <p>Arsenal</p>
          </div>
          <div class="ptmatchie ptvs">vs</div>
          <div class="ptmatchie">
            <img src="<?=base_url();?>public/img/mancity.png">
            <p>Arsenal</p>
          </div>
        </div>
        <p class="ptph">
          Arsenal will most likely be glad when the Premier League comes to an end. Two games played so far have...
        </p>
        <div class="pttip">
          <p>OUT TIP:</p>
          <span>CHELSEA WIN</span>
          <a href="<?=base_url();?>Welcome/previewstips2"><button>View More</button></a>
        </div>
      </div>

        <div class="col-md-12 no-pad ls-ban">
          <a href="#">
          <img src="<?=base_url();?>public/img/ls-wp10.jpg" alt="Weekly Perfect 10">
        </a>
        </div>

        <div class="col-md-12 no-pad ls-ban">
          <a href="#">
          <img src="<?=base_url();?>public/img/ls-gg.jpg" alt="The Golden Goal">
        </a>
        </div>

      </div>
    </div>
    <!-- right panel end -->
  </div>
  <!--/container-->
</div>
<!--/liveStream-->
