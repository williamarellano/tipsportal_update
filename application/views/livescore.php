<style type="text/css">
	#secMenu li:nth-of-type(5) a{background: #0e6857 !important;}
</style>
<div id="livescore">
<div id="livescorecontainer">
<div id="livescorecontainer2">
	<div id="lsmain" class="col-md-8 no-pad left-bar">
		<h3>Live Score</h3>
		<div id="lsleft">
			<div class="lsleft-tab">
				<div class="lsleft-title"><span class="fa fa-star orange-c"></span>My Leagues</div>
				<span><img src="<?=base_url();?>public/img/h2hmap.jpg">Premier League</span>
				<span><img src="<?=base_url();?>public/img/h2hmap.jpg">Ligue 1</span>
				<span><img src="<?=base_url();?>public/img/h2hmap.jpg">Bundesliga</span>
				<span><img src="<?=base_url();?>public/img/h2hmap.jpg">Premier League</span>
				<span><img src="<?=base_url();?>public/img/h2hmap.jpg">Premier League</span>
				<span><img src="<?=base_url();?>public/img/h2hmap.jpg">Premier League</span>
				<span><img src="<?=base_url();?>public/img/h2hmap.jpg">Premier League</span>
			</div>
			<div class="lsleft-tab">
				<div class="lsleft-title"><span class="fa fa-star orange-c"></span>My Teams</div>
				<p>To select your teams, just click the star icon located next to the name of the team.</p>
			</div>
			<div class="lsleft-tab">
				<div class="lsleft-title lscountry">Countries</div>
				<p>Albania</p>
				<p>Algeria</p>
				<p>Andorra</p>
				<p>Andorra</p>
				<p>Andorra</p>
				<p>Andorra</p>
				<div class="morecount">More</div>
			</div>
		</div>
		<div id="lsmiddle">
			<div id="lsnav">
				<ul>
					<li class="active">All Game</li>
					<li>My games (0)</li>
					<li>LIVE Games</li>
					<li>Finished</li>
					<li>Scheduled</li>
				</ul>
				<div class="lsdate">
					<span><</span>
					<span><i class="fa fa-calendar" aria-hidden="true"></i>23/08 Thu</span>
					<span>></span>
				</div>
			</div>
			<div class="lscontent">
				<table>
					<tbody>
						<tr class="active">
							<td>
								<input type="checkbox" name="" value="">
							</td>
							<th colspan="5">
								<img src="<?=base_url();?>public/img/h2hmap.jpg">
								EUROPE: Champions League - Qualifications
								<span class="fa fa-star orange-c"></span>
							</th>
						</tr>
						<tr>
							<td>
								<input type="checkbox" name="" value="">
							</td>
							<td>03:00</td>
							<td class="color-green">Finished</td>
							<td>Ajax (Ned)</td>
							<td>3 - 1</td>
							<td>Dyn. Kyiv (Ukr)</td>
						</tr>
						<tr>
							<td>
								<input type="checkbox" name="" value="">
							</td>
							<td>03:00</td>
							<td class="color-red">Live</td>
							<td>MOL Vidi (Hun)</td>
							<td>1 - 2</td>
							<td>AEK Athens FC (Gre)</td>
						</tr>
						<tr>
							<td>
								<input type="checkbox" name="" value="">
							</td>
							<td>03:00</td>
							<td class="color-blue">Scheduled</td>
							<td>Young Boys</td>
							<td>1 - 1</td>
							<td>D. Zagreb (Cro)</td>
						</tr>
					</tbody>
				</table>
				<table>
					<tbody>
						<tr>
							<td>
								<input type="checkbox" name="" value="">
							</td>
							<th colspan="5">
								<img src="<?=base_url();?>public/img/h2hmap.jpg">
								EUROPE: Champions League - Qualifications
								<span class="fa fa-star d-grey"></span>
							</th>
						</tr>
						<tr>
							<td>
								<input type="checkbox" name="" value="">
							</td>
							<td>03:00</td>
							<td class="color-green">Finished</td>
							<td>Ajax (Ned)</td>
							<td>3 - 1</td>
							<td>Dyn. Kyiv (Ukr)</td>
						</tr>
					</tbody>
				</table>			</div>
		</div>
		<!-- responsive part -->
		<div id="lsmobile">

			<div id="lsnav" class="moblsnav">
				<ul>
					<li class="active">All Game</li>
					<li>My games (0)</li>
					<li>LIVE Games</li>
					<li>Finished</li>
					<li>Scheduled</li>
				</ul>
			</div>

			<div class="lsdate">
					<span class="arrw"><</span>
					<span><i class="fa fa-calendar" aria-hidden="true"></i>23/08 Thu</span>
					<span class="arrw">></span>
			</div>
			<table>
				<tbody>
					<tr>
						<th colspan="4">
							<img src="<?=base_url();?>public/img/h2hmap.jpg">
								EUROPE: Champions League - Qualifications
							<span class="fa fa-star orange-c"></span>
						</th>
					</tr>
					<tr>
						<td rowspan="2">FT</td>
						<td>Ajax (Ned)</td>
						<td>3</td>
						<td rowspan="2"><span class="fa fa-star d-grey"></span></td>
					</tr>
					<tr>
						<td>Dyn. Kyiv (Ukr)</td>
						<td>1</td>
					</tr>
					<!-- aa -->
					<tr>
						<td rowspan="2">23:00</td>
						<td>Ajax (Ned)</td>
						<td>?</td>
						<td rowspan="2"><span class="fa fa-star d-grey"></span></td>
					</tr>
					<tr>
						<td>Dyn. Kyiv (Ukr)</td>
						<td>?</td>
					</tr>
					<!-- aa -->
					<tr>
						<td rowspan="2">23:00</td>
						<td>Ajax (Ned)</td>
						<td>?</td>
						<td rowspan="2"><span class="fa fa-star d-grey"></span></td>
					</tr>
					<tr>
						<td>Dyn. Kyiv (Ukr)</td>
						<td>?</td>
					</tr>
					<!-- aa -->
					<tr>
						<td rowspan="2">23:00</td>
						<td>Ajax (Ned)</td>
						<td>?</td>
						<td rowspan="2"><span class="fa fa-star d-grey"></span></td>
					</tr>
					<tr>
						<td>Dyn. Kyiv (Ukr)</td>
						<td>?</td>
					</tr>
					<!-- aa -->
					<tr>
						<td rowspan="2">23:00</td>
						<td>Ajax (Ned)</td>
						<td>?</td>
						<td rowspan="2"><span class="fa fa-star d-grey"></span></td>
					</tr>
					<tr>
						<td>Dyn. Kyiv (Ukr)</td>
						<td>?</td>
					</tr>
				</tbody>
			</table>

			<table>
				<tbody>
					<tr>
						<th colspan="4">
							<img src="<?=base_url();?>public/img/h2hmap.jpg">
								EUROPE: Champions League - Qualifications
							<span class="fa fa-star orange-c"></span>
						</th>
					</tr>
					<tr>
						<td rowspan="2">FT</td>
						<td>Ajax (Ned)</td>
						<td>3</td>
						<td rowspan="2"><span class="fa fa-star d-grey"></span></td>
					</tr>
					<tr>
						<td>Dyn. Kyiv (Ukr)</td>
						<td>1</td>
					</tr>
					<!-- aa -->
					<tr>
						<td rowspan="2">23:00</td>
						<td>Ajax (Ned)</td>
						<td>?</td>
						<td rowspan="2"><span class="fa fa-star d-grey"></span></td>
					</tr>
					<tr>
						<td>Dyn. Kyiv (Ukr)</td>
						<td>?</td>
					</tr>
				</tbody>
			</table>


		</div>
	</div>
<!-- </div> -->
	<div class="clearfix hidden-lg hidden-md"></div>
	<div class="col-md-4 side-bar sideTitle bluegreen">
		League Table
	</div>
	<div id="side-football-tables" class="col-xs-12 col-md-4 side-bar border pull-right white-bg">
    <div class="col-xs-12 no-pad">
      <div class="col-xs-12 page-header no-margin no-pad">
        <!-- <h5 class="side-utitle pull-left"><strong>Scottist Prem </strong></h5> -->
        <div class="btn-group pull-left" style="padding-top:8px;">
          <button class="btn btn-default side-btn btn-xs dropdown-toggle blueGreen font-bold" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              Scottish Prem. <span class="caret"></span>
            </button>
						<ul class="dropdown-menu f-l side-bar">
	            <li><a href="<?=base_url();?>Welcome/livescore">English Premier League</a></li>
	            <li><a href="<?=base_url();?>Welcome/livescore">English Championship</a></li>
	            <li><a href="<?=base_url();?>Welcome/livescore">Spain La Liga</a></li>
	            <li><a href="<?=base_url();?>Welcome/livescore">German Bundes Liga</a></li>
	            <li><a href="<?=base_url();?>Welcome/livescore">Nations League</a></li>
	            <li><a href="<?=base_url();?>Welcome/livescore">French Ligue 1</a></li>
	            <li><a href="<?=base_url();?>Welcome/livescore">Italy Serie A</a></li>
	            <li><a href="<?=base_url();?>Welcome/livescore">Dutch Eredivisie</a></li>
	            <li><a href="<?=base_url();?>Welcome/livescore"><strong>View all leagues</strong></a></li>
	          </ul>

        </div>
      </div>
    </div>
    <div id="side-table-scores" class="col-xs-12">
      <table class="table table-hover">
        <thead class="home-thead">
          <tr>
            <th> </th>
            <th>Team</th>
            <th>P</th>
            <th>GD</th>
            <th>Pts</th>
          </tr>
        </thead>
        <tbody>
          <tr class="success">
            <td class="dash" scope="row">1</td>
            <th class="dash">Aberdeen</th>
            <td class="dash">1</td>
            <td class="dash">2</td>
            <td class="table-cell-bold">3</td>
          </tr>
          <tr class="table-break success">
            <td scope="row">2</td>
            <th>Aberdeen</th>
            <td>0</td>
            <td>0</td>
            <td class="table-cell-bold">0</td>
          </tr>
          <tr class="success">
            <td scope="row">3</td>
            <th>AJJJberdeen</th>
            <td>0</td>
            <td>0</td>
            <td class="table-cell-bold">0</td>
          </tr>
          <tr class="table-break tble-yellow">
            <td scope="row">4</td>
            <th>Aberdeen</th>
            <td>0</td>
            <td>0</td>
            <td class="table-cell-bold">0</td>
          </tr>
          <tr class="tble-yellow">
            <td scope="row">5</td>
            <th>Aberdeen</th>
            <td>0</td>
            <td>0</td>
            <td class="table-cell-bold">0</td>
          </tr>
          <tr class="tble-yellow">
            <td scope="row">6</td>
            <th>Aberdeen</th>
            <td>0</td>
            <td>0</td>
            <td class="table-cell-bold">0</td>
          </tr>
          <tr class="tble-yellow">
            <td scope="row">7</td>
            <th>Aberdeen</th>
            <td>0</td>
            <td>0</td>
            <td class="table-cell-bold">0</td>
          </tr>
          <tr class="">
            <td scope="row">8</td>
            <th>Aberdeen</th>
            <td>0</td>
            <td>0</td>
            <td class="table-cell-bold">0</td>
          </tr>
          <tr class="table-break">
            <td scope="row">9</td>
            <th>Werder Bremen (Ger)</th>
            <td>0</td>
            <td>0</td>
            <td class="table-cell-bold">0</td>
          </tr>
          <tr>
            <td scope="row">10</td>
            <th>Aberdeen</th>
            <td>0</td>
            <td>0</td>
            <td class="table-cell-bold">0</td>
          </tr>
          <tr>
            <td scope="row">11</td>
            <th>Aberdeen</th>
            <td>0</td>
            <td>0</td>
            <td class="table-cell-bold">0</td>
          </tr>
          <tr>
            <td scope="row">12</td>
            <th>Werder Bremen (Ger)</th>
            <td>0</td>
            <td>0</td>
            <td class="table-cell-bold">0</td>
          </tr>
          <tr>
            <td scope="row">13</td>
            <th>Manchester City</th>
            <td>0</td>
            <td>0</td>
            <td class="table-cell-bold">0</td>
          </tr>
          <tr>
            <td scope="row">14</td>
            <th>Aberdeen</th>
            <td>0</td>
            <td>0</td>
            <td class="table-cell-bold">0</td>
          </tr>
          <tr>
            <td scope="row">15</td>
            <th>Manchester City</th>
            <td>0</td>
            <td>0</td>
            <td class="table-cell-bold">0</td>
          </tr>
          <tr>
            <td scope="row">16</td>
            <th>Aberdeen</th>
            <td>0</td>
            <td>0</td>
            <td class="table-cell-bold">0</td>
          </tr>
          <tr>
            <td scope="row">17</td>
            <th>Manchester City</th>
            <td>0</td>
            <td>0</td>
            <td class="table-cell-bold">0</td>
          </tr>
          <tr class="tble-red">
            <td scope="row">18</td>
            <th>Aberdeen</th>
            <td>0</td>
            <td>0</td>
            <td class="table-cell-bold">0</td>
          </tr>
          <tr class="tble-red">
            <td scope="row">19</td>
            <th>Bournemouth</th>
            <td>0</td>
            <td>0</td>
            <td class="table-cell-bold">0</td>
          </tr>
          <tr class="tble-red">
            <td scope="row">20</td>
            <th>Bournemouth</th>
            <td>0</td>
            <td>0</td>
            <td class="table-cell-bold">0</td>
          </tr>
        </tbody>
      </table>
    </div>
  </div>
</div>
</div>
</div>
