<style type="text/css">
	#secMenu li:nth-of-type(3) a{background: #0e6857 !important;}
</style>
<div id="pt">
	<div id="ptcontainer">
	<div class="tccontainercont">
		<div id="pttitle">
			<h3>Previews & Tips</h3>
			<p class="med">Featured games with previews and tips from our in-house tipster!</p>
		</div>
		<div id="pttblcontainer">

			<!-- table start -->
			<!-- <div class="pttbl">
				<div class="pttop">07/25 &#8226; 01:30 AM</div>
				<div class="ptpagetitle">
					<h4>PREMIER LEAGUE</h4>
					<p>Betting Tips</p>
				</div>
				<div class="ptmatch">
					<div class="ptmatchie">
						<img src="<?=base_url();?>public/img/mancity.png">
						<p>Arsenal</p>
					</div>
					<div class="ptmatchie ptvs">vs</div>
					<div class="ptmatchie">
						<img src="<?=base_url();?>public/img/mancity.png">
						<p>Arsenal</p>
					</div>
				</div>
				<p class="ptph">
					Arsenal will most likely be glad when the Premier League comes to an end. Two games played so far have...
				</p>
				<div class="pttip">
					<p>OUT TIP:</p>
					<span>CHELSEA WIN</span>
					<a href="<?=base_url();?>Welcome/previewstips2"><button>View More</button></a>
				</div>
			</div> -->
			<?php
			foreach ($tips as $t) {
			
			?>
			<div class="pttbl">
				<div class="pttop"><?= date("m/d • h:i A", strtotime($t['date_start']));?></div>
				<div class="ptpagetitle">
					<h4><?=strtoupper($t['league_nickname']);?></h4>
					<p>Betting Tips</p>
				</div>
				<div class="ptmatch">
					<div class="ptmatchie">
						<img src="<?=base_url().$this->config->item('upload_teams_logo').'/'.$t['home_team_logo'];?>">
						<p><?=$t['home_team_nickname'];?></p>
					</div>
					<div class="ptmatchie ptvs">vs</div>
					<div class="ptmatchie">
						<img src="<?=base_url().$this->config->item('upload_teams_logo').'/'.$t['away_team_logo'];?>">
						<p><?=$t['away_team_nickname'];?></p>
					</div>
				</div>
				<p class="ptph">
					<!-- Arsenal will most likely be glad when the Premier League comes to an end. Two games played so far have... -->
					<?php
						// echo substr(strip_tags($t['tptip_desc']), 1, 36);
					// $text = '<p>Test paragraph.</p><!-- Comment --> <a href="#fragment">Other text</a>';
					// echo strip_tags($text); 

					echo substr($t['tptip_desc'],0,93)."...";
					?>
				</p>
				<div class="pttip">
					<p>OUT TIP:</p>
					<span><?=strtoupper($t['tptip']);?></p></span>
					<a href="<?=base_url();?>Welcome/previewstips2"><button>View More</button></a>
				</div>
			</div>

			<?php
			}
			?>



			
			<!-- table end -->


			<center style="float: left; width: 100%;">
			<a href="<?=base_url();?>Welcome/previewstips3"><button class="tcvpt">View  Previous Tips</button></a>
			</center>
			<div class="sideshit">
				<p><b>Football Betting Tips and Advice for successful football betting</b></p>
				<p>Are you a football fan? Would you like to win more of your football bets? Discover the essential factors to bear in mind before making your football forecasts. </p>
				<br>
				<p>Whether you do the Football Pools or bet through a bookmaker, the following football betting tips are designed to help you improve your football results forecasts. By following our football betting tips, you'll increase your chances of winning your bets on matches in the Premier League, the Championship, the Champions League or any other football competitions.
				</p>
				<br>
				<p><b>Tip 1: Consider the motivation and team spirit</b></p>
				<p>Motivation has a major influence on a football club's sporting performances. The consequences of a particular encounter (league title, Champions League qualification, relegation, etc.) and the size of the match win bonus are key motivating factors for a football team.</p>
			</div>

		</div>
	</div>	
	</div>
</div>
