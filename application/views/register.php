<div id="register" class="container">
  <div class="row">
    <div class="col-md-9 col-centered no-pad text-center">
      <div class="panel panel-default">
        <div class="panel-heading">Register</div>
        <div class="panel-body register-contents">
          <form class="form-horizontal">
            <div class="form-group"> <label for="a" class="col-sm-3 control-label">Username *</label>
              <div class="col-sm-9"> <input type="text" class="form-control" id="a" placeholder=""> </div>
            </div>
            <div class="form-group"> <label for="b" class="col-sm-3 control-label">Password *</label>
              <div class="col-sm-9"> <input type="password" class="form-control" id="b" placeholder=""> </div>
            </div>
            <div class="form-group"> <label for="c" class="col-sm-3 control-label">Confirm Password *</label>
              <div class="col-sm-9"> <input type="password" class="form-control" id="c" placeholder=""> </div>
            </div>
            <div class="form-group"> <label for="d" class="col-sm-3 control-label">Email Address *</label>
              <div class="col-sm-9"> <input type="email" class="form-control" id="d" placeholder=""> </div>
            </div>
            <div class="form-group"> <label for="e" class="col-sm-3 control-label">Phone Number *</label>
              <div class="col-sm-9"> <input type="number" class="form-control" id="e" placeholder=""> </div>
            </div>
          </form>
          <div class="col-xs-12 text-center ftp-contest-submit">
            <button class="submit" type="button" name="submit">Submit</button>
          </div>
          <p>Already have an account? <a href="#">Log In</a></p>
        </div>
      </div>
    </div>
  </div>
</div>
