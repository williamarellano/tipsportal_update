<?php
// var_dump($tips);
?>

<style type="text/css">
	#secMenu li:nth-of-type(2) a{background: #0e6857 !important;}
</style>
<div id="tipscomparison">
		<div id="tccontainer">
		<div class="tccontainercont">
			<h3>Tipsters Comparison</h3>
			<p class="med">Bringing the best tips on the internet together for your convenience!</p>
			<div class="w100fl">
			<table class="tctable1">
				<tbody>
					<tr>
						<th>Tipster</th>
						<th>Tip Form</th>
						<th>Match</th>
						<th>Tip</th>
						<th>Odds</th>
						<th></th>
					</tr>
					<?php
						foreach ($tips as $t) {
					?>
					<tr>
						<td><img src="<?=base_url().$this->config->item('upload_tipsters_logo').'/'.$t['tipster_logo'];?>"></td>
						<td>
							<img src="<?=base_url().$this->config->item('star_img').'/star'.$t['tip_form'];?>.png">
						</td>
						<td>
							<img src="<?=base_url().$this->config->item('upload_teams_logo').'/'.$t['home_team_logo'];?>">
							<p><?=$t['home_team_nickname'];?>
								<br>
							<?=$t['away_team_nickname'];?></p>
							<img src="<?=base_url().$this->config->item('upload_teams_logo').'/'.$t['away_team_logo'];?>">
						</td>
						<td><?=$t['tip_main'];?></td>
						<td><?=$t['odds'];?></td>
						<td><a href="<?=$t['link'];?>" target="_blank"><button>View More</button></a></td>
					</tr>
					<?php
						}
					?>
				</tbody>
			</table>
			</div>

			<table id="tctable2">
				<tbody>
					<tr>
						<td><img src="<?=base_url();?>public/img/star1.png"></td>
						<td><img src="<?=base_url();?>public/img/star2.png"></td>
						<td><img src="<?=base_url();?>public/img/star3.png"></td>
						<td><img src="<?=base_url();?>public/img/star4.png"></td>
						<td><img src="<?=base_url();?>public/img/star5.png"></td>
					</tr>
					<tr>
						<td>Strike rate 0%~20%</td>
						<td>Strike rate 21%~40%</td>
						<td>Strike rate 41%~60%</td>
						<td>Strike rate 61%~80%</td>
						<td>Strike rate 81%~100%</td>
					</tr>
				</tbody>
			</table>
			<p class="tcnote">*Only EPL tips were taken into consideration</p>
			<center>
			<a href="<?=base_url();?>Welcome/tcomparison2"><button class="tcvpt">View  Previous Tips</button></a>
			</center>
			<div class="sideshit">
				<p><b>Football Betting Tips and Advice for successful football betting</b></p>
				<p>re you a football fan? Would you like to win more of your football bets? Discover the essential factors to bear in mind before making your football forecasts. </p>
				<br>
				<p>Whether you do the Football Pools or bet through a bookmaker, the following football betting tips are designed to help you improve your football results forecasts.</p>
				<br>
				<p><b>Tip 1: Consider the motivation and team spirit</b></p>
				<p>Motivation has a major influence on a football club's sporting performances. The consequences of a particular encounter (league title, Champions League qualification, relegation, etc.) and the size of the match win bonus are key motivating factors for a football team.</p>
			</div>

		</div>
		</div>
	</div>
