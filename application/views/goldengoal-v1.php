<div class="jumbotron home-banner">
</div>

<div id="ftp-contents" class="container-fluid">
  <div class="container ftp-container">
    <div class="row">
      <div class="bs-example bs-example-tabs" data-example-id="togglable-tabs">
          <ul class="nav nav-tabs nav-justified" id="ftpTabs" role="tablist">
            <li role="presentation" class="active"><a href="#ftp-contest-content" id="ftp-contest-tab" role="tab" data-toggle="tab" aria-controls="home" aria-expanded="false">Contest</a></li>
            <li role="presentation" class=""><a href="#ftp-play-content" role="tab" id="ftp-play-tab" data-toggle="tab" aria-controls="profile" aria-expanded="true">How to play</a></li>
            <li role="presentation" class=""> <a href="#ftp-leaderboard-content" id="ftp-contest-tab" role="tab" data-toggle="tab" aria-controls="home" aria-expanded="false">Leaderboard</a>
            </li>
          </ul>
          <div class="tab-content" id="myTabContent">
            <div class="tab-pane fade active in" role="tabpanel" id="ftp-contest-content" aria-labelledby="ftp-contest-tab">
              <h1 class="text-center text-uppercase">the golden goals</h1>
              <img src="<?=base_url();?>public/img/ftp-contest-banner.jpg" alt="ftp-contest-banner" class="img-responsive">
                <br>
                <!-- Split button -->
          <div class="row">
              <div class="col-xs-12 col-centered text-center">
                  <div class="col-md-1 col-md-offset-4">
                <div class="btn-group">
                  <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Select Player <span class="caret"></span>
                  </button>
                  <ul class="dropdown-menu">
                    <li><a href="#">Lorem ipsum dolor</a></li>
                    <li><a href="#">Lorem ipsum dolor</a></li>
                    <li><a href="#">Lorem ipsum dolor</a></li>
                    <li role="separator" class="divider"></li>
                    <li><a href="#">Lorem ipsum dolor</a></li>
                  </ul>
                  </div>
              </div>
              <div class="col-md-1"></div>
              <div class="col-md-1">
                <div class="btn-group">
                  <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Select Goal Minute <span class="caret"></span>
                  </button>
                  <ul class="dropdown-menu">
                    <li><a href="#">Lorem ipsum dolor</a></li>
                    <li><a href="#">Lorem ipsum dolor</a></li>
                    <li><a href="#">Lorem ipsum dolor</a></li>
                    <li role="separator" class="divider"></li>
                    <li><a href="#">Lorem ipsum dolor</a></li>
                  </ul>
                  </div>
              </div>
            </div>

            <div class="col-xs-12 text-center ftp-contest-submit">
              <button class="submit" type="button" name="submit">Submit</button>
            </div>
          </div>
            </div>
            <div class="tab-pane fade" role="tabpanel" id="ftp-play-content" aria-labelledby="ftp-play-tab">
              <p class="text-center">It’s simple; correctly predict who will score the opening goal and in which minute of the featured match to win <span class="font-bold">SGD 1,888</span> and earn points for the seasonal leaderboard! <br>
                  Perfect Prediction: <span class="font-bold">20 points</span> <br>
                  Correct Goalscorer only: <span class="font-bold">5 points</span><br>
                  Correct Minute only: <span class="font-bold">5 points</span></p><br>
              <p class="text-center font-bold prize-str">Prize Structure</p>
              <p class="perfect-pred text-center">Perfect prediction SGD 1,888</p>
              <div class="col-md-7 col-centered no-pad">
                <div class="well terms-condition">
                  <h3>Terms & Conditions</h3>
                  <ul>
                    <li> Participation is free.</li>
                    <li> Member will gain points for the seasonal leaderboard through the games offered on our site.</li>
                    <li> One entry per player.</li>
                    <li> Winners will be notified by email/sms.</li>
                    <li> Prize will be paid in credit on one of the sponsoring sites.</li>
                    <li> Limited to 1 entry per household.</li>
                    <li> All decisions regarding this offer are final and binding.</li>
                  </ul>
                </div>
              </div>
            </div>
            <div class="tab-pane fade" role="tabpanel" id="ftp-leaderboard-content" aria-labelledby="ftp-leader-tab">
              <div class="row col-centered text-center">
<div class="col-xs-12 col-md-11 text-center col-centered">
  <p class="text-center">Take part in our free to play games and earn points each week for the seasonal leaderboard! After each season, if you are ranked top 100, you can receive a cash prize up to <span class="font-bold">SGD 15,000!</span></p>
                </div>              <br>
                <div class="col-xs-12 col-md-10 text-center no-pad col-centered">
                  <table class="table table-hover table-striped table-bordered ">
                    <thead class="home-thead century">
                      <tr>
                        <th class="text-center no-pad">Rank</th>
                        <th class="text-center no-pad">Points</th>
                        <th class="text-center no-pad">Username</th>
                        <th class="text-center no-pad">Prize</th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                        <td>1st</td>
                        <td>100</td>
                        <td>0</td>
                        <td>SGD15,000</td>
                      </tr>
                      <tr>
                        <td>2nd</td>
                        <td>100</td>
                        <td>0</td>
                        <td>SGD15,000</td>
                      </tr>
                      <tr>
                        <td>3rd</td>
                        <td>100</td>
                        <td>0</td>
                        <td>SGD15,000</td>
                      </tr>
                      <tr>
                        <td>4th</td>
                        <td>100</td>
                        <td>0</td>
                        <td>SGD15,000</td>
                      </tr>
                      <tr>
                        <td>5th</td>
                        <td>100</td>
                        <td>0</td>
                        <td>SGD15,000</td>
                      </tr>
                      <tr>
                        <td>6th</td>
                        <td>100</td>
                        <td>0</td>
                        <td>SGD15,000</td>
                      </tr>
                      <tr>
                        <td>7th</td>
                        <td>100</td>
                        <td>0</td>
                        <td>SGD15,000</td>
                      </tr>
                      <tr>
                        <td>8th</td>
                        <td>100</td>
                        <td>0</td>
                        <td>SGD15,000</td>
                      </tr>
                      <tr>
                        <td>9th</td>
                        <td>100</td>
                        <td>0</td>
                        <td>SGD15,000</td>
                      </tr>
                      <tr>
                        <td>10th</td>
                        <td>100</td>
                        <td>0</td>
                        <td>SGD15,000</td>
                      </tr>
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
  </div>
</div>
