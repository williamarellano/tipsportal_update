<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<title>
		<?= $title ?>
	</title>
	<link rel="stylesheet" type="text/css" href="<?=base_url();?>public/css/bootstrap.css">
  <link rel="stylesheet" type="text/css" href="<?=base_url();?>public/css/style2.css">
  <link rel="stylesheet" href="<?=base_url();?>public/css/chloe.css">
	<link rel="stylesheet" type="text/css" href="<?=base_url();?>public/css/media.css">
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
<script src="<?=base_url();?>public/js/jquery-3.3.1.js"></script>
</head>

<body>
	<!-- =====Navigation===== -->
	<nav class="navbar navbar-inverse navbar-static-top ">
		<div class="container">
			<div class="navbar-header">
				<a class="navbar-brand" href="<?=base_url();?>Welcome/home"><img src="<?=base_url();?>public/img/logo.png" alt="Tips Portal"></a>
			</div>
			<div id="navbar" class="">
				<div class="navbar-right"><button type="submit" class="btn btn-success orange" data-toggle="modal" data-target="#loginModal"><img src="<?=base_url();?>public/img/login.png" alt=""> Login</button>
					<!-- <a class="btn btn-success orange  font-bold" href="<?=base_url();?>Welcome/register" role="button"><img src="<?=base_url();?>public/img/register.png" alt=""> Register</a> -->
				</div>
				<!-- </form> -->
			</div>
		</div>
	</nav>

	<!-- =====Second Navigation===== -->

  <div class="wrapper" id="secNav">
    <div class="inner-wrapper">
      <ul id="secMenu" class="">
        <li class=""> <a class="secLink" tabindex="1" href="<?=base_url();?>Welcome/home"><img src="<?=base_url();?>public/img/home.png" alt=""> Home</a></li>
				<li> <a class="secLink" href="<?=base_url();?>Welcome/tcomparison"><img src="<?=base_url();?>public/img/tipsters-comparison.png" alt=""> Tipsters Comparison</a></li>
				<li> <a class="secLink" href="<?=base_url();?>Welcome/previewstips"><img src="<?=base_url();?>public/img/preview-tips.png" alt="">  Preview &amp; Tips</a></li>
        <li> <a class="secLink" href="<?=base_url();?>Welcome/freetoplay"><img src="<?=base_url();?>public/img/play-competition.png" alt=""> Free Games</a></li>
        <li> <a class="secLink" href="<?=base_url();?>Welcome/livescore"><img src="<?=base_url();?>public/img/live-score.png" alt=""> Live Score</a></li>
        <li> <a class="secLink" href="<?=base_url();?>Welcome/livestream"><img src="<?=base_url();?>public/img/live-stream.png" alt=""> Live Stream</a></li>
      </ul>
    </div>
  </div>

<!-- ===== Modal ===== -->
  <div class="modal fade" id="loginModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
      <div class="modal-content  no-radius">
        <div class="modal-header dark-bg">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title text-center white modal-tits" id="login-tit">Login</h4>
          <h4 class="modal-title text-center white modal-tits" id="register-tit">Register</h4>
          <h4 class="modal-title text-center white modal-tits" id="forgot-tit">Forgot Password?</h4>
        </div>

		<div class="modal-body" id="loginbody">
        	<p class="text-center">Login with your social network</p>

						<div class="col-xs-12 no-pad col-centered text-center">
								<div class="col-xs-12 col-sm-4 h-align no-pad ">
									<a href="#"><img class="scimg col-xs-4 no-pad " src="<?=base_url();?>public/img/facebook.jpg"></a>
	        			</div>

								<div class="col-xs-12 col-sm-4 h-align no-pad ">
									<a href="#"><img class="scimg col-xs-4 no-pad " src="<?=base_url();?>public/img/twitterlink.jpg"></a>
	        			</div>

							<div class="col-xs-12 col-sm-4 h-align no-pad ">
								<a href="#"><img class="scimg col-xs-4 no-pad " src="<?=base_url();?>public/img/googlelink.jpg"></a>
							</div>
						</div>

				<div class="clearfix"></div>

      	<div class="individer">
			  <span>
			    or
			  </span>
			</div>
			<!-- login -->
			<form id="loginna" class="form-horizontal col-centered text-center nanana">
				<div class="form-group">
					<div class="col-sm-12">
						<input type="text" class="form-control no-radius" id="a" placeholder="Email">
					</div>
				</div>
				<div class="form-group">
					<div class="col-sm-12">
						<input type="password" class="form-control no-radius" id="b" placeholder="Password">
					</div>
				</div>
				<div class="checkbox text-left text-muted">
    				<label><input type="checkbox"> Remember me</label>
  				</div>
				<div class="col-centered text-center  login-modal no-pad">
					<button class="submit col-xs-12 text-center no-radius" type="button" name="submit">Login</button><br>
				</div>
				<div>
				</div>
				<p class="login-link no-margin text-right"><span id="forgot-link">Forgot Password?</span></p>
				<p class="login-link no-margin text-right">Not a member? <span id="register-link"> Register</span></p>
			</form>

			<!-- register -->
			<form id="registerna" class="form-horizontal col-centered text-center nanana">
				<div class="form-group">
					<div class="col-sm-12">
						<input type="text" class="form-control no-radius"  placeholder="Username">
					</div>
				</div>
				<div class="form-group">
					<div class="col-sm-12">
						<input type="password" class="form-control no-radius" placeholder="Password">
					</div>
				</div>
				<div class="form-group">
					<div class="col-sm-12">
						<input type="password" class="form-control no-radius" placeholder="Confirm Password">
					</div>
				</div>
				<div class="form-group">
					<div class="col-sm-12">
						<input type="password" class="form-control no-radius" placeholder="Email">
					</div>
				</div>
				<div class="form-group">
					<div class="col-sm-12">
						<input type="password" class="form-control no-radius" placeholder="Phone">
					</div>
				</div>
				<!-- <div class="form-group">
					<div class="col-sm-8">
						<input type="password" class="form-control no-radius" placeholder="Verification code">
					</div>
					<div class="col-sm-3" style="background: #0e6857; color: white; padding: 7px 0; width: 29%;">
						<span>8m5h1</span>
					</div>
				</div> -->
				<div class="col-xs-12 col-centered text-center login-modal no-pad">
					<button class="submit col-xs-12 no-pad text-center no-radius" type="button" name="submit">Register</button><br>
				</div>
				<div>
				</div>
				<p class="login-link no-margin text-right">Already have an account? <span id="login-link"> Login</span></p>
			</form>


			<!-- forgot -->
			<form id="forgotna" class="form-horizontal col-centered text-center nanana">
				<div class="form-group">
					<div class="col-sm-12">
						<input type="text" class="form-control no-radius"  placeholder="Enter email to reset password">
					</div>
				</div>

				<div class="col-xs-12 col-centered text-center login-modal no-pad">
					<button class="submit col-xs-12 no-pad text-center no-radius" type="button" name="submit">Reset</button><br>
				</div>
				<div>
				</div>
				<p class="login-link no-margin text-right"><span id="forgot-login-link"> Login</span></p>
			</form>

  		</div>

    </div>
  </div>
</div>

<script>
$(function(){
	$("#register-link").click(function(){
    	$(".nanana").css('display','none');
    	$("#registerna").css('display','block');
    	$(".modal-tits").css('display','none');
    	$("#register-tit").css('display','block');
	});
	$("#login-link").click(function(){
    	$(".nanana").css('display','none');
    	$("#loginna").css('display','block');
    	$(".modal-tits").css('display','none');
    	$("#login-tit").css('display','block');
	});
	$("#forgot-link").click(function(){
    	$(".nanana").css('display','none');
    	$("#forgotna").css('display','block');
    	$(".modal-tits").css('display','none');
    	$("#forgot-tit").css('display','block');
	});
	$("#forgot-login-link").click(function(){
    	$(".nanana").css('display','none');
    	$("#loginna").css('display','block');
    	$(".modal-tits").css('display','none');
    	$("#login-tit").css('display','block');
	});
});

</script>
