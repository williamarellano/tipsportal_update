 <header class="main-header">
    <!-- Logo -->
    <a href="<?=base_url();?>Admin/admin" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><b>TP</b></span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg">TipsPortal</span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </a>

      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
          <!-- Messages: style can be found in dropdown.less-->
       
          
          <!-- User Account: style can be found in dropdown.less -->
          <li class="dropdown user user-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <img src="<?=base_url();?>public/img/user-image.png" class="user-image" alt="User Image">
              <span class="hidden-xs"><?php echo $_SESSION["username"] ?></span>
            </a>
            <ul class="dropdown-menu">
              <!-- User image -->
              <li class="user-header">
                <img src="<?=base_url();?>public/img/user-image.png" class="img-circle" alt="User Image">

                <p>
                  <?=$this->session->userdata('username')?>
                  <small>Siegreich Solutions TipsPortal - Admin</small>
                </p>
              </li>
              <!-- Menu Body -->
           
              <!-- Menu Footer-->
              <li class="user-footer">
                <div class="pull-left">
                  <button data-toggle="modal" data-target="#changePassModal" class="btn btn-info btn-flat">Change Password</button>
                </div>
                <div class="pull-right">
                  <a href="<?=base_url();?>Admin/logout" class="btn btn-danger btn-flat">Sign out</a>
                </div>
              </li>
            </ul>
          </li>
          <!-- Control Sidebar Toggle Button -->
       <!--    <li>
            <a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>
          </li> -->
        </ul>
      </div>
    </nav>
  </header>
  <!-- Modal -->
  <div class="modal fade" id="changePassModal" role="dialog">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header info-modal">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Change Password</h4>
        </div>
        <div class="modal-body">
           <?php echo form_open_multipart(base_url( 'changePass' ), array( 'id' => 'change-pass-form', 'class' => 'form-horizontal form-label-left' )); ?>
              <div class="box-body">
              <!--   <div class="form-group">
                  <label for="texty"><span style="color:red">*</span> Current Password</label>
                  <input type="password" class="form-control" id="cur_pass" placeholder="Current Password">
                </div> -->
                <div class="form-group">
                  <label for="password"><span style="color:red">*</span>New Password</label>
                  <input type="password" class="form-control" id="new_pass" placeholder="New Password">
                </div>
                 <div class="form-group">
                  <label for="name"><span style="color:red">*</span> Confirm New Password</label>
                  <input type="password" class="form-control" id="new_pass_c" placeholder="Confirm New Password">
                </div>              
              </div> 
        </div>

        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
           <button type="submit" class="btn btn-info">Save</button>
            <?php echo form_close(); ?>
        </div>
      </div>
    </div>
  </div>
  <!-- =============================================== -->

  <!-- Left side column. contains the sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
          <img src="<?=base_url();?>public/img/user-image.png" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
          <p><?=$this->session->userdata('username')?></p>
          <!-- <a href="#"><i class="fa fa-circle text-success"></i> Online</a> -->
        </div>
      </div>
     
      <!-- /.search form -->
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu" data-widget="tree">
        <li class="header">MAIN NAVIGATION</li>
        <li  <?php echo ($url[''] == '/Admin/admin'||$url[''] == '/admin/admin'||$url[''] == '/Admin/')? "class=' active'" : "class=''"; ?>>
          <a href="<?=base_url()?>Admin/admin">
            <i class="fa fa-dashboard"></i> <span>Dashboard</span>
          </a>
        </li>

        <li <?php echo ($url[''] == '/Leagues/leagues'||$url[''] == '/Leagues/leagueStages'||$url[''] == '/Leagues/stageRound') ? "class='treeview menu-open active'" : "class='treeview'"; ?>>
          <a href="#">
            <i class="fa fa-trophy"></i> <span>Leagues</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li <?php echo ($url[''] == '/Leagues/leagues'||$url[''] == '/Leagues/viewLeagues')? "class=' active'" : "class=''"; ?>><a href="<?=base_url()?>Leagues/leagues"><i class="fa fa-circle-o"></i> Leagues</a></li>
            <li <?php echo ($url[''] == '/Leagues/leagueStages')? "class=' active'" : "class=''"; ?>><a href="<?=base_url()?>Leagues/leagueStages"><i class="fa fa-circle-o"></i> League Stages</a></li>
            <li <?php echo ($url[''] == '/Leagues/stageRound')? "class=' active'" : "class=''"; ?>><a href="<?=base_url()?>Leagues/stageRound"><i class="fa fa-circle-o"></i> Stage Round</a></li>
           

          </ul>
        </li>

        <li <?php echo ($url[''] == '/Sites/sites'||$url[''] == '/Sites/tips') ? "class='treeview menu-open active'" : "class='treeview'"; ?>>
          <a href="#">
            <i class="fa fa-globe"></i> <span>Sites</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li <?php echo ($url[''] == '/Sites/sites')? "class=' active'" : "class=''"; ?>><a href="<?=base_url()?>Sites/sites"><i class="fa fa-circle-o"></i> Tipster</a></li>

            <li <?php echo ($url[''] == '/Sites/tips')? "class=' active'" : "class=''"; ?>><a href="<?=base_url()?>Sites/tips"><i class="fa fa-circle-o"></i> Tipster Comparison</a></li>

            <li <?php echo ($url[''] == '/Sites/tptips')? "class=' active'" : "class=''"; ?>><a href="<?=base_url()?>Sites/tptips"><i class="fa fa-circle-o"></i> Preview & Tips</a></li>


          </ul>
        </li>

        <li <?php echo ($url[''] == '/Results/bets'||$url[''] == '/Results/tipRes') ? "class='treeview menu-open active'" : "class='treeview'"; ?>>
          <a href="#">
            <i class="fa fa-list"></i> <span>Results</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li <?php echo ($url[''] == '/Results/bets')? "class=' active'" : "class=''"; ?>><a href="<?=base_url()?>Results/bets"><i class="fa fa-circle-o"></i> Bets</a></li>
            <li <?php echo ($url[''] == '/Results/tipRes')? "class=' active'" : "class=''"; ?>><a href="<?=base_url()?>Results/tipRes"><i class="fa fa-circle-o"></i> Tip Results</a></li>


          </ul>
        </li>
         <li <?php echo ($url[''] == 'TipsCms'||$url[''] == 'HomeCms') ? "class='treeview menu-open active'" : "class='treeview'"; ?>>
          <a href="#">
            <i class="fa fa-object-group"></i> <span>CMS Pages</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li <?php echo ($url[''] == '/HomeCms')? "class=' active'" : "class=''"; ?>><a href="<?=base_url()?>HomeCms"><i class="fa fa-circle-o"></i> Home</a></li>
            <li <?php echo ($url[''] == '/HomeCms/tipCom')? "class=' active'" : "class=''"; ?>><a href="<?=base_url()?>HomeCms/tipCom"><i class="fa fa-circle-o"></i> Tipsters Comparison</a></li>
            <li <?php echo ($url[''] == '/HomeCms/prevnTips')? "class=' active'" : "class=''"; ?>><a href="<?=base_url()?>HomeCms/prevnTips"><i class="fa fa-circle-o"></i> Preview & Tips</a></li>
            <li <?php echo ($url[''] == '/HomeCms/freeGames')? "class=' active'" : "class=''"; ?>><a href="<?=base_url()?>HomeCms/freeGames"><i class="fa fa-circle-o"></i> Free Games</a></li>
            <li <?php echo ($url[''] == '/HomeCms/freeGames')? "class=' active'" : "class=''"; ?>><a href="<?=base_url()?>HomeCms/freeGames"><i class="fa fa-circle-o"></i> Live Score</a></li>
            <li <?php echo ($url[''] == '/HomeCms/liveStream')? "class=' active'" : "class=''"; ?>><a href="<?=base_url()?>HomeCms/liveStream"><i class="fa fa-circle-o"></i> Live Stream</a></li>


          </ul>
        </li>
        
        <li <?php echo ($url[''] == '/Management/users'||$url[''] == '/Management/members' || $url[''] == '/Management/category' || $url[''] == '/Management/stage_masterlist'||$url[''] == '/Management/match_masterlist'||$url[''] == '/Management/league_masterlist') ? "class='treeview menu-open active'" : "class='treeview'"; ?>>
          <a href="#">
            <i class="fa fa-cog"></i> <span>Management</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li <?php echo ($url[''] == '/Management/users')? "class=' active'" : "class=''"; ?>><a href="<?=base_url()?>Management/users"><i class="fa fa-circle-o"></i> Users</a></li>
            <li <?php echo ($url[''] == '/Management/members')? "class=' active'" : "class=''"; ?>><a href="<?=base_url()?>Management/members"><i class="fa fa-circle-o"></i> Members</a></li>
             <li <?php echo ($url[''] == '/Management/league_masterlist')? "class=' active'" : "class=''"; ?>><a href="<?=base_url()?>Management/league_masterlist"><i class="fa fa-circle-o"></i> League Masterlist</a></li>

             <li <?php echo ($url[''] == '/Management/stages_masterlist')? "class=' active'" : "class=''"; ?>><a href="<?=base_url()?>Management/stages_masterlist"><i class="fa fa-circle-o"></i> Stages Masterlist</a></li>

             <li <?php echo ($url[''] == '/Management/teams_masterlist')? "class=' active'" : "class=''"; ?>><a href="<?=base_url()?>Management/teams_masterlist"><i class="fa fa-circle-o"></i> Teams Masterlist</a></li>

             <li <?php echo ($url[''] == '/Management/players_masterlist')? "class=' active'" : "class=''"; ?>><a href="<?=base_url()?>Management/players_masterlist"><i class="fa fa-circle-o"></i> Players Masterlist</a></li>
            

            <li <?php echo ($url[''] == '/Management/setting')? "class=' active'" : "class=''"; ?>><a href="<?=base_url()?>Management/setting"><i class="fa fa-circle-o"></i> Settings</a></li>
            <!-- <li <?php echo ($url[''] == '/Management/category')? "class=' active'" : "class=''"; ?>><a href="<?=base_url()?>Management/category"><i class="fa fa-circle-o"></i> Category Masterlist</a></li> -->
           
          </ul>
   
        </li>
        
      </ul>

       
    </section>
    <!-- /.sidebar -->
  </aside>

  <!-- =============================================== -->