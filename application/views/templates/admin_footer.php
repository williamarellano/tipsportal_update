<!-- jQuery 3 -->
<script src="<?=base_url()?>assets/bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="<?=base_url()?>assets/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- SlimScroll -->
<script src="<?=base_url()?>assets/bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>

<!-- date-range-picker -->
<script src="<?=base_url()?>assets/bower_components/moment/min/moment.min.js"></script>
<script src="<?=base_url()?>assets/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>

<!-- FastClick -->
<script src="<?=base_url()?>assets/bower_components/fastclick/lib/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="<?=base_url()?>assets/dist/js/adminlte.min.js"></script>


<!-- Datatables -->
<script src="<?=base_url()?>assets/bower_components/datatables.net/js/jquery.dataTables.js"></script>
<script src="<?=base_url()?>assets/bower_components/datatables.net-bs/js/dataTables.bootstrap.js"></script>

<!-- Select2 -->
<script src="<?=base_url()?>assets/bower_components/select2/dist/js/select2.full.min.js"></script>

<script src="<?=base_url()?>assets/bower_components/moment/moment.js"></script>
<script src="<?=base_url()?>assets/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>

<!-- Sweet Alert -->
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script src="<?=base_url()?>public/js/ssi.js"></script>

<!-- CK Editor -->
<script src="<?=base_url()?>assets/bower_components/ckeditor/ckeditor.js"></script>
<!-- Bootstrap WYSIHTML5 -->
<script src="<?=base_url()?>assets/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>


<script>
  $(document).ready(function () {
    $('.sidebar-menu').tree();

  })

   $("#change-pass-form").submit(function(e){
        e.preventDefault();

        changePass();

      });
	 
function changePass(){

          // var cur_pass = $('#cur_pass').val();
          var new_pass = $('#new_pass').val();
          var new_pass_c = $('#new_pass_c').val();
          

   // if(cur_pass==''||new_pass==''||new_pass_c==''){
   //    swal("Error Change Password!","Please Input All Fields","error");
   //  }
   if(new_pass==''||new_pass_c==''){
      swal("Error Change Password!","Please Input All Fields","error");
    }
   else if(new_pass !== new_pass_c){
       swal("Error Change Password!","Confirm Password Mismatch!","error");
   }
    
    else
    {
            var formData = new FormData();
            // formData.append('cur_pass', cur_pass);
            formData.append('new_pass', new_pass);
            formData.append('new_pass_c', new_pass_c);

            

            //console.log($('#upload_save'));
            $.ajax({
              url :  "../Admin/changePass",
              type: "POST",
              dataType : 'json',
              cache :  false,
              contentType : false,
              processData : false,
              async: false,
              data : formData,
              success: function(data, textStatus, jqXHR){
                
               
                if (data.typ == 'success'){
                  resetPassFields();
                   swal(data.ttl, data.msg, data.typ);
                   $("#changePassModal").modal('toggle');
                           
                }else{
                  swal(data.ttl, data.msg, data.typ);
                
                }
              },
              error: function (jqXHR, textStatus, errorThrown)
              {
    
              } 


            });
    }
   }
     function resetPassFields(){
        $('#cur_pass').val('');
        $('#new_pass').val('');
        $('#new_pass_c').val('');
      }
</script>
</body>
</html>