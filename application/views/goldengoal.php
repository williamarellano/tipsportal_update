<style type="text/css">
	#secMenu li:nth-of-type(4) a{background: #0e6857 !important;}
</style>

<div id="" class="carousel slide" data-ride="carousel">
  <!-- Wrapper for slides -->
  <div class="carousel-inner" role="listbox">
    <div class="item active">
      <img src="<?=base_url();?>public/img/home-banner.jpg" alt="...">
    </div>
    <div class="item">
      <img src="<?=base_url();?>public/img/home-banner.jpg" alt="...">
    </div>
  </div>
</div>
<div id="ftpp10" class="width100floatleft">

	<div class="width100floatleft">
	<div class="size1050px">
		<div id="ftppgoldtabs">
			<ul>
				<li class="ftpp10active" onclick="ftppgold('1')">Contest</li>
				<li class="" onclick="ftppgold('2')">How to play</li>
				<li class="" onclick="ftppgold('3')">Past Winners</li>
			</ul>
		</div>
		<div id="ftppgold1" class="ftppgoldtabcont ftpp10actived">
			              <h2 class="text-center text-uppercase">the golden goals</h2>
              <img src="<?=base_url();?>public/img/ftp-contest-banner.jpg" alt="ftp-contest-banner" class="img-responsive">
                <br>
                <!-- Split button -->
          <div class="row">
              <div class="col-xs-12 col-centered text-center">
                  <div class="col-md-1 col-md-offset-4">
                <div class="btn-group">
                  <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Select Player <span class="caret"></span>
                  </button>
                  <ul class="dropdown-menu">
                    <li><a href="#">Lorem ipsum dolor</a></li>
                    <li><a href="#">Lorem ipsum dolor</a></li>
                    <li><a href="#">Lorem ipsum dolor</a></li>
                    <li role="separator" class="divider"></li>
                    <li><a href="#">Lorem ipsum dolor</a></li>
                  </ul>
                  </div>
              </div>
              <div class="col-md-1"></div>
              <div class="col-md-1">
                <div class="btn-group">
                  <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Select Goal Minute <span class="caret"></span>
                  </button>
                  <ul class="dropdown-menu">
                    <li><a href="#">Lorem ipsum dolor</a></li>
                    <li><a href="#">Lorem ipsum dolor</a></li>
                    <li><a href="#">Lorem ipsum dolor</a></li>
                    <li role="separator" class="divider"></li>
                    <li><a href="#">Lorem ipsum dolor</a></li>
                  </ul>
                  </div>
              </div>
            </div>

            <div class="col-xs-12 text-center ftp-contest-submit">
              <button class="submit" type="button" name="submit">Submit</button>
            </div>
          </div>
		</div>
		<div id="ftppgold2" class="ftppgoldtabcont">
			<h2>HOW TO PLAY THE GOLDEN GOALS</h2>
			<p class="text-center">It’s simple; correctly predict who will score the opening goal and in which minute of the featured match to win <span class="font-bold">SGD 1,888</span> and earn points for the seasonal leaderboard! <br><br>
                  -Perfect Prediction: <span class="font-bold">20 points</span> <br>
                  -Correct Goalscorer only: <span class="font-bold">5 points</span><br>
                  -Correct Minute only: <span class="font-bold">5 points</span></p><br><br>
              <p class="text-center font-bold prize-str">Prize Structure</p>
              <p style="margin: 10px 5px 0px 0px; float: left;">-Perfect predictions:</p><p class="perfect-pred text-center"> SGD 1,888</p>
              <br><br>
              <p class="text-center font-bold prize-str">Other Prizes</p>
              <p style="margin: 10px 5px 0px 0px; float: left;">-Correct Goal Scorer only: </p><p class="perfect-pred text-center"> SGD 200</p>
              <p style="margin: 10px 5px 0px 0px; float: left;">-Correct Minute only: </p><p class="perfect-pred text-center"> SGD 200</p>
              <br>
              <div class="col-md-7 col-centered no-pad">
                  <h3>Terms & Conditions</h3>
                  <ul>
                    <li> Participation is free.</li>
                    <li> Member will gain points for the seasonal leaderboard through the games offered on our site.</li>
                    <li> One entry per player.</li>
                    <li> Winners will be notified by email/sms.</li>
                    <li> Prize will be paid in credit on one of the sponsoring sites.</li>
                    <li> Limited to 1 entry per household.</li>
                    <li> All decisions regarding this offer are final and binding.</li>
                  </ul>
              </div>
		</div>
		<div id="ftppgold3" class="ftppgoldtabcont">
			<h2>THE GOLDEN GOALS LEADERBOARD</h2>
			<p>Take part in our free to play games and earn points each week for the seasonal leaderboard! After each season, if you are ranked top 100, you can receive a cash prize up to <b>SGD 15,000!</b></p>
			<table>
				<tbody>
					<tr>
						<th>Rank</th>
						<th>Points</th>
						<th>Username</th>
						<th>Prize</th>
					</tr>
					<tr>
						<td>1st</td>
						<td>123</td>
						<td>tran ng</td>
						<td>SGD15,000</td>
					</tr>
					<tr>
						<td>2nd</td>
						<td>123</td>
						<td>tran ng</td>
						<td>SGD15,000</td>
					</tr>
					<tr>
						<td>3rd</td>
						<td>123</td>
						<td>tran ng</td>
						<td>SGD15,000</td>
					</tr>
					<tr>
						<td>4th</td>
						<td>123</td>
						<td>tran ng</td>
						<td>SGD15,000</td>
					</tr>
					<tr>
						<td>5th</td>
						<td>123</td>
						<td>tran ng</td>
						<td>SGD15,000</td>
					</tr>
					<tr>
						<td>6th</td>
						<td>123</td>
						<td>tran ng</td>
						<td>SGD15,000</td>
					</tr>
					<tr>
						<td>7th</td>
						<td>123</td>
						<td>tran ng</td>
						<td>SGD15,000</td>
					</tr>
					<tr>
						<td>8th</td>
						<td>123</td>
						<td>tran ng</td>
						<td>SGD15,000</td>
					</tr>
					<tr>
						<td>9th</td>
						<td>123</td>
						<td>tran ng</td>
						<td>SGD15,000</td>
					</tr>
					<tr>
						<td>10th</td>
						<td>123</td>
						<td>tran ng</td>
						<td>SGD15,000</td>
					</tr>
				</tbody>
			</table>
			<div class="your-rank">
				<p>Your current ranking: <b>98th</b></p>
			</div>
		</div>
	</div>
	</div>
</div>
<script>
	function ftppgold(x){
		//tabs
		$('#ftppgoldtabs ul li').removeClass('ftpp10active');
		$('#ftppgoldtabs ul li:nth-of-type(' + x +')').addClass('ftpp10active');
		//tab content
		$('.ftppgoldtabcont').css('display','none')
		$('#ftppgold' + x).css('display','block')
	}
</script>
