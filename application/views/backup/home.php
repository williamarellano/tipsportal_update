<style type="text/css">
	#secMenu li:nth-of-type(1) a{background: #0e6857 !important;}
</style>
<div id="home-carousel" class="carousel slide" data-ride="carousel">
  <!-- Wrapper for slides -->
  <div class="carousel-inner" role="listbox">
    <div class="item active">
      <img src="<?=base_url();?>public/img/home-banner.jpg" alt="...">
    </div>
    <div class="item">
      <img src="<?=base_url();?>public/img/home-banner.jpg" alt="...">
    </div>
  </div>
</div>
<!-- <div class="jumbotron home-banner">
  </div> -->

<div id="home" class="container no-pad">
  <div class="col-md-8 no-pad left-bar-h">
    <div class="col-xs-12 no-pad">
      <div id="home-panel" class="col-md-12 no-pad ">
        <div class="panel panel-default no-border no-borderr no-margin">
          <div class="panel-heading text-left med">Featured Tip of the Week -
          <?php
            foreach ($tips as $t) {
              if($t['featured'] == 1)
              {
                echo $t['match_title'];
              }
            }
          ?>
          <!--  Manchester City vs Manchester United -->
          </div>
          <div class="panel-body">
          <?php
          foreach ($tips as $t) {
            if($t['featured'] == 1)
            {
          ?>
            <div class="col-sm-4 rhe home-ft-img">
              <div class="row orange-b col-sm-12 col-xs-8 col-sm-offset-0 col-xs-offset-2 no-pad" id="box-search">
                <div class="thumbnail text-center f-thumb">
                  <div class="caption">
                    
                      <div class="col-xs-8 border-time light-grey no-pad col-centered">
                        <p><?= date("m/d • h:i A", strtotime($t['date_start']));?></p>
                      </div>
                      <h6 class=" font-bold"><?=strtoupper($t['league_nickname']);?></h6>
                      <div class="col-xs-6 h-vs no-pad">
                        <img src="<?=base_url().$this->config->item('upload_teams_logo').'/'.$t['home_team_logo'];?>">
                        <div><?=$t['home_team_nickname'];?></div>
                      </div>
                      <div class="col-xs-6 h-vs no-pad" style="pading-left:4px;">
                        <img src="<?=base_url().$this->config->item('upload_teams_logo').'/'.$t['away_team_logo'];?>">
                        <div><?=$t['away_team_nickname'];?></div>
                      </div>

                  </div>
                </div>
              </div>
              <!--/Premiver League row-->
            </div>
            <!--/col-4-->
            <div id="home-panel-left" class="col-lg-8 hjs no-pad">
              <!--======Limit Characters=====-->
              <div id="home-panel-desc" class="col-xs-12 col-sm-8 hjs col-lg-12 no-pad poppins">
               <p><?= strip_tags(substr($t['tptip_desc'],0,230))."...";?></p>

               <h1 class="col-xs-8 col-sm-9 home-tip blueGreen font-bold no-pad">Tip: <?=strtoupper($t['tptip']);?></h1>
                <a href="<?=base_url();?>Welcome/blog"><button id="home-rm" type="button" name="button" class="pull-right orange no-border no-borderr montserrat">Read more</button></a>
              </div>
              <!--/home-panel-desc-->
            </div>
            <!--/col-8-->
          </div>

          <?php
              }
            }
          ?>
          <!--/panel body-->
        </div>

        <!--/panel-->
        <div id="perfect-ten" class="col-xs-12 no-pad hidden-sm hidden-xs">
<a href="<?=base_url();?>Welcome/ftpperfect10"><img src="<?=base_url();?>public/img/perfect-10.jpg" alt="The Perfect 10 League" class="img-responsive">
          </a>        </div>

        <div id="perfect-ten-mobile" class="col-xs-12 no-pad hidden-lg hidden-md">
<a href="<?=base_url();?>Welcome/ftpperfect10"><img src="<?=base_url();?>public/img/perfect-10-mobile.jpg" alt="The Perfect 10 League" class="img-responsive">
          </a>        </div>

      </div>
      <!--/home panel-->
    </div>
    <!-- /col-12-->
      <div class="col-md-7 no-pad">
      <div id="home-panel-tips" class="col-xs-12 no-pad border">
        <div class="panel panel-default no-border no-borderr">
          <div class="panel-heading text-center">Tipsters Comparison</div>
        </div>
        <div class="col-xs-12 no-pad panel panel-default no-border no-borderr text-center med white">
          <div class="col-xs-2 no-pad tipsters dark-grey">Tipster</div>
          <div class="col-xs-3 no-pad tipsters border dark-grey">Tip Form</div>
          <div class="col-xs-7 no-pad tipsters dark-grey">Match</div>
        </div>



        <?php
        $n = 1;
        $even = true;
        $odd = false;
        foreach ($tipster as $t) {
          if($n<6)
          { 
            if($even)
            {
            ?>
              <div class="col-xs-12 no-pad panel panel-default no-border no-borderr text-center font-bold white-bg">
              <div class="col-xs-2 no-pad hm-betdsa v-align">
             
                <img src="<?=base_url().$this->config->item('upload_tipsters_logo').'/'.$t['tipster_logo'];?>" class="img-responsive">
              </div>
              <div class="col-xs-3 no-pad tipsters border v-align h-align tc-m ">
                <img src="<?=base_url().$this->config->item('star_img').'/star'.$t['tip_form'];?>.png" class="star">
              </div>
              <div class="col-xs-7 no-pad tipsters tc-m d-grey v-align h-align no-pad ">
                <div class="col-xs-2  no-pad"><img src="<?=base_url().$this->config->item('upload_teams_logo').'/'.$t['home_team_logo'];?>" alt="" class=""></div>
                <div class="col-xs-7  no-pad match"><?=$t['home_team_nickname'];?><br><?=$t['away_team_nickname'];?></div>
                <div class="col-xs-2  no-pad"><img src="<?=base_url().$this->config->item('upload_teams_logo').'/'.$t['away_team_logo'];?>" alt="" class=""></div>
              </div>
              </div>
              
            <?php
              $even = false;
            }
            else{
            ?>

              <div class="col-xs-12 no-pad panel panel-default no-border no-borderr text-center font-bold l-grey">
              <div class="col-xs-2 no-pad hm-betdsa v-align">
             
                <img src="<?=base_url().$this->config->item('upload_tipsters_logo').'/'.$t['tipster_logo'];?>" class="img-responsive">
              </div>
              <div class="col-xs-3 no-pad tipsters border v-align h-align tc-m ">
                <img src="<?=base_url().$this->config->item('star_img').'/star'.$t['tip_form'];?>.png" class="star">
              </div>
              <div class="col-xs-7 no-pad tipsters tc-m d-grey v-align h-align no-pad ">
                <div class="col-xs-2  no-pad"><img src="<?=base_url().$this->config->item('upload_teams_logo').'/'.$t['home_team_logo'];?>" alt="" class=""></div>
                <div class="col-xs-7  no-pad match"><?=$t['home_team_nickname'];?><br><?=$t['away_team_nickname'];?></div>
                <div class="col-xs-2  no-pad"><img src="<?=base_url().$this->config->item('upload_teams_logo').'/'.$t['away_team_logo'];?>" alt="" class=""></div>
              </div>
              </div>
            <?php
              $even = true;
            }

        ?>

          
        <?php
          }
          $n++;
        }
        ?>

        <div class="col-xs-12 no-pad panel panel-default no-border no-borderr 

        <div class="col-xs-12 text-center more-b no-pad">
          <a href="<?=base_url();?>Welcome/tcomparison" class="">
            <p class="hm-more no-margin text-center">More</p>
          </a>
        </div>
      </div>
    </div>
    <br>
    <div id="side-ad" class="col-md-5 no-pad hm-side-tip">
      <a href="<?=base_url();?>Welcome/goldengoal"><img src="<?=base_url();?>public/img/home-side-ads.jpg" alt="side advert" class="no-pad no-margin img-responsive">
        </a>
    </div>

    <div class="col-xs-12 no-pad border">
      <div class="col-xs-12 no-pad">
        <div class="panel-heading uls-head text-center">Upcoming Live Streams</div>
      </div>
      <div class="uls-hm-cont">
        <div class="uls-hm-feed">
          <div class="col-xs-12 uls-hm-feed-c no-pad no-margin white-bg">
            <div class="col-xs-12 col-md-3 hPl uls-hm text-center no-pad aa h-align v-align">
              <p class="text-center no-margin">Premier League <br> <span class="lsmdate hidden-sm hidden-xs">01 Aug - 19:30</span></p>
            </div>

            <div class="col-xs-12 col-md-7 hLeague no-pad h-align v-align aa">
              <div class="col-xs-2 no-pad v-align h-align">
                <img src="<?=base_url();?>public/img/burnley38x38.png" alt="" class=" pull-right">
              </div>
              <div class="col-xs-3 no-pad match">
                <p class="no-margin text-left">Man City</p>
              </div>
              <div class="col-xs-1 no-pad match text-center ">
                <p class="no-margin">vs</p>
              </div>
              <div class="col-xs-3 no-pad match">
                <p class="no-margin text-right">Man Utd</p>
              </div>
              <div class="col-xs-2 no-pad h-align v-align">
                <img src="<?=base_url();?>public/img/burnley38x38.png" alt="" class="pull-left">
              </div>
            </div>

            <div class="hWatch col-xs-12 col-md-2 no-pad text-center h-align v-align aa">
              <a href="<?=base_url();?>Welcome/livestream"><button class="submit no-pad h-align v-align" type="button" name="submit">Watch</button></a>
            </div>
          </div>
        </div>

        <div class="uls-hm-feed">
          <div class="col-xs-12 uls-hm-feed-c no-pad no-margin wash">
            <div class="col-xs-12 col-md-3 hPl uls-hm text-center no-pad lsm-b aa h-align v-align">
              <p class="text-center no-margin">Premier League <br> <span class="lsmdate hidden-sm hidden-xs">01 Aug - 19:30</span></p>
            </div>

            <div class="col-xs-12 col-md-7 hLeague no-pad h-align v-align aa">
              <div class="col-xs-2 no-pad v-align h-align">
                <img src="<?=base_url();?>public/img/burnley38x38.png" alt="" class=" pull-right">
              </div>
              <div class="col-xs-3 no-pad match">
                <p class="no-margin text-left">Man City</p>
              </div>
              <div class="col-xs-1 no-pad match text-center ">
                <p class="no-margin">vs</p>
              </div>
              <div class="col-xs-3 no-pad match">
                <p class="no-margin text-right">Man Utd</p>
              </div>
              <div class="col-xs-2 no-pad h-align v-align">
                <img src="<?=base_url();?>public/img/burnley38x38.png" alt="" class="pull-left">
              </div>
            </div>

            <div class="hWatch col-xs-12 col-md-2 no-pad text-center h-align v-align aa">
              <a href="<?=base_url();?>Welcome/livestream"><button class="submit no-pad h-align v-align" type="button" name="submit">Watch</button></a>
            </div>
          </div>
        </div>

        <div class="uls-hm-feed">
          <div class="col-xs-12 uls-hm-feed-c no-pad no-margin white-bg">
            <div class="col-xs-12 col-md-3 hPl uls-hm text-center no-pad lsm-b aa h-align v-align">
              <p class="text-center no-margin">Premier League <br> <span class="lsmdate hidden-sm hidden-xs">01 Aug - 19:30</span></p>
            </div>

            <div class="col-xs-12 col-md-7 hLeague no-pad h-align v-align aa">
              <div class="col-xs-2 no-pad v-align h-align">
                <img src="<?=base_url();?>public/img/burnley38x38.png" alt="" class=" pull-right">
              </div>
              <div class="col-xs-3 no-pad match">
                <p class="no-margin text-left">Man City</p>
              </div>
              <div class="col-xs-1 no-pad match text-center ">
                <p class="no-margin">vs</p>
              </div>
              <div class="col-xs-3 no-pad match">
                <p class="no-margin text-right">Man Utd</p>
              </div>
              <div class="col-xs-2 no-pad h-align v-align">
                <img src="<?=base_url();?>public/img/burnley38x38.png" alt="" class="pull-left">
              </div>
            </div>

            <div class="hWatch col-xs-12 col-md-2 no-pad text-center h-align v-align aa">
              <a href="<?=base_url();?>Welcome/livestream"><button class="submit no-pad h-align v-align" type="button" name="submit">Watch</button></a>
            </div>
          </div>
        </div>

        <div class="uls-hm-feed">
          <div class="col-xs-12 uls-hm-feed-c no-pad no-margin wash">
            <div class="col-xs-12 col-md-3 hPl uls-hm text-center no-pad lsm-b aa h-align v-align">
              <p class="text-center no-margin">Premier League <br> <span class="lsmdate hidden-sm hidden-xs">01 Aug - 19:30</span></p>
            </div>

            <div class="col-xs-12 col-md-7 hLeague no-pad h-align v-align aa">
              <div class="col-xs-2 no-pad v-align h-align">
                <img src="<?=base_url();?>public/img/burnley38x38.png" alt="" class=" pull-right">
              </div>
              <div class="col-xs-3 no-pad match">
                <p class="no-margin text-left">Man City</p>
              </div>
              <div class="col-xs-1 no-pad match text-center ">
                <p class="no-margin">vs</p>
              </div>
              <div class="col-xs-3 no-pad match">
                <p class="no-margin text-right">Man Utd</p>
              </div>
              <div class="col-xs-2 no-pad h-align v-align">
                <img src="<?=base_url();?>public/img/burnley38x38.png" alt="" class="pull-left">
              </div>
            </div>

            <div class="hWatch col-xs-12 col-md-2 no-pad text-center h-align v-align aa">
              <a href="<?=base_url();?>Welcome/livestream"><button class="submit no-pad h-align v-align" type="button" name="submit">Watch</button></a>
            </div>
          </div>
        </div>

        <div class="uls-hm-feed">
          <div class="col-xs-12 uls-hm-feed-c no-pad no-margin white-bg">
            <div class="col-xs-12 col-md-3 hPl uls-hm text-center no-pad aa h-align v-align">
              <p class="text-center no-margin">Premier League <br> <span class="lsmdate hidden-sm hidden-xs">01 Aug - 19:30</span></p>
            </div>

            <div class="col-xs-12 col-md-7 hLeague no-pad h-align v-align aa">
              <div class="col-xs-2 no-pad v-align h-align">
                <img src="<?=base_url();?>public/img/burnley38x38.png" alt="" class=" pull-right">
              </div>
              <div class="col-xs-3 no-pad match">
                <p class="no-margin text-left">Man City</p>
              </div>
              <div class="col-xs-1 no-pad match text-center ">
                <p class="no-margin">vs</p>
              </div>
              <div class="col-xs-3 no-pad match">
                <p class="no-margin text-right">Man Utd</p>
              </div>
              <div class="col-xs-2 no-pad h-align v-align">
                <img src="<?=base_url();?>public/img/burnley38x38.png" alt="" class="pull-left">
              </div>
            </div>

            <div class="hWatch col-xs-12 col-md-2 no-pad text-center h-align v-align aa">
              <a href="<?=base_url();?>Welcome/livestream"><button class="submit no-pad h-align v-align" type="button" name="submit">Watch</button></a>
            </div>
          </div>
        </div>
      </div>

      <div class="col-xs-12 text-center hmore-b no-pad">
        <a href="<?=base_url();?>Welcome/tcomparison" class="">
          <p class="hm-more no-margin">More</p>
        </a>
      </div>
    </div>

  </div>

  <!--/col-md-8-->
  <!--LeftPanel-->
  <div class="clearfix hidden-lg hidden-md"></div>
  <div class="col-md-4 side-bar-h sideTitle dark-bg">
    Fixtures
  </div>
  <div id="home-panel-right" class="col-md-4 side-bar-h border pull-right">
    <div class="col-xs-12 no-pad">
      <!-- <div class="hpr-title">
          <h4><strong>Football Scores</strong></h4>
        </div> -->
      <div class="col-xs-12 page-header no-margin no-pad">
        <!-- <h5 class="side-utitle pull-left"><strong>Champions League</strong></h5> -->

        <div class="btn-group " style="padding-top:8px;">
          <button class="btn btn-default side-btn btn-xs dropdown-toggle blueGreen font-bold" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <span id='league_active_f'></span>  <span class="caret"></span>
            </button>
          <ul class="dropdown-menu f-l" id='league_list_f'>
            
          </ul>
        </div>
      </div>
    </div>

    <div id="hm-champ-lg" class="col-xs-12 container no-pad">
     <p class="champ-text uppercase wash text-center med text-uppercase" id='datefixture'> </p>
      <div class="row">
        <div class="col-xs-12 no-pad col-centered text-center first-champ" id='fixtures'>
          
        </div>

        <div class="col-xs-12 text-center vm-pa">
          <a href="<?=base_url();?>Welcome/livescore" class="vm" role="button">
            <p class="med no-margin">View More</p>
          </a>
        </div>
        <!-- <div class="col-xs-12 text-center more-b no-pad">
            <a href="<?=base_url();?>Welcome/tcomparison" class="">
              <p class="hm-more no-margin">More</p>
            </a>
          </div> -->
      </div>
      <!--/row-->
    </div>
    <div class="clearfix"></div>
  </div>
  <!-- /col-4-->
  <!--RightPanel-->
  <div class="clearfix hidden-lg hidden-md"></div>
  <div class="col-md-4 side-bar-h sideTitle dark-bg">
    League Table
  </div>
  <div id="side-football-tables" class="col-xs-12 col-md-4 side-bar-h border pull-right">
    <div class="col-xs-12 no-pad">
      <div class="col-xs-12 page-header no-margin no-pad">
        <!-- <h5 class="side-utitle pull-left"><strong>Scottist Prem </strong></h5> -->
        <div class="btn-group pull-left" style="padding-top:8px;">
          <button class="btn btn-default side-btn btn-xs dropdown-toggle blueGreen font-bold" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              <span id="league_active_s">  </span> <span class="caret"></span>
            </button>
          <ul class="dropdown-menu f-l side-bar-h" id="league_list_s">
           
          </ul>
          <input type="hidden" id="prio_id" name="">
        </div>
      </div>
    </div>
    <div id="side-table-scores" class="col-xs-12">
      <table class="table table-hover">
        <thead class="home-thead">
          <tr>
            <th> </th>
            <th>Team</th>
            <th>P</th>
            <th>GD</th>
            <th>Pts</th>
          </tr>
        </thead>
        <tbody id='tbodyl'>
         
         
        </tbody>
      </table>
    </div>
  </div>
</div>
<?php $this->load->view("template/footer");  ?>
<script src="<?=base_url()?>public/js/moment.min.js"></script>
<script>
$(document).ready(function() {
  getLeaguelist();
  
   var start =  moment();
   $('#datefixture').text(start.format('dddd do MMMM YYYY'));
    // $('#inputdaterange').val( + ',' + start.format('YYYY-M-D'));

 });


  function getLeaguelist()
{
      $.ajax({
        url: "<?=base_url();?>HomeCms/getLeague", 
        type: "POST", 
        dataType: 'json', 
         
       
        success: function(data, textStatus, jqXHR)

        {
          var html = '';

          for(var x = 0 ; x<data.length ; x++){
            if(data[x]['priority']==1){
              $('#league_active_f').text(data[x]['league_name'] +" - "+data[x]['country_name']);
            }
            
            html += "<li style='cursor:pointer' onclick=\"loadFTable('"+data[x]['season_id']+"','"+data[x]['league_name']+"','"+data[x]['country_name']+"')\"><a>"+data[x]['league_name']+"-"+data[x]['country_name']+"</a></li>";

          }
          $('#league_list_f').html(html);

           var html = '';

          for(var x = 0 ; x<data.length ; x++){
            if(data[x]['priority']==1){
              $('#league_active_s').text(data[x]['league_name'] +" - "+data[x]['country_name']);
            }
            
            html += "<li style='cursor:pointer' onclick=\"loadLtable('"+data[x]['season_id']+"','"+data[x]['league_name']+"','"+data[x]['country_name']+"')\"><a>"+data[x]['league_name']+"-"+data[x]['country_name']+"</a></li>";

          }
          $('#league_list_s').html(html);
          var league_val = $('#league_active_s').text();

          var league_arr = league_val.split('-');
          // console.log(league_arr);
          getPriority(league_arr[0],league_arr[1]);

          
       }}); 
}
function loadFTable(season_id,league_name,country_name){
   $('#league_active_f').text(league_name +" - "+country_name);
   $.ajax({
        url: "<?=base_url();?>HomeCms/getFixtures", 
        type: "POST", 
        dataType: 'json', 
         data:{season_id:season_id},
       
        success: function(data, textStatus, jqXHR)

        {
          console.log(data)
        }});
}
function getPriority(league_name,country_name){
  $.ajax({
        url: "<?=base_url();?>HomeCms/getPriority", 
        type: "POST", 
        dataType: 'json', 
        success: function(data, textStatus, jqXHR)

        {
          var sid = data[0]['season_id'];
          
         loadLtable(sid,league_name,country_name);
          
       }}); 
}
function loadLtable(season_id,league_name,country_name)
{
  // if($('#league_active_s').text() != "undefined - undefined"){
    $('#league_active_s').text(league_name +" - "+country_name);

  // }
  

 $.ajax({
        url: "<?=base_url();?>HomeCms/getStandings", 
        type: "POST", 
        dataType: 'json', 
         data:{season_id:season_id},
       
        success: function(data, textStatus, jqXHR)

        {
          // console.log(data);
          formatarr = data['format'][0]['format_standing'].split(',');
          var topcnt = parseInt(formatarr[0]);
          var highcnt = parseInt(formatarr[1]);
          var midcnt = parseInt(formatarr[2]);
          var lowcnt = parseInt(formatarr[3]);
          var cnt = 0;
          var dash = 0;
          var html ="";
          // console.log(topcnt);
          for(var x = 0 ; x<topcnt ; x++){
            cnt +=1;
            html +="<tr class='success'>";
            html += "<td class='dash' scope='row'>"+cnt+"</td>";
            html += "<th class='dash'>"+data['data'][x]['team_name']+"</th>";
            html += "<td class='dash'>"+data['data'][x]['games_played']+"</td>";
            html += "<td class='dash'>"+data['data'][x]['goal_diff']+"</td>";
            html += "<td class='table-cell-bold'>"+data['data'][x]['points']+"</td>";

            html +="<tr>";

          }
         
            // cnt-=1;
          for(var x = 0 ; x<highcnt ; x++){
            // dash = cnt + 1 ;
            cnt+=1;
            html +="<tr class='tble-yellow'>";
            html += "<td class='dash' scope='row'>"+cnt+"</td>";
            html += "<th class='dash'>"+data['data'][cnt-1]['team_name']+"</th>";
            html += "<td class='dash'>"+data['data'][cnt-1]['games_played']+"</td>";
            html += "<td class='dash'>"+data['data'][cnt-1]['goal_diff']+"</td>";
            html += "<td class='table-cell-bold'>"+data['data'][cnt-1]['points']+"</td>";
            html +="<tr>";
            
            

          }
          
        
         for(var x = 0 ; x<midcnt ; x++){
           
            cnt+=1;

            html +="<tr class=''>";
            html += "<td class='dash' scope='row'>"+cnt+"</td>";
            html += "<th class='dash'>"+data['data'][cnt-1]['team_name']+"</th>";
            html += "<td class='dash'>"+data['data'][cnt-1]['games_played']+"</td>";
            html += "<td class='dash'>"+data['data'][cnt-1]['goal_diff']+"</td>";
            html += "<td class='table-cell-bold'>"+data['data'][cnt-1]['points']+"</td>";
            html +="<tr>";
           
          }
          for(var x = 0 ; x<lowcnt ; x++){
            cnt+=1;
            html +="<tr class='tble-red'>";
            html += "<td class='dash' scope='row'>"+cnt+"</td>";
            html += "<th class='dash'>"+data['data'][cnt-1]['team_name']+"</th>";
            html += "<td class='dash'>"+data['data'][cnt-1]['games_played']+"</td>";
            html += "<td class='dash'>"+data['data'][cnt-1]['goal_diff']+"</td>";
            html += "<td class='table-cell-bold'>"+data['data'][cnt-1]['points']+"</td>";
            html +="<tr>"

          }
          
         
          $('#tbodyl').html(html);


          
       }}); 
}

</script>
