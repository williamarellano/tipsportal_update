<div class="loader" id="loader">
</div>
<body class="hold-transition skin-green sidebar-mini">
<!-- Site wrapper -->
<div class="wrapper">

 

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper margin-top_cont">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Stage Masterlist
       
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Stage Masterlist</a></li>
        
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title"><i class='fa fa-table'></i> Stage Masterlist Table</h3>

          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                    title="Collapse">
              <i class="fa fa-minus"></i></button>
         
          </div>
        </div>
        <div class="box-body">
          <div class="container" style='margin-left: 35px;'>
             <!-- <div id="overlay"><img src="<?=base_url();?>public/img/loading.gif" alt="Be patient..." /></div> -->
        <div class="row">
            <div class="col-sm-6" >
              <table class="categorytable">
                <thead>
                  <th>
                   <div class="dropdown">
                          <button style="margin-bottom: 15px;" class="btn btn-secondary btn-primary dropdown-toggle btncat" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            League Names <i class="fa fa-caret-down"></i>
                          </button>
                          <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                            <a class="dropdown-item" id='addLeague' role="button" href="javascript:void(0);" onclick='addShowL();'>Add</a>
                            <a class="dropdown-item" href="javascript:void(0);" onclick='edtiShowL();'>Edit</a>
                            <hr>
                            <a class="dropdown-item" href="javascript:void(0);" onclick="delShowL()">Delete</a>
                          </div>
                    </div>
                  </th>    
                </thead>

                <tbody>
                  <tr>
                    <td>
                      <select size="15" class="select_category form-control" id='league'>
                        
                      </select>
                    </td>
                    <td>

                    
                    </td>
                  </tr>
                </tbody>
              </table>  
              </div>
            <div class="col-sm-6" >
              <table class="categorytable" >
                    <thead>
                      <th>
                        <div class="dropdown">
                          <button style="margin-bottom: 15px;" class="btn btn-secondary btn-primary dropdown-toggle btncat" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            Stages Name <i class="fa fa-caret-down"></i>
                          </button>
                          <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                            <a class="dropdown-item" id='addBtn' role="button" href="javascript:void(0);" onclick='addShow();'>Add</a>
                            <a class="dropdown-item" href="javascript:void(0);" onclick='editShow();'>Edit</a>
                            <hr>
                            <a class="dropdown-item" href="javascript:void(0);" onclick="delShow()">Delete</a>
                          </div>
                        </div>
                      </th>
                    </thead>

                    <tbody>
                      <tr>
                   
                        <td>

                          <select size="15" class="select_category form-control" id='stages'>
                            <option id="op">Select first the League on the left Pane</option>
                          </select>
                        </td>
                      </tr>
                    </tbody>
                  </table> 
            </div> 
          </div>
        </div>
        </div>
        <!-- /.box-body -->
        <div class="box-footer">
         
        </div>
        <!-- /.box-footer-->
      </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

<footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> 2.4.0
    </div>
    <strong>Copyright &copy; 2018 <a href="#">Siegreich Solutions inc.</a></strong> All rights
    reserved.
  </footer>

 <!-- MODAL FOR DELETION -->
<div class="modal fade delModal" tabindex="-1" role="dialog" aria-hidden="true" sstyle="width:400px" id=delCatModal>
  <div class="modal-dialog modal-sm">
    <div class="modal-content">

      <div class="modal-header danger-modal">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
        </button>
        <h4 class="modal-title" id="delModalLabel">DELETE STAGE?</h4>
      </div>
      <div class="modal-body">
        <span class='colorblack' style='font-size:18px'>League Name: </span><b><span class='colorred' id=del_body style='font-size:18px'></span></b><br>
         <span class='colorblack' style='font-size:18px'>Stage ID: </span><b><span class='colorred' id=del_title style='font-size:18px'></span></b><br>
        <span class='colorblack' style='font-size:18px'>Stage Name: </span><b><span class='colorred' id=del_sub style='font-size:18px'></span></b><br>
        <br>

       
      </div>
      <div class="modal-footer">
        <input type=hidden id=delid value=''>
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
        <button type="button" class="btn btn-danger" onclick='rmSubcat();'>Delete</button>
      </div>

    </div>
  </div>
</div>
    <!-- Modal -->
  <div class="modal fade" id="addStageModal" role="dialog">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header primary-modal">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Add Stage</h4>
        </div>
        <div class="modal-body">
           <?php echo form_open_multipart(base_url( 'addStage' ), array( 'id' => 'add-stages-form', 'class' => 'form-horizontal form-label-left' )); ?>
              <div class="box-body">
                
                <div class="form-group">
                  <label for="league_id">League ID:</label>
                  <input type="text" class="form-control" id="league_id" placeholder="Category" readonly> 
                </div>

                <div class="form-group">
                  <label for="stage_name"><span style="color:red">*</span> Stage Name:</label>
                  <input type="text" class="form-control" id="stage_name" placeholder="Stage Name">
                </div>
                 <!-- Date and time range -->
              <div class="form-group">
                <label>Date Start and Date End:</label>

                <div class="input-group">
                  <div class="input-group-addon">
                    <i class="fa fa-clock-o"></i>
                  </div>
                  <input type="text" class="form-control" id="dstartend">
                </div>
                <!-- /.input group -->
              </div>
             
           </div>
        </div>

        <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            <button type="submit" class="btn btn-primary">Submit</button>
          <?php echo form_close(); ?>
        </div>
      </div>
    </div>
  </div>

<div class="modal fade catModal" tabindex="-1" role="dialog" aria-hidden="true" sstyle="width:600px" id=editStagesModal>
  <div class="modal-dialog modal-md">
    <div class="modal-content">

      <div class="modal-header info-modal">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
        </button>
        <h4 class="modal-title" id="editModalLabel">EDIT STAGE<span id=editStageID style='font-size:18px'></span></h4>

      </div>
      <div class="modal-body">

        <form class="form-horizontal form-label-left">
          <div class="form-group">
            <label class="control-label col-md-4 col-sm-4 col-xs-12">League ID:</label>
            <div class="col-md-6 col-sm-6 col-xs-12">
              <input type="text" id="editLeague" name="" class="form-control col-md-7 col-xs-12" placeholder="League Name" readonly="">
            </div>
          </div>
        </form>

         
        <form class="form-horizontal form-label-left">
          <div class="form-group">
            <label class="control-label col-md-4 col-sm-4 col-xs-12">Stage Name:</label>
            <div class="col-md-6 col-sm-6 col-xs-12">
              <input type="text" id="editStage" name="" class="form-control col-md-7 col-xs-12" placeholder="Stage Name">
            </div>
          </div>
        </form>   

        <form class="form-horizontal form-label-left">
          <div class="form-group">
            <label class="control-label col-md-4 col-sm-4 col-xs-12">Date Start And Date End:</label>
            <div class="col-md-6 col-sm-6 col-xs-12">
                <div class="input-group" style='width:307px'>
                  <div class="input-group-addon">
                    <i class="fa fa-clock-o"></i>
                  </div>
                  <input type="text" class="form-control pull-right" id="editdstartend">
                </div>
            </div>
          </div>
        </form>   

        

        
      </div>
      <div class="modal-footer">
        <input type=hidden id=editID value=''>
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-info" onclick='updateStage();'>Save</button>
      </div>

    </div>
  </div>
</div>

<!-- EDIT POST END -->
<!-- Modal Add league-->
  <div class="modal fade" id="addLeagueModal" role="dialog">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header primary-modal">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Add League</h4>
        </div>
        <div class="modal-body">
           <?php echo form_open_multipart(base_url( 'addLeague' ), array( 'id' => 'add-leagues-form', 'class' => 'form-horizontal form-label-left' )); ?>
              <div class="box-body">
                

              <div class="form-group">
                  <label for="stage_name"><span style="color:red">*</span> League Name:</label>
                  <input type="text" class="form-control" id="league_name" placeholder="League Name">
              </div>
              <div class="form-group">
              <label for="status"><span style="color:red">*</span>  Country:</label>
                  <select class="form-control" id="addcountry">
                    <option value=''>SELECT COUNTRY..</option>
                  </select>
              </div>
              <!-- Date and time range -->
              <div class="form-group">
                <label>Date Start and Date End:</label>
                <div class="input-group">
                  <div class="input-group-addon">
                    <i class="fa fa-clock-o"></i>
                  </div>
                  <input type="text" class="form-control" id="dstartend_l">
                </div>
                <!-- /.input group -->
              </div>
               
           </div>
        </div>

        <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            <button type="submit" class="btn btn-primary">Submit</button>
          <?php echo form_close(); ?>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- ./wrapper -->
</div>
<!-- ./wrapper -->


<?php $this->load->view('templates/admin_footer');?>

<script>

$( document ).ready(function() {
  $('#loader').hide();
  getLeague();
   // League SELECT
  $('#league').change(function(){
     
      var league_id = $('#league').val(); 
      $('#stage_name').val('');
       getStages(league_id); 

     
  });
  // Stage SELECT
   $('#stages').change(function(){

    var id = $(this).children(":selected").attr("id");

    $('#stages').attr('data-id', id);

  });


  
});
$(function() {
  $('#dstartend').daterangepicker({
    timePicker: true,
    startDate: moment().startOf('hour'),
    endDate: moment().startOf('hour').add(32, 'hour'),
    locale: {
      format: 'YYYY/MM/DD HH:mm:ss'
    }
  });
});
$(function() {
  $('#dstartend_l').daterangepicker({
    timePicker: true,
    startDate: moment().startOf('hour'),
    endDate: moment().startOf('hour').add(32, 'hour'),
    locale: {
      format: 'YYYY/MM/DD HH:mm:ss'
    }
  });
});
   // ADD Stages
 $("#add-leagues-form").submit(function(e){
        e.preventDefault();
        
        addLeague();
    });
   // ADD Stages
 $("#add-stages-form").submit(function(e){
        e.preventDefault();
        
        addStages();
    });
function addStages()
{
    
    var league_id = $('#league_id').val();
    var stage_name = $('#stage_name').val();
    
    var dstartend = $('#dstartend').val();
       

    if(league_id==''||stage_name==''||dstartend=='')
    {
      swal("Error Add Stages!","Please Input All Fields","error");
    }
    
    else
    {
            var formData = new FormData();
            formData.append('league_id', league_id);
            formData.append('stage_name', stage_name);
           
            formData.append('dstartend', dstartend);

            

            //console.log($('#upload_save'));
            $.ajax({
              url :  "addStage",
              type: "POST",
              dataType : 'json',
              dom: 'Bfrtip',
              cache :  false,
              contentType : false,
              processData : false,
              async: false,
              data : formData,
              success: function(data, textStatus, jqXHR){
                
                swal(data.alert_ttl, data.alert_msg, data.alert_typ);

                if (data.alert_typ == 'success'){
                   swal(data.alert_ttl, data.alert_msg, data.alert_typ);
                    $("#addStageModal").modal('toggle'); 
                    $('#stage_name').val('');
                    getStages(league_id); 
                       
                }else{

                  swal(data.alert_ttl, data.alert_msg, data.alert_typ);

                 
                }
              },
              error: function (jqXHR, textStatus, errorThrown)
              {
    
              } 


            });
    }
}
function addShow()
{
  var league = $('#league').val();
  if(league==null){
        swal("ERROR MESSAGE", "CHOOSE FIRST LEAGUE" , "error");
       
      }
      else{
        $('#league_id').val(league);
        $('#addStageModal').modal('show');

      }
}
function addShowL()
{
  $('#league_id').val(league);
  $('#addLeagueModal').modal('show');
}
function addLeague()
{
    
    var league_name = $('#league_name').val();
    var country = $('#addcountry').val();
    var dstartend = $('#dstartend_l').val();
 
    if(league_name==''||country==''||dstartend=='')
    {
      swal("Error Add Stages!","Please Input All Fields","error");
    }
    
    else
    {
            var formData = new FormData();
            formData.append('league_name', league_name);
            formData.append('country', country);
            formData.append('dstartend', dstartend);

            

            //console.log($('#upload_save'));
            $.ajax({
              url :  "addLeague",
              type: "POST",
              dataType : 'json',
              dom: 'Bfrtip',
              cache :  false,
              contentType : false,
              processData : false,
              async: false,
              data : formData,
              success: function(data, textStatus, jqXHR){
                
                swal(data.alert_ttl, data.alert_msg, data.alert_typ);

                if (data.alert_typ == 'success'){
                   swal(data.alert_ttl, data.alert_msg, data.alert_typ);
                    $("#addLeagueModal").modal('toggle'); 
                    $('#league_name').val('');
                    // getStages(league_id); 
                       
                }else{

                  swal(data.alert_ttl, data.alert_msg, data.alert_typ);

                 
                }
              },
              error: function (jqXHR, textStatus, errorThrown)
              {
    
              } 


            });
    }
}
function editShow()
{

  var league = $('#league').val();
  var stages = $('#stages').val();
  var sid = $('#stages').attr('data-id');
  getStageDetails(sid);
 
  if(stages==null||league==null){
        swal("ERROR MESSAGE", "CHOOSE LEAGUE/STAGES" , "error");
       
      }
  else if(stages=='no'){
     swal("ERROR MESSAGE", "STAGES EMPTY" , "error");
  }
  else{
        $('#editLeague').val(league);
        $('#editStage').val(stages);
        $('#editStagesModal').modal('show');
      }
}
function updateStage()
{

    var league = $('#editLeague').val();
    var stages = $('#editStage').val();
   
    var startend = $('#editdstartend').val();
    var sid = $('#stages').attr('data-id');
   
    


    if(league==''||stages==''||sid==''||startend=='')
    {
      swal("Error Edit Stage!","Please Input All Fields","error");
    }
    
    else
    {
            var formData = new FormData();
            formData.append('sid', sid);
            formData.append('league', league);
            formData.append('stages', stages);
           
            formData.append('startend', startend);

            

            //console.log($('#upload_save'));
            $.ajax({
              url :  "editStage",
              type: "POST",
              dom: 'Bfrtip',
              dataType : 'json',
              cache :  false,
              contentType : false,
              processData : false,
              async: false,
              data : formData,
              success: function(data, textStatus, jqXHR){
                
                swal(data.alert_ttl, data.alert_msg, data.alert_typ);

                if (data.alert_typ == 'success'){
                   swal(data.alert_ttl, data.alert_msg, data.alert_typ);
                    $("#editStagesModal").modal('toggle');  
                    getStages(league); 
                       
                }else{
                  swal(data.alert_ttl, data.alert_msg, data.alert_typ);

                 
                }
              },
              error: function (jqXHR, textStatus, errorThrown)
              {
    
              } 


            });
    }

}
function delShow()
{
  var league = $('#league').val();
  var stages = $('#stages').val();
  var sid = $('#stages').attr('data-id');

  if(league==null||stages==null){
        swal("ERROR MESSAGE", "CHOOSE LEAGUE/STAGES" , "error");
       
      }
  else if(stages=='no'){
     swal("ERROR MESSAGE", "STAGES EMPTY" , "error");
  }
  else{
        $('#del_title').html(sid);
        $('#del_body').html(league);
        $('#del_sub').html(stages);
        $('#delCatModal').modal('show');
      }
}
function rmSubcat()
{
  var sid =  $('#del_title').html();
  var league =  $('#del_body').html();
  var formData = new FormData();
  formData.append('sid', sid);

    $.ajax({
              url :  "delStage",
              type: "POST",
              dom: 'Bfrtip',
              dataType : 'json',
              cache :  false,
              contentType : false,
              processData : false,
              async: false,
              data : formData,
              success: function(data, textStatus, jqXHR){
                
                swal(data.alert_ttl, data.alert_msg, data.alert_typ);

                if (data.alert_typ == 'success'){
                   swal(data.alert_ttl, data.alert_msg, data.alert_typ);
                    $("#delCatModal").modal('toggle');  
                    getStages(league); 
                       
                }else{
                  swal(data.alert_ttl, data.alert_msg, data.alert_typ);

                 
                }
              },
              error: function (jqXHR, textStatus, errorThrown)
              {
    
              } 


            });
}
function getStageDetails(sid)
{
  var formData = new FormData();
  formData.append('sid', sid);

    $.ajax({
              url :  "getStageD",
              type: "post",
              dom: 'Bfrtip',
              dataType : 'json',
              cache :  false,
              contentType : false,
              processData : false,
              processing: true,
              async: false,
              data: formData,
              success: function(data, textStatus, jqXHR){
                 
              // $('#editdstartend').val(data[0]['date_start'] + ' - ' + data[0]['date_end']);
              $('#editdstartend').daterangepicker({
                timePicker: true,
                startDate: data[0]['date_start'],
                endDate: data[0]['date_end'],
                locale: {
                  format: 'YYYY/MM/DD HH:mm:ss'
                }
              });
                
              },
              error: function (jqXHR, textStatus, errorThrown)
              {
    
              } 


            });
}
function getStages(league_id)
{
 
  var formData = new FormData();
  formData.append('league_id', league_id);

          $.ajax({
              url :  "getStage",
              type: "post",
              dom: 'Bfrtip',
              dataType : 'json',
              cache :  false,
              contentType : false,
              processData : false,
              processing: true,
              async: false,
              data: formData,
              success: function(data, textStatus, jqXHR){
                 
              var length = data.length;
              var html = '';
              $('#stages option').remove();

              if(data['alert_typ']=='error'){
                $('#stages').append("<option value='no'>"+"No Stages"+"</option>");
              }
              else{
              

                for(var i=0;i<data.length;i++){
                 $('#stages').append("<option id="+data[i]['stage_id']+">"+data[i]['stage_name']+"</option>");
                }

              }

                
              },
              error: function (jqXHR, textStatus, errorThrown)
              {
    
              } 


            });
}
function getLeague()
{
 
  $.ajax({
              url :  "getLeague",
              type: "post",
              dom: 'Bfrtip',
              dataType : 'json',
              cache :  false,
              contentType : false,
              processData : false,
              processing: true,
              async: false,
              success: function(data, textStatus, jqXHR){
                 
              var length = data.length;
              var html = '';
              

                for(var i=0;i<data.length;i++){
                 $('#league').append("<option value="+data[i]['league_id']+" >"+data[i]['country_name']+" - "+data[i]['league_name']+"</option>");
                }

                
              },
              error: function (jqXHR, textStatus, errorThrown)
              {
    
              } 


            });
}
function loadCountries()
{
 
  $.ajax({
              url :  "getCountries",
              type: "post",
              dom: 'Bfrtip',
              dataType : 'json',
              cache :  false,
              contentType : false,
              processData : false,
              processing: true,
              async: false,
              success: function(data, textStatus, jqXHR){
                 
              var length = data.length;
              var html = '';
              

                for(var i=0;i<data.length;i++){
                 $('#addcountry').append("<option>"+data[i]['country_name']+"</option>");
                }

                
              },
              error: function (jqXHR, textStatus, errorThrown)
              {
    
              } 


            });
}
 
</script>