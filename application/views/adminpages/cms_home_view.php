<div class="loader" id="loader">
</div>
<body class="hold-transition skin-green sidebar-mini">
<!-- Site wrapper -->
<div class="wrapper">

 

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper margin-top_cont">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Home CMS
      
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home CMS</a></li>
       
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title">Edit</h3>

          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                    title="Collapse">
              <i class="fa fa-minus"></i></button>
          
          </div>
        </div>
    <div class="box-body">
      <div class="row">
        <!-- left column -->
        <div class="col-md-6">
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Table League Standings</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
           
              <div class="box-body">
              <div class="form-group">
                <label>League List </label>
                  <select class="form-control select2" id='league_list_s' style="width: 100%;">
                    
                  </select>
              </div>
                <div class="form-group">
                <label>Priority</label>
                  <select class="form-control" id='priority_sel' style="width: 100%;">
                      <?php for($x = 1 ; $x<=10 ; $x++) {?>
                        <option><?php echo (string)$x ?></option>
                      <?php } ?>
                  </select>
                </div>
                <div class="form-group">
                <label>Number of Top Teams</label>
                  <select class="form-control" id='num_top_sel' style="width: 100%;">
                      <?php for($x = 1 ; $x<=10 ; $x++) {?>
                        <option><?php echo $x ?></option>
                      <?php } ?>
                  </select>
                </div>
                 <div class="form-group">
                <label>Number of High Teams</label>
                  <select class="form-control" id='num_high_sel' style="width: 100%;">
                      <?php for($x = 1 ; $x<=10 ; $x++) {?>
                        <option><?php echo $x ?></option>
                      <?php } ?>
                  </select>
                </div>
                <div class="form-group">
                <label>Number of Mid Teams</label>
                  <select class="form-control" id='num_mid_sel' style="width: 100%;">
                        <?php for($x = 1 ; $x<=20 ; $x++) {?>
                        <option><?php echo  $x ?></option>
                      <?php } ?>
                  </select>
                </div>
                <div class="form-group">
                <label>Number of Low Teams</label>
                  <select class="form-control" id='num_low_sel' style="width: 100%;">
                        <?php for($x = 1 ; $x<=10 ; $x++) {?>
                        <option><?php echo $x ?></option>
                      <?php } ?>
                  </select>
                </div>
                <input type="hidden" id='cntstand' name="">
              <!-- /.box-body -->

              <div class="box-footer">
                <button type="submit" onclick='updPrio()' class="btn btn-primary">Save</button>
              </div>
           
          </div>
        </div>
       
        </div>
         <div class="col-md-6">
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Table League Fixtures</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
           
              <div class="box-body">
              
                <div class="form-group">
                <label>Max Fixture</label>
                  <select class="form-control" id='max_fix' style="width: 100%;">
                      <?php for($x = 1 ; $x<=10 ; $x++) {?>
                        <option><?php echo (string)$x ?></option>
                      <?php } ?>
                  </select>
                </div>
              <!-- /.box-body -->

              <div class="box-footer">
                <button type="submit" class="btn btn-primary">Save</button>
              </div>
           
          </div>
        </div>
      </div>
        <!-- /.box-body -->
     


        <!-- /.box-footer-->
      </div>
      <!-- /.box -->

      <!-- CMS FOR HOME BANNERS -->
      <?php echo form_open_multipart(base_url( 'upload/create' ), array( 'id' => 'updTopBannerForm', 'class' => 'form-horizontal form-label-left' )); ?>
      <div class="row">
        <!-- left column -->
        <div class="col-md-4">
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Top Banner</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
           
              <div class="box-body">
                <label>Update Top Banner</label>
                <div class="form-group">
                  <img style="width: 100%;" src="<?=base_url().$this->config->item('cms_home');?>/home_top_banner.jpg">
                  <br><br>
                  <div class="clear"></div>
                  <input type="file" id="updtopxFileTxt" name="updtopxFileTxt" class="form-control" accept="image/jpg"> 
                </div>
              <!-- /.box-body -->

              <div class="box-footer">
                <button type="submit" class="btn btn-primary">Save</button>
              </div>
           
          </div>
        </div>
      </div>
    </form>

      <?php echo form_open_multipart(base_url( 'upload/create' ), array( 'id' => 'updMidBannerForm', 'class' => 'form-horizontal form-label-left' )); ?>
       <div class="col-md-4">
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Mid Banner</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
           
              <div class="box-body">
                <label>Update Mid Banner</label>
                <div class="form-group">
                
                  <img style="width: 100%;" src="<?=base_url().$this->config->item('cms_home');?>/home_mid_banner.jpg">
                  <br><br>
                  <div class="clear"></div>
                  <input type="file" id="updmidxFileTxt" name="updmidxFileTxt" class="form-control" accept="image/jpg"> 
                </div>
              <!-- /.box-body -->

              <div class="box-footer">
                <button type="submit" class="btn btn-primary">Save</button>
              </div>
           
          </div>
        </div>
      </div>
      </form>

      <?php echo form_open_multipart(base_url( 'upload/create' ), array( 'id' => 'updSideBannerForm', 'class' => 'form-horizontal form-label-left' )); ?>
      <div class="col-md-4">
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Side Banner</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
           
              <div class="box-body">
                <label>Update Side Banner</label>
                <div class="form-group">
                
                  <img style="width: 100%;" src="<?=base_url().$this->config->item('cms_home');?>/home_side_banner.jpg">
                  <br><br>
                  <div class="clear"></div>
                  <input type="file" id="updsidexFileTxt" name="updsidexFileTxt" class="form-control" accept="image/jpg"> 
                </div>
              <!-- /.box-body -->

              <div class="box-footer">
                <button type="submit" class="btn btn-primary">Save</button>
              </div>
           
          </div>
        </div>
      </div>
    </form>
        <!-- /.box-body -->
     

     
        <!-- /.box-footer-->
      </div>

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

<footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> 2.4.0
    </div>
    <strong>Copyright &copy; 2018 <a href="#">Siegreich Solutions inc.</a></strong> All rights
    reserved.
  </footer>
  <!-- Control Sidebar -->

  <!-- /.control-sidebar -->
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->

</div>
<!-- ./wrapper -->
<?php $this->load->view('templates/admin_footer');?>

<script>

$(document).ready(function() {
  $('#loader').hide();
  // loadData();
  $('.sidebar-menu').tree();
  $('.select2').select2();
  getLeaguelist();

 

 });

$("#updTopBannerForm").submit(function(e){
    e.preventDefault();
    // alert('asd');
    var cmsid = 1;
    var file = $("#updmidxFileTxt").val().split(".");
    var data = new FormData();

    data.append('cmsid', cmsid);
    data.append('updtopxFileTxt', filetemp);
    if($("#updtopxFileTxt").val()=='')
    {
      swal("Error", "Please select image to upload!", "error");
    }else{
      $.ajax({
                url : "<?=base_url();?>HomeCms/updBanner",
                type: "POST",
                dataType : 'json',
                cache :  false,
                contentType : false,
                processData : false,
                async: false,
                dataType : 'json',
                data : data,
                beforeSend: function(){
                $('#loader').show();
                },
                complete: function(){
                    $('#loader').hide();
                },
                success: function(data, textStatus, jqXHR){
                  swal(data.ttl,data.msg,data.typ);
                  // $("#updModal").modal('toggle'); 
                  // reloadData( $("#teamsTable") );

                },
                error: function (jqXHR, textStatus, errorThrown){
                  //Custom Error
                  swal("System Error", "There is a problem with the server! Please contact IT support", "error");
                }
            });
    }

});

$("#updMidBannerForm").submit(function(e){
    e.preventDefault();
    // alert('asd');
    var cmsid = 2;
    var file = $("#updmidxFileTxt").val().split(".");
    var data = new FormData();

    data.append('cmsid', cmsid);
    data.append('updmidxFileTxt', filetemp);
    if($("#updmidxFileTxt").val()=='')
    {
      swal("Error", "Please select image to upload!", "error");
    }else{
      $.ajax({
                url : "<?=base_url();?>HomeCms/updBanner",
                type: "POST",
                dataType : 'json',
                cache :  false,
                contentType : false,
                processData : false,
                async: false,
                dataType : 'json',
                data : data,
                beforeSend: function(){
                $('#loader').show();
                },
                complete: function(){
                    $('#loader').hide();
                },
                success: function(data, textStatus, jqXHR){
                  swal(data.ttl,data.msg,data.typ);
                  // $("#updModal").modal('toggle'); 
                  // reloadData( $("#teamsTable") );

                },
                error: function (jqXHR, textStatus, errorThrown){
                  //Custom Error
                  swal("System Error", "There is a problem with the server! Please contact IT support", "error");
                }
            });
    }

});

$("#updSideBannerForm").submit(function(e){
    e.preventDefault();
    // alert('asd');
    var cmsid = 3;
    var file = $("#updsidexFileTxt").val().split(".");
    var data = new FormData();

    data.append('cmsid', cmsid);
    data.append('updsidexFileTxt', filetemp);
    if($("#updsidexFileTxt").val()=='')
    {
      swal("Error", "Please select image to upload!", "error");
    }else{
      $.ajax({
                url : "<?=base_url();?>HomeCms/updBanner",
                type: "POST",
                dataType : 'json',
                cache :  false,
                contentType : false,
                processData : false,
                async: false,
                dataType : 'json',
                data : data,
                beforeSend: function(){
                $('#loader').show();
                },
                complete: function(){
                    $('#loader').hide();
                },
                success: function(data, textStatus, jqXHR){
                  swal(data.ttl,data.msg,data.typ);
                  // $("#updModal").modal('toggle'); 
                  // reloadData( $("#teamsTable") );

                },
                error: function (jqXHR, textStatus, errorThrown){
                  //Custom Error
                  swal("System Error", "There is a problem with the server! Please contact IT support", "error");
                }
            });
    }

});

$("#league_list_s").change(function() {
    var league_list_s = $("#league_list_s").val();
    var ls_idarr = league_list_s.split(',');
    var league_id = ls_idarr[0];
    var season_id = ls_idarr[1];

    getCntStand(season_id);
    

  });
function updFixtures()
{
    var max_fix = $("#max_fix").val();
   
   
  // console.log(total_team);
      var formData = { max_fix: max_fix,   
                    };

        $.ajax({
        url: "<?=base_url();?>HomeCms/updFixture", 
        type: "POST", 
        dataType: 'json', 
        data: formData, 
        beforeSend: function(){
        $('#loader').show();
        },
        complete: function(){
            $('#loader').hide();
        },
        success: function(data, textStatus, jqXHR)

        {
        swal(data.ttl,data.msg,data.typ);
        
       }}); 
      
     
     
}
function updPrio()
{
    var league_list_s = $("#league_list_s").val();
    var ls_idarr = league_list_s.split(',');
    var league_id = ls_idarr[0];
    var season_id = ls_idarr[1];
    var cntstand = $('#cntstand').val();
    // console.log(cntstand);
    var priority_sel = $("#priority_sel").val();
    var num_top_sel = $("#num_top_sel").val();
    var num_high_sel = $("#num_high_sel").val();
    var num_mid_sel = $("#num_mid_sel").val();
    var num_low_sel = $("#num_low_sel").val();
    var total_team = parseInt(num_top_sel) + parseInt(num_high_sel) + parseInt(num_mid_sel) + parseInt(num_low_sel);

   
  // console.log(total_team);
      var formData = { league_id: league_id, 
                      priority_sel: priority_sel,
                      num_top_sel:num_top_sel,
                     num_high_sel:num_high_sel,
                     num_mid_sel:num_mid_sel,
                     num_low_sel:num_low_sel,
                     
                    };

      if(league_list_s==""){
        swal("Edit CMS","Please Select League","error");
      }
      else if(total_team!=parseInt(cntstand)){
        swal("Edit CMS","Currently Team Count is "+cntstand+"","error");
      }
      else{
         $.ajax({
        url: "<?=base_url();?>HomeCms/updPriority", 
        type: "POST", 
        dataType: 'json', 
        data: formData, 
        beforeSend: function(){
        $('#loader').show();
        },
        complete: function(){
            $('#loader').hide();
        },
        success: function(data, textStatus, jqXHR)

        {
        swal(data.ttl,data.msg,data.typ);
        
       }}); 
      }
     
     
}
function getLeaguelist()
{
      $.ajax({
        url: "<?=base_url();?>HomeCms/getLeague", 
        type: "GET", 
        dataType: 'json', 
         
       
        success: function(data, textStatus, jqXHR)

        {
          var html = '';
          // console.log(data);
          for(var x = 0 ; x<data.length ; x++){
            html += "<option value="+data[x]['league_id']+','+data[x]['season_id']+">"+data[x]['league_name']+" - "+data[x]['country_name']+"</option>";

          }
          $('#league_list_s').html(html);
            var league_list_s = $("#league_list_s").val();
            var ls_idarr = league_list_s.split(',');
            var league_id = ls_idarr[0];
            var season_id = ls_idarr[1];

             getCntStand(season_id);

          
       }}); 
}
function getCntStand(sid)
{
  $.ajax({
        url: "<?=base_url();?>HomeCms/getCntStanding", 
        type: "POST", 
        dataType: 'json', 
        data:{sid:sid},
         beforeSend: function(){
          $('#loader').show();
          },
          complete: function(){
              $('#loader').hide();
          },
         
        success: function(data, textStatus, jqXHR)

        {
         $('#cntstand').val(data[0]['cnt']);
         

          
       }}); 
}

$(function() {
  // We can attach the `fileselect` event to all file inputs on the page
    $(document).on('change', ':file', function() {
      var input = $(this),
          numFiles = input.get(0).files ? input.get(0).files.length : 1,
          label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
      input.trigger('fileselect', [numFiles, label]);
      //store to var
      filetemp = input.get(0).files[0];
    });

    // We can watch for our custom `fileselect` event like this
      $(document).ready( function() {
          $(':file').on('fileselect', function(event, numFiles, label) {
              var input = $(this).parents('.input-group').find(':text'),
                  log = numFiles > 1 ? numFiles + ' files selected' : label;

              if( input.length ) {
                  input.val(log);
                  if (numFiles > 1) { input.val(''); }
              }
          });
      });
  });
</script>