<div class="loader" id="loader">
</div>

<body class="hold-transition skin-green sidebar-mini">
<!-- Site wrapper -->
<div class="wrapper">

 

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper margin-top_cont">
  <?php echo form_open_multipart(base_url( 'upload/create' ), array( 'id' => 'addForm', 'class' => 'form-horizontal form-label-left' )); ?>
  <div class="modal fade addModal" tabindex="-1" role="dialog" aria-hidden="true" sstyle="width:600px" id=addModal>
  <div class="modal-dialog modal-md">
    <div class="modal-content">

      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
        </button>
        <h4 class="modal-title" id="addUserLabel">Add Tipsters</h4>

      </div>
      <div class="modal-body">

      <form class="form-horizontal form-label-left">
        <div class="form-group">
          <label class="control-label col-md-4 col-sm-4 col-xs-12">Tipsters Name</label>
          <div class="col-md-6 col-sm-6 col-xs-12">
            <input type="text" id="addtipstername" name="tipstername" class="form-control col-md-7 col-xs-12" >
          </div>
        </div>

        <div class="form-group">
          <label class="control-label col-md-4 col-sm-4 col-xs-12">Tipsters Link</label>
          <div class="col-md-6 col-sm-6 col-xs-12">
            <input type="text" id="addtipsterlink" name="tipsterlink" placeholder="http(s)://www.domain.com" class="form-control col-md-7 col-xs-12" >
          </div>
        </div>

        <div class="form-group">
          <label class="control-label col-md-4 col-sm-4 col-xs-12">Tipsters Logo</label>
          <div class="col-md-6 col-sm-6 col-xs-12">
            <input type="file" id="addxFileTxt" name="addxFileTxt" class="form-control col-md-7 col-xs-12" accept="image/gif,image/jpeg,image/png"> 
          </div>
        </div>

        <div class="form-group">
          <label class="control-label col-md-4 col-sm-4 col-xs-12">Status</label>
          <div class="col-md-6 col-sm-6 col-xs-12">
            <select class="form-control col-md-7 col-xs-12" name="addstatus"  id="addstatus" style='width: 100%'>
              <!-- <option value="">-- SELECT --</option> -->
              <option value="A" selected="selected">Active</option>
              <option value="I">Inactive</option>
            </select>
          </div>
        </div>
      </form>      

      </div>
      <div class="modal-footer">
        <input type=hidden id=addID value=''>
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Save</button>
      </div>

    </div>
  </div>
</div>
</form>


<!-- EDIT MODAL -->
<?php echo form_open_multipart(base_url( 'upload/create' ), array( 'id' => 'updForm', 'class' => 'form-horizontal form-label-left' )); ?>
<div class="modal fade updModal" tabindex="-1" role="dialog" aria-hidden="true" sstyle="width:600px" id=updModal>
  <div class="modal-dialog modal-md">
    <div class="modal-content">

      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
        </button>
        <h4 class="modal-title" id="editUserLabel">Update Tipster</h4>

      </div>
      <div class="modal-body">

      <form class="form-horizontal form-label-left">
        <div class="form-group">
          <label class="control-label col-md-4 col-sm-4 col-xs-12">Tipsters Name</label>
          <div class="col-md-6 col-sm-6 col-xs-12">
            <input type="text" id="updtipstername" name="updtipstername" class="form-control col-md-7 col-xs-12" >
          </div>
        </div>

        <div class="form-group">
          <label class="control-label col-md-4 col-sm-4 col-xs-12">Tipsters Link</label>
          <div class="col-md-6 col-sm-6 col-xs-12">
            <input type="text" id="updtipsterlink" name="updtipsterlink" placeholder="http(s)://www.domain.com" class="form-control col-md-7 col-xs-12" >
          </div>
        </div>

        <div class="form-group">
          <label class="control-label col-md-4 col-sm-4 col-xs-12">Tipsters Logo</label>
          <div class="col-md-6 col-sm-6 col-xs-12">
            <input type="file" id="updxFileTxt" name="updxFileTxt" class="form-control col-md-7 col-xs-12" accept="image/gif,image/jpeg,image/png"> 
          </div>
        </div>

        <div class="form-group">
          <label class="control-label col-md-4 col-sm-4 col-xs-12">Status</label>
          <div class="col-md-6 col-sm-6 col-xs-12">
            <select class="select2_multiple form-control col-md-7 col-xs-12" name="updstatus"  id="updstatus" style='width: 100%'>
              <!-- <option value="">-- SELECT --</option> -->
              <option value="A" selected="selected">Active</option>
              <option value="I">Inactive</option>
              
            </select>
          </div>
        </div>
      </form>      

      </div>
      <div class="modal-footer">
        <input type=hidden id=editID value=''>
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary" >Save</button>
      </div>

    </div>
  </div>
</div>
</form>

<!--- END FOR EDIT MODAL -->

<!-- MODAL FOR DELETION -->
<div class="modal fade delModal" tabindex="-1" role="dialog" aria-hidden="true" sstyle="width:400px" id=delModal>
  <div class="modal-dialog modal-sm">
    <div class="modal-content">

      <div class="modal-header bgred">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
        </button>
        <h4 class="modal-title" id="delModalLabel"></h4>
      </div>
      <div class="modal-body">
        <b><span class='colorblack' style='font-size:18px'> </span><span class='colorred' id=del_user style='font-size:18px'></span></b><br>
        <br>

        
      </div>
      <div class="modal-footer">
        <input type=hidden id=delid value=''>
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
        <button type="button" class="btn btn-danger" onclick='deleteUser();'>Delete</button>
      </div>

    </div>
  </div>
</div>
<!-- END MODAL FOR DELETION -->
 

    

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="box">
        <div class="box-header with-border">
          <h1 class="box-title">Sites</h1>
          <button class="btn btn-md btn-info" data-toggle="modal" data-target=".addModal"><i class='fa fa-plus'></i> Add</button>
        </div>
        
        <div class="box-body">

          <table id="siteTable" class="table table-striped table-bordered" data-page-length='50' style='width:100%'>
          <thead>
            <tr>
              <th style="width: 50px; text-align: center;" >
              <!-- <th>Site ID -->
              <th>Image
              <th>Tipsters Name
              <th>Tipsters Link
              <th>Status
              <th>Created Date
              <th>Created By
            </tr>
          </thead>

          </table>
        </div>
        <!-- /.box-body -->
        
        <!-- /.box-footer-->
      </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

 <footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> 2.4.0
    </div>
    <strong>Copyright &copy; 2018 <a href="#">Siegreich Solutions inc.</a></strong> All rights
    reserved.
  </footer>


</div>
<!-- ./wrapper -->


<?php $this->load->view('templates/admin_footer');?>

<script>
var datatable;
var filetemp;
$(document).ready(function() {
  $('#loader').hide();
  loadData();
  $('.sidebar-menu').tree();
 });

$("#addForm").submit(function(e){
    e.preventDefault();
    var tipstername = $('#addtipstername').val();
    var tipsterlink = $('#addtipsterlink').val();
    var status = $('#addstatus').val();
    var file = $("#addxFileTxt").val().split(".");

    var data = new FormData();
    data.append('tipstername', tipstername);
    data.append('tipsterlink', tipsterlink);
    data.append('status', status);
    data.append('addxFileTxt', filetemp);
    
    if (tipstername == "" || tipsterlink == "" || status == "" || $("#addxFileTxt").val() == '')
    {
      swal("Error","All fields are required!","error");
    } 
    else if(tipsterlink.indexOf("http://") && tipsterlink.indexOf("https://"))
    {
      swal("Error","Cannot be added! Please check your URL (include http(s)://)!","error");
    }
    else
    {
      $.ajax({url: "<?=base_url();?>Sites/createTipster", 
          type: "POST", dataType : 'json', cache :  false, contentType : false,
                processData : false, async: false, dataType : 'json', data : data,
                beforeSend: function(){
                $('#loader').show();
                },
                complete: function(){
                    $('#loader').hide();
                },
               success: function(data){
          $("#addModal").modal('toggle');
          swal(data.ttl,data.msg,data.typ);
          reloadData( $("#siteTable") );
      }}); 
    }
    });

  function updModal(id,site,link,status) {
    $("#updModal").modal(); 
    $("#editID").val(id);
    $("#editUserLabel").html("Update Tipster");
    $("#updtipstername").val(site);
    $("#updtipsterlink").val(link);
    $("#updstatus").val(status);
    // console.log(link);
  }

$("#updForm").submit(function(e){
    e.preventDefault();
    
    $("#editUserLabel").html("Update Tipster");
    var tipid = $("#editID").val();
    var tipstername = $("#updtipstername").val();
    var tipsterlink = $("#updtipsterlink").val();
    var status = $("#updstatus").val();
    var file = $("#updxFileTxt").val().split(".");

    var data = new FormData();

    data.append('tipid', tipid);
    data.append('tipstername', tipstername);
    data.append('tipsterlink', tipsterlink);
    data.append('status', status);
    data.append('updxFileTxt', filetemp);

    if(tipstername == '' || tipsterlink == '' || status == '')
      {
        swal("Error", "All fields are required!", "error");
      }
    else if(tipsterlink.indexOf("http://") && tipsterlink.indexOf("https://"))
      {
        swal("Error","Cannot be added! Please check your URL (include http(s)://)!","error");
      }
    else{
        $.ajax({
                url : "<?=base_url();?>Sites/updTipster",
                type: "POST",
                dataType : 'json',
                cache :  false,
                contentType : false,
                processData : false,
                async: false,
                dataType : 'json',
                data : data,
                beforeSend: function(){
                $('#loader').show();
                },
                complete: function(){
                    $('#loader').hide();
                },
                success: function(data, textStatus, jqXHR){
                  swal(data.ttl,data.msg,data.typ);
                  $("#updModal").modal('toggle'); 
                  reloadData( $("#siteTable") );

                },
                error: function (jqXHR, textStatus, errorThrown){
                  //Custom Error
                  swal("System Error", "There is a problem with the server! Please contact IT support", "error");
                }
            });
      }

  });

  function delSite(id,site) {
    $("#delModal").modal(); 
    $("#delid").val(id);
    $("#delModalLabel").html("Delete Tipster");
    $("#del_user").html("Are you sur you want to delete user "+site+" ?");
  }
 
  function deleteUser()
  {
    var delID = $('#delid').val();
    // alert(delID);
    var formData = { delID : delID};
    $.ajax({
          url :  "<?=base_url();?>Sites/delSite",
          type: "POST",
          dataType : 'json',
          data : formData,
          success: function(data, textStatus, jqXHR){
              // reloadData($("#userTable"));
              
              swal(data.ttl,data.msg,data.typ); 
              $("#delModal").modal('toggle'); 
              reloadData( $("#siteTable") );
              
            
          },
          error: function (jqXHR, textStatus, errorThrown)
          {
            //Custom Error
          } 


        });
  }

  

  function loadData()
  {
    datatable = $('#siteTable').dataTable( {
    "order": [[ 1, "asc" ]],
      responsive: true, 
      dom: 'Bfrtip',

      "buttons": [
              'excel',
              'pdf'
          ],
          "scrollX": true,
      "pagingType": "full_numbers",
      "processing": true,
      "serverSide": true,
      "ajax":{
        url :"<?=base_url();?>Sites/listing_site?k=" + Math.random(), // json datasource
        type: "post",  // method  , by default get      
        error: function(){  // error handling
          $(".grid-error").html("");
          $("#roleTable").append('<tbody class="grid-error"><tr><th colspan="7">NO DATA FOUND</th></tr></tbody>');
          $("#roleTable_processing").css("display","none");        
        }
      },
      "columns" : [
        // { "data" : "site_id" },
        { "data" : "site_id" },
        { "data" : "image_path" },
        { "data" : "site" },
        { 
         "data": "link",
         "render": function(data, type, row, meta){
            if(type === 'display')
            {
                data = '<a href="' + data + '" target="_blank" >' + data + '</a>';
            }
            return data;
         }
        },
        { "data" : "status" },
        { "data" : "created_date" },
        { "data" : "created_by" }
      ]
      ,
       "columnDefs" : [ {
        "targets" : 0,
        "orderable": false, className : "text-center",
          "render": function( data, type, row, meta ) {
            var html = "";
            
              html = " <a href='#'  onclick=\"updModal(" + row["site_id"] + ",'" + row["site"] + "','" + row["link"] + "','" + row["status"] + "')\"> <span class='glyphicon glyphicon-edit' aria-hidden='true'></span></a>";
              html += "&nbsp;&nbsp;"+"<a href='#' onclick=\"delSite(" + row["site_id"] + ",'"+ row["site"] + "')\"><span class='glyphicon glyphicon-trash' style='color:red;' aria-hidden='true'></span> </a>";
              
            
            return html;
            }
       },
       {
        "targets" : 1,
        className : "text-center",
        "orderable": false,
        "render": function( data, type, row, meta ) {
              var html = "";
              var img = row["image_path"];
              // html += img;
              if(img) 
              {
                html += "<span >";
                html += "<img src='<?=base_url().$this->config->item('upload_tipsters_logo');?>/"+ img +"' class='tipster_logo_img'>";
                html += "</span>";

              }
              return html;
            }
        },
        {
          "targets" : 4,  
          "render" : function( data, type, row, meta ) {
          var html;
              switch(data){
                case "A" : html = "<span class='label label-success'>Active</span>"; break;
                case "I" : html = "<span class='label label-danger'>Inactive</span>"; break;
              }
          return html;
          }     
        }]
    }); 
    }
$(function() {
  // We can attach the `fileselect` event to all file inputs on the page
    $(document).on('change', ':file', function() {
      var input = $(this),
          numFiles = input.get(0).files ? input.get(0).files.length : 1,
          label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
      input.trigger('fileselect', [numFiles, label]);
      //store to var
      filetemp = input.get(0).files[0];
    });

    // We can watch for our custom `fileselect` event like this
      $(document).ready( function() {
          $(':file').on('fileselect', function(event, numFiles, label) {
              var input = $(this).parents('.input-group').find(':text'),
                  log = numFiles > 1 ? numFiles + ' files selected' : label;

              if( input.length ) {
                  input.val(log);
                  if (numFiles > 1) { input.val(''); }
              }
          });
      });
  });

</script>