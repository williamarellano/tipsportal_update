<div class="loader" id="loader">
</div>

<!-- Add Team Modal -->
<?php echo form_open_multipart(base_url( 'upload/create' ), array( 'id' => 'addForm', 'class' => 'form-horizontal form-label-left' )); ?>
<div class="modal fade addTeamModal" tabindex="-1" role="dialog" aria-hidden="true" sstyle="width:600px" id=addTeamModal>
  <div class="modal-dialog modal-md">
    <div class="modal-content">

      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
        </button>
        <h4 class="modal-title" id="addUserLabel">Add Team</h4>

      </div>
      <div class="modal-body">

        <form class="form-horizontal form-label-left">
        <div class="form-group">
          <label class="control-label col-md-4 col-sm-4 col-xs-12">Team Name</label>
          <div class="col-md-6 col-sm-6 col-xs-12">
            <input type="text" id="addteamname" name="addteamname" class="form-control col-md-7 col-xs-12" >
          </div>
        </div>

       

        <div class="form-group">
          <label class="control-label col-md-4 col-sm-4 col-xs-12">Team Nickname</label>
          <div class="col-md-6 col-sm-6 col-xs-12">
            <input type="text" id="addteamnickname" name="addteamnickname" class="form-control col-md-7 col-xs-12" >
          </div>
        </div>

        <div class="form-group">
          <label class="control-label col-md-4 col-sm-4 col-xs-12">Country</label>
          <div class="col-md-6 col-sm-6 col-xs-12">
            <select class="form-control select2 col-md-7 col-xs-12" name="addselcountry"  id="addselcountry" style='width: 100%'>
              
            </select>
          </div>
        </div>
        </form>

        <div class="form-group">
          <label class="control-label col-md-4 col-sm-4 col-xs-12">Twitter</label>
          <div class="col-md-6 col-sm-6 col-xs-12">
            <input type="text" id="addtwitter" name="addtwitter" class="form-control col-md-7 col-xs-12" >
          </div>
        </div>

        <div class="form-group">
          <label class="control-label col-md-4 col-sm-4 col-xs-12">Founded</label>
          <div class="col-md-6 col-sm-6 col-xs-12">
            <input type="text" id="addfounded" name="addfounded" class="form-control col-md-7 col-xs-12" >
          </div>
        </div>

        <div class="form-group">
          <label class="control-label col-md-4 col-sm-4 col-xs-12">Team Logo</label>
          <div class="col-md-6 col-sm-6 col-xs-12">
            <input type="file" id="addxFileTxt" name="addxFileTxt" class="form-control col-md-7 col-xs-12" accept="image/gif,image/jpeg,image/png"> 
          </div>
        </div>

        

        
    

        <div class="form-group">
          <label class="control-label col-md-4 col-sm-4 col-xs-12">Status</label>
          <div class="col-md-6 col-sm-6 col-xs-12">
            <select class="form-control col-md-7 col-xs-12" name="addselStatus"  id="addselStatus" style='width: 100%'>
              <option value="">-- SELECT --</option>
              <option value="A" selected="selected">Active</option>
              <option value="I">Inactive</option>
            </select>
          </div>
        </div>
        </form>      

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Save</button>
      </div>

    </div>
  </div>
</div>
</form>

<!-- End for Add Team Modal -->

<body class="hold-transition skin-green sidebar-mini">
<!-- Site wrapper -->
<div class="wrapper">

<!-- EDIT MODAL -->
<?php echo form_open_multipart(base_url( 'upload/create' ), array( 'id' => 'updateForm', 'class' => 'form-horizontal form-label-left' )); ?>

<div class="modal fade updModal" tabindex="-1" role="dialog" aria-hidden="true" sstyle="width:600px" id=updModal>
  <div class="modal-dialog modal-md">
    <div class="modal-content">

      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
        </button>
        <h4 class="modal-title" id="updModalLabel">Add Team</h4>

      </div>

      <div class="modal-body">

        <form class="form-horizontal form-label-left">

        <div class="form-group">
          <label class="control-label col-md-4 col-sm-4 col-xs-12">Team Nickname</label>
          <div class="col-md-6 col-sm-6 col-xs-12">
            <input type="text" id="updteamnickname" name="updteamnickname" class="form-control col-md-7 col-xs-12" >
          </div>
        </div>

        <div class="form-group">
          <label class="control-label col-md-4 col-sm-4 col-xs-12">Team Name</label>
          <div class="col-md-6 col-sm-6 col-xs-12">
            <input type="text" disabled="true" id="updteamname" name="updteamname" class="form-control col-md-7 col-xs-12" >
          </div>
        </div>

        <div class="form-group">
          <label class="control-label col-md-4 col-sm-4 col-xs-12">Founded</label>
          <div class="col-md-6 col-sm-6 col-xs-12">
            <input type="text" disabled="true" id="updfounded" name="updfounded" class="form-control col-md-7 col-xs-12" >
          </div>
        </div>

        <div class="form-group">
          <label class="control-label col-md-4 col-sm-4 col-xs-12">Team Logo</label>
          <div class="col-md-6 col-sm-6 col-xs-12">
            <input type="file" id="updxFileTxt" name="updxFileTxt" class="form-control col-md-7 col-xs-12" accept="image/gif,image/jpeg,image/png"> 
          </div>
        </div>

        <div class="form-group">
          <label class="control-label col-md-4 col-sm-4 col-xs-12">Twitter</label>
          <div class="col-md-6 col-sm-6 col-xs-12">
            <input type="text" id="updtwitter" name="updtwitter" class="form-control col-md-7 col-xs-12" >
          </div>
        </div>

        
    
        <!-- <div class="form-group">
          <label class="control-label col-md-4 col-sm-4 col-xs-12">Status</label>
          <div class="col-md-6 col-sm-6 col-xs-12">
            <select class="select2_multiple form-control col-md-7 col-xs-12" name="selStatus"  id="selStatus" style='width: 100%'>
              <option value="">-- SELECT --</option>
              <option value="N">New</option>
              <option value="O">Open</option>
              <option value="S">Start</option>
              <option value="E">End</option>
              <option value="C">Calculating</option>
              <option value="F">Finish</option>
            </select>
          </div>
        </div> -->

        </form>      

      </div>
      <div class="modal-footer">
        <input type=hidden id=teamid value=''>
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
        <button type="submit" class="btn btn-primary" >Confirm</button>
      </div>

    </div>
  </div>
</div>
</form>

<!--- END FOR EDIT MODAL -->
 

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper margin-top_cont">

    

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="box">
        <div class="box-header with-border">
            <button class="btn btn-md btn-info" data-toggle="modal" data-target=".addTeamModal">
            <i class='fa fa-plus'></i> Add Team</button>
            <!-- <br><br><h1 class="box-title">Leagues</h1> -->
        </div>

    <!-- Fitler content -->

    <section class="content" style='min-height:0px'>
       <div class="row">
        <div class="col-xs-12">
          <div class="box">
          <div class="box-header">
            <h3 class="box-title"><i class="fa fa-filter"></i> Teams Filter Box</h3>
            <hr>
          </div>
           <div class="box-body">

             <div class="col-sm-2">
                <div class="form-group">
                  <label for="countryfilter">Country</label>
                    <select class="form-control filter select2" id="countryfilter" style="width: 100%;" onchange="getLeagueList();getSeasonList();">
                      
                    </select>
                </div>
              </div>

              <div class="col-sm-2">
                <div class="form-group">
                  <label for="leaguefilter">League</label>
                    <select class="form-control filter select2" id="leaguefilter" style="width: 100%;" onchange="getSeasonList();" >
                      
                    </select>
                </div>
              </div>

              <div class="col-sm-2">
                <div class="form-group">
                  <label for="seasonfilter">Season</label>
                  <select class="form-control filter select2" id="seasonfilter" style="width: 100%;">
                    <option value=''>All</option>
                  </select>
                </div>
              </div>

              <div class="col-sm-2">
                <div class="form-group">
                  <label for="statusfilter">Status</label>
                  <select class="form-control filter select2" id="statusfilter" style="width: 100%;">
                    <option value=''>All</option>
                    <option value='N'>Not Open</option>
                    <option value='O'>Open</option>
                    <option value='S'>Start</option>
                    <option value='E'>End</option>
                    <option value='C'>Calculating</option>
                    <option value='F'>Finish</option>
                  </select>
                </div>
              </div>
              <div class="col-sm-1"> 
                <div class="form-group">
                <button class="btn btn-default" id="btnReset"  style="margin-top: 23px;">Reset</button>
                </div>
              </div>
            
           </div>
          </div>
         </div>
        </div>
    </section>
    <!-- End Fitler content -->

    <!-- Main content -->
        
        
        <div class="box-body">

          <table id="teamsTable" class="table table-striped table-bordered" data-page-length='50' style='width:100%'>
          <thead>
            <tr>
              <!-- <th style="width: 50px; text-align: center;" > -->
              <th>Team ID
              <th>Team Logo
              <th>Team Name
              <th>Nickname
              <th>Twitter
              <th>Founded
              <th>Status
          </thead>

          </table>
        </div>
        <!-- /.box-body -->
        
        <!-- /.box-footer-->
      </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> 2.4.0
    </div>
    <strong>Copyright &copy; 2018 <a href="#">Siegreich Solutions inc.</a></strong> All rights
    reserved.
  </footer>

  
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>

  <input type="hidden" name="country_id" id="country_id" value="">
</div>
<!-- ./wrapper -->

<?php $this->load->view('templates/admin_footer');?>

<script>
var datatable;
var filetemp;
// var country_id = $('#country_id').val();
$(document).ready(function() {
  $('#loader').hide();
  $('.select2').select2();
  getCountryList();
  // getLeagueList();
  // getSeasonList();
  // getStatusList();

  loadData();
  });

function loadData(cntry,selLeague,selSeasonsel,status)
  {
    // var cntry = $("#countryfilter").val();
    // var selLeague = $("#leaguefilter").val();
    // var selSeason = $("#seasonfilter").val();
    // var selstatus = $("#statusfilter").val();

    // alert(cntry+' '+selLeague+' '+selSeason+' '+selstatus+' ');
    // var params = {cntry:cntry, selLeague:selLeague, selSeason:selSeason, selstatus:selstatus };

    datatable = $('#teamsTable').dataTable( {
    "order": [[ 5, "asc" ]],
      responsive: true, 
      dom: 'Bfrtip',

      "buttons": [
              'excel',
              'pdf'
          ],
          "scrollX": true,
      "pagingType": "full_numbers",
      "processing": true,
      "serverSide": true,
      "ajax":{
        url :"<?=base_url();?>Teams/teamsListing/?k=" + Math.random(), // json datasource
        type: "post",  // method  , by default get  
        // data:{cntry:cntry,
        //       selLeague:selLeague,
        //       selSeason:selSeason,
        //       selstatus:selstatus
        //       },  // method  , by default g
        error: function(){  // error handling
          $(".grid-error").html("");
          $("#roleTable").append('<tbody class="grid-error"><tr><th colspan="7">NO DATA FOUND</th></tr></tbody>');
          $("#roleTable_processing").css("display","none");        
        }
      },
      "columns" : [
        // { "data" : "league_id" },
        { "data" : "team_id" },
        { "data" : "logo_path"  },
        { "data" : "team_name" },
        { "data" : "team_nickname"  },
        { "data" : "twitter"  },
        { "data" : "founded" },
        { "data" : "status", visible: false }
      ]
      ,
       "columnDefs" : [ {
        "targets" : 0,
        className : "text-center",
        "orderable": false,
        "render": function( data, type, row, meta ) {
            var html = "";
            // var team_nick = row["team_nickname"];
            // var team_name = row["team_name"];
            
              
              html = " <a href='#'  onclick=\"updTeamModal(" + row["team_id"] + ",'" + row["twitter"] + "','"+ row["founded"] + "','"+ row["status"] +"')\"> <span class='glyphicon glyphicon-edit' aria-hidden='true'></span></a>";
              // console.log(team_nick);
              // html += "&nbsp;&nbsp;"+"<a href='#' onclick=\"delUser(" + row["user_id"] + ",'"+ row["realname"] + "')\"><span class='glyphicon glyphicon-trash' style='color:red;' aria-hidden='true'></span> </a>";
              
            
            return html;
            }
       },
        {
        "targets" : 1,
        className : "text-center",
        "orderable": false,
        "render": function( data, type, row, meta ) {
              var html = "";
              var img = row["logo_path"];
              // html += img;
              if(img) 
              {
                html += "<span >";
                html += "<img src='<?=base_url().$this->config->item('upload_teams_logo');?>/"+ img +"' class='league_logo_img'>";
                html += "</span>";

              }
              return html;
            }
        }

        ]
    }); 
    }

    $("#addForm").submit(function(e){
      e.preventDefault();
      // alert('add modal');
      var team_name = $("#addteamname").val();
      var team_nickname = $("#addteamnickname").val();
      var twitter = $("#addtwitter").val();
      var founded = $("#addfounded").val();
      var country_id = $("#addselcountry").val();
      var status = $("#addselStatus").val();
      var file = $("#addxFileTxt").val().split(".");

      var data = new FormData();

      data.append('team_name', team_name);
      data.append('team_nickname', team_nickname);
      data.append('twitter', twitter);
      data.append('founded', founded);
      data.append('country_id', country_id);
      data.append('status', status);
      data.append('addxFileTxt', filetemp);

      if($("#addxFileTxt").val() == '' || team_name == '' || team_nickname == '' || founded == '' || status == '')
      {
        swal("Error", "All fields are required!", "error");
      }else{
        $.ajax({
                url : "<?=base_url();?>Teams/addTeam",
                type: "POST",
                dataType : 'json',
                cache :  false,
                contentType : false,
                processData : false,
                async: false,
                dataType : 'json',
                data : data,
                beforeSend: function(){
                $('#loader').show();
                },
                complete: function(){
                    $('#loader').hide();
                },
                success: function(data, textStatus, jqXHR){
                  swal(data.ttl,data.msg,data.typ);
                  $("#addTeamModal").modal('toggle'); 
                  reloadData( $("#teamsTable") );

                },
                error: function (jqXHR, textStatus, errorThrown){
                  //Custom Error
                  swal("System Error", "There is a problem with the server! Please contact IT support", "error");
                }
            });

      }



    });

    function updTeamModal(team_id,twitter,founded,status){
      // alert(seasonid);
      $("#teamid").val(team_id);
      // $("#updteamname").val(team_name);
      // $("#updteamnickname").val(team_nickname);
      $("#updtwitter").val(twitter);
      $("#updfounded").val(founded);
      $("#updModal").modal(); 
      $("#updModalLabel").html("Update Team");
      // $("#upd_league").html("Are you sure you want to add league "+ldname+" ?");

      $.ajax({
          url : "<?=base_url(); ?>Teams/getTeamDet",
          type: "POST",
          dataType : 'json',
          data : {team_id : team_id},
          success: function (data, textStatus, errorThrown){
            console.log(data);
            $("#updteamname").val(data[0].team_name);
            $("#updteamnickname").val(data[0].team_nickname);
          },
          error: function (jqXHR, textStatus, errorThrown){
          }
        });
    }

    $("#updateForm").submit(function(e){
      e.preventDefault();

      var team_id = $("#teamid").val();
      var team_name = $("#updteamname").val();
      var team_nickname = $("#updteamnickname").val();
      var twitter = $("#updtwitter").val();
      var founded = $("#updfounded").val();
      var file = $("#updxFileTxt").val().split(".");
      // alert('asd');

      var data = new FormData();

      data.append('team_id', team_id);
      data.append('team_name', team_name);
      data.append('team_nickname', team_nickname);
      data.append('twitter', twitter);
      data.append('founded', founded);
      data.append('updxFileTxt', filetemp);

      if(team_name == '' || team_nickname == '' || founded == '')
      {
        swal("Error", "All fields are required!", "error");
      }else{
        $.ajax({
                url : "<?=base_url();?>Teams/updTeam",
                type: "POST",
                dataType : 'json',
                cache :  false,
                contentType : false,
                processData : false,
                async: false,
                dataType : 'json',
                data : data,
                beforeSend: function(){
                $('#loader').show();
                },
                complete: function(){
                    $('#loader').hide();
                },
                success: function(data, textStatus, jqXHR){
                  swal(data.ttl,data.msg,data.typ);
                  $("#updModal").modal('toggle'); 
                  reloadData( $("#teamsTable") );

                },
                error: function (jqXHR, textStatus, errorThrown){
                  //Custom Error
                  swal("System Error", "There is a problem with the server! Please contact IT support", "error");
                }
            });
      }



      

    });

  function getCountryList()
  {
    var html = "";
    var sel = $("#addselcountry");
    // var params = { c:brandid , wcacc:wcacc};
    $.ajax({
        url : "<?php echo $this->config->item('base_url'); ?>Teams/getCountryList",
        type: "POST",
        dataType : 'json',
        // data : params,
        success: function (data, textStatus, errorThrown){
          sel.append($('<option>',
               {
                  value: "",
                  text : "-- SELECT --"
              }));
          for (var i = 0; i < data.length; i++)
          { 
               sel.append($('<option>',
               {
                  value: data[i].country_id,
                  text : data[i].country_name
              }));
          }
        },
        error: function (jqXHR, textStatus, errorThrown){
          swal("System Error", "There is a problem with the server! Please contact IT support", "error");
        }
      });
  }

  $(function() {
  // We can attach the `fileselect` event to all file inputs on the page
    $(document).on('change', ':file', function() {
      var input = $(this),
          numFiles = input.get(0).files ? input.get(0).files.length : 1,
          label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
      input.trigger('fileselect', [numFiles, label]);
      //store to var
      filetemp = input.get(0).files[0];
    });

    // We can watch for our custom `fileselect` event like this
      $(document).ready( function() {
          $(':file').on('fileselect', function(event, numFiles, label) {
              var input = $(this).parents('.input-group').find(':text'),
                  log = numFiles > 1 ? numFiles + ' files selected' : label;

              if( input.length ) {
                  input.val(log);
                  if (numFiles > 1) { input.val(''); }
              }
          });
      });
  });

function remove_quotes(values1)
{
        var values = values1.toString();
        var str = "";
        var blockList = ['"','\'','\\']; // This is the list of key words to be escaped
        var flag = 0;
        for(var i = 0;i<values.length;i++)
        {
            for(var j=0;j<blockList.length;j++)
            {
                if(values[i] == blockList[j])
                {
                    flag = 1;
                    break;
                }
            }
            if(flag == 0)
            str += values[i];
            else
            {
                str += '\\';
                str += values[i];
                flag = 0;
            }
        }
        return str;

    }

</script>