<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Players extends CI_Controller {

  function __construct(){
    parent::__construct();
    //$this->load->database('default',TRUE);
    $this->load->model("query_model");
    $this->load->library('utilities');
  } 

  public function playersListing()
  {
    // $country_id = $this->query_model->clean('cntry');
    // $league_id = $this->query_model->clean('selLeague');
    // $season = $this->query_model->clean('selSeason');
    // $status = $this->query_model->clean('selstatus');
    // var_dump($country_id);

    // echo "SELECT * FROM (SELECT * FROM tp_league WHERE status <>'X')X WHERE 1=1 AND country_id = '".$country_id."' ";
    // echo $country_id." ".$league_id." ".$season." ".$status;
    // echo "COUNTRY ID: ".$country_id;
    // exit;

    $requestData= $_REQUEST;
    $columns = array( 
      0 => 'player_id',
      1 => 'image_path',
      2 => 'team_name',
      3 => 'country_name',
      4 => 'common_name',
      5 => 'fullname',
      6 => 'firstname',
      7 => 'lastname',
      8 => 'nationality',
      9 => 'position',
      10 => 'created_date',
      11 => 'created_by'

    );

    $sql = "";
    $totalData = 0;
    $totalFiltered = 0;

    $sql = "SELECT * FROM (SELECT  p.player_id, p.image_path, p.team_id, (SELECT team_name FROM tp_teams WHERE team_id = p.team_id) AS team_name,
		p.country_id, (SELECT country_name FROM tp_countries WHERE country_id = p.country_id) AS country_name,
		p.common_name, p.fullname, p.firstname, p.lastname, p.nationality, p.position, p.created_date, 
		(SELECT realname FROM tp_users WHERE user_id = p.created_by) AS created_by
		FROM tp_players p)X WHERE 1=1  ";
   
    $records = $this->query_model->getDataCount($sql);

      $totalData = $records["count"];
      $totalFiltered = $totalData;
      // if(!empty($brandfilter)){
      //   $filterbrand = $this->query_model->getDataArray("SELECT * from brands Where status='A' AND brand_name='".$brandfilter."'");
      //   $sql.=" AND brand_name = '".$filterbrand[0]['id']."'"; 
      // }
      // if(!empty($country_id)){
      //   $sql.=" AND team_id = '".$country_id."' "; 
      // }

      // if(!empty($league_id)){
      //   $sql.=" AND league_id = '".$league_id."'"; 
      // }

      // if(!empty($season)){
      //   $sql.=" AND season = '".$season."'"; 
      // }

      // if(!empty($status)){
      //   $sql.=" AND status = '".$status."'"; 
      // }
      // if(!empty($statusfilter)){
      //   $sql.=" AND status LIKE '%".$statusfilter."%'"; 
      // }
      //This is for search function...
      if( !empty($requestData['search']['value']) ) {
        $sql.=" AND (team_name LIKE '%".$requestData['search']['value']."%' "; 
        $sql.=" OR fullname LIKE '%".$requestData['search']['value']."%' ";    
        $sql.=" OR firstname LIKE '%".$requestData['search']['value']."%' ";    
        $sql.=" OR lastname LIKE '%".$requestData['search']['value']."%') ";

        // $sql.=" OR season LIKE '%".$requestData['search']['value']."%') ";
      }  
      $totalFiltered = $this->query_model->getDataCount($sql)["count"];
      //end of search function


    $sql.=" ORDER BY ". $columns[$requestData['order'][0]['column']]." ".$requestData['order'][0]['dir']." LIMIT ".$requestData['start']." ,".$requestData['length']."   ";
    
    //$totalFiltered = $this->query_model->getDataCount($sql)["count"];
    // echo $sql;

    $records = $this->query_model->getDataArray($sql);


    $data = array();

    foreach ($records as $row) {
      $nestedData=array();
      $nestedData["player_id"] = $row["player_id"];
      $nestedData["image_path"] = $row["image_path"];
      $nestedData["team_id"] = $row["team_id"];
      $nestedData["team_name"] = $row["team_name"];
      $nestedData["country_id"] = $row["country_id"];
      $nestedData["country_name"] = $row["country_name"];
      $nestedData["common_name"] = $row["common_name"];
      $nestedData["fullname"] = $row["fullname"];
      $nestedData["firstname"] = $row["firstname"];
      $nestedData["lastname"] = $row["lastname"];
      $nestedData["nationality"] = $row["nationality"];
      $nestedData["position"] = $row["position"];
      $nestedData["created_date"] = $row["created_date"];
      $nestedData["created_by"] = $row["created_by"];
    

      $data[] = $nestedData;  
    }
    $json_data = array(
          "draw"            => intval( $requestData['draw'] ),   // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw. 
          "recordsTotal"    => intval( $totalData ),  // total number of records
          "recordsFiltered" => intval( $totalFiltered ), // total number of records after searching, if there is no searching then totalFiltered = totalData
          "data"            => $data   // total data array
          );

    echo json_encode($json_data);  
  }
}
?>