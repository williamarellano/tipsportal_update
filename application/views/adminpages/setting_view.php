<div class="loader" id="loader">
</div>

<!-- EDIT MODAL -->
<div class="modal fade updModal" tabindex="-1" role="dialog" aria-hidden="true" sstyle="width:600px" id=updModal>
  <div class="modal-dialog modal-md">
    <div class="modal-content">

      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
        </button>
        <h4 class="modal-title" id="editapiLabel">Update API Key</h4>

      </div>
      <div class="modal-body">

        <form class="form-horizontal form-label-left">
          <div class="form-group">
            <label class="control-label col-md-4 col-sm-4 col-xs-12">API Key</label>
            <div class="col-md-6 col-sm-6 col-xs-12">
              <input type="text" id="editapi" name="editapi" class="form-control col-md-7 col-xs-12" value="">
            </div>
          </div>
        </form>      

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary" onclick="updAPI();">Save</button>
      </div>

    </div>
  </div>
</div>

<!--- END FOR EDIT MODAL -->
<?php
// var_dump($ap)
?>

<body class="hold-transition skin-green sidebar-mini">
<!-- Site wrapper -->
<div class="wrapper">

    
 

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper margin-top_cont">
    <!-- Content Header (Page header) -->
    

    <!-- Main content -->
    <section class="content">
      <!-- Default box -->
      <div class="box">
       <div class="box-header with-border">
          <h3 class="box-title">Getting Latest Data on API</h3>

    
        </div>
        <div class="box-header with-border">
          <button class="btn btn-md btn-info" onclick="getCountries();">
          <i class='fa fa-plus'></i> Get Countries / Leagues / Seasons</button>
          <button class="btn btn-md btn-info" onclick="getTeams();">
          <i class='fa fa-plus'></i> Get Teams/Players </button>

          <button class="btn btn-md btn-info" onclick="getFixtures();">
          <i class='fa fa-plus'></i> Get Fixtures </button>

          <button class="btn btn-md btn-info" onclick="getMatches();">
          <i class='fa fa-plus'></i> Get Matches </button>

          <button class="btn btn-md btn-info" onclick="getStandings();">
          <i class='fa fa-plus'></i> Get Standings </button>

            <br><br>
            <!-- <h1 class="box-title">Leagues</h1> -->

        
          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                    title="Collapse">
              <i class="fa fa-minus"></i></button>
         
          </div>
        </div>
        </div>
      <!-- Default box -->
      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title">Setting API</h3>

    
        </div>
        <div class="box-body">
        <div class="col-md-5">

        <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Api Key</h3>
            </div>
            <!-- /.box-header -->
          
              <div class="box-body">
                <!-- <div class="form-group">
                  <label for="exampleInputEmail1">Api Key</label>
                  <input type="apikey" class="form-control" id="apikey" disabled="" value="<?=$key;?>">
                </div> -->

                <div class="form-group">
                <!-- <label class="control-label col-md-4 col-sm-4 col-xs-12">API Key</label> -->
                  <div class="col-md-6 col-sm-6 col-xs-12" style="width: 100%;">
                    <input type="text" id="apikey" name="apikey" class="form-control col-md-7 col-xs-12" value="<?=$key;?>" disabled="" >
                  </div>
                </div>
              </div>
              <!-- /.box-body -->


              <div class="box-footer">
                <button class="btn btn-primary" onclick="updModal();">Update</button>
              </div>
          </div>
          <!-- end form -->
          </div> <!-- end for <div class="col-md-6"> -->
       
        </div>
        <!-- /.box-body -->
     
        <!-- /.box-footer-->
      </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

<footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> 2.4.0
    </div>
    <strong>Copyright &copy; 2018 <a href="#">Siegreich Solutions inc.</a></strong> All rights
    reserved.
  </footer>


</div>
<!-- ./wrapper -->


<?php $this->load->view('templates/admin_footer');?>

<script>
var datatable;
$(document).ready(function() {
  $('#loader').hide();
  $('.sidebar-menu').tree();
 });

function updModal()
{
  var key = $("#apikey").val();
  $("#updModal").modal('toggle'); 
  $("#editapi").val(key);
}

function updAPI()
{
  var api = $("#editapi").val();
  var formData = { api : api};

  console.log(formData);
  $.ajax({
        url :  "<?=base_url();?>Setting/updAPI",
        type: "POST",
        dataType : 'json',
        data : formData,
        success: function(data, textStatus, jqXHR){
            
            swal(data.ttl,data.msg,data.typ); 
            $("#updModal").modal('toggle'); 
            $("#apikey").val(api);
            
          
        },
        error: function (jqXHR, textStatus, errorThrown)
        {
          //Custom Error
        } 


      });

}
function getCountries()
{
    $.ajax({url: "<?=base_url();?>Setting/getCountries", 
    async: true, type: "POST", dataType: 'json',  
    beforeSend: function(){
    $('#loader').show();
    },
    complete: function(){
        $('#loader').hide();
    },
    success: function(data){
    swal(data.ttl,data.msg,data.typ);
   
    },
      error: function (jqXHR, textStatus, errorThrown){
        //Custom Error
        swal("System Error", "There is a problem with the server! Please contact IT support", "error");
      }
    }); 
}

function getTeams()
{
    $.ajax({url: "<?=base_url();?>Setting/getTeam", 
    async: true, type: "POST", dataType: 'json',  
    beforeSend: function(){
    $('#loader').show();
    },
    complete: function(){
        $('#loader').hide();
    },
    success: function(data){
    swal(data.ttl,data.msg,data.typ);
  
    },
      error: function (jqXHR, textStatus, errorThrown){
        //Custom Error
        swal("System Error", "There is a problem with the server! Please contact IT support", "error");
      }
    }); 
}
function getStandings()
{
    $.ajax({url: "<?=base_url();?>Setting/getStandings", 
    async: true, type: "POST", dataType: 'json',  
    beforeSend: function(){
    $('#loader').show();
    },
    complete: function(){
        $('#loader').hide();
    },
    success: function(data){
    swal(data.ttl,data.msg,data.typ);
  
    },
      error: function (jqXHR, textStatus, errorThrown){
        //Custom Error
        swal("System Error", "There is a problem with the server! Please contact IT support", "error");
      }
    }); 
}


function getMatches()
{
    $.ajax({url: "<?=base_url();?>Setting/getHead2Head", 
    async: true, type: "POST", dataType: 'json',  
    beforeSend: function(){
    $('#loader').show();
    },
    complete: function(){
        $('#loader').hide();
    },
    success: function(data){
    swal(data.ttl,data.msg,data.typ);
   
    },
      error: function (jqXHR, textStatus, errorThrown){
        //Custom Error
        swal("System Error", "There is a problem with the server! Please contact IT support", "error");
      }
    }); 
}
function getFixtures()
{
    $.ajax({url: "<?=base_url();?>Setting/getFixtures", 
    async: true, type: "POST", dataType: 'json',  
    beforeSend: function(){
    $('#loader').show();
    },
    complete: function(){
        $('#loader').hide();
    },
    success: function(data){
    swal(data.ttl,data.msg,data.typ);
   
    },
      error: function (jqXHR, textStatus, errorThrown){
        //Custom Error
        swal("System Error", "There is a problem with the server! Please contact IT support", "error");
      }
    }); 
}


</script>