<div class="loader" id="loader">
</div>

<body class="hold-transition skin-green sidebar-mini">
<!-- Site wrapper -->
<div class="wrapper">
<!-- VIEW MATCH -->
<div class="modal fade" id="modalMatch">
          <div class="modal-dialog modal-lg">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id='league_title'></h4>
              </div>
              <div class="modal-body" >
               <div class="container">
                <div class="row" id="tbody">
                  
                 
                </div>
                </div>
                    
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-danger pull-right" data-dismiss="modal">Close</button>
                
              </div>
            </div>
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>
<!-- EDIT MODAL -->
<div class="modal fade updModal" tabindex="-1" role="dialog" aria-hidden="true" sstyle="width:600px" id=updModal>
  <div class="modal-dialog modal-md">
    <div class="modal-content">

      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
        </button>
        <h4 class="modal-title" id="updModalLabel">Edit Round</h4>

      </div>

      <div class="modal-body">

        <form class="form-horizontal form-label-left">
        <div class="form-group">
          <label class="control-label col-md-4 col-sm-4 col-xs-12">League</label>
          <div class="col-md-6 col-sm-6 col-xs-12">
            <input type="text" disabled="true" id="updleaguename" name="updleague" class="form-control col-md-7 col-xs-12" >
          </div>
        </div>

        <div class="form-group">
          <label class="control-label col-md-4 col-sm-4 col-xs-12">Season</label>
          <div class="col-md-6 col-sm-6 col-xs-12">
            <input type="text" disabled="true" id="updseason" name="updseason" class="form-control col-md-7 col-xs-12" >
          </div>
        </div>
          <div class="form-group">
          <label class="control-label col-md-4 col-sm-4 col-xs-12">Stagename</label>
          <div class="col-md-6 col-sm-6 col-xs-12">
            <input type="text" disabled="true" id="updstagename" name="updstagename" class="form-control col-md-7 col-xs-12" >
          </div>
        </div>
         <div class="form-group">
          <label class="control-label col-md-4 col-sm-4 col-xs-12">Round Name</label>
          <div class="col-md-6 col-sm-6 col-xs-12">
            <input type="text" disabled="true" id="updroundname" name="updstagename" class="form-control col-md-7 col-xs-12" >
          </div>
        </div>

        
    
        <div class="form-group">
          <label class="control-label col-md-4 col-sm-4 col-xs-12">Status</label>
          <div class="col-md-6 col-sm-6 col-xs-12">
            <select class="select2_multiple form-control col-md-7 col-xs-12" name="selStatus"  id="selStatus" style='width: 100%'>
              <option value="">-- SELECT --</option>
              <option value="N">Not Open</option>
              <option value="O">Open</option>
              <option value="S">Start</option>
              <option value="E">End</option>
              <option value="C">Calculating</option>
              <option value="F">Finish</option>
            </select>
          </div>
        </div>

        </form>      

      </div>
      <div class="modal-footer">
        <input type=hidden id=ref_season_id value=''>
        <input type=hidden id=ref_round_id value=''>
        <input type=hidden id=round_id value=''>
        <!-- <input type=hidden id=country_name value=''> -->  
        <!-- <input type=hidden id=league_name value=''> -->
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
        <button type="button" class="btn btn-primary" onclick='updStages();'>Confirm</button>
      </div>

    </div>
  </div>
</div>

<!--- END FOR EDIT MODAL -->
 

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper margin-top_cont">

    

 
        <!-- <div class="box-header with-border">
            <button class="btn btn-md btn-info" onclick="getCountries();">
            <i class='fa fa-plus'></i> Get Countries / Leagues</button>
            <br><br><h1 class="box-title">Leagues</h1>
        </div> -->

    <!-- Fitler content -->
    <section class="content" style='min-height:0px'>
       <div class="row">
        <div class="col-xs-12">
          <div class="box">
          <div class="box-header">
            <h3 class="box-title"><i class="fa fa-filter"></i> Round Filter Box</h3>
            <hr>
          </div>
           <div class="box-body">

             <div class="col-sm-2">
                <div class="form-group">
                  <label for="countryfilter">Country</label>
                    <select class="form-control filter select2" id="countryfilter" style="width: 100%;" onchange="getLeagueList();getSeasonList();">
                      
                    </select>
                </div>
              </div>

              <div class="col-sm-2">
                <div class="form-group">
                  <label for="leaguefilter">League</label>
                    <select class="form-control filter select2" id="leaguefilter" style="width: 100%;" onchange="getSeasonList();" >
                      
                    </select>
                </div>
              </div>

              <div class="col-sm-2">
                <div class="form-group">
                  <label for="seasonfilter">Season</label>
                  <select class="form-control filter select2" id="seasonfilter" style="width: 100%;">
                    <option value=''>All</option>
                  </select>
                </div>
              </div>

              <div class="col-sm-2">
                <div class="form-group">
                  <label for="statusfilter">Status</label>
                  <select class="form-control filter select2" id="statusfilter" style="width: 100%;">
                    <option value=''>All</option>
                    <option value='N'>Not Open</option>
                    <option value='O'>Open</option>
                    <option value='S'>Start</option>
                    <option value='E'>End</option>
                    <option value='C'>Calculating</option>
                    <option value='F'>Finish</option>
                  </select>
                </div>
              </div>
              <div class="col-sm-1"> 
                <div class="form-group">
                <button class="btn btn-default" id="btnReset"  style="margin-top: 23px;">Reset</button>
                </div>
              </div>
            
           </div>
          </div>
         </div>
        </div>
    </section>
    <!-- End Fitler content -->

    <!-- Main content -->
           <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="box">
          <div class="box-header">
            <h3 class="box-title"><i class="fa fa-sitemap"></i> Rounds List</h3>
            <hr>
            <div>
            <ul style="list-style-type: square">
              <li style='color:#5043c3'>Not Open</li>
              <li style='color:#3c8dbc'>Open</li>
              <li style='color:#00a65a'>Finish</li>
            </ul>
              
            </div>
          </div>
        <div class="box-body" id='leaguebody'>
        


        </div>
            <!-- /.box-body -->
            
            <!-- /.box-footer-->
    </div>
          <!-- /.box -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> 2.4.0
    </div>
    <strong>Copyright &copy; 2018 <a href="#">Siegreich Solutions inc.</a></strong> All rights
    reserved.
  </footer>

  
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>

  <input type="hidden" name="country_id" id="country_id" value="">
</div>
<!-- ./wrapper -->

<?php $this->load->view('templates/admin_footer');?>

<script>
var datatable;
// var country_id = $('#country_id').val();
$(document).ready(function() {
  $('#loader').hide();
  // getCountryList();
  // getLeagueList();
  // getSeasonList();
  // getStatusList();
  // getSeasonNames();
  getLoadRounds();
  

  // loadData();
  
  $('.sidebar-menu').tree();
  $('.select2').select2();

  // $( "#btnReset" ).on( "click", function() {
  //       $('.filter').empty();
  //       getCountryList();
  //       getLeagueList();
  //       getSeasonList();
  //       getStatusList();
       
        
  //   });

 //   $(".filter").change(function() {
 //      var cntry = $('#countryfilter').val();
 //      var selLeague = $('#leaguefilter').val();
 //      var selSeason = $('#seasonfilter').val();
 //      var selstatus = $('#statusfilter').val();
 //      var filterarray = [];


 //      filterarray.push({'cntry' : cntry});
 //      filterarray.push({'selLeague' : selLeague});
 //      filterarray.push({'selSeason' : selSeason});
 //      filterarray.push({'selstatus' : selstatus});


 //      $('#leagueListTable').DataTable().destroy();
 //      // datatable.destroy();

 //      if(filterarray !=null)
 //      {
 //        loadData(cntry,selLeague,selSeason,selstatus)
 //      }
 //      else
 //      {
 //        loadData();
 //      }

           
 //    });

 });


  function getCountryList()
  {

    var html = "";
    var sel = $("#countryfilter");
    // var params = { c:brandid , wcacc:wcacc};
    
    $.ajax({
        url : "<?php echo $this->config->item('base_url'); ?>Leagues/getCountryList",
        type: "POST",
        dataType : 'json',
        // data : params,
        success: function (data, textStatus, errorThrown){
          
          sel.append($('<option>',
               {
                  value: "",
                  text : "All"
              }));

          for (var i = 0; i < data.length; i++)
          { 
            // console.log(data);
               sel.append($('<option>',
               {
                  value: data[i].country_id,
                  text : data[i].country_name
              }));
              // $("#leaguefilter").change();
          }


        },
        error: function (jqXHR, textStatus, errorThrown){
          swal("System Error", "There is a problem with the server! Please contact IT support", "error");
        }
      });


  }

  function getLeagueList()
  {
    
    var html = "";
    var cntry = $("#countryfilter").val();
    var selLeague = $("#leaguefilter");
    var params = { cntry : cntry};

    // console.log(cntry);
    selLeague.empty();
    $.ajax({
        url : "<?php echo $this->config->item('base_url'); ?>Leagues/getLeagueList",
        type: "POST",
        dataType : 'json',
        data : params,
        async:false,

        success: function (data, textStatus, errorThrown){
          selLeague.append($('<option>',
               {
                  value: "",
                  text : "All"
              }));

          for (var i = 0; i < data.length; i++)
          { 
            // console.log(data);
               selLeague.append($('<option>',
               {
                  value: data[i].league_id,
                  text : data[i].league_name
              }));
          }
        },
        error: function (jqXHR, textStatus, errorThrown){
          swal("System Error", "There is a problem with the server! Please contact IT support", "error");
        }
      });
    // loadData();
  }

  function getSeasonList()
  {
    
    var html = "";
    var cntry = $("#countryfilter").val();
    var selLeague = $("#leaguefilter").val();
    var selSeason = $("#seasonfilter");
    
    var params = { cntry : cntry, selLeague : selLeague};

    // console.log(cntry);
    selSeason.empty();
    $.ajax({
        url : "<?php echo $this->config->item('base_url'); ?>Leagues/getSeasonList",
        type: "POST",
        dataType : 'json',
        data : params,
        async:false,

        success: function (data, textStatus, errorThrown){
          selSeason.append($('<option>',
               {
                  value: "",
                  text : "All"
              }));

          for (var i = 0; i < data.length; i++)
          { 
            // console.log(data);
               selSeason.append($('<option>',
               {
                  value: data[i].season_name,
                  text : data[i].season_name
              }));
          }
        },
        error: function (jqXHR, textStatus, errorThrown){
          swal("System Error", "There is a problem with the server! Please contact IT support", "error");
        }
      });
  }

  function getStatusList()
  {
    
    var html = "";
    // var cntry = $("#countryfilter").val();
    // var selLeague = $("#leaguefilter").val();
    // var selSeason = $("#seasonfilter").val();
    var selstatus = $("#statusfilter");
    // selstatus.empty();
    
    // var params = { cntry : cntry, selLeague : selLeague, selstatus:selstatus };

    var stat_code = ['N','O','S','E','C','F'];
    var stat = ['Not Open','Open','Start','End','Calculating','Finish'];


    selstatus.append($('<option>',
               {
                  value: "",
                  text : "All"
              }));
    for (var i = 0; i < stat_code.length; i++)
          { 
            // console.log(stat_code[i]);
            // console.log(stat);
               selstatus.append($('<option>',
               {
                  value: stat_code[i],
                  text : stat[i]
              }));
          }

    // // console.log(cntry);
    

  }

 
   function showModal(league_name,season_name,stage_name,round_name,status,round_id,ref_round_id,ref_season_id){
      
      
      $("#updModal").modal(); 
      $('#updleaguename').val(league_name);
      $('#updseason').val(season_name);
      $('#updstagename').val(stage_name);
      $('#updroundname').val(round_name);
      $('#selStatus').val(status);
      $('#round_id').val(round_id);
      $('#ref_round_id').val(ref_round_id);
      $('#ref_season_id').val(ref_season_id);
      
     
   }
  function showMatch(round_id,league_name,season_name){
      
    

     var formData = { round_id: round_id,  
                    };
     $.ajax({
        url: "<?=base_url();?>StageRound/MatchShow", 
        type: "POST", 
        dataType: 'json', 
        data: formData, 
        beforeSend: function(){
        $('#loader').show();
        },
        complete: function(){
            $('#loader').hide();
        },
        success: function(data, textStatus, jqXHR)

        {
          
        var html = '';
        $("#modalMatch").modal(); 
        $("#league_title").text(league_name +" - "+season_name);
        console.log(data.length);
        html += '<table><tbody>';

        for(var x = 0 ; x < data.length ; x++){
          // console.log(data[x]['team_name']);
          if(data[x]['home_team_logo'] != null || data[x]['away_team_logo'] != null){
            html += " <div class='col-sm-5' ><img style='width:30%' src='<?=base_url() ?>uploads/teams/"+data[x]['home_team_logo']+"'><span style='font-weight:bold;font-size: 42px;'> - </span><img style='width:30%' src='<?=base_url() ?>uploads/teams/"+data[x]['away_team_logo']+"'> <br><div style='width:60%'><span>"+data[x]['home_team_name']+"</span><span style='float: right;'>"+data[x]['away_team_name']+"</span></div></div><br>";
          }
          else{
           html += " <div class='col-sm-5' ><img style='width:30%' src='<?php echo $this->config->item('base_url'); ?>public/img/noimage.png'><span style='font-weight:bold;font-size: 42px;'> - </span><img style='width:30%' src='<?=base_url() ?>public/img/noimage.png'> <br><div style='width:60%'><span>"+data[x]['home_team_name']+"</span><span style='float: right;'>"+data[x]['away_team_name']+"</span></div></div>";
         
          }
          
        }
        html +="</tbody></table>"
    
        $('#tbody').html(html);
       }}); 
     
       
   }
  
 function updStages(){
    var round_id = $("#round_id").val();
    var status = $("#selStatus").val();
    var ref_round_id = $("#ref_round_id").val();
    var ref_season_id = $("#ref_season_id").val();
   
    if (status == ""){
    swal("ERROR MESSAGE", "PLEASE SELECT STATUS" , "error");
    }else{
      var formData = { round_id: round_id, 
                      status: status,
                     ref_round_id:ref_round_id,
                     ref_season_id:ref_season_id,
                     
                    };
     
      $.ajax({
        url: "<?=base_url();?>StageRound/updRounds", 
        type: "POST", 
        dataType: 'json', 
        data: formData, 
        beforeSend: function(){
        $('#loader').show();
        },
        complete: function(){
            $('#loader').hide();
        },
        success: function(data, textStatus, jqXHR)

        {
        swal(data.ttl,data.msg,data.typ);
        $("#updModal").modal('toggle');
        getLoadRounds();
       }}); 
 }
}
    function getCountries()
    {
        $.ajax({url: "<?=base_url();?>Leagues/getCountries", 
        async: true, type: "POST", dataType: 'json',  
        beforeSend: function(){
        $('#loader').show();
        },
        complete: function(){
            $('#loader').hide();
        },
        success: function(data){
        swal(data.ttl,data.msg,data.typ);
        reloadData( $("#countryTable") );
        },
          error: function (jqXHR, textStatus, errorThrown){
            //Custom Error
            swal("System Error", "There is a problem with the server! Please contact IT support", "error");
          }
        }); 
    }

    function getLoadRounds()
    {
        $.ajax({
        url : "<?php echo $this->config->item('base_url'); ?>StageRound/getLoadRounds",
        type: "POST",
        dataType : 'json',
        async:false,

        success: function (data, textStatus, errorThrown){

          var roundscount = data['rounds'].length;
          var stagescount = data['stages'].length;
          // console.log(roundscount);
          var html = "";
          for(var i=0;i<stagescount;i++)
            {
              var sid = data['stages'][i]['stage_id'];
              var season_name = data['stages'][i]['season_name'];
              var league_name = data['stages'][i]['league_name'];
              var stage_name = data['stages'][i]['stage_name'];
              var country_name = data['stages'][i]['country_name'];
              html+= "<div class='row'>";
              html+= "<div class='box-header'>";
              html+= "<h3 class='box-title alert alert-success'> <i class='fa fa-trophy'></i> "+ league_name+" - "+country_name+" - "+season_name+" - "+stage_name+"</h3>";
              html+= "</div>";
              for(var x=0;x<roundscount;x++){
                 
                if(data['rounds'].length>0){
                  var stage_id = data['rounds'][x]['stage_id'];
                  var league_id = data['rounds'][x]['league_id'];
                  var ref_season_id = data['rounds'][x]['ref_season_id'];
                  var season_id = data['rounds'][x]['season_id'];
                  var league_name = data['rounds'][x]['league_name'];
                  var season_name = data['rounds'][x]['season_name'];
                  var stage_name = data['rounds'][x]['stage_name'];
                  var round_id = data['rounds'][x]['round_id'];
                  var round_name = data['rounds'][x]['round_name'];
                  var ref_stage_id = data['rounds'][x]['ref_round_id'];
                  var ref_round_id = data['rounds'][x]['ref_round_id'];
                  var status = data['rounds'][x]['status'];
                if(stage_id==sid){
                  var bgcolor = "";  
                  switch(status){
                      case 'N':
                        bgcolor='bg-newcolor';
                      break;
                      case 'O':
                        bgcolor='bg-opencolor';
                      break;
                      case 'F':
                        bgcolor='bg-finishcolor';
                      break;


                  }
                  // console.log(bgcolor);

                  html+= "<div class='col-md-3 league-col'>";
                  html+= "<div class='info-box "+bgcolor+" ' >";
                  html+= "<span class='info-box-icon cursor' onclick=\"showModal('" + league_name + "','" + season_name + "','" + stage_name + "','"+ round_name + "','"+ status +"',"+ round_id +","+ ref_round_id +","+ ref_season_id +")\"><i class='fa fa-soccer-ball-o'></i></span>";
                  html+= "<div class='info-box-content'>";
                  html+= "<span class='info-box-number cursor' onclick=\"showModal('" + league_name + "','" + season_name + "','" + stage_name + "','"+ round_name + "','"+ status +"',"+ round_id +","+ ref_round_id +","+ ref_season_id +")\"> Week "+round_name+"</span>";
                  html+= "<span class='progress-description cursor' onclick=\"showModal('" + league_name + "','" + season_name + "','" + stage_name + "','"+ round_name + "','"+ status +"',"+ round_id +","+ ref_round_id +","+ ref_season_id +")\">"+stage_name+"</span>";
                  if(status=='O'){
                    html+= "<button class='btn btn-info '  onclick=\"showMatch(" + round_id + ",'"+ league_name + "','"+ season_name + "')\">View Matches</button>";
                  }
                  html+= "</div>";
                  html+= "</div>";
                  html+= "</div>";
                }
                }
                
                 
              }
              html+= "</div>";
            }
            $('#leaguebody').html(html);
          
        },
        error: function (jqXHR, textStatus, errorThrown){
          swal("System Error", "There is a problem with the server! Please contact IT support", "error");
        }
      });
    }
  
    </script>
