<div class="loader" id="loader">
</div>

<body class="hold-transition skin-green sidebar-mini">
<!-- Site wrapper -->
<div class="wrapper">

<!-- EDIT MODAL -->
<?php echo form_open_multipart(base_url( 'upload/create' ), array( 'id' => 'updateForm', 'class' => 'form-horizontal form-label-left' )); ?>

<div class="modal fade updModal" tabindex="-1" role="dialog" aria-hidden="true" sstyle="width:600px" id=updModal>
  <div class="modal-dialog modal-md">
    <div class="modal-content">

      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
        </button>
        <h4 class="modal-title" id="updModalLabel">Add User</h4>

      </div>

      <div class="modal-body">

        <form class="form-horizontal form-label-left">

        <div class="form-group">
          <label class="control-label col-md-4 col-sm-4 col-xs-12">League Nickname</label>
          <div class="col-md-6 col-sm-6 col-xs-12">
            <input type="text" id="updleaguenick" name="updleaguenick" class="form-control col-md-7 col-xs-12" >
          </div>
        </div>

        <div class="form-group">
          <label class="control-label col-md-4 col-sm-4 col-xs-12">League</label>
          <div class="col-md-6 col-sm-6 col-xs-12">
            <input type="text" disabled="true" id="updleague" name="updleague" class="form-control col-md-7 col-xs-12" >
          </div>
        </div>

        <div class="form-group">
          <label class="control-label col-md-4 col-sm-4 col-xs-12">Country</label>
          <div class="col-md-6 col-sm-6 col-xs-12">
            <input type="text" disabled="true" id="updcountry" name="updcountry" class="form-control col-md-7 col-xs-12" >
          </div>
        </div>

        <div class="form-group">
          <label class="control-label col-md-4 col-sm-4 col-xs-12">League Logo</label>
          <div class="col-md-6 col-sm-6 col-xs-12">
            <input type="file" id="updxFileTxt" name="updxFileTxt" class="form-control col-md-7 col-xs-12" accept="image/gif,image/jpeg,image/png"> 
          </div>
        </div>

        
    
        <!-- <div class="form-group">
          <label class="control-label col-md-4 col-sm-4 col-xs-12">Status</label>
          <div class="col-md-6 col-sm-6 col-xs-12">
            <select class="select2_multiple form-control col-md-7 col-xs-12" name="selStatus"  id="selStatus" style='width: 100%'>
              <option value="">-- SELECT --</option>
              <option value="N">New</option>
              <option value="O">Open</option>
              <option value="S">Start</option>
              <option value="E">End</option>
              <option value="C">Calculating</option>
              <option value="F">Finish</option>
            </select>
          </div>
        </div> -->

        </form>      

      </div>
      <div class="modal-footer">
        <input type=hidden id=cntryid value=''>
        <input type=hidden id=league_id value=''>
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
        <button type="submit" class="btn btn-primary" >Confirm</button>
      </div>

    </div>
  </div>
</div>
</form>

<!--- END FOR EDIT MODAL -->
 

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper margin-top_cont">

    

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="box">
        <!-- <div class="box-header with-border">
            <button class="btn btn-md btn-info" onclick="getCountries();">
            <i class='fa fa-plus'></i> Get Countries / Leagues</button>
            <br><br><h1 class="box-title">Leagues</h1>
        </div> -->

    <!-- Fitler content -->
    <section class="content" style='min-height:0px'>
       <div class="row">
        <div class="col-xs-12">
          <div class="box">
          <div class="box-header">
            <h3 class="box-title"><i class="fa fa-filter"></i> League Filter Box</h3>
            <hr>
          </div>
           <div class="box-body">

             <div class="col-sm-2">
                <div class="form-group">
                  <label for="countryfilter">Country</label>
                    <select class="form-control filter select2" id="countryfilter" style="width: 100%;">
                      
                    </select>
                </div>
              </div>

              <div class="col-sm-2">
                <div class="form-group">
                  <label for="leaguefilter">League</label>
                    <select class="form-control filter select2" id="leaguefilter" style="width: 100%;" >
                      
                    </select>
                </div>
              </div>

              <div class="col-sm-2">
                <div class="form-group">
                  <label for="seasonfilter">Season</label>
                  <select class="form-control filter select2" id="seasonfilter" style="width: 100%;">
                    <option value=''>All</option>
                  </select>
                </div>
              </div>

              <div class="col-sm-2">
                <div class="form-group">
                  <label for="statusfilter">Status</label>
                  <select class="form-control filter select2" id="statusfilter" style="width: 100%;">
                    <option value=''>All</option>
                    <option value='N'>Not Open</option>
                    <option value='O'>Open</option>
                    <option value='S'>Start</option>
                    <option value='E'>End</option>
                    <option value='C'>Calculating</option>
                    <option value='F'>Finish</option>
                  </select>
                </div>
              </div>
              <div class="col-sm-1"> 
                <div class="form-group">
                <button class="btn btn-default" id="btnReset"  style="margin-top: 23px;">Reset</button>
                </div>
              </div>
            
           </div>
          </div>
         </div>
        </div>
    </section>
    <!-- End Fitler content -->

    <!-- Main content -->
        
        
        <div class="box-body">

          <table id="leagueListTable" class="table table-striped table-bordered" data-page-length='50' style='width:100%'>
          <thead>
            <tr>
              <th style="width: 50px; text-align: center;" >
              <th>League ID
              <th>Country ID
              <th>Ref League ID
              <th>Ref Country ID
              <th>Premier League
              <th>League Nickname
              <!-- <th>Season -->
              <th>Country Name
              <!-- <th>Year Start
              <th>Year End -->
              
              <th>Status
              <th>Image
          </thead>

          </table>
        </div>
        <!-- /.box-body -->
        
        <!-- /.box-footer-->
      </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> 2.4.0
    </div>
    <strong>Copyright &copy; 2018 <a href="#">Siegreich Solutions inc.</a></strong> All rights
    reserved.
  </footer>

  
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>

  <input type="hidden" name="country_id" id="country_id" value="">
</div>
<!-- ./wrapper -->

<?php $this->load->view('templates/admin_footer');?>

<script>
var datatable;
var filetemp;
// var country_id = $('#country_id').val();
$(document).ready(function() {
  $('#loader').hide();
  // getCountryList();
  // getLeagueList();
  // getSeasonList();
  // getStatusList();

  loadData();
  
  $('.sidebar-menu').tree();
  $('.select2').select2();

  $( "#btnReset" ).on( "click", function() {
        $('.filter').empty();
        getCountryList();
        getLeagueList();
        getSeasonList();
        getStatusList();
        $('#leagueListTable').DataTable().destroy();
        loadData();
         // datatable.destroy();
        
    });

   $(".filter").change(function() {
      var cntry = $('#countryfilter').val();
      var selLeague = $('#leaguefilter').val();
      var selSeason = $('#seasonfilter').val();
      var selstatus = $('#statusfilter').val();
      var filterarray = [];


      filterarray.push({'cntry' : cntry});
      filterarray.push({'selLeague' : selLeague});
      filterarray.push({'selSeason' : selSeason});
      filterarray.push({'selstatus' : selstatus});


      $('#leagueListTable').DataTable().destroy();
      // datatable.destroy();

      if(filterarray !=null)
      {
        loadData(cntry,selLeague,selSeason,selstatus)
      }
      else
      {
        loadData();
      }

           
    });

 });


  function getCountryList()
  {

    var html = "";
    var sel = $("#countryfilter");
    // var params = { c:brandid , wcacc:wcacc};
    
    $.ajax({
        url : "<?php echo $this->config->item('base_url'); ?>Setting/getCountryList",
        type: "POST",
        dataType : 'json',
        // data : params,
        success: function (data, textStatus, errorThrown){
          
          sel.append($('<option>',
               {
                  value: "",
                  text : "All"
              }));

          for (var i = 0; i < data.length; i++)
          { 
            // console.log(data);
               sel.append($('<option>',
               {
                  value: data[i].country_id,
                  text : data[i].country_name
              }));
              // $("#leaguefilter").change();
          }


        },
        error: function (jqXHR, textStatus, errorThrown){
          swal("System Error", "There is a problem with the server! Please contact IT support", "error");
        }
      });


  }

  function getLeagueList()
  {
    
    var html = "";
    var cntry = $("#countryfilter").val();
    var selLeague = $("#leaguefilter");
    var params = { cntry : cntry};

    // console.log(cntry);
    selLeague.empty();
    $.ajax({
        url : "<?php echo $this->config->item('base_url'); ?>Setting/getLeagueList",
        type: "POST",
        dataType : 'json',
        data : params,
        async:false,

        success: function (data, textStatus, errorThrown){
          selLeague.append($('<option>',
               {
                  value: "",
                  text : "All"
              }));

          for (var i = 0; i < data.length; i++)
          { 
            // console.log(data);
               selLeague.append($('<option>',
               {
                  value: data[i].league_id,
                  text : data[i].league_name
              }));
          }
        },
        error: function (jqXHR, textStatus, errorThrown){
          swal("System Error", "There is a problem with the server! Please contact IT support", "error");
        }
      });
    // loadData();
  }

  function getSeasonList()
  {
    
    var html = "";
    var cntry = $("#countryfilter").val();
    var selLeague = $("#leaguefilter").val();
    var selSeason = $("#seasonfilter");
    
    var params = { cntry : cntry, selLeague : selLeague};

    // console.log(cntry);
    selSeason.empty();
    $.ajax({
        url : "<?php echo $this->config->item('base_url'); ?>Setting/getSeasonList",
        type: "POST",
        dataType : 'json',
        data : params,
        async:false,

        success: function (data, textStatus, errorThrown){
          selSeason.append($('<option>',
               {
                  value: "",
                  text : "All"
              }));

          for (var i = 0; i < data.length; i++)
          { 
            // console.log(data);
               selSeason.append($('<option>',
               {
                  value: data[i].season_name,
                  text : data[i].season_name
              }));
          }
        },
        error: function (jqXHR, textStatus, errorThrown){
          swal("System Error", "There is a problem with the server! Please contact IT support", "error");
        }
      });
  }

  function getStatusList()
  {
    
    var html = "";
    // var cntry = $("#countryfilter").val();
    // var selLeague = $("#leaguefilter").val();
    // var selSeason = $("#seasonfilter").val();
    var selstatus = $("#statusfilter");
    // selstatus.empty();
    
    // var params = { cntry : cntry, selLeague : selLeague, selstatus:selstatus };

    var stat_code = ['N','O','S','E','C','F'];
    var stat = ['Not Open','Open','Start','End','Calculating','Finish'];


    selstatus.append($('<option>',
               {
                  value: "",
                  text : "All"
              }));
    for (var i = 0; i < stat_code.length; i++)
          { 
            // console.log(stat_code[i]);
            // console.log(stat);
               selstatus.append($('<option>',
               {
                  value: stat_code[i],
                  text : stat[i]
              }));
          }

    // // console.log(cntry);
    

  }



  function loadData(cntry,selLeague,selSeasonsel,status)
  {
    var cntry = $("#countryfilter").val();
    var selLeague = $("#leaguefilter").val();
    var selSeason = $("#seasonfilter").val();
    var selstatus = $("#statusfilter").val();

    // alert(cntry+' '+selLeague+' '+selSeason+' '+selstatus+' ');
    var params = {cntry:cntry, selLeague:selLeague, selSeason:selSeason, selstatus:selstatus };

    datatable = $('#leagueListTable').dataTable( {
    "order": [[ 5, "asc" ]],
      responsive: true, 
      dom: 'Bfrtip',

      "buttons": [
              'excel',
              'pdf'
          ],
          "scrollX": true,
      "pagingType": "full_numbers",
      "processing": true,
      "serverSide": true,
      "ajax":{
        url :"<?=base_url();?>Setting/leagueListing/?k=" + Math.random(), // json datasource
        type: "post",  // method  , by default get  
        data:{cntry:cntry,
              selLeague:selLeague,
              selSeason:selSeason,
              selstatus:selstatus
              },  // method  , by default g
        error: function(){  // error handling
          $(".grid-error").html("");
          $("#roleTable").append('<tbody class="grid-error"><tr><th colspan="7">NO DATA FOUND</th></tr></tbody>');
          $("#roleTable_processing").css("display","none");        
        }
      },
      "columns" : [
        // { "data" : "league_id" },
        { "data" : "league_id" },
        { "data" : "league_id" ,visible: false },
        { "data" : "country_id" ,visible: false },
        { "data" : "ref_league_id" ,visible: false },
        { "data" : "ref_country_id" ,visible: false },
        { "data" : "league_name" },
        { "data" : "league_nick" },
        
        // { "data" : "season" },
        // { "data" : "year_start" },
        // { "data" : "year_end" },
        { "data" : "country_name" },
        { "data" : "status",visible: false  },
        { "data" : "img_path" }
      ]
      ,
       "columnDefs" : [ {
        "targets" : 0,
        className : "text-center",
        "orderable": false,
        "render": function( data, type, row, meta ) {
            var html = "";
            var nickname = row["league_nick"];
            if(nickname == null || nickname == '')
            {
              nickname = '';
            }
            
              
              html = " <a href='#'  onclick=\"updLeagueModal(" + row["country_id"] + ",'" + row["country_name"] + "','" + row["league_id"] + "','" + row["league_name"] + "','"+ row["league_nick"] + "','"+ row["status"] +"')\"> <span class='glyphicon glyphicon-edit' aria-hidden='true'></span></a>";
              // html += "&nbsp;&nbsp;"+"<a href='#' onclick=\"delUser(" + row["user_id"] + ",'"+ row["realname"] + "')\"><span class='glyphicon glyphicon-trash' style='color:red;' aria-hidden='true'></span> </a>";
              
            
            return html;
            }
       },{
          "targets" : 8,  
          // "orderable": true,
          "render" : function( data, type, row, meta ) {
            var html;
              switch(data){
                case "N" : html = "<span class='label label-new'>Not Open</span>"; break;
                case "O" : html = "<span class='label label-primary'>Open</span>"; break;
                case "S" : html = "<span class='label label-start'>Start</span>"; break;
                case "E" : html = "<span class='label label-danger'>End</span>"; break;
                case "C" : html = "<span class='label label-calculating'>Calculating</span>"; break;
                case "F" : html = "<span class='label label-success'>Finish</span></span>"; break;
              }
              return html;
            }     
        },
        {
        "targets" : 9,
        className : "text-center",
        "orderable": false,
        "render": function( data, type, row, meta ) {
              var html = "";
              var img = row["img_path"];
              if(img) 
              {
                html += "<span >";
                html += "<img src='<?=base_url();?>uploads/leagues/"+ row["img_path"] +"' class='league_logo_img'>";
                html += "</span>";
              }
              return html;
            }
        }

        ]
    }); 
    }

    function updLeagueModal(cntryid,cntryname,lgid,ldname,leaguenick){
      // alert(seasonid);
      $("#cntryid").val(cntryid);
      $("#updcountry").val(cntryname);
      $("#league_id").val(lgid);
      $("#updleague").val(ldname);
      $("#updleaguenick").val(leaguenick);
      // $("#selStatus").val(status);
      $("#updModal").modal(); 
      $("#updModalLabel").html("Update League");
      // $("#upd_league").html("Are you sure you want to add league "+ldname+" ?");
    }

    $("#updateForm").submit(function(e){
      e.preventDefault();
      var cntryid = $("#cntryid").val();
      var league_id = $("#league_id").val();
      var lgnick  = $("#updleaguenick").val();
      var file = $("#updxFileTxt").val().split(".");

      var data = new FormData();

      data.append('cntryid', cntryid);
      data.append('league_id', league_id);
      data.append('lgnick', lgnick);
      data.append('updxFileTxt', filetemp);

      if(lgnick == '')
      {
        swal("Error", "All fields are required!", "error");
      }else{
        $.ajax({
                url : "<?=base_url();?>Setting/updLeague",
                type: "POST",
                dataType : 'json',
                cache :  false,
                contentType : false,
                processData : false,
                async: false,
                dataType : 'json',
                data : data,
                beforeSend: function(){
                $('#loader').show();
                },
                complete: function(){
                    $('#loader').hide();
                },
                success: function(data, textStatus, jqXHR){
                  swal(data.ttl,data.msg,data.typ);
                  $("#updModal").modal('toggle'); 
                  reloadData( $("#leagueListTable") );

                },
                error: function (jqXHR, textStatus, errorThrown){
                  //Custom Error
                  swal("System Error", "There is a problem with the server! Please contact IT support", "error");
                }
            });
      }

    });

   
    function getCountries()
    {
        $.ajax({url: "<?=base_url();?>Leagues/getCountries", 
        async: true, type: "POST", dataType: 'json',  
        beforeSend: function(){
        $('#loader').show();
        },
        complete: function(){
            $('#loader').hide();
        },
        success: function(data){
        swal(data.ttl,data.msg,data.typ);
        reloadData( $("#countryTable") );
        },
          error: function (jqXHR, textStatus, errorThrown){
            //Custom Error
            swal("System Error", "There is a problem with the server! Please contact IT support", "error");
          }
        }); 
    }

  $(function() {

  // We can attach the `fileselect` event to all file inputs on the page
    $(document).on('change', ':file', function() {
      var input = $(this),
          numFiles = input.get(0).files ? input.get(0).files.length : 1,
          label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
      input.trigger('fileselect', [numFiles, label]);
      //store to var
      filetemp = input.get(0).files[0];
    });

    // We can watch for our custom `fileselect` event like this
      $(document).ready( function() {
          $(':file').on('fileselect', function(event, numFiles, label) {
              var input = $(this).parents('.input-group').find(':text'),
                  log = numFiles > 1 ? numFiles + ' files selected' : label;

              if( input.length ) {
                  input.val(log);
                  if (numFiles > 1) { input.val(''); }
              }

          });
      });

  });
    </script>
