<div class="loader" id="loader">
</div>

<!-- EDIT MODAL -->
<div class="modal fade updModal" tabindex="-1" role="dialog" aria-hidden="true" sstyle="width:600px" id=updModal>
  <div class="modal-dialog modal-md">
    <div class="modal-content">

      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
        </button>
        <h4 class="modal-title" id="updModalLabel">Add User</span></h4>

      </div>
      <div class="modal-body">

        <form class="form-horizontal form-label-left">
        <div class="form-group">
          <label class="control-label col-md-4 col-sm-4 col-xs-12">League</label>
          <div class="col-md-6 col-sm-6 col-xs-12">
            <input type="text" disabled="true" id="updleague" name="updleague" class="form-control col-md-7 col-xs-12" >
          </div>
        </div>

        <div class="form-group">
          <label class="control-label col-md-4 col-sm-4 col-xs-12">Country</label>
          <div class="col-md-6 col-sm-6 col-xs-12">
            <input type="text" disabled="true" id="updcountry" name="updcountry" class="form-control col-md-7 col-xs-12" >
          </div>
        </div>

        <div class="form-group">
          <label class="control-label col-md-4 col-sm-4 col-xs-12">Stage Name</label>
          <div class="col-md-6 col-sm-6 col-xs-12">
            <input type="text" disabled="true" id="updStageName" name="updStageName" class="form-control col-md-7 col-xs-12" >
          </div>
        </div>

        
    
        <div class="form-group">
          <label class="control-label col-md-4 col-sm-4 col-xs-12">Status</label>
          <div class="col-md-6 col-sm-6 col-xs-12">
            <select class="select2_multiple form-control col-md-7 col-xs-12" name="updStatus"  id="updStatus" style='width: 100%'>
              <option value="">-- SELECT --</option>
              <option value="N">New</option>
              <option value="O">Open</option>
              <option value="S">Start</option>
              <option value="E">End</option>
              <option value="C">Calculating</option>
              <option value="F">Finish</option>
            </select>
          </div>
        </div>

        </form>      

      </div>
      <div class="modal-footer">
        <input type=hidden id=cntryid value=''>
        <input type=hidden id=stgid value=''>
        <!-- <input type=hidden id=country_name value=''> -->
        <input type=hidden id=league_id value=''>
        <!-- <input type=hidden id=league_name value=''> -->
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
        <button type="button" class="btn btn-primary" onclick='updLeague();'>Confirm</button>
      </div>

    </div>
  </div>
</div>

<!--- END FOR EDIT MODAL -->

<body class="hold-transition skin-green sidebar-mini">
<!-- Site wrapper -->
<div class="wrapper">

 

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper margin-top_cont">

    

    <!-- Main content -->
    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="box">
        
        <div class="box-header with-border">
          <h1 class="box-title">Countries</h1>&nbsp;
          <button class="btn btn-md btn-info" onclick="getCountries();"><i class='fa fa-plus'></i> Get Countries / Leagues</button>
        </div>
        <div class="box-body">

          <table id="countryTable" class="table table-striped table-bordered" data-page-length='50' style='width:100%'>
          <thead>
            <tr>
              <!-- <th style="width: 50px; text-align: center;" > -->
              <th>Country ID
              <th>Ref Country ID
              <th >Country Name
              <th>Status

          </thead>

          </table>
        </div>
        <!-- /.box-body -->
        
        <!-- /.box-footer-->
      </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

 <footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> 2.4.0
    </div>
    <strong>Copyright &copy; 2018 <a href="#">Siegreich Solutions inc.</a></strong> All rights
    reserved.
  </footer>

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Create the tabs -->
    <ul class="nav nav-tabs nav-justified control-sidebar-tabs">
      <li><a href="#control-sidebar-home-tab" data-toggle="tab"><i class="fa fa-home"></i></a></li>

      <li><a href="#control-sidebar-settings-tab" data-toggle="tab"><i class="fa fa-gears"></i></a></li>
    </ul>
    <!-- Tab panes -->
    <div class="tab-content">
      <!-- Home tab content -->
      <div class="tab-pane" id="control-sidebar-home-tab">
        <h3 class="control-sidebar-heading">Recent Activity</h3>
        <ul class="control-sidebar-menu">
          <li>
            <a href="javascript:void(0)">
              <i class="menu-icon fa fa-birthday-cake bg-red"></i>

              <div class="menu-info">
                <h4 class="control-sidebar-subheading">Langdon's Birthday</h4>

                <p>Will be 23 on April 24th</p>
              </div>
            </a>
          </li>
          <li>
            <a href="javascript:void(0)">
              <i class="menu-icon fa fa-user bg-yellow"></i>

              <div class="menu-info">
                <h4 class="control-sidebar-subheading">Frodo Updated His Profile</h4>

                <p>New phone +1(800)555-1234</p>
              </div>
            </a>
          </li>
          <li>
            <a href="javascript:void(0)">
              <i class="menu-icon fa fa-envelope-o bg-light-blue"></i>

              <div class="menu-info">
                <h4 class="control-sidebar-subheading">Nora Joined Mailing List</h4>

                <p>nora@example.com</p>
              </div>
            </a>
          </li>
          <li>
            <a href="javascript:void(0)">
              <i class="menu-icon fa fa-file-code-o bg-green"></i>

              <div class="menu-info">
                <h4 class="control-sidebar-subheading">Cron Job 254 Executed</h4>

                <p>Execution time 5 seconds</p>
              </div>
            </a>
          </li>
        </ul>
        <!-- /.control-sidebar-menu -->

        <h3 class="control-sidebar-heading">Tasks Progress</h3>
        <ul class="control-sidebar-menu">
          <li>
            <a href="javascript:void(0)">
              <h4 class="control-sidebar-subheading">
                Custom Template Design
                <span class="label label-danger pull-right">70%</span>
              </h4>

              <div class="progress progress-xxs">
                <div class="progress-bar progress-bar-danger" style="width: 70%"></div>
              </div>
            </a>
          </li>
          <li>
            <a href="javascript:void(0)">
              <h4 class="control-sidebar-subheading">
                Update Resume
                <span class="label label-success pull-right">95%</span>
              </h4>

              <div class="progress progress-xxs">
                <div class="progress-bar progress-bar-success" style="width: 95%"></div>
              </div>
            </a>
          </li>
          <li>
            <a href="javascript:void(0)">
              <h4 class="control-sidebar-subheading">
                Laravel Integration
                <span class="label label-warning pull-right">50%</span>
              </h4>

              <div class="progress progress-xxs">
                <div class="progress-bar progress-bar-warning" style="width: 50%"></div>
              </div>
            </a>
          </li>
          <li>
            <a href="javascript:void(0)">
              <h4 class="control-sidebar-subheading">
                Back End Framework
                <span class="label label-primary pull-right">68%</span>
              </h4>

              <div class="progress progress-xxs">
                <div class="progress-bar progress-bar-primary" style="width: 68%"></div>
              </div>
            </a>
          </li>
        </ul>
        <!-- /.control-sidebar-menu -->

      </div>
      <!-- /.tab-pane -->
      <!-- Stats tab content -->
      <div class="tab-pane" id="control-sidebar-stats-tab">Stats Tab Content</div>
      <!-- /.tab-pane -->
      <!-- Settings tab content -->
      <div class="tab-pane" id="control-sidebar-settings-tab">
        <form method="post">
          <h3 class="control-sidebar-heading">General Settings</h3>

          <div class="form-group">
            <label class="control-sidebar-subheading">
              Report panel usage
              <input type="checkbox" class="pull-right" checked>
            </label>

            <p>
              Some information about this general settings option
            </p>
          </div>
          <!-- /.form-group -->

          <div class="form-group">
            <label class="control-sidebar-subheading">
              Allow mail redirect
              <input type="checkbox" class="pull-right" checked>
            </label>

            <p>
              Other sets of options are available
            </p>
          </div>
          <!-- /.form-group -->

          <div class="form-group">
            <label class="control-sidebar-subheading">
              Expose author name in posts
              <input type="checkbox" class="pull-right" checked>
            </label>

            <p>
              Allow the user to show his name in blog posts
            </p>
          </div>
          <!-- /.form-group -->

          <h3 class="control-sidebar-heading">Chat Settings</h3>

          <div class="form-group">
            <label class="control-sidebar-subheading">
              Show me as online
              <input type="checkbox" class="pull-right" checked>
            </label>
          </div>
          <!-- /.form-group -->

          <div class="form-group">
            <label class="control-sidebar-subheading">
              Turn off notifications
              <input type="checkbox" class="pull-right">
            </label>
          </div>
          <!-- /.form-group -->

          <div class="form-group">
            <label class="control-sidebar-subheading">
              Delete chat history
              <a href="javascript:void(0)" class="text-red pull-right"><i class="fa fa-trash-o"></i></a>
            </label>
          </div>
          <!-- /.form-group -->
        </form>
      </div>
      <!-- /.tab-pane -->
    </div>
  </aside>
  <!-- /.control-sidebar -->
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<?php $this->load->view('templates/admin_footer');?>

<script>
var datatable;
$(document).ready(function() {
  $('#loader').hide();
  $('.sidebar-menu').tree();
  loadData();
 });


function loadData()
  {
    datatable = $('#countryTable').dataTable( {
    "order": [[ 2, "asc" ]],
      responsive: true, 
      dom: 'Bfrtip',

      "buttons": [
              'excel',
              'pdf'
          ],
          "scrollX": true,
      "pagingType": "full_numbers",
      "processing": true,
      "serverSide": true,
      "ajax":{
        url :"<?=base_url();?>Leagues/countrylisting?k=" + Math.random(), // json datasource
        type: "post",  // method  , by default get      
        error: function(){  // error handling
          $(".grid-error").html("");
          $("#roleTable").append('<tbody class="grid-error"><tr><th colspan="7">NO DATA FOUND</th></tr></tbody>');
          $("#roleTable_processing").css("display","none");        
        }
      },
      "columns" : [
        { "data" : "country_id" },
        { "data" : "ref_country_id" },
        { "data" : "country_name" },
        { "data" : "status" }
      ]
      ,
       "columnDefs" : [ 
       {
        "targets" : 0,
        "orderable": false
      
       },
       {
        "targets" : 2,
        "orderable": true,
        "render": function( data, type, row, meta ) {
            var html = "";
            
              html = " <a href='#'  onclick=\"loadLeagues(" + row["country_id"] + ")\"> <span>"+row["country_name"]+"</span></a>";
            
            return html;
            }
       },{
          "targets" : 3,  
          "orderable": false,
          "render" : function( data, type, row, meta ) {
            var html;
              switch(data){
                case "N" : html = "New"; break;
                case "O" : html = "Open"; break;
                case "S" : html = "Start"; break;
                case "E" : html = "End"; break;
                case "C" : html = "Calculating"; break;
                case "F" : html = "Finish"; break;
              }
              return html;
            }     
        }]
    }); 
    }

    function getCountries()
    {
        $.ajax({url: "<?=base_url();?>Leagues/getCountries", 
        async: true, type: "POST", dataType: 'json',  
        beforeSend: function(){
        $('#loader').show();
        },
        complete: function(){
            $('#loader').hide();
        },
        success: function(data){
        swal(data.ttl,data.msg,data.typ);
        reloadData( $("#countryTable") );
        },
          error: function (jqXHR, textStatus, errorThrown){
            //Custom Error
            swal("System Error", "There is a problem with the server! Please contact IT support", "error");
          }
        }); 
    }

    function loadLeagues(id)
    {
      // alert(id);
      console.log(id);
      var url = "<?=base_url();?>Leagues/viewLeagues";
      window.location = url+"/"+id;
    }
    </script>
