<div class="loader" id="loader">
</div>

<body class="hold-transition skin-green sidebar-mini">
<!-- Site wrapper -->
<div class="wrapper">

<!-- EDIT MODAL -->
<div class="modal fade updModal" tabindex="-1" role="dialog" aria-hidden="true" sstyle="width:600px" id=updModal>
  <div class="modal-dialog modal-md">
    <div class="modal-content">

      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
        </button>
        <h4 class="modal-title" id="updModalLabel">Open/Finish Stage</h4>

      </div>

      <div class="modal-body">

        <form class="form-horizontal form-label-left">
        <div class="form-group">
          <label class="control-label col-md-4 col-sm-4 col-xs-12">League</label>
          <div class="col-md-6 col-sm-6 col-xs-12">
            <input type="text" disabled="true" id="updleaguename" name="updleague" class="form-control col-md-7 col-xs-12" >
          </div>
        </div>

        <div class="form-group">
          <label class="control-label col-md-4 col-sm-4 col-xs-12">Season</label>
          <div class="col-md-6 col-sm-6 col-xs-12">
            <input type="text" disabled="true" id="updseason" name="updseason" class="form-control col-md-7 col-xs-12" >
          </div>
        </div>
          <div class="form-group">
          <label class="control-label col-md-4 col-sm-4 col-xs-12">Stagename</label>
          <div class="col-md-6 col-sm-6 col-xs-12">
            <input type="text" disabled="true" id="updstagename" name="updstagename" class="form-control col-md-7 col-xs-12" >
          </div>
        </div>

        
    
        <div class="form-group">
          <label class="control-label col-md-4 col-sm-4 col-xs-12">Status</label>
          <div class="col-md-6 col-sm-6 col-xs-12">
            <select class="select2_multiple form-control col-md-7 col-xs-12" name="selStatus"  id="selStatus" style='width: 100%'>
              <option value="">-- SELECT --</option>
              <option value="N">Not Open</option>
              <option value="O">Open</option>
              <option value="F">Finish</option>
            </select>
          </div>
        </div>

        </form>      

      </div>
      <div class="modal-footer">
        <input type=hidden id=ref_season_id value=''>
        <input type=hidden id=stage_id value=''>
        <!-- <input type=hidden id=country_name value=''> -->
        <input type=hidden id=league_id value=''>
        <input type=hidden id=season_id value=''>
        <!-- <input type=hidden id=league_name value=''> -->
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
        <button type="button" class="btn btn-primary" onclick='updStages();'>Confirm</button>
      </div>

    </div>
  </div>
</div>

<!--- END FOR EDIT MODAL -->
 

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper margin-top_cont">

    

 
        <!-- <div class="box-header with-border">
            <button class="btn btn-md btn-info" onclick="getCountries();">
            <i class='fa fa-plus'></i> Get Countries / Leagues</button>
            <br><br><h1 class="box-title">Leagues</h1>
        </div> -->

    <!-- Fitler content -->
    <section class="content" style='min-height:0px'>
       <div class="row">
        <div class="col-xs-12">
          <div class="box">
          <div class="box-header">
            <h3 class="box-title"><i class="fa fa-filter"></i> Stages Filter Box</h3>
            <hr>
          </div>
           <div class="box-body">

             <div class="col-sm-2">
                <div class="form-group">
                  <label for="countryfilter">Country</label>
                    <select class="form-control filter select2" id="countryfilter" style="width: 100%;" onchange="getLeagueList();getSeasonList();">
                      
                    </select>
                </div>
              </div>

              <div class="col-sm-2">
                <div class="form-group">
                  <label for="leaguefilter">League</label>
                    <select class="form-control filter select2" id="leaguefilter" style="width: 100%;" onchange="getSeasonList();" >
                      
                    </select>
                </div>
              </div>

              <div class="col-sm-2">
                <div class="form-group">
                  <label for="seasonfilter">Season</label>
                  <select class="form-control filter select2" id="seasonfilter" style="width: 100%;">
                    <option value=''>All</option>
                  </select>
                </div>
              </div>

              <div class="col-sm-2">
                <div class="form-group">
                  <label for="statusfilter">Status</label>
                  <select class="form-control filter select2" id="statusfilter" style="width: 100%;">
                    <option value=''>All</option>
                    <option value='N'>Not Open</option>
                    <option value='O'>Open</option>
                    <option value='S'>Start</option>
                    <option value='E'>End</option>
                    <option value='C'>Calculating</option>
                    <option value='F'>Finish</option>
                  </select>
                </div>
              </div>
              <div class="col-sm-1"> 
                <div class="form-group">
                <button class="btn btn-default" id="btnReset"  style="margin-top: 23px;">Reset</button>
                </div>
              </div>
            
           </div>
          </div>
         </div>
        </div>
    </section>
    <!-- End Fitler content -->

           <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="box">
          <div class="box-header">
            <h3 class="box-title"><i class="fa fa-sitemap"></i> Stages List</h3>
            <hr>
            <div>
            <ul style="list-style-type: square">
              <li style='color:#5043c3'>Not Open</li>
              <li style='color:#3c8dbc'>Open</li>
              <li style='color:#00a65a'>Finish</li>
            </ul>
              
            </div>
          </div>
        <div class="box-body" id='leaguebody'>
        


        </div>
            <!-- /.box-body -->
            
            <!-- /.box-footer-->
    </div>
          <!-- /.box -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> 2.4.0
    </div>
    <strong>Copyright &copy; 2018 <a href="#">Siegreich Solutions inc.</a></strong> All rights
    reserved.
  </footer>

  
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>

  <input type="hidden" name="country_id" id="country_id" value="">
</div>
<!-- ./wrapper -->

<?php $this->load->view('templates/admin_footer');?>

<script>
var datatable;
// var country_id = $('#country_id').val();
$(document).ready(function() {
  $('#loader').hide();
  // getCountryList();
  // getLeagueList();
  // getSeasonList();
  // getStatusList();
  // getSeasonNames();
  getLoadStage();
  

  // loadData();
  
  $('.sidebar-menu').tree();
  $('.select2').select2();

  // $( "#btnReset" ).on( "click", function() {
  //       $('.filter').empty();
  //       getCountryList();
  //       getLeagueList();
  //       getSeasonList();
  //       getStatusList();
       
        
  //   });

 //   $(".filter").change(function() {
 //      var cntry = $('#countryfilter').val();
 //      var selLeague = $('#leaguefilter').val();
 //      var selSeason = $('#seasonfilter').val();
 //      var selstatus = $('#statusfilter').val();
 //      var filterarray = [];


 //      filterarray.push({'cntry' : cntry});
 //      filterarray.push({'selLeague' : selLeague});
 //      filterarray.push({'selSeason' : selSeason});
 //      filterarray.push({'selstatus' : selstatus});


 //      $('#leagueListTable').DataTable().destroy();
 //      // datatable.destroy();

 //      if(filterarray !=null)
 //      {
 //        loadData(cntry,selLeague,selSeason,selstatus)
 //      }
 //      else
 //      {
 //        loadData();
 //      }

           
 //    });

 });


  function getCountryList()
  {

    var html = "";
    var sel = $("#countryfilter");
    // var params = { c:brandid , wcacc:wcacc};
    
    $.ajax({
        url : "<?php echo $this->config->item('base_url'); ?>Leagues/getCountryList",
        type: "POST",
        dataType : 'json',
        // data : params,
        success: function (data, textStatus, errorThrown){
          
          sel.append($('<option>',
               {
                  value: "",
                  text : "All"
              }));

          for (var i = 0; i < data.length; i++)
          { 
            // console.log(data);
               sel.append($('<option>',
               {
                  value: data[i].country_id,
                  text : data[i].country_name
              }));
              // $("#leaguefilter").change();
          }


        },
        error: function (jqXHR, textStatus, errorThrown){
          swal("System Error", "There is a problem with the server! Please contact IT support", "error");
        }
      });


  }

  function getLeagueList()
  {
    
    var html = "";
    var cntry = $("#countryfilter").val();
    var selLeague = $("#leaguefilter");
    var params = { cntry : cntry};

    // console.log(cntry);
    selLeague.empty();
    $.ajax({
        url : "<?php echo $this->config->item('base_url'); ?>Leagues/getLeagueList",
        type: "POST",
        dataType : 'json',
        data : params,
        async:false,

        success: function (data, textStatus, errorThrown){
          selLeague.append($('<option>',
               {
                  value: "",
                  text : "All"
              }));

          for (var i = 0; i < data.length; i++)
          { 
            // console.log(data);
               selLeague.append($('<option>',
               {
                  value: data[i].league_id,
                  text : data[i].league_name
              }));
          }
        },
        error: function (jqXHR, textStatus, errorThrown){
          swal("System Error", "There is a problem with the server! Please contact IT support", "error");
        }
      });
    // loadData();
  }

  function getSeasonList()
  {
    
    var html = "";
    var cntry = $("#countryfilter").val();
    var selLeague = $("#leaguefilter").val();
    var selSeason = $("#seasonfilter");
    
    var params = { cntry : cntry, selLeague : selLeague};

    // console.log(cntry);
    selSeason.empty();
    $.ajax({
        url : "<?php echo $this->config->item('base_url'); ?>Leagues/getSeasonList",
        type: "POST",
        dataType : 'json',
        data : params,
        async:false,

        success: function (data, textStatus, errorThrown){
          selSeason.append($('<option>',
               {
                  value: "",
                  text : "All"
              }));

          for (var i = 0; i < data.length; i++)
          { 
            // console.log(data);
               selSeason.append($('<option>',
               {
                  value: data[i].season_name,
                  text : data[i].season_name
              }));
          }
        },
        error: function (jqXHR, textStatus, errorThrown){
          swal("System Error", "There is a problem with the server! Please contact IT support", "error");
        }
      });
  }

  function getStatusList()
  {
    
    var html = "";
    // var cntry = $("#countryfilter").val();
    // var selLeague = $("#leaguefilter").val();
    // var selSeason = $("#seasonfilter").val();
    var selstatus = $("#statusfilter");
    // selstatus.empty();
    
    // var params = { cntry : cntry, selLeague : selLeague, selstatus:selstatus };

    var stat_code = ['N','O','S','E','C','F'];
    var stat = ['Not Open','Open','Start','End','Calculating','Finish'];


    selstatus.append($('<option>',
               {
                  value: "",
                  text : "All"
              }));
    for (var i = 0; i < stat_code.length; i++)
          { 
            // console.log(stat_code[i]);
            // console.log(stat);
               selstatus.append($('<option>',
               {
                  value: stat_code[i],
                  text : stat[i]
              }));
          }

    // // console.log(cntry);
    

  }

 
   function showModal(season_id,ref_season_id,stage_id,league,status,season,stage_name){
      
      
      $("#updModal").modal(); 
      $('#updleaguename').val(league);
      $('#updseason').val(season);
      $('#stage_id').val(stage_id);
      $('#updstagename').val(stage_name);
      $('#selStatus').val(status);
      $('#season_id').val(season_id);
      $('#ref_season_id').val(ref_season_id);
      
     
   }
  
 function updStages(){
    var season_id = $("#season_id").val();
    var status = $("#selStatus").val();
    var ref_season_id = $("#ref_season_id").val();
    var stage_id = $("#stage_id").val();
   
    if (status == ""){
    swal("ERROR MESSAGE", "PLEASE SELECT STATUS" , "error");
    }else{
      var formData = { season_id: season_id, 
                      status: status,
                     ref_season_id:ref_season_id,
                     stage_id:stage_id,
                     
                    };
     
      $.ajax({
        url: "<?=base_url();?>LeagueStages/updStage", 
        type: "POST", 
        dataType: 'json', 
        data: formData, 
        beforeSend: function(){
        $('#loader').show();
        },
        complete: function(){
            $('#loader').hide();
        },
        success: function(data, textStatus, jqXHR)

        {
        swal(data.ttl,data.msg,data.typ);
        $("#updModal").modal('toggle');
        getLoadStage();
       }}); 
 }
}
    function getCountries()
    {
        $.ajax({url: "<?=base_url();?>Leagues/getCountries", 
        async: true, type: "POST", dataType: 'json',  
        beforeSend: function(){
        $('#loader').show();
        },
        complete: function(){
            $('#loader').hide();
        },
        success: function(data){
        swal(data.ttl,data.msg,data.typ);
        reloadData( $("#countryTable") );
        },
          error: function (jqXHR, textStatus, errorThrown){
            //Custom Error
            swal("System Error", "There is a problem with the server! Please contact IT support", "error");
          }
        }); 
    }

  $(function() {

  // We can attach the `fileselect` event to all file inputs on the page
    $(document).on('change', ':file', function() {
      var input = $(this),
      numFiles = input.get(0).files ? input.get(0).files.length : 1,
      label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
      input.trigger('fileselect', [numFiles, label]);
      //store to var
      filetemp = input.get(0).files[0];
    });

    // We can watch for our custom `fileselect` event like this
      $(document).ready( function() {
          $(':file').on('fileselect', function(event, numFiles, label) {
              var input = $(this).parents('.input-group').find(':text'),
                  log = numFiles > 1 ? numFiles + ' files selected' : label;

              if( input.length ) {
                  input.val(log);
                  if (numFiles > 1) { input.val(''); }
              }

          });
      });

  });
    function getLoadStage() {
      $.ajax({
      url : "<?php echo $this->config->item('base_url'); ?>LeagueStages/getLoadStages",
      type: "POST",
      dataType : 'json',
      async:false,

        success: function (data, textStatus, errorThrown){

          var stagescount = data['stages'].length;
          var seasoncount = data['season'].length;
          var html = "";
          for(var i=0;i<seasoncount;i++)
            {
              var sid = data['season'][i]['season_id'];
              var season_name = data['season'][i]['season_name'];
              var league_name = data['season'][i]['league_name'];
              var country_name = data['season'][i]['country_name'];
              html+= "<div class='row'>";
              html+= "<div class='box-header'>";
              html+= "<h3 class='box-title alert alert-success'> <i class='fa fa-trophy'></i> "+ league_name+" - "+country_name+" - "+season_name+"</h3>";
              html+= "</div>";
              for(var x=0;x<stagescount;x++){
                  var season_id = data['stages'][x]['season_id'];
                  var stage_id = data['stages'][x]['stage_id'];
                  var stage_name = data['stages'][x]['stage_name'];
                  var ref_season_id = data['stages'][x]['ref_season_id'];
                  var season = data['stages'][x]['season_name'];
                  var league = data['stages'][x]['league_name'];
                  var status = data['stages'][x]['stat'];
                 
                  
                if(season_id==sid){
                  var bgcolor = "";  
                  switch(status){
                      case 'N':
                        bgcolor='bg-newcolor';
                      break;
                      case 'O':
                        bgcolor='bg-opencolor';
                      break;
                      case 'F':
                        bgcolor='bg-finishcolor';
                      break;


                  }
                  // console.log(bgcolor);

                  html+= "<div class='col-md-3 league-col'>";
                  html+= "<div class='info-box "+bgcolor+" cursor' onclick=\"showModal(" + season_id + "," + ref_season_id + "," + stage_id + ",'"+ league + "','"+ status + "','" + season + "','" + stage_name + "')\">";
                  html+= "<span class='info-box-icon'><i class='fa fa-sitemap'></i></span>";
                  html+= "<div class='info-box-content'>";
                  html+= "<span class='info-box-number'>"+stage_name+"</span>";
                  // html+= "<span class='progress-description'>"+season+"</span>";
                  html+= "</div>";
                  html+= "</div>";
                  html+= "</div>";
                }
                 
              }
              html+= "</div>";
            }
            $('#leaguebody').html(html);
          
        },error: function (jqXHR, textStatus, errorThrown){
          swal("System Error", "There is a problem with the server! Please contact IT support", "error");
        }
      });
    }
  
    </script>
