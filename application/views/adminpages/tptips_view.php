<div class="loader" id="loader">
</div>
<body class="hold-transition skin-green sidebar-mini">
<!-- Site wrapper -->
<div class="wrapper">

 

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper margin-top_cont">

  <?php echo form_open_multipart(base_url( 'upload/create' ), array( 'id' => 'addForm', 'class' => 'form-horizontal form-label-left' )); ?>
  <div class="modal fade addTipModal" tabindex="-1" role="dialog" aria-hidden="true" sstyle="width:600px" id=addTipModal>
  <div class="modal-dialog modal-lg">
    <div class="modal-content">

      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
        </button>
        <h4 class="modal-title" id="addUserLabel">Add Tip</h4>

      </div>
      <div class="modal-body">

      <form class="form-horizontal form-label-left">

        <div class="col-md-6">
        <div class="form-group">
          <label class="control-label col-md-4 col-sm-4 col-xs-12">Leagues</label>
          <div class="col-md-6 col-sm-6 col-xs-12">
            <select class="form-control select2 col-md-7 col-xs-12" name="match"  id="selLeague" style='width: 100%' onchange="shwLeagueStages(); shwMatches();">
              <option value="" selected="selected">-- SELECT --</option>
              <?php
                foreach($leagues as $l)
                {
                ?>
                  <option value="<?=$l['league_id'];?>"><?=$l['country_name']." - ".$l['league_name'];?></option>
                <?php 
                }
                ?>
            </select>
          
          </div>
        </div>

        <div class="form-group">
          <label class="control-label col-md-4 col-sm-4 col-xs-12">League Stage</label>
          <div class="col-md-6 col-sm-6 col-xs-12">
            <select class="form-control col-md-7 col-xs-12" name="selLeagueStages"  id="selLeagueStages" style='width: 100%' onchange="shwMatches();">
              <option value="" selected="selected">-- SELECT --</option>
            </select>
          </div>
        </div>


        <div class="form-group">
          <label class="control-label col-md-4 col-sm-4 col-xs-12">Match</label>
          <div class="col-md-6 col-sm-6 col-xs-12">
            <select class="select2_multiple form-control col-md-7 col-xs-12" name="addselmatch"  id="addselmatch" style='width: 100%'>
              <option value="" selected="selected">-- SELECT --</option>
              <!-- <option value="1">asd1 vs. asd2</option>
              <option value="2">asd3 vs. asd4</option> -->
            </select>
          </div>
        </div>


        <!-- <div class="form-group">
          <label class="control-label col-md-4 col-sm-4 col-xs-12">Tips</label>
          <div class="col-md-6 col-sm-6 col-xs-12">
            <textarea id="tips" name="tips" name="message" rows="8" class="form-control col-md-7 col-xs-12"></textarea>
          </div>
        </div> -->



        <div class="form-group">
          <label class="control-label col-md-4 col-sm-4 col-xs-12">Priority</label>
          <div class="col-md-6 col-sm-6 col-xs-12">
            <select class="form-control col-md-7 col-xs-12" name="addpriority"  id="addpriority" style='width: 100%'>
              <!-- <option value="">-- SELECT --</option> -->
              <option value="" selected="selected">-- Select --</option>
              <?php
              for ($i = 1; $i<=10; $i++)
              {
                echo '<option value="'.$i.'">'.$i.'</option>';
              }
              ?>
              
            </select>
          </div>
        </div>

        

        <div class="form-group">
          <label class="control-label col-md-4 col-sm-4 col-xs-12">Featured</label>
          <div class="col-md-6 col-sm-6 col-xs-12">
           
              <input type="checkbox" id="addfeatured" name="addfeatured" style="margin-top: 11px;" onclick="checkFeatured();">
           
          </div>
        </div>

        </div>  <!-- end for div 1 -->


        <div class="col-md-6">


       
        <div class="form-group">
          <label class="control-label col-md-4 col-sm-4 col-xs-12">Status</label>
          <div class="col-md-6 col-sm-6 col-xs-12">
            <select class="select2_multiple form-control col-md-7 col-xs-12" name="addstatus"  id="addstatus" style='width: 100%'>
              <!-- <option value="">-- SELECT --</option> -->
              <option value="A" selected="selected">Active</option>
              <option value="C">Closed</option>
            </select>
          </div>
        </div>




        <div class="form-group">
          <label class="control-label col-md-4 col-sm-4 col-xs-12">Tip Main</label>
          <div class="col-md-6 col-sm-6 col-xs-12">
            <input type="text" id="addtipMain" name="addtipMain" class="form-control col-md-7 col-xs-12" placeholder="Over 2.5 Goals" >
          </div>
        </div>

        <!-- <div class="form-group">
          <label class="control-label col-md-4 col-sm-4 col-xs-12">Tip Form</label>
          <div class="col-md-6 col-sm-6 col-xs-12">

            <select class="form-control col-md-7 col-xs-12" name="addtipForm"  id="addtipForm" style='width: 100%'>
              <option value="" selected="selected">-- SELECT --</option>
              <?php
                for($i=1; $i<=5; $i++)
                {
                  echo '<option value="'.$i.'">'.$i.'</option>';
                }
              ?>
            </select>
          </div>
        </div> -->


        <div class="form-group">
          <label class="control-label col-md-4 col-sm-4 col-xs-12">Odds</label>
          <div class="col-md-6 col-sm-6 col-xs-12">
            <input type="text" id="addtipOdds" name="addtipOdds" class="form-control col-md-7 col-xs-12" placeholder="0.52">
          </div>
        </div>
        </div>

        <div class="clearfix"></div>


         <div class="box-body pad">
          
            <textarea id="addtipeditor" name="addtipeditor" rows="10" cols="80">
                                    
            </textarea>
        </div>

        
      </form>      

      </div>
      <div class="modal-footer">
        <input type=hidden id=addID value=''>
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Save</button>
      </div>

    </div>
  </div>
</div>

</form>


<!-- EDIT MODAL -->

<?php echo form_open_multipart(base_url( 'upload/create' ), array( 'id' => 'updateForm', 'class' => 'form-horizontal form-label-left' )); ?>
<div class="modal fade updTipModal" tabindex="-1" role="dialog" aria-hidden="true" sstyle="width:600px" id=updTipModal>
  <div class="modal-dialog modal-lg">
    <div class="modal-content">

      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
        </button>
        <h4 class="modal-title" id="editTipLabel">Update Tip</h4>

      </div>
      <div class="modal-body">

      <form class="form-horizontal form-label-left">

        
        <div class="col-md-6">
        <div class="form-group">
          <label class="control-label col-md-4 col-sm-4 col-xs-12">Match</label>
          <div class="col-md-6 col-sm-6 col-xs-12">
            <select class="select2_multiple form-control col-md-7 col-xs-12" name="updselmatch"  id="updselmatch" style='width: 100%'>
              <option value="" selected="selected">-- SELECT --</option>
              <!-- <option value="1">asd1 vs. asd2</option>
              <option value="2">asd3 vs. asd4</option> -->
            </select>
          </div>
        </div>

        <!-- <div class="form-group">
          <label class="control-label col-md-4 col-sm-4 col-xs-12">Tips</label>
          <div class="col-md-6 col-sm-6 col-xs-12">
            <textarea id="tips" name="tips" name="message" rows="8" class="form-control col-md-7 col-xs-12"></textarea>
          </div>
        </div> -->

        <div class="form-group">
          <label class="control-label col-md-4 col-sm-4 col-xs-12">Priority</label>
          <div class="col-md-6 col-sm-6 col-xs-12">
            <select class="form-control col-md-7 col-xs-12" name="updpriority"  id="updpriority" style='width: 100%'>
              <!-- <option value="">-- SELECT --</option> -->
              <option value="" selected="selected">-- Select --</option>
              <?php
              for ($i = 1; $i<=10; $i++)
              {
                echo '<option value="'.$i.'">'.$i.'</option>';
              }
              ?>
              
            </select>
          </div>
        </div>

        <div class="form-group">
          <label class="control-label col-md-4 col-sm-4 col-xs-12">Featured</label>
          <div class="col-md-6 col-sm-6 col-xs-12">
           
              <input type="checkbox" id="updfeatured" name="updfeatured" style="margin-top: 11px;" onclick="checkFeatured();">
           
          </div>
        </div>

        </div>

        <div class="col-md-6">

        <div class="form-group">
          <label class="control-label col-md-4 col-sm-4 col-xs-12">Tip</label>
          <div class="col-md-6 col-sm-6 col-xs-12">
            <input type="text" id="updtipMain" name="updtipMain" class="form-control col-md-7 col-xs-12" placeholder="Over 2.5 Goals">
          </div>
        </div>

        <!-- <div class="form-group">
          <label class="control-label col-md-4 col-sm-4 col-xs-12">Tip Form</label>
          <div class="col-md-6 col-sm-6 col-xs-12">
            <select class="form-control col-md-7 col-xs-12" name="updtipForm"  id="updtipForm" style='width: 100%'>
              <option value="">-- SELECT --</option>
              <option value="" selected="selected">-- Select --</option>
              <?php
              for ($i = 1; $i<=5; $i++)
              {
                echo '<option value="'.$i.'">'.$i.'</option>';
              }
              ?>
              
            </select>
          </div>
        </div> -->

<!-- 
        <div class="form-group">
          <label class="control-label col-md-4 col-sm-4 col-xs-12">Odds</label>
          <div class="col-md-6 col-sm-6 col-xs-12">
            <input type="text" id="updtipOdds" name="updtipOdds" class="form-control col-md-7 col-xs-12" placeholder="0.52">
          </div>
        </div> -->

        <div class="form-group">
          <label class="control-label col-md-4 col-sm-4 col-xs-12">Status</label>
          <div class="col-md-6 col-sm-6 col-xs-12">
            <select class="select2_multiple form-control col-md-7 col-xs-12" name="updstatus"  id="updstatus" style='width: 100%'>
              <!-- <option value="">-- SELECT --</option> -->
              <option value="A" selected="selected">Active</option>
              <option value="C">Closed</option>
              <option value="X">Delete</option>
            </select>
          </div>
        </div>

        </div>

        <div class="clearfix"></div>


        <div class="box-body pad">
          
            <textarea id="updtipeditor" name="updtipeditor" rows="10" cols="80">
                                    
            </textarea>
        </div>

        
      </form>      


       

        

      </div>
      <div class="modal-footer">
        <input type=hidden id=editid value=''>
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="save" class="btn btn-primary" >Save</button>
      </div>

    </div>
  </div>
</div>
</form>

<!--- END FOR EDIT MODAL -->

<!-- MODAL FOR DELETION -->
<div class="modal fade delModal" tabindex="-1" role="dialog" aria-hidden="true" sstyle="width:400px" id=delModal>
  <div class="modal-dialog modal-sm">
    <div class="modal-content">

      <div class="modal-header bgred">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
        </button>
        <h4 class="modal-title" id="delModalLabel">Delete Tip</h4>
      </div>
      <div class="modal-body">
        <b><span class='colorblack' style='font-size:18px'> </span><span class='colorred' id=del_tip style='font-size:18px'></span></b><br>
        <br>

       Are you sure you want to permanently delete this tip?
      </div>
      <div class="modal-footer">
        <input type=hidden id=delid value=''>
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
        <button type="button" class="btn btn-danger" onclick='deleteTip();'>Delete</button>
      </div>

    </div>
  </div>
</div>
<!-- END MODAL FOR DELETION -->
 

    

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="box">
        <div class="box-header with-border">
          <h1 class="box-title">Tips</h1>
          <button class="btn btn-md btn-info" data-toggle="modal" data-target=".addTipModal"><i class='fa fa-plus'></i> Add</button>
        </div>
        
        <div class="box-body">

          <table id="tipsTable" class="table table-striped table-bordered" data-page-length='50' style='width:100%'>
          <thead>
            <tr>
              <th style="width: 50px; text-align: center;" >
              <!-- <th>Tip ID -->
              <th>Match ID
              <th>Match Title
              <th>Tip Desc
              <th>Tip Main
              <th>Priority
              <th>Featured
              <th>Status
              <th>Created Date
              <th>Created By
            </tr>
          </thead>

          </table>
        </div>
        <!-- /.box-body -->
        
        <!-- /.box-footer-->
      </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

 <footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> 2.4.0
    </div>
    <strong>Copyright &copy; 2018 <a href="#">Siegreich Solutions inc.</a></strong> All rights
    reserved.
  </footer>


</div>
<!-- ./wrapper -->


<?php $this->load->view('templates/admin_footer');?>

<script>
var datatable;
$(document).ready(function() {
  $('#loader').hide();
  loadData();
  $('.sidebar-menu').tree();
  CKEDITOR.replace('addtipeditor');
  CKEDITOR.replace('updtipeditor');
  // $('.textarea').wysihtml5();


 });

  function checkFeatured()
  {
    var opt = '';
    var priotity = $("#addpriority");
    var featured = $("#addfeatured");
    var updpriotity = $("#updpriority");
    var updfeatured = $("#updfeatured");
    if (featured.is(":checked") || updfeatured.is(":checked")) {
      // alert('asdasd');
      // priotity.html('<option value="11">11</option>');
      priotity.prop('disabled', 'disabled');
      // updpriotity.html('<option value="11">11</option>');
      updpriotity.prop('disabled', 'disabled');
    }else{
      priotity.removeAttr("disabled");
      updpriotity.removeAttr("disabled");
    }
    
  }

  function updTipModal(tip_id, match_id, tip_main, priority, featured, status){
    $("#updTipModal").modal(); 
    $("#editid").val(tip_id);
    $("#updselmatch").val(match_id);
    $("#updpriority").val(priority);
    $("#updtipMain").val(tip_main);
    $("#updstatus").val(status);
    
    $.ajax({
          url : "<?=base_url(); ?>Sites/populateMatch/"+match_id,
          type: "POST",
          dataType : 'text',
          success: function (data, textStatus, errorThrown){
            // console.log(data);
            $("#updselmatch").html(data);
            $("#updselmatch").val(match_id);
          },
          error: function (jqXHR, textStatus, errorThrown){
          }
        });
    // $("#updtipeditor").val(tip_desc);
    // CKEDITOR.instances['updtipeditor'].setData(tip_desc);
    // console.log(tip_desc);
    if(featured == 1)
    {
      $( "#updfeatured" ).prop( "checked", true );
      // $( "#updpriority" ).html('<option value="11">11</option>');
      $( "#updpriority" ).prop('disabled', 'disabled');
    }else{
      $( "#updfeatured" ).prop( "checked", false );
      // $( "#updpriority" ).html('<option value="11">11</option>');
      $( "#updpriority" ).prop('disabled', false);
    }

    $.ajax({
          url : "<?=base_url(); ?>TPtips/getTipDesc",
          type: "POST",
          dataType : 'json',
          data : {tip_id : tip_id},
          success: function (data, textStatus, errorThrown){
            // console.log(data);
            var td = data[0].tptip_desc;
            CKEDITOR.instances['updtipeditor'].setData(td);
          },
          error: function (jqXHR, textStatus, errorThrown){
          }
        });

  }

   $("#updateForm").submit(function(e){
    e.preventDefault();
    var tipid =  $("#editid").val();
    var matchid =  $("#updselmatch").val();
    var priority = $("#updpriority").val();
    var tipmain = $("#updtipMain").val();
    var status = $("#updstatus").val();
    var tipdesc = CKEDITOR.instances['updtipeditor'].getData();
    var featured = $("#updfeatured");
    // alert("1" + CKEDITOR.instances['addtipeditor'].getData());
    // alert("2" + CKEDITOR.instances.addtipeditor.getData());

    if(featured.is(":checked"))
    {
      featured = 1;
      if( matchid == '' || tipdesc == '' || tipmain == '' )
      {
        swal("Error", "All fields are required!", "error");
      }else{
        var formdata = {  tipid : tipid,
                          matchid : matchid,
                          priority :priority,
                          tipmain :tipmain,
                          featured : featured,
                          status : status,
                          tipdesc : tipdesc
                        };
                        console.log(formdata);


        $.ajax({
                url : "<?=base_url();?>TPtips/updateTip",
                type: "POST",
                dataType : 'json',
                data : formdata,
                beforeSend: function(){
                $('#loader').show();
                },
                complete: function(){
                    $('#loader').hide();
                },
                success: function(data, textStatus, jqXHR){
                  swal(data.ttl,data.msg,data.typ);
                  $("#updTipModal").modal('toggle'); 
                  reloadData( $("#tipsTable") );

                },
                error: function (jqXHR, textStatus, errorThrown){
                  //Custom Error
                  swal("System Error", "There is a problem with the server! Please contact IT support", "error");
                }
            });

      }
    }else{
      featured = 0;
      if(matchid == '' ||  priority == ''    ||   tipdesc == '' || tipmain == '' )
      {
        swal("Error", "All fields are required!", "error");
      }else{
        var formdata = {   tipid : tipid,
                          matchid : matchid,
                          priority :priority,
                          tipmain :tipmain,
                          featured : featured,
                          status : status,
                          tipdesc : tipdesc
                        };
                        console.log(formdata);
         $.ajax({
                url : "<?=base_url();?>TPtips/updateTip",
                type: "POST",
                dataType : 'json',
                data : formdata,
                beforeSend: function(){
                $('#loader').show();
                },
                complete: function(){
                    $('#loader').hide();
                },
                success: function(data, textStatus, jqXHR){
                  swal(data.ttl,data.msg,data.typ);
                  $("#updTipModal").modal('toggle'); 
                  reloadData( $("#tipsTable") );

                },
                error: function (jqXHR, textStatus, errorThrown){
                  //Custom Error
                  swal("System Error", "There is a problem with the server! Please contact IT support", "error");
                }
            });
      }

    }


  });

  function delTip(id, title){
    $("#delModal").modal(); 
    $("#delid").val(id);
    $("#del_tip").html(title);
  }

  function deleteTip(){

  var delid = $("#delid").val();
  // alert(delid);

  var formData = { delid: delid };
  $.ajax({url: "Sites/delTip", type: "POST", dataType: 'json', data: formData,
    beforeSend: function(){
      $('#loader').show();
    },
    complete: function(){
      $('#loader').hide();
    },
    success: function(data){
      swal(data.ttl,data.msg,data.typ);
      $("#delModal").modal('toggle');
      reloadData( $("#tipsTable") );
  }}); 
}


  $("#addForm").submit(function(e){
    e.preventDefault();
    // $("#addtipeditor").empty();

    var league = $("#selLeague").val();
    var stage = $("#selLeague").val();
    var match = $("#addselmatch").val();
    var priority = $("#addpriority").val();
    var featured = $("#addfeatured");
    var tipdesc = CKEDITOR.instances['addtipeditor'].getData();
    var tipmain = $("#addtipMain").val();
    var status = $("#addstatus").val();
    // tipdesc = replaceHtml(tipdesc);
    // console.log(CKEDITOR.instances['addtipeditor'].getData());


    if(featured.is(":checked"))
    {
      featured = 1;
      if(league == '' || stage == '' || match == '' ||  tipdesc == '' || tipmain == '' )
      {
        swal("Error", "All fields are required!", "error");
      }else{
        var formdata = { league : league,
                          stage :stage,
                          match :match,
                          featured :featured,
                          tipdesc : tipdesc,
                          tipmain : tipmain,
                          status : status
                        };
                        console.log(formdata);


        $.ajax({
                url : "<?=base_url();?>TPtips/addTip",
                type: "POST",
                dataType : 'json',
                data : formdata,
                beforeSend: function(){
                $('#loader').show();
                },
                complete: function(){
                    $('#loader').hide();
                },
                success: function(data, textStatus, jqXHR){
                  swal(data.ttl,data.msg,data.typ);
                  $("#addTipModal").modal('toggle'); 
                  reloadData( $("#tipsTable") );

                },
                error: function (jqXHR, textStatus, errorThrown){
                  //Custom Error
                  swal("System Error", "There is a problem with the server! Please contact IT support", "error");
                }
            });

      }
    }else{
      featured = 0;
      if(league == '' || stage == '' || match == '' || priority == '' || tipdesc == '' || tipmain == '' )
      {
        swal("Error", "All fields are required!", "error");
      }else{
        var formdata = { league : league,
                          stage :stage,
                          match :match,
                          featured :featured,
                          priority :priority,
                          tipdesc : tipdesc,
                          tipmain : tipmain,
                          status : status
                        };
                        console.log(formdata);
         $.ajax({
                url : "<?=base_url();?>TPtips/addTip",
                type: "POST",
                dataType : 'json',
                data : formdata,
                beforeSend: function(){
                $('#loader').show();
                },
                complete: function(){
                    $('#loader').hide();
                },
                success: function(data, textStatus, jqXHR){
                  swal(data.ttl,data.msg,data.typ);
                  $("#addTipModal").modal('toggle'); 
                  reloadData( $("#tipsTable") );

                },
                error: function (jqXHR, textStatus, errorThrown){
                  //Custom Error
                  swal("System Error", "There is a problem with the server! Please contact IT support", "error");
                }
            });
      }

    }

  });

  function shwLeagueStages()
  {
    var html = "";
    var league_id = $('#selLeague').val();
    var stages = $('#selLeagueStages');
    var params = { league_id : league_id};

    stages.empty();
    $.ajax({
      url : "<?=base_url();?>Sites/getStages",
      type: "POST",
      dataType : 'json',
      data : params,
      success: function (data, textStatus, errorThrown){
        stages.append($('<option>',
             {
                value: "",
                text : "-- SELECT --"
            }));
        for (var i = 0; i < data.length; i++)
        { 
             stages.append($('<option>',
             {
                value: data[i].stage_id,
                text : data[i].stage_name
            }));
        }
      },
      error: function (jqXHR, textStatus, errorThrown){
        swal("System Error", "There is a problem with the server! Please contact IT support", "error");
      }
    });
  }

  function shwMatches()
  {
    var html = "";
    var stageid = $('#selLeagueStages').val();
    var match = $('#addselmatch');
    var params = { stageid : stageid};

    match.empty();
    $.ajax({
      url : "<?=base_url();?>Sites/getMatches",
      type: "POST",
      dataType : 'json',
      data : params,
      success: function (data, textStatus, errorThrown){
        match.append($('<option>',
             {
                value: "",
                text : "-- SELECT --"
            }));
        for (var i = 0; i < data.length; i++)
        { 
             match.append($('<option>',
             {
                value: data[i].match_id,
                text : data[i].match_title
            }));
        }
      },
      error: function (jqXHR, textStatus, errorThrown){
        swal("System Error", "There is a problem with the server! Please contact IT support", "error");
      }
    });
  }

  function loadData()
  {
    datatable = $('#tipsTable').dataTable( {
    "order": [[ 0, "asc" ]],
      responsive: true, 
      dom: 'Bfrtip',

      "buttons": [
              'excel',
              'pdf'
          ],
          "scrollX": true,
      "pagingType": "full_numbers",
      "processing": true,
      "serverSide": true,
      "ajax":{
        url :"<?=base_url();?>TPtips/listing_tptips?k=" + Math.random(), // json datasource
        type: "post",  // method  , by default get      
        error: function(){  // error handling
          $(".grid-error").html("");
          $("#roleTable").append('<tbody class="grid-error"><tr><th colspan="7">NO DATA FOUND</th></tr></tbody>');
          $("#roleTable_processing").css("display","none");        
        }
      },
      "columns" : [
        { "data" : "tptip_id"},
        { "data" : "match_id",visible: false },
        { "data" : "match_title" },
        { "data" : "tptip_desc" },
        { "data" : "tptip" },
        { "data" : "priority" },
        { "data" : "featured" },
        { "data" : "status" },
        { "data" : "created_date" },
        { "data" : "created_by" }
      ]
      ,
       "columnDefs" : [ {
        "targets" : 0,
        "orderable": false, className : "text-center",
          "render": function( data, type, row, meta ) {
            var html = "";
            
              html = " <a href='#'  onclick=\"updTipModal(" + row["tptip_id"] + ",'" + row["match_id"] +  "','" + row["tptip"] + "','" + row["priority"] + "','" + row["featured"] + "','" + row["status"] + "')\"> <span class='glyphicon glyphicon-edit' aria-hidden='true'></span></a>";
              // html += "&nbsp;&nbsp;"+"<a href='#' onclick=\"delTip(" + row["tip_id"] + ",'"+ row["tip_title"] + "')\"><span class='glyphicon glyphicon-trash' style='color:red;' aria-hidden='true'></span> </a>";
              
            
            return html;
            }
       },{
          "targets" : 6,  
          "render" : function( data, type, row, meta ) {
          var html;
              switch(data){
                case "1" : html = "<span class='label label-success'>Yes</span>"; break;
                case "0" : html = "<span class='label label-danger'>No</span>"; break;
              }
          return html;
          }
        },{
        "targets" : 7,  
        "render" : function( data, type, row, meta ) {
              var html;
                  switch(data){
                    case "A" : html = "<span class='label label-success'>Active</span>"; break;
                    case "I" : html = "<span class='label label-danger'>Inactive</span>"; break;
                  }
              return html;
             }
        }]
    }); 
  }


</script>