<div class="loader" id="loader">
</div>

<!-- EDIT MODAL -->
<div class="modal fade updModal" tabindex="-1" role="dialog" aria-hidden="true" sstyle="width:600px" id=updModal>
  <div class="modal-dialog modal-md">
    <div class="modal-content">

      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
        </button>
        <h4 class="modal-title" id="updModalLabel">Add User</h4>

      </div>
      <div class="modal-body">

        <form class="form-horizontal form-label-left">
        <div class="form-group">
          <label class="control-label col-md-4 col-sm-4 col-xs-12">League</label>
          <div class="col-md-6 col-sm-6 col-xs-12">
            <input type="text" disabled="true" id="updleague" name="updleague" class="form-control col-md-7 col-xs-12" >
          </div>
        </div>

        <div class="form-group">
          <label class="control-label col-md-4 col-sm-4 col-xs-12">Country</label>
          <div class="col-md-6 col-sm-6 col-xs-12">
            <input type="text" disabled="true" id="updcountry" name="updcountry" class="form-control col-md-7 col-xs-12" >
          </div>
        </div>

        <div class="form-group">
          <label class="control-label col-md-4 col-sm-4 col-xs-12">Stage Name</label>
          <div class="col-md-6 col-sm-6 col-xs-12">
            <input type="text" disabled="true" id="updStageName" name="updStageName" class="form-control col-md-7 col-xs-12" >
          </div>
        </div>

        
    
        <div class="form-group">
          <label class="control-label col-md-4 col-sm-4 col-xs-12">Status</label>
          <div class="col-md-6 col-sm-6 col-xs-12">
            <select class="select2_multiple form-control col-md-7 col-xs-12" name="updStatus"  id="updStatus" style='width: 100%'>
              <option value="">-- SELECT --</option>
              <option value="N">Not Open</option>
              <option value="O">Open</option>
              <option value="S">Start</option>
              <option value="E">End</option>
              <option value="C">Calculating</option>
              <option value="F">Finish</option>
            </select>
          </div>
        </div>

        </form>      

      </div>
      <div class="modal-footer">
        <input type=hidden id=cntryid value=''>
        <input type=hidden id=stgid value=''>
        <input type=hidden id=matchid value=''>
        <!-- <input type=hidden id=country_name value=''> -->
        <input type=hidden id=league_id value=''>
        <!-- <input type=hidden id=league_name value=''> -->
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
        <button type="button" class="btn btn-primary" onclick='updLeague();'>Confirm</button>
      </div>

    </div>
  </div>
</div>

<!--- END FOR EDIT MODAL -->

<body class="hold-transition skin-green sidebar-mini">
<!-- Site wrapper -->
<div class="wrapper">

 

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper margin-top_cont">
    <!-- Content Header (Page header) -->
    

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="box">
        <div class="box-header with-border">
          <h1 class="box-title">Stages Matches</h1>&nbsp;
          <!-- <button class="btn btn-md btn-info" onclick="getCountries();"><i class='fa fa-plus'></i> Get Countries / Leagues</button> -->
        </div>

       
        <!-- Fitler content -->
    <section class="content" style='min-height:0px'>
       <div class="row">
        <div class="col-xs-12">
          <div class="box">
          <div class="box-header">
            <h3 class="box-title"><i class="fa fa-filter"></i> Stages Filter Box</h3>
            <hr>
          </div>
           <div class="box-body">

             <div class="col-sm-2">
                <div class="form-group">
                  <label for="countryfilter">Country</label>
                    <select class="form-control filter select2" id="countryfilter" style="width: 100%;" onchange="getLeagueList();getStageList();getSeasonList();">
                      
                    </select>
                </div>
              </div>

              <div class="col-sm-2">
                <div class="form-group">
                  <label for="leaguefilter">League</label>
                    <select class="form-control filter select2" id="leaguefilter" style="width: 100%;" onchange="getStageList();getSeasonList();" >
                      
                    </select>
                </div>
              </div>

              <div class="col-sm-2">
                <div class="form-group">
                  <label for="stagefilter">Stage Name</label>
                  <select class="form-control filter select2" id="stagefilter" style="width: 100%;">
                    <option value=''>All</option>
                  </select>
                </div>
              </div>

              <div class="col-sm-2">
                <div class="form-group">
                  <label for="seasonfilter">Season</label>
                  <select class="form-control filter select2" id="seasonfilter" style="width: 100%;">
                    <option value=''>All</option>
                  </select>
                </div>
              </div>

              <div class="col-sm-2">
                <div class="form-group">
                  <label for="statusfilter">Status</label>
                  <select class="form-control filter select2" id="statusfilter" style="width: 100%;">
                    <option value=''>All</option>
                    <option value='N'>Not Open</option>
                    <option value='O'>Open</option>
                    <option value='S'>Start</option>
                    <option value='E'>End</option>
                    <option value='C'>Calculating</option>
                    <option value='F'>Finish</option>
                  </select>
                </div>
              </div>
              <div class="col-sm-1"> 
                <div class="form-group">
                <button class="btn btn-default" id="btnReset"  style="margin-top: 23px;">Reset</button>
                </div>
              </div>
            
           </div>
          </div>
         </div>
        </div>
    </section>
    <!-- End Fitler content -->

        <div class="box-body">
            <table id="match_table" class="table table-striped table-bordered" data-page-length='50' style='width:100%'>
          <thead>
            <tr>
            <th style="width: 50px; text-align: center;" >
              <!-- <th>Match ID -->
              <th>Stage ID
              <th>League ID
              <th>Country ID
              <th>Country Name
              <th>League Name
              <th>Stage Name
              <th>Season
              <th>Date Start
              <th>Date End
              <th>Match date
              <th>Home Team
              <th>Away team
              <th>Home Team Score
              <th>Away Team Score
              <th>Status
            </tr>
          </thead>

          </table>
        </div>
        <!-- /.box-body -->
      <!--   <div class="box-footer">
          Footer
        </div> -->
        <!-- /.box-footer-->
      </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

 <footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> 2.4.0
    </div>
    <strong>Copyright &copy; 2018 <a href="#">Siegreich Solutions inc.</a></strong> All rights
    reserved.
  </footer>

 
  <!-- /.control-sidebar -->
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->
<?php $this->load->view('templates/admin_footer');?>

<script>
   $(document).ready(function () {
   $('#loader').hide();
   
    getCountryList();
    getLeagueList();
    getStageList();
    getSeasonList(); 


    loadData_match();


  $( "#btnReset" ).on( "click", function() {
        $('.filter').empty();
        getCountryList();
        getLeagueList();
        getStageList();
        getSeasonList();
        getStatusList();

        $('#match_table').DataTable().destroy();
        loadData_match();
         // datatable.destroy();
        
    });

  $(".filter").change(function() {
      var cntry = $('#countryfilter').val();
      var selLeague = $('#leaguefilter').val();
      var selStage = $('#stagefilter').val();
      var selSeason = $('#seasonfilter').val();
      var selstatus = $('#statusfilter').val();
      var filterarray = [];


      filterarray.push({'cntry' : cntry});
      filterarray.push({'selLeague' : selLeague});
      filterarray.push({'selStage' : selStage});
      filterarray.push({'selSeason' : selSeason});
      filterarray.push({'selstatus' : selstatus});


      $('#match_table').DataTable().destroy();
      // datatable.destroy();

      if(filterarray !=null)
      {
        loadData_match(cntry,selLeague,selStage,selSeason,selstatus)
      }
      else
      {
        loadData_match();
      }

           
    });
   
  });

   function updLeagueModal(matchid,cntryid,cntryname,lgid,ldname,stgid,stgname,status){

      $("#cntryid").val(cntryid);
      $("#updcountry").val(cntryname);
      $("#league_id").val(lgid);
      $("#updleague").val(ldname);
      $("#updStageName").val(stgname);
      $("#updStatus").val(status);
      $("#stgid").val(stgid);
      $("#matchid").val(matchid);

      // console.log(ldname);

      $("#updModal").modal(); 
      // $("#addid").val(id);
      $("#updModalLabel").html("Update Stage Match");
      // $("#upd_league").html("Are you sure you want to add league "+ldname+" ?");
    }

function updLeague(){
  var matchid = $("#matchid").val();
  var status = $("#updStatus").val();
  // var league_name = $("#league_name").val();

  // console.log(cntryid);

  var params = {"matchid":matchid,"status": status};
  if(status == '')
  {
    swal("Error", "Please select status!", "error");
  }else{
  // console.log(params);
    $.ajax({
        url : "<?=base_url();?>StageMatches/updLeague",
        type: "POST",
        data : params,
        dataType : 'json',
        success: function(data, textStatus, jqXHR){
          swal(data.ttl,data.msg,data.typ);
          $("#updModal").modal('toggle'); 
          reloadData( $("#match_table") );

        },
        error: function (jqXHR, textStatus, errorThrown){
          //Custom Error
          swal("System Error", "There is a problem with the server! Please contact IT support", "error");
        }
    });
  }
}

  function getCountryList()
  {

    var html = "";
    var sel = $("#countryfilter");
    // var params = { c:brandid , wcacc:wcacc};
    
    $.ajax({
        url : "<?php echo $this->config->item('base_url'); ?>StageMatches/getCountryList",
        type: "POST",
        dataType : 'json',
        // data : params,
        success: function (data, textStatus, errorThrown){
          
          sel.append($('<option>',
               {
                  value: "",
                  text : "All"
              }));

          for (var i = 0; i < data.length; i++)
          { 
            // console.log(data);
               sel.append($('<option>',
               {
                  value: data[i].country_id,
                  text : data[i].country_name
              }));
              // $("#leaguefilter").change();
          }


        },
        error: function (jqXHR, textStatus, errorThrown){
          swal("System Error", "There is a problem with the server! Please contact IT support", "error");
        }
      });


  }

  function getStageList()
  {
    
    var html = "";
    var cntry = $("#countryfilter").val();
    var selLeague = $("#leaguefilter").val();
    var selStage = $("#stagefilter");
    
    var params = { cntry : cntry, selLeague : selLeague};

    // console.log(cntry);
    selStage.empty();
    $.ajax({
        url : "<?php echo $this->config->item('base_url'); ?>StageMatches/getStageList",
        type: "POST",
        dataType : 'json',
        data : params,
        async:false,

        success: function (data, textStatus, errorThrown){
          selStage.append($('<option>',
               {
                  value: "",
                  text : "All"
              }));

          for (var i = 0; i < data.length; i++)
          { 
            // console.log(data);
               selStage.append($('<option>',
               {
                  value: data[i].stage_id,
                  text : data[i].stage_name
              }));
          }
        },
        error: function (jqXHR, textStatus, errorThrown){
          swal("System Error", "There is a problem with the server! Please contact IT support", "error");
        }
      });
  }

  function getLeagueList()
  {
    
    var html = "";
    var cntry = $("#countryfilter").val();
    var selLeague = $("#leaguefilter");
    var params = { cntry : cntry};

    // console.log(cntry);
    selLeague.empty();
    $.ajax({
        url : "<?php echo $this->config->item('base_url'); ?>StageMatches/getLeagueList",
        type: "POST",
        dataType : 'json',
        data : params,
        async:false,

        success: function (data, textStatus, errorThrown){
          selLeague.append($('<option>',
               {
                  value: "",
                  text : "All"
              }));

          for (var i = 0; i < data.length; i++)
          { 
            // console.log(data);
               selLeague.append($('<option>',
               {
                  value: data[i].league_id,
                  text : data[i].league_name
              }));
          }
        },
        error: function (jqXHR, textStatus, errorThrown){
          swal("System Error", "There is a problem with the server! Please contact IT support", "error");
        }
      });
    // loadData();
  }

  function getSeasonList()
  {
    
    var html = "";
    var cntry = $("#countryfilter").val();
    var selLeague = $("#leaguefilter").val();
    var selSeason = $("#seasonfilter");
    
    var params = { cntry : cntry, selLeague : selLeague};

    // console.log(cntry);
    selSeason.empty();
    $.ajax({
        url : "<?php echo $this->config->item('base_url'); ?>StageMatches/getSeasonList",
        type: "POST",
        dataType : 'json',
        data : params,
        async:false,

        success: function (data, textStatus, errorThrown){
          selSeason.append($('<option>',
               {
                  value: "",
                  text : "All"
              }));

          for (var i = 0; i < data.length; i++)
          { 
            // console.log(data);
               selSeason.append($('<option>',
               {
                  value: data[i].season,
                  text : data[i].season
              }));
          }
        },
        error: function (jqXHR, textStatus, errorThrown){
          swal("System Error", "There is a problem with the server! Please contact IT support", "error");
        }
      });
  }

  function getStatusList()
  {
    
    var html = "";
    // var cntry = $("#countryfilter").val();
    // var selLeague = $("#leaguefilter").val();
    // var selSeason = $("#seasonfilter").val();
    var selstatus = $("#statusfilter");
    // selstatus.empty();
    
    // var params = { cntry : cntry, selLeague : selLeague, selstatus:selstatus };

    var stat_code = ['N','O','S','E','C','F'];
    var stat = ['Not Open','Open','Start','End','Calculating','Finish'];


    selstatus.append($('<option>',
               {
                  value: "",
                  text : "All"
              }));
    for (var i = 0; i < stat_code.length; i++)
          { 
            // console.log(stat_code[i]);
            // console.log(stat);
               selstatus.append($('<option>',
               {
                  value: stat_code[i],
                  text : stat[i]
              }));
          }

    // // console.log(cntry);
    

  }

   function loadData_match()
  {
    var cntry = $("#countryfilter").val();
    var selLeague = $("#leaguefilter").val();
    var selStage = $("#stagefilter").val();
    var selSeason = $("#seasonfilter").val();
    var selstatus = $("#statusfilter").val();

    datatable = $('#match_table').dataTable( {
    "order": [[ 4, "asc" ]],
      responsive: true, 
      dom: 'Bfrtip',
      "buttons": [
              'excel',
              'pdf'
          ],
      "pagingType": "full_numbers",
      "processing": true,
      "serverSide": true,
      "scrollX": true,
      "oLanguage": {
        "sSearch": "Match Search:"
        },
      "ajax":{
       url :"<?=base_url();?>StageMatches/listing_match?k=" + Math.random(), // json datasource   
        type: "post",  // method  , by default get 
        data:{cntry:cntry,
              selLeague:selLeague,
              selStage:selStage,
              selSeason:selSeason,
              selstatus:selstatus
              },  // method  , by default g        
        error: function(){  // error handling
          $(".grid-error").html("");
          $("#roleTable").append('<tbody class="grid-error"><tr><th colspan="7">NO DATA FOUND</th></tr></tbody>');
          $("#roleTable_processing").css("display","none");        
        }
      },
      "columns" : [
        { "data" : "match_id" },
        { "data" : "stage_id" ,visible: false},
        { "data" : "league_id",visible: false},
        { "data" : "country_id",visible: false},
        { "data" : "country_name"},
        { "data" : "league_name"},
        { "data" : "stage_name"},
        { "data" : "season"},
        { "data" : "date_start"},
        { "data" : "date_end"},
        { "data" : "match_date"},
        { "data" : "home_team" },
        { "data" : "away_team" },
        { "data" : "home_team_score" },
        { "data" : "away_team_score" },
        { "data" : "status" }
        // { "data" : "action" ,orderable:false},
       
        

       
      ],

       "columnDefs" : [ 
       {
        "targets" : 0,
        className : "text-center",
        "orderable": false,
        "render": function( data, type, row, meta ) {
            var html = "";
            
              
              html = " <a href='#'  onclick=\"updLeagueModal(" + row["match_id"] + "," + row["country_id"] + ",'" + row["country_name"] + "','" + row["league_id"] + "','" + row["league_name"] + "','"+ row["stage_id"]+ "','"+row["stage_name"] + "','"+row["status"] +"')\"> <span class='glyphicon glyphicon-edit' aria-hidden='true'></span></a>";
              // html += "&nbsp;&nbsp;"+"<a href='#' onclick=\"delUser(" + row["user_id"] + ",'"+ row["realname"] + "')\"><span class='glyphicon glyphicon-trash' style='color:red;' aria-hidden='true'></span> </a>";
              
            
            return html;
            }
       },
       {
          "targets" : 15,  
          "orderable": true,
          "render" : function( data, type, row, meta ) {
            var html;
              switch(data){
                case "N" : html = "<span class='label label-new'>Not Open</span>"; break;
                case "O" : html = "<span class='label label-primary'>Open</span>"; break;
                case "S" : html = "<span class='label label-start'>Start</span>"; break;
                case "E" : html = "<span class='label label-danger'>End</span>"; break;
                case "C" : html = "<span class='label label-calculating'>Calculating</span>"; break;
                case "F" : html = "<span class='label label-success'>Finish</span></span>"; break;
              }
              return html;
            }     
        }]
      
      

    });


    }
</script>

