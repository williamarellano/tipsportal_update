
<body class="hold-transition skin-green sidebar-mini">
<!-- Site wrapper -->
<div class="wrapper">

 

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper margin-top_cont">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Bets
        <small></small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Bets</a></li>
     
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title"><i class='fa fa-table'></i> Bets Table</h3>

          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                    title="Collapse">
              <i class="fa fa-minus"></i></button>
          
          </div>
        </div>
        <div class="box-body">
            <table id="bets_table" class="table table-striped table-bordered" data-page-length='50' style='width:100%'>
          <thead>
            <tr>
              <th>Bet ID
              <th>Match Name
              <th>Home Team
              <th>Away Team
              <th>Home Score Bet
              <th>Away Score Bet
              <th>Username
              <th>Points
              <th>Date Submitted
            </tr>
          </thead>

          </table>
        </div>
        <!-- /.box-body -->
      <!--   <div class="box-footer">
          Footer
        </div> -->
        <!-- /.box-footer-->
      </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

 <footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> 2.4.0
    </div>
    <strong>Copyright &copy; 2018 <a href="#">Siegreich Solutions inc.</a></strong> All rights
    reserved.
  </footer>

 
  <!-- /.control-sidebar -->
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->
<?php $this->load->view('templates/admin_footer');?>

<script>
   $(document).ready(function () {
   
    loadData_bets();
   
  });

   function loadData_bets()
  {

    datatable = $('#bets_table').dataTable( {
    "order": [[ 0, "asc" ]],
      responsive: true, 
      dom: 'Bfrtip',
      "buttons": [
              'excel',
              'pdf'
          ],
      "pagingType": "full_numbers",
      "processing": true,
      "serverSide": true,
      "scrollX": true,
      "oLanguage": {
        "sSearch": "Match Search:"
        },
      "ajax":{
        url :"listing_bets?k=" + Math.random(), // json datasource
        type: "post",  // method  , by default get      
        error: function(){  // error handling
          $(".grid-error").html("");
          $("#roleTable").append('<tbody class="grid-error"><tr><th colspan="7">NO DATA FOUND</th></tr></tbody>');
          $("#roleTable_processing").css("display","none");        
        }
      },
      "columns" : [
        { "data" : "bet_id" ,visible: false},
        { "data" : "match_id"},
        { "data" : "home_team" },
        { "data" : "away_team" },
        { "data" : "home_score_bet" },
        { "data" : "away_score_bet" },
        { "data" : "username" },
        { "data" : "points" },
        { "data" : "date_submitted" },
       
        

       
      ],
      
      

    });


    }
</script>

