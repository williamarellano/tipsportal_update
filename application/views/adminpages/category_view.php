<div class="loader" id="loader">
</div>
<body class="hold-transition skin-green sidebar-mini">
<!-- Site wrapper -->
<div class="wrapper">

 

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper margin-top_cont">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Category Masterlist
       
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Category Masterlist</a></li>
        
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title"><i class='fa fa-table'></i> Category Masterlist Table</h3>

          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                    title="Collapse">
              <i class="fa fa-minus"></i></button>
         
          </div>
        </div>
        <div class="box-body">
          <div class="container" style='margin-left: 35px;'>
             <!-- <div id="overlay"><img src="<?=base_url();?>public/img/loading.gif" alt="Be patient..." /></div> -->
        <div class="row">
            <div class="col-sm-6" >
              <table class="categorytable">
                <thead>
                  <th>
                  <button class="no-click btn btn-primary" style="margin-bottom:18px;">Category</button>
                  </th>    
                </thead>

                <tbody>
                  <tr>
                    <td>
                      <select size="15" class="select_category form-control" id='cat'>
                          <option value=''></option>
                      </select>
                    </td>
                    <td>

                    
                    </td>
                  </tr>
                </tbody>
              </table>  
              </div>
            <div class="col-sm-6" >
              <table class="categorytable" >
                    <thead>
                      <th>
                        <div class="dropdown">
                          <button style="margin-bottom: 15px;" class="btn btn-secondary btn-primary dropdown-toggle btncat" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            Sub Category <i class="fa fa-caret-down"></i>
                          </button>
                          <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                            <a class="dropdown-item" id='addBtn' role="button" href="javascript:void(0);" onclick='addShow();'>Add</a>
                            <a class="dropdown-item" href="javascript:void(0);" onclick='editShow();'>Edit</a>
                            <hr>
                            <a class="dropdown-item" href="javascript:void(0);" onclick="delShow()">Delete</a>
                          </div>
                        </div>
                      </th>
                    </thead>

                    <tbody>
                      <tr>
                   
                        <td>

                          <select size="15" class="select_category form-control" id='sub_cat'>
                            <option id="op">Select first the category on the left Pane</option>
                          </select>
                        </td>
                      </tr>
                    </tbody>
                  </table> 
            </div> 
          </div>
        </div>
        </div>
        <!-- /.box-body -->
        <div class="box-footer">
         
        </div>
        <!-- /.box-footer-->
      </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

<footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> 2.4.0
    </div>
    <strong>Copyright &copy; 2018 <a href="#">Siegreich Solutions inc.</a></strong> All rights
    reserved.
  </footer>


  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->


<?php $this->load->view('templates/admin_footer');?>
<script>

$( document ).ready(function() {
  $('#loader').hide();
   // CATEGORY SELECT
  $('#cat').change(function(){
     
      var category = $('#cat').val(); 
      $('#category_name').val('');
       getSubCat(category); 

     
  });
  // SUB CATEGORY SELECT
   $('#sub_cat').change(function(){

    var id = $(this).children(":selected").attr("id");

    $('#sub_cat').attr('data-id', id);

  });
   // ADD SUB CAT
 $("#add-subcat-form").submit(function(e){
        e.preventDefault();
        addSubCat();
    });
});
  

function addSubCat()
{
    

    var category_type = $('#category_type').val();
    var category_name = $('#category_name').val();
    var status = $('#status').val();

    if(category_type==''||category_name==''||status=='')
    {
      swal("Error Add Sub Category!","Please Input All Fields","error");
    }
    
    else
    {
            var formData = new FormData();
            formData.append('category_type', category_type);
            formData.append('category_name', category_name);
            formData.append('status', status);

            

            //console.log($('#upload_save'));
            $.ajax({
              url :  "Management/addCat",
              type: "POST",
              dataType : 'json',
              dom: 'Bfrtip',
              cache :  false,
              contentType : false,
              processData : false,
              async: false,
              data : formData,
              success: function(data, textStatus, jqXHR){
                
                swal(data.alert_ttl, data.alert_msg, data.alert_typ);

                if (data.alert_typ == 'success'){
                   swal(data.alert_ttl, data.alert_msg, data.alert_typ);
                    $("#addCatModal").modal('toggle'); 
                    $('#category_name').val('');
                    getSubCat(category_type); 
                       
                }else{

                  swal(data.alert_ttl, data.alert_msg, data.alert_typ);

                 
                }
              },
              error: function (jqXHR, textStatus, errorThrown)
              {
    
              } 


            });
    }
}
function addShow()
{
  var category = $('#cat').val();

  if(category==null){
        swal("ERROR MESSAGE", "CHOOSE FIRST CATEGORY" , "error");
       
      }
      else{
        $('#category_type').val(category);
        $('#addCatModal').modal('show');

      }
}
function editShow()
{

  var category = $('#cat').val();
  var sub_cat = $('#sub_cat').val();
  var id = $('#sub_cat').attr('class');

  if(sub_cat==null||category==null){
        swal("ERROR MESSAGE", "CHOOSE CATEGORY/SUB CATEGORY" , "error");
       
      }
  else if(sub_cat=='no'){
     swal("ERROR MESSAGE", "SUB CATEGORY EMPTY" , "error");
  }
  else{
        $('#editCat').val(category);
        $('#editSub').val(sub_cat);
        $('#editCatModal').modal('show');
      }
}
function updateSub()
{

    var category_type = $('#editCat').val();
    var category_name = $('#editSub').val();
    var status = $('#statuscat_edit').val();
    var sid = $('#sub_cat').attr('data-id');
   
    


    if(category_type==''||category_name==''||status==''||sid=='')
    {
      swal("Error Edit Sub Category!","Please Input All Fields","error");
    }
    
    else
    {
            var formData = new FormData();
            formData.append('id', sid);
            formData.append('category_type', category_type);
            formData.append('category_name', category_name);
            formData.append('status', status);

            

            //console.log($('#upload_save'));
            $.ajax({
              url :  "Management/editCat",
              type: "POST",
              dom: 'Bfrtip',
              dataType : 'json',
              cache :  false,
              contentType : false,
              processData : false,
              async: false,
              data : formData,
              success: function(data, textStatus, jqXHR){
                
                swal(data.alert_ttl, data.alert_msg, data.alert_typ);

                if (data.alert_typ == 'success'){
                   swal(data.alert_ttl, data.alert_msg, data.alert_typ);
                    $("#editCatModal").modal('toggle');  
                    getSubCat(category_type); 
                       
                }else{
                  swal(data.alert_ttl, data.alert_msg, data.alert_typ);

                 
                }
              },
              error: function (jqXHR, textStatus, errorThrown)
              {
    
              } 


            });
    }

}
function delShow()
{
  var category = $('#cat').val();
  var sub_cat = $('#sub_cat').val();
  var sid = $('#sub_cat').attr('data-id');

  if(sub_cat==null||category==null){
        swal("ERROR MESSAGE", "CHOOSE CATEGORY/SUB CATEGORY" , "error");
       
      }
  else if(sub_cat=='no'){
     swal("ERROR MESSAGE", "SUB CATEGORY EMPTY" , "error");
  }
  else{
        $('#del_title').html(sid);
        $('#del_body').html(category);
        $('#del_sub').html(sub_cat);
        $('#delCatModal').modal('show');
      }
}
function rmSubcat()
{
  var sid =  $('#del_title').html();
  var category_type =  $('#del_body').html();
  var formData = new FormData();
  formData.append('id', sid);

    $.ajax({
              url :  "Management/delCat",
              type: "POST",
              dom: 'Bfrtip',
              dataType : 'json',
              cache :  false,
              contentType : false,
              processData : false,
              async: false,
              data : formData,
              success: function(data, textStatus, jqXHR){
                
                swal(data.alert_ttl, data.alert_msg, data.alert_typ);

                if (data.alert_typ == 'success'){
                   swal(data.alert_ttl, data.alert_msg, data.alert_typ);
                    $("#delCatModal").modal('toggle');  
                    getSubCat(category_type); 
                       
                }else{
                  swal(data.alert_ttl, data.alert_msg, data.alert_typ);

                 
                }
              },
              error: function (jqXHR, textStatus, errorThrown)
              {
    
              } 


            });
}





function getSubCat(category_type)
{
 
  var formData = new FormData();
  formData.append('category_type', category_type);

  $.ajax({
              url :  "Management/getCat",
              type: "post",
              dom: 'Bfrtip',
              dataType : 'json',
              cache :  false,
              contentType : false,
              processData : false,
              processing: true,
              async: false,
              data: formData,
              success: function(data, textStatus, jqXHR){
                 
              var length = data.length;
              var html = '';
              $('#sub_cat option').remove();

              if(data['alert_typ']=='error'){
                $('#sub_cat').append("<option value='no'>"+"No Sub Category"+"</option>");
              }
              else{
              

                for(var i=0;i<data.length;i++){
                 $('#sub_cat').append("<option id="+data[i]['id']+" >"+data[i]['category_name']+"</option>");
                }

              }

                
              },
              error: function (jqXHR, textStatus, errorThrown)
              {
    
              } 


            });
}

</script>