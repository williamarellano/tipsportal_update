<div class="loader" id="loader">
</div>
<body class="hold-transition skin-green sidebar-mini">
<!-- Site wrapper -->
<div class="wrapper">

 

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper margin-top_cont">

  <div class="modal fade listModal" tabindex="-1" role="dialog" aria-hidden="true" sstyle="width:600px" id=addUserModal>
  <div class="modal-dialog modal-md">
    <div class="modal-content">

      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
        </button>
        <h4 class="modal-title" id="addUserLabel">Add Member</h4>

      </div>
      <div class="modal-body">

        <form class="form-horizontal form-label-left">
        <div class="form-group">
          <label class="control-label col-md-4 col-sm-4 col-xs-12">Username</label>
          <div class="col-md-6 col-sm-6 col-xs-12">
            <input type="text" id="username" name="username" class="form-control col-md-7 col-xs-12" >
          </div>
        </div>

        <div class="form-group">
          <label class="control-label col-md-4 col-sm-4 col-xs-12">First Name</label>
          <div class="col-md-6 col-sm-6 col-xs-12">
            <input type="text" id="firstname" name="firstname" class="form-control col-md-7 col-xs-12" >
          </div>
        </div>

        <div class="form-group">
          <label class="control-label col-md-4 col-sm-4 col-xs-12">Last Name</label>
          <div class="col-md-6 col-sm-6 col-xs-12">
            <input type="text" id="lastname" name="lastname" class="form-control col-md-7 col-xs-12" >
          </div>
        </div>

       

        <div class="form-group">
          <label class="control-label col-md-4 col-sm-4 col-xs-12">Password</label>
          <div class="col-md-6 col-sm-6 col-xs-12">
            <input type="password" id="password" name="password" class="form-control col-md-7 col-xs-12" >
          </div>
        </div>



        <div class="form-group">
          <label class="control-label col-md-4 col-sm-4 col-xs-12">Status</label>
          <div class="col-md-6 col-sm-6 col-xs-12">
            <select class="select2_multiple form-control col-md-7 col-xs-12" name="selStatus"  id="selStatus" style='width: 100%'>
              <option value="">-- SELECT --</option>
              <option value="A" selected="selected">Active</option>
              <option value="I">Inactive</option>
            </select>
          </div>
        </div>
        </form>      

      </div>
      <div class="modal-footer">
        <input type=hidden id=addID value=''>
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary" onclick='addMember();'>Save</button>
      </div>

    </div>
  </div>
</div>


<!-- EDIT MODAL -->
<div class="modal fade editUserModal" tabindex="-1" role="dialog" aria-hidden="true" sstyle="width:600px" id=editUserModal>
  <div class="modal-dialog modal-md">
    <div class="modal-content">

      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
        </button>
        <h4 class="modal-title" id="editUserLabel">Add Member</h4>

      </div>
      <div class="modal-body">

        <form class="form-horizontal form-label-left">
        <div class="form-group">
          <label class="control-label col-md-4 col-sm-4 col-xs-12">Username</label>
          <div class="col-md-6 col-sm-6 col-xs-12">
            <input type="text" id="editusername" name="editusername" class="form-control col-md-7 col-xs-12" >
          </div>
        </div>

        <div class="form-group">
          <label class="control-label col-md-4 col-sm-4 col-xs-12">First Name</label>
          <div class="col-md-6 col-sm-6 col-xs-12">
            <input type="text" id="editfirstname" name="editfirstname" class="form-control col-md-7 col-xs-12" >
          </div>
        </div>

        <div class="form-group">
          <label class="control-label col-md-4 col-sm-4 col-xs-12">Last Name</label>
          <div class="col-md-6 col-sm-6 col-xs-12">
            <input type="text" id="editlastname" name="editlastname" class="form-control col-md-7 col-xs-12" >
          </div>
        </div>

       

        <div class="form-group">
          <label class="control-label col-md-4 col-sm-4 col-xs-12">Password</label>
          <div class="col-md-6 col-sm-6 col-xs-12">
            <input type="password" id="editpassword" name="editpassword" class="form-control col-md-7 col-xs-12" >
          </div>
        </div>



        <div class="form-group">
          <label class="control-label col-md-4 col-sm-4 col-xs-12">Status</label>
          <div class="col-md-6 col-sm-6 col-xs-12">
            <select class="select2_multiple form-control col-md-7 col-xs-12" name="editselStatus"  id="editselStatus" style='width: 100%'>
              <option value="">-- SELECT --</option>
              <option value="A" selected="selected">Active</option>
              <option value="I">Inactive</option>
            </select>
          </div>
        </div>
        </form>      

      </div>
      <div class="modal-footer">
        <input type=hidden id=editID value=''>
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary" onclick='edMember();'>Save</button>
      </div>

    </div>
  </div>
</div>

<!--- END FOR EDIT MODAL -->

<!-- MODAL FOR DELETION -->
<div class="modal fade delModal" tabindex="-1" role="dialog" aria-hidden="true" sstyle="width:400px" id=delModal>
  <div class="modal-dialog modal-sm">
    <div class="modal-content">

      <div class="modal-header bgred">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
        </button>
        <h4 class="modal-title" id="delModalLabel"></h4>
      </div>
      <div class="modal-body">
        <b><span class='colorblack' style='font-size:18px'> </span><span class='colorred' id=del_user style='font-size:18px'></span></b><br>
        <br>

        
      </div>
      <div class="modal-footer">
        <input type=hidden id=delid value=''>
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
        <button type="button" class="btn btn-danger" onclick='deleteMember();'>Delete</button>
      </div>

    </div>
  </div>
</div>
<!-- END MODAL FOR DELETION -->
 

    

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="box">
        <div class="box-header with-border">
          <h1 class="box-title">Members</h1> &nbsp;
          <button class="btn btn-md btn-info" data-toggle="modal" data-target=".listModal"><i class='fa fa-plus'></i> Add</button>
        </div>
        
        <div class="box-body">

          <table id="memTable" class="table table-striped table-bordered" data-page-length='50' style='width:100%'>
          <thead>
            <tr>
              <th style="width: 50px; text-align: center;" >
              <th>Member ID
              <th>Username
              <th>First Name
              <th>Last Name
              <th>Status
              <th>Created Date
              <th>Created By
            </tr>
          </thead>

          </table>
        </div>
        <!-- /.box-body -->
        
        <!-- /.box-footer-->
      </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

 <footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> 2.4.0
    </div>
    <strong>Copyright &copy; 2018 <a href="#">Siegreich Solutions inc.</a></strong> All rights
    reserved.
  </footer>


</div>
<!-- ./wrapper -->


<?php $this->load->view('templates/admin_footer');?>

<script>
var datatable;
$(document).ready(function() {
  $('#loader').hide();
  loadData();
  $('.sidebar-menu').tree();
 });


function addUser()
{

  var username = $('#username').val();
  var password = $('#password').val();
  var firstname = $('#firstname').val();
  var lastname = $('#lastname').val();
  // var selUserRole = $('#selUserRole').val();
  var selStatus = $('#selStatus').val();
  


  if (username == "" || password == "" || firstname == "" || lastname == "" ||   selStatus == "")
  {
    swal("Error","All fields are required!","error");
  }
  else{
  var formData = { 
                    username : username,
                    password : password,
                    firstname : firstname,
                    lastname : lastname,
                    selStatus : selStatus
                };
  $.ajax({url: "<?=base_url();?>Members/createMember", 
      async: true, type: "POST", dataType: 'json', data: formData, success: function(data){
      $("#addUserModal").modal('toggle');
      swal(data.ttl,data.msg,data.typ);
      reloadData( $("#memTable") );
  }}); 
}


}

function editMember(id,username,password,firstname,lastname,status) {
    $("#editpassword").val('');
    $("#editUserModal").modal(); 
    $("#editID").val(id);
    $("#editUserLabel").html("Edit User "+ firstname);
    $("#editfirstname").val(firstname);
    $("#editlastname").val(lastname);
    $("#editusername").val(username);
    // $("#editpassword").val(password);
    // $("#editselUserRole").val(user_role);
    $("#editselStatus").val(status);

    // $("#del_user").html("Are you sur you want to delete user "+realname+" ?");
  }

function edMember()
{
  var editID = $('#editID').val();
  var username = $('#editusername').val();
  var password = $('#editpassword').val();
  var firstname = $('#editfirstname').val();
  var lastname = $('#editlastname').val();
  // var selUserRole = $('#editselUserRole').val();
  var selStatus = $('#editselStatus').val();
  var formData = { 
                      editID : editID,
                      username : username,
                      password : password,
                      firstname : firstname,
                      lastname : lastname,
                      // selUserRole : selUserRole,
                      selStatus : selStatus
                  };
  if (username == "" || firstname == "" || lastname == "" ||   selStatus == "")
  {
    swal("Error","All fields are required!","error");
  }else{
    $.ajax({
          url :  "<?=base_url();?>Members/createMember",
          type: "POST",
          dataType : 'json',
          data : formData,
          success: function(data, textStatus, jqXHR){
              // reloadData($("#userTable"));
              
              swal(data.ttl,data.msg,data.typ); 
              $("#editUserModal").modal('toggle'); 
              reloadData( $("#memTable") );
              
            
          },
          error: function (jqXHR, textStatus, errorThrown)
          {
            //Custom Error
          } 
        });
  }
}

function clearfields()
{
  $('#addID').val("");
  $('#username').trigger("reset");
  $('#password').val("");
  $('#realname').val("");
  // $('#selUserRole').val("");
  $('#selStatus').val("");
}

function delMember(id,realname) {
    $("#delModal").modal(); 
    $("#delid").val(id);
    $("#delModalLabel").html("Delete Member");
    $("#del_user").html("Are you sur you want to delete Member "+realname+" ?");
  }

function deleteMember()
{
 
  var delID = $('#delid').val();
  // alert(delID);
  var formData = { delID : delID};
  $.ajax({
        url :  "<?=base_url();?>Members/delMember",
        type: "POST",
        dataType : 'json',
        data : formData,
        success: function(data, textStatus, jqXHR){
            // reloadData($("#userTable"));
            
            swal(data.ttl,data.msg,data.typ); 
            $("#delModal").modal('toggle'); 
            reloadData( $("#memTable") );
            
          
        },
        error: function (jqXHR, textStatus, errorThrown)
        {
          //Custom Error
        } 


      });
}


 function loadData()
  {
    datatable = $('#memTable').dataTable( {
    "order": [[ 0, "asc" ]],
      responsive: true, 
      dom: 'Bfrtip',

      "buttons": [
              'excel',
              'pdf'
          ],
          "scrollX": true,
      "pagingType": "full_numbers",
      "processing": true,
      "serverSide": true,
      "ajax":{
        url :"<?=base_url();?>Members/listing?k=" + Math.random(), // json datasource
        type: "post",  // method  , by default get      
        error: function(){  // error handling
          $(".grid-error").html("");
          $("#roleTable").append('<tbody class="grid-error"><tr><th colspan="7">NO DATA FOUND</th></tr></tbody>');
          $("#roleTable_processing").css("display","none");        
        }
      },
      "columns" : [
        { "data" : "m_id" },
        { "data" : "m_id" },
        { "data" : "username" },
        { "data" : "firstname" },
        { "data" : "lastname" },
        // { "data" : "user_role" },
        { "data" : "status" },
        { "data" : "created_date" },
        { "data" : "created_by" }
      ]
      ,
       "columnDefs" : [ {
        "targets" : 0,
        "orderable": false, className : "text-center",
          "render": function( data, type, row, meta ) {
            var html = "";
            
              html = " <a href='#'  onclick=\"editMember(" + row["m_id"] + ",'" + row["username"] + "','" + row["password"] + "','" + row["firstname"] + "','" + row["lastname"] +  "','" + row["status"] + "')\"> <span class='glyphicon glyphicon-edit' aria-hidden='true'></span></a>";
              html += "&nbsp;&nbsp;"+"<a href='#' onclick=\"delMember(" + row["m_id"] + ",'"+ row["realname"] + "')\"><span class='glyphicon glyphicon-trash' style='color:red;' aria-hidden='true'></span> </a>";
              
            
            return html;
            }
       },{
          "targets" :5,  
          "render" : function( data, type, row, meta ) {
          var html;
            switch(data){
              case "A" : html = "<span class='label label-success'>Active</span>"; break;
              case "I" : html = "<span class='label label-danger'>Inactive</span>"; break;
            }
          return html;
          }     
        }]
    }); 
    }


</script>