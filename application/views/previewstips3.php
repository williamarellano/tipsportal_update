<style type="text/css">
	#secMenu li:nth-of-type(3) a{background: #0e6857 !important;}
</style>
<div id="pt">
	<div id="ptcontainer" class="ptheight">
	<div class="tccontainercont">
		<div id="pttitle">
			<h3>Previews & Tips</h3>
			<p>Featured games with previews and tips from our in-house tipster!</p>
		</div>
		<div id = "tips_table">
		</div>
		<div id="pttblcontainer3" class="tips_table">

			<!-- table start -->
			<!-- <div class="pttbl">
				<div class="pttop">07/25 &#8226; 01:30 AM</div>
				<div class="ptpagetitle">
					<h4>PREMIER LEAGUE</h4>
					<p>Betting Tips</p>
				</div>
				<div class="ptmatch">
					<div class="ptmatchie">
						<img src="<?=base_url();?>public/img/mancity.png">
						<p>Arsenal</p>
					</div>
					<div class="ptmatchie ptvs">vs</div>
					<div class="ptmatchie">
						<img src="<?=base_url();?>public/img/mancity.png">
						<p>Arsenal</p>
					</div>
				</div>
				<p class="ptph">
					Arsenal will most likely be glad when the Premier League comes to an end. Two games played so far have...
				</p>
				<div class="pttip">
					<p>OUT TIP:</p>
					<span>CHELSEA WIN</span>
					<a href="<?=base_url();?>Welcome/previewstips2"><button>View More</button></a>
				</div>
			</div>

			

			<div class="tctickers" id="pagination_link">
				<!-- <ul>
					<li><</li>
					<li class="tctickers-active">1</li>
					<li>2</li>
					<li>3</li>
					<li>4</li>
					<li>5</li>
					<li>6</li>
					<li>></li>
				</ul> -->
			</div>

			
			<center style="float: left; width: 100%;">
			<a href="<?=base_url();?>Welcome/previewstips"><button class="tcvpt">View Latest Tips</button></a>
			</center>
			<div class="sideshit">
				<p><b>Football Betting Tips and Advice for successful football betting</b></p>
				<p>Are you a football fan? Would you like to win more of your football bets? Discover the essential factors to bear in mind before making your football forecasts. </p>
				<br>
				<p>Whether you do the Football Pools or bet through a bookmaker, the following football betting tips are designed to help you improve your football results forecasts. By following our football betting tips, you'll increase your chances of winning your bets on matches in the Premier League, the Championship, the Champions League or any other football competitions.
				</p>
				<br>
				<p><b>Tip 1: Consider the motivation and team spirit</b></p>
				<p>Motivation has a major influence on a football club's sporting performances. The consequences of a particular encounter (league title, Champions League qualification, relegation, etc.) and the size of the match win bonus are key motivating factors for a football team.</p>
			</div>

		</div>
	</div>
	</div>
</div>

<?php $this->load->view('template/footer');?>

<script>

$(document).ready(function() {
// alert('asd');
var html;
load_data(1);

$(document).on("click", ".pagination li a", function(event){
  event.preventDefault();
  var page = $(this).data("ci-pagination-page");
  load_data(page);
 });

});

function load_data(page)
{
	var html = '';
	$.ajax({
	url:"<?php echo base_url(); ?>Welcome/viewPrevTips/"+page,
	method:"GET",
	dataType:"json",
	success:function(data)
	{
		var dtlength = data.tips_table.length;
		var dt = data.tips_table;
		// console.log(data.tipsters_table.length);
		console.log(data);
		for(var i=0; i<dtlength; i++)
		{
			
			

			html += '<div class="pttbl">';
			html += '<div class="pttop">';
			html += '09/21 • 10:25 AM';
			html += '</div>';
			html += '<div class="ptpagetitle">';
			html += '<h4>'+ dt[i].league_nickname +'</h4>';
			html += '<p>Betting Tips</p>';
			html += '</div>';
			html += '<div class="ptmatch">';
			html += '<div class="ptmatchie">';
			html += '<img src="<?=base_url().$this->config->item('upload_teams_logo').'/';?>'+dt[i].home_team_logo+'">';
			html += '<p>'+dt[i].home_team_nickname+'</p>';
			html += '</div>';
			html += '<div class="ptmatchie ptvs">vs</div>';
			html += '<div class="ptmatchie">';
			html += '<img src="<?=base_url().$this->config->item('upload_teams_logo').'/';?>'+dt[i].away_team_logo+'">';
			html += '<p>'+dt[i].away_team_nickname+'</p>';
			html += '</div>';
			html += '</div>';
			html += '<p class="ptph">';
			html += dt[i].tptip_desc.substring(0,93)+'...</p>';

			html += '<div class="pttip">';
			html += '<p>OUT TIP:</p>';
			html += '<span>'+dt[i].tptip+'</span>';
			html += '<a href="<?=base_url();?>Welcome/previewstips2"><button>View More</button></a>';
			html += '</div>';
			html += '</div>';
		}

	$('#tips_table').html(html);
	$('#pagination_link').html(data.pagination_link);
	}
	});
}

</script>