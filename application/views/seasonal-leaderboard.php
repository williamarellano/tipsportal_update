<style type="text/css">
  #secMenu li:nth-of-type(4) a{background: #0e6857 !important;}
</style>
<div class="container-fluid no-pad">
  <div id="s-lead" class="container century s-lead">
    <div class="row">
      <div class="col-xs-12 no-pad text-center">
        <h2>SEASONAL LEADER BOARD</h2>
        <p class="col-xs-12 col-md-10 col-md-offset-1">Take part in our free to play games and earn points each week for the seasonal leaderboard! After each season, if you are ranked top 100, you can receive a cash prize up to <span class="font-bold">SGD 15,000!</span></p>
      </div>
      <div class="col-md-10 text-center no-pad col-centered" id="slbtbl">
        <table class="table table-hover table-striped table-bordered table-responsive">
          <thead class="home-thead">
            <tr>
              <th class="text-center">Rank</th>
              <th class="text-center">Points</th>
              <th class="text-center">Username</th>
              <th class="text-center">Prize</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td>1st</td>
              <td>100</td>
              <td>0</td>
              <td>SGD15,000</td>
            </tr>
            <tr>
              <td>2nd</td>
              <td>100</td>
              <td>0</td>
              <td>SGD15,000</td>
            </tr>
            <tr>
              <td>3rd</td>
              <td>100</td>
              <td>0</td>
              <td>SGD15,000</td>
            </tr>
            <tr>
              <td>4th</td>
              <td>100</td>
              <td>0</td>
              <td>SGD15,000</td>
            </tr>
            <tr>
              <td>5th</td>
              <td>100</td>
              <td>0</td>
              <td>SGD15,000</td>
            </tr>
            <tr>
              <td>6th</td>
              <td>100</td>
              <td>0</td>
              <td>SGD15,000</td>
            </tr>
            <tr>
              <td>7th</td>
              <td>100</td>
              <td>0</td>
              <td>SGD15,000</td>
            </tr>
            <tr>
              <td>8th</td>
              <td>100</td>
              <td>0</td>
              <td>SGD15,000</td>
            </tr>
            <tr>
              <td>9th</td>
              <td>100</td>
              <td>0</td>
              <td>SGD15,000</td>
            </tr>
            <tr>
              <td>10th</td>
              <td>100</td>
              <td>0</td>
              <td>SGD15,000</td>
            </tr>
          </tbody>
        </table>
      </div>

      <div class="col-xs-12 no-pad" id="slbtnc">
        <div class="col-xs-10 col-md-offset-1 well terms-condition text-left">
          <h3 class="no-margin font-bold">Terms & Conditions</h3>
          <ul>
            <li> Participation is free.</li>
            <li> Member will gain points for the seasonal leaderboard through the games offered on our site.</li>
            <li> One entry per player.</li>
            <li> Winners will be notified by email/sms.</li>
            <li> Prize will be paid in credit on one of the sponsoring sites.</li>
            <li> Limited to 1 entry per household.</li>
            <li> All decisions regarding this offer are final and binding.</li>
          </ul>
        </div>
      </div>
    </div>
  </div>
</div>
