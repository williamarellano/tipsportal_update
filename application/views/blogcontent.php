<div id="blogContent" class="container-fluid ">
  <div class="container liveStream no-pad ">

      <div class="backtoblog col-md-8 no-pad left-bar text-left"><a href="<?=base_url();?>Welcome/blog"><i class="fa fa-chevron-left"></i> Back to Blogs</a></div>

    <div class="col-md-8 no-pad left-bar text-center white-bg">
      <img src="<?=base_url();?>public/img/blog-banner.jpg" alt="" class="img-responsive">
      <div id="article" class="col-md-12 text-left ">
        <div class="row">
          <div class="col-md-12">
            <small>August 21st, 2018</small>
            <h3>NEW On-Demand Crawl: Quick Insights for Sales, Prospecting, & Competitive Analysis</h3>

            <p class="font-bold">1. Know (or discover) your key transactional terms</p>
            <p>I haven't done much on that here because hopefully you've already done that. You already know what your key transactional terms are. Because whatever happens you don't want to end up developing location pages for too many different keyword types because it's gong to bloat your site, you probably just need to pick one or two key transactional terms that you're going to make up the local variants of. For this purpose, I'm going to talk through an SEO job board as an example.
</p>
            <p class="font-bold">2. Categorize your keywords as implicit, explicit, or near me and log their search volumes</p>
            <p>We might have "SEO jobs" as our core head term. We then want to figure out what the implicit, explicit, and near me versions of that keyword are and what the different volumes are. In this case, the implicit version is probably just "SEO jobs." If you search for "SEO jobs" now, like if you open a new tab in your browser, you're probably going to find that a lot of local orientated results appear because that is an implicitly local term and actually an awful lot of terms are using local data to affect rankings now, which does affect how you should consider your rank tracking, but we'll get on to that later.</p>

            <p class="font-bold">2. Categorize your keywords as implicit, explicit, or near me and log their search volumes</p>
            <p>We might have "SEO jobs" as our core head term. We then want to figure out what the implicit, explicit, and near me versions of that keyword are and what the different volumes are. In this case, the implicit version is probably just "SEO jobs." If you search for "SEO jobs" now, like if you open a new tab in your browser, you're probably going to find that a lot of local orientated results appear because that is an implicitly local term and actually an awful lot of terms are using local data to affect rankings now, which does affect how you should consider your rank tracking, but we'll get on to that later.</p>
<hr>
            <p class="font-bold">Related articles:</p>
            <p class="related">
              <ol>
                <li><a href="">NEW On-Demand Crawl: Quick Insights for Sales, Prospecting, & Competitive Analysis</a></li>
                <li><a href="">NEW On-Demand Crawl: Quick Insights for Sales, Prospecting, & Competitive Analysis</a></li>
                <li><a href="">NEW On-Demand Crawl: Quick Insights for Sales, Prospecting, & Competitive Analysis</a></li>
                <li><a href="">NEW On-Demand Crawl: Quick Insights for Sales, Prospecting, & Competitive Analysis</a></li>
              </ol>
            </p>
          </div>
        </div>

      </div>
      <!--/ls-feed-->
      <div style="height:800px;"></div>

    </div>
    <!--/col-md-8 Left Panel-->
    <div class="clearfix hidden-lg hidden-md"></div>
    <!--======Right Panel=========-->
    <div class="col-md-4 side-bar sideTitle no-pad v-align h-align pull-right">
      <!-- <h1 class="no-margin">Hello</h1> -->
      <div class="hidden-sm hidden-xs">
       
      <div class="pttbl pttbl-side">
        <div class="pttop">07/25 &#8226; 01:30 AM</div>
        <div class="ptpagetitle">
          <h4>PREMIER LEAGUE</h4>
          <p>Betting Tips</p>
        </div>
        <div class="ptmatch">
          <div class="ptmatchie">
            <img src="<?=base_url();?>public/img/mancity.png">
            <p>Arsenal</p>
          </div>
          <div class="ptmatchie ptvs">vs</div>
          <div class="ptmatchie">
            <img src="<?=base_url();?>public/img/mancity.png">
            <p>Arsenal</p>
          </div>
        </div>
        <p class="ptph">
          Arsenal will most likely be glad when the Premier League comes to an end. Two games played so far have...
        </p>
        <div class="pttip">
          <p>OUT TIP:</p>
          <span>CHELSEA WIN</span>
          <a href="<?=base_url();?>Welcome/previewstips2"><button>View More</button></a>
        </div>
      </div>
      
        <div class="col-md-12 no-pad ls-ban">
          <a href="<?=base_url();?>Welcome/ftpperfect10">
          <img src="<?=base_url();?>public/img/ls-wp10.jpg" alt="Weekly Perfect 10">
        </a>
        </div>

        <div class="col-md-12 no-pad ls-ban">
          <a href="<?=base_url();?>Welcome/goldengoal">
          <img src="<?=base_url();?>public/img/ls-gg.jpg" alt="The Golden Goal">
        </a>
        </div>

      </div>
    </div>
    <!-- right panel end -->
    </div>
  </div>
  <!--/container-->
</div>
<!--/liveStream-->
