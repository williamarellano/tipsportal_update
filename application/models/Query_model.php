<?php

Class Query_model extends CI_Model {

	function __construct()
	{
		parent::__construct();
		// $this->load->database();
    	$CI = &get_instance();
      $this->db = $CI->load->database('default',TRUE);	// Main
      date_default_timezone_set('Asia/Singapore');
	}

	public function getDataCount($sql){
		$query = $this->db->query($sql);
		return array("data" => $query->result_array(), "count" => $query->num_rows());		
	}

	public function getDataArray($sql){
		$query = $this->db->query($sql);
		return $query->result_array();
	}

	public function getDataRow($sql){
		$query = $this->db->query($sql);
		return $query->row();
	}

	public function getData($sql){
		$query = $this->db->query($sql);
		return $query->result();
		//return $query->result_array();
	}

	public function getCell($sql, $column){
		$query = $this->db->query($sql);
		return $query->row($column);
	}
	
	public function execSQL($sql){
		$this->db->query($sql);
	}
	public function execSQLID($sql){
		$this->db->query($sql);
		$insert_id = $this->db->insert_id();
		
		return $this->db->insert_id();
	}
		public function insertexcel($data)
	 {
	  $this->db->insert_batch('domains', $data);
	 }
	 public function insertexcel_u($data)
	 {
	  $this->db->insert_batch('domains_urgent', $data);
	 }

	public function clean($x, $typ = 'str'){
		$r = $this->input->post($x, TRUE);
		if ($r == ''){
			$r = $this->input->get($x, TRUE);
		}

		$r = html_purify($r);
		$r = htmlentities($r, ENT_QUOTES);
		//$r = $this->db->escape($r);

		switch ($typ){
			case "id":
				$r = str_replace("'","",$r);
				$int = (int)$r;
				return $int;
				break;

			case "money":
				$r = str_replace("'","",$r);
				$r = str_replace(',', '', $r);
				$float = (float)$r;
				return $float;
				break;

			case "clean":
				return str_replace("'", "", $r);			
				break;

			default:
				return $r;			

		}

	}


	public function cleanvar($x, $typ = 'str'){
		$r = html_purify($x);
		$r = htmlentities($r, ENT_QUOTES);
		//$r = $this->db->escape($r);

		switch ($typ){
			case "id":
				$r = str_replace("'","",$r);
				$int = (int)$r;
				return $int;
				break;

			case "money":
				$r = str_replace("'","",$r);
				$r = str_replace(',', '', $r);
				$float = (float)$r;
				return $float;
				break;

			case "clean":
				return str_replace("'", "", $r);			
				break;

			default:
				return $r;			

		}

	}

}